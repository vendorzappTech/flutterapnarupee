class UploadFile{

  String leadDocId , docId,catName,docName,fileName,fileFullUrl;

  UploadFile({this.leadDocId,this.docId,this.catName,this.docName,this.fileName,this.fileFullUrl});

  factory UploadFile.fromJson(Map<String, dynamic> json) {
    return new UploadFile(
        leadDocId: json['LeadDocId'].toString(),
        docId: json['DocumentId'].toString(),
        catName: json['CategoryName'],
        docName: json['DocumentName'],
        fileName: json['FileName'],
        fileFullUrl:json['FileFullUrl']
     );
  }

}
