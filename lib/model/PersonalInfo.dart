
class PersonalInfo{
  String leadId,gender,dob,pancard,aadharcard,address,pincode,requestType;
  Map<String, dynamic> toJson() =>
      {
        'LeadId': leadId,
        'Gender': gender,
        'DOB': dob,
        'PanCardNo': pancard,
        "AadharNo": aadharcard,
        "Address": address,
        "Pincode": pincode,
        "RequestType": requestType
      };
}
