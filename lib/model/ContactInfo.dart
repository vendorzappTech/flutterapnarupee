
class ContactInfo{
  String leadId,fullName,mobileNo,emailId,cityId,loanAmntReq,tenure,requestType;



  Map<String, dynamic> toJson() =>
      {
        'LeadId': leadId,
        'Name': fullName,
        'MobileNo': mobileNo,
        'EmailId': emailId,
        "CityId": cityId,
        "LoanAmount": loanAmntReq,
        "TenureInMonths": tenure,
        "RequestType": requestType
      };
  Map<String, dynamic> toJson1() =>
      {
        'LeadId': leadId,
        'FirstFullName': fullName,
        'FirstMobile': mobileNo,
        'FirstEmailId': emailId,
        "FirstCity": cityId,
        "RequestType": requestType
      };
  Map<String, dynamic> toJson2() =>
      {
        'LeadId': leadId,
        'SecondFullName': fullName,
        'SecondMobile': mobileNo,
        'SecondEmailId': emailId,
        "SecondCity": cityId,
        "RequestType": requestType
      };
}

