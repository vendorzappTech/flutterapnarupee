
class LogInInfo{
  String leadId,bankId,loginDate,bankApplId,bankRMName,bankRMNo,bankBranch,documentStatus,requestType,employeeId,productTypeId,loanAmount,approvedAmouunt,addedDate,disburseAmnt,disbursedId;
  Map<String, dynamic> toJson() =>
      {
        'LeadId': leadId,
        'BankId': bankId,
        'FileLoginDate': loginDate,
        'BankApplicationId': bankApplId,
        "BankRmNo": "",
        "BankRMName": bankRMName,
        "BankRMMobile": bankRMNo,
        "BankBranch": bankBranch,
        "DocPickupStatus": documentStatus ,
        "RequestType": requestType
      };

  Map<String, dynamic> toJson1() =>
      {
        'LeadId': leadId,
        'EmployeeId': employeeId,
        'BankId': bankId,
        'ProductTypeId': productTypeId,
        "BankApplicationId": bankApplId,
        "LoanAmount": loanAmount,
        "ApprovedAmount": approvedAmouunt,
      };

  Map<String, dynamic> toJson2() =>
      {
        'LeadId': leadId,
        'EmployeeId': employeeId,
        'BankId': bankId,
        'ProductTypeId': productTypeId,
        "BankApplicationId": bankApplId,
        "LoanAmount": loanAmount,
        "ApprovedAmount": approvedAmouunt,
        "AddedDate": addedDate,
        "DisbursedAmount": disburseAmnt,
      };

  Map<String, dynamic> toJson3() =>
      {
        'DisbursedId': disbursedId,
        'LeadId': leadId,
        'EmployeeId': employeeId,
        'BankId': bankId,
        'ProductTypeId': productTypeId,
        "BankApplicationId": bankApplId,
        "LoanAmount": loanAmount,
        "ApprovedAmount": approvedAmouunt,
      };

}
