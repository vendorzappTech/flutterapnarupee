import 'dart:convert';
import 'dart:io';

import 'package:apanarupee/model/ApiInterface.dart';
import 'package:apanarupee/screens/OTPVerification.dart';
import 'package:apanarupee/utils/Helper.dart';
import 'package:apanarupee/utils/HexColor.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
class Profile extends StatefulWidget  {

  @override
  ProfileState createState() => ProfileState();
}

class ProfileState extends State<Profile> implements ApiInterface{

  String _name = "",_mobile = "" ,_email = "",_bankName = "",_accountNo = "",_ifscCode = "",_bankBranch = "",_photo = "";

  final formKey = new GlobalKey<FormState>();
  bool _isLoading = true,_isFirstTimeLoading = true,isFileSelected = false;

  static const int GET_PROFILE_DETAILS_RESULT_CODE = 1,ERROR_CODE = -1;
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  File file;

  @override
  void initState() {
    // TODO: implement initState

    print("In init");

    setState() {
    _isLoading = true;
    _isFirstTimeLoading = true;
    };
    // call webservices to get profile details
    super.initState();
    _getData();

    FirebaseMessaging _firebaseMessaging;

    _firebaseMessaging = FirebaseMessaging();

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print('on message $message');

        // show notification here
//        _showNotificationWithDefaultSound();
        var title = message['notification']['title'];
        var body = message['notification']['body'];
        // show notification here
        _showDialog(title,body);



      },
      onResume: (Map<String, dynamic> message) async {
        print('on resume $message');
      },
      onLaunch: (Map<String, dynamic> message) async {
        print('on launch $message');
      },

    );
    //super.initState();
  }

  _getData() async
  {
    await Helper.callAuthGetApi(Uri.encodeFull( Helper.GET_PROFILE_DETAILS),
        json.encode({

        }), context, this, GET_PROFILE_DETAILS_RESULT_CODE);


  }

  _uploadFile() async{
    setState(() {
      isFileSelected = false;
      file = null;
    });

    file = await ImagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      if(file != null)
      isFileSelected = true;
      _photo = null;
    });


  }

  upload() async
  {
    String fileName ="";
    if(file != null){
    fileName=file.path.split("/").last;
    }

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString("accessToken");



    Dio dio = new Dio();
    FormData formdata = new FormData(); // just like JS
    if(file!=null)
    formdata.add("file", new UploadFileInfo(file, fileName));
    formdata.add('Name',_name);
    formdata.add('EmailId',_email);
    formdata.add('BankName',_bankName);
    formdata.add('AccountNo',_accountNo);
    formdata.add('IFSCCode',_ifscCode);
    formdata.add('BankBranch',_bankBranch);

    Response response = await dio.post(Helper.UPDATE_PROFILE_URL, data: formdata, options: Options(
        method: 'POST',
        responseType:  ResponseType.json,
        headers:  {"Accept": "application/json","Content-Type": "application/json", "Authorization": "Basic "+token}
    ));

    var data = json.decode(response.toString());

    try {
      setState(() => _isLoading = false);

      if (data["responseCode"] == Helper.SUCCESS_STATUS_CODE) {
        print("got data");
        _showSnackBar(data["messages"]["messageDescription"], Colors.green);


      } else {
        print("Error=" + data["messages"]["messageDescription"]);

        _showSnackBar(data["messages"]["messageDescription"], Colors.red);
      }
      if (data == null)
        print("Unable to get data");
    } catch (e) {
      _showSnackBar("Some problem occurred", Colors.red);
    }


  }

  showResponse()
  {

  }



  _submit(){

    var form = formKey.currentState;

    if (form.validate()) {
      setState(() => _isLoading = true);
      form.save();
      upload();


    }


  }

  @override
  Widget build(BuildContext context) {

    final logo = Hero(
        tag: 'hero',
        child:
        new Container(

            height: 280,
//          width: 600.0,
            decoration: new BoxDecoration(
              image: new DecorationImage(image: new AssetImage("assets/loginbg.png"), fit: BoxFit.fill,
              ),
            ),

            child:
            new Column(
              children: <Widget>[
                AppBar
                  (

                    elevation: 0.0,
                    backgroundColor: Colors.transparent,
                    iconTheme: IconThemeData(
                        color: Colors.white),title:Text( "Profile",style: TextStyle(fontSize: 24),)),
                new Padding(
                  padding: EdgeInsets.only(top: 20),
                  child:
                  Center(
                    child:(_photo != null)?CircleAvatar(
                      minRadius: 60,
                      maxRadius: 60,

                      backgroundImage:   NetworkImage(
                        _photo,
                      ),
                    )
                      :(isFileSelected)? new Image.file(file,width: 100,height: 100,):new Image.asset("assets/profile.png",width: 100,height: 100)
                  ),),
                new Padding(
                  padding: EdgeInsets.only(left:100),
                  child:
                  Center(
                    child:
                    new GestureDetector(
                      onTap: (){
                        _uploadFile();
                      },
                    child:
                    new Icon(Icons.camera,size:20,color:Color(HexColor.getAppOrangeColor()),),)                //new Image.asset("assets/profile.png",width: 150,height: 150),
                  ),)

              ],
            )


        )
    );

    var name =
    new TextFormField(

      initialValue: _name,
      autovalidate: true,
      style: TextStyle(fontSize: 20 ,color: Colors.black),
      decoration:  InputDecoration(
        //border: const OutlineInputBorder(),
          hintText: "Enter Name",
          labelText: "Name",
          hintStyle: TextStyle(color:Colors.grey),
          labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor()),fontSize: 18)
      ),
      autocorrect: true,
      onSaved: (str) => _name = str,
      keyboardType: TextInputType.text,
      enabled: true,

      validator: (str) =>
      (str.length >0)
          ? null
          : "Enter Name",


    );

    var email =
    new TextFormField(
      initialValue: _email,
      style: TextStyle(fontSize: 20 ,color: Colors.black),
      decoration:  InputDecoration(
        //border: const OutlineInputBorder(),
          hintText: "Enter Email",
          labelText: "Email",
          hintStyle: TextStyle(color:Colors.grey),
          labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor()),fontSize: 18)
      ),
      autocorrect: true,
      validator: ( String str) {
        // apply email validation here
        Pattern pattern =
            r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
        RegExp regex = new RegExp(pattern);
        if (!regex.hasMatch(str))
          return 'Enter Valid Email';
        else
          return null;
      },
      onSaved: (str) => _email = str,
      keyboardType: TextInputType.emailAddress,
      enabled: true,


    );

    var mobile =
    new TextFormField(
      initialValue: _mobile,
      style: TextStyle(fontSize: 20 ,color: Colors.black),
      decoration:  InputDecoration(
        // border: const OutlineInputBorder(),
          hintText: "Enter Mobile Number",
          labelText: "Mobile",
          hintStyle: TextStyle(color:Colors.grey),
          labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor()),fontSize: 18)
      ),
      autocorrect: true,

      validator: (str) =>
      (str.length == 10)
          ? null
          : "Invalid mobile number",
      onSaved: (str) => _mobile = str,
      keyboardType: TextInputType.phone,
      enabled: true,
      inputFormatters: [
        LengthLimitingTextInputFormatter(10),
        WhitelistingTextInputFormatter.digitsOnly,
      ],

    );


    var bank_name =
    new TextFormField(
      initialValue: _bankName,
      style: TextStyle(fontSize: 20 ,color: Colors.black),
      decoration:  InputDecoration(
        //border: const OutlineInputBorder(),
          hintText: "Enter Bank Name",
          labelText: "Bank Name",
          hintStyle: TextStyle(color:Colors.grey),
          labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor()),fontSize: 18)
      ),
      autocorrect: true,

      onSaved: (str) => _bankName = str,
      keyboardType: TextInputType.text,
      enabled: true,
    );


    var account_no =
    new TextFormField(
      initialValue: _accountNo,
      style: TextStyle(fontSize: 20 ,color: Colors.black),
      decoration:  InputDecoration(
        //border: const OutlineInputBorder(),
          hintText: "Enter account number",
          labelText: "Account Number",
          hintStyle: TextStyle(color:Colors.grey),
          labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor()),fontSize: 18)
      ),
      autocorrect: true,
      validator: ( String str) {
        // apply email validation here
        null;
      },
      onSaved: (str) => _accountNo = str,
      keyboardType: TextInputType.text,
      enabled: true,
    );

    var ifscCode =
    new TextFormField(
      initialValue: _ifscCode,
      style: TextStyle(fontSize: 20 ,color: Colors.black),
      decoration:  InputDecoration(
        //border: const OutlineInputBorder(),
          hintText: "Enter IFSC Code",
          labelText: "IFSC Code",
          hintStyle: TextStyle(color:Colors.grey),
          labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor()),fontSize: 18)
      ),
      autocorrect: true,
      validator: ( String str) {
        // apply email validation here
        null;
      },
      onSaved: (str) => _ifscCode = str,
      keyboardType: TextInputType.text,
      enabled: true,
    );

    var bank_branch =
    new TextFormField(
      initialValue: _bankBranch,
      style: TextStyle(fontSize: 20 ,color: Colors.black),
      decoration:  InputDecoration(
        //border: const OutlineInputBorder(),
          hintText: "Enter Bank Branch",
          labelText: "Bank branch",
          hintStyle: TextStyle(color:Colors.grey),
          labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor()),fontSize: 18)
      ),
      autocorrect: true,
      validator: ( String str) {
        // apply email validation here
        null;
      },
      onSaved: (str) => _bankBranch = str,
      keyboardType: TextInputType.text,
      enabled: true,
    );


    var submitButton =
    ButtonTheme(
        minWidth: 50.0,
        height: 50.0,
        child:
        new RaisedButton(
          color: HexColor("#fb6616"),
          shape: new RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(5.0)),
          splashColor: Colors.blue,
          onPressed: _submit,
          child: new Text("SAVE", style: TextStyle(color:Colors.white,fontSize: 20)),
        ));

    return new Scaffold(
        key: scaffoldKey,
        backgroundColor: Colors.white,

        body:
        new Form(
          key:formKey,
          child: ListView(

              padding: EdgeInsets.only(top: 0),

              children: <Widget>[
                logo,
              (!_isFirstTimeLoading)?(
                new Column(
                  children: <Widget>[
                new Padding(
                    padding: EdgeInsets.only(left:24,right: 24,top:10),
                    child:name),

                new Padding(
                    padding: EdgeInsets.only(left:24,right: 24,top:10),
                    child:email),

                new Padding(
                    padding: EdgeInsets.only(left:24,right: 24,top:10),
                    child:mobile),

                new Padding(
                    padding: EdgeInsets.only(left:24,right: 24,top:10),
                    child:bank_name),
                new Padding(
                    padding: EdgeInsets.only(left:24,right: 24,top:10),
                    child:account_no),

                new Padding(
                    padding: EdgeInsets.only(left:24,right: 24,top:10),
                    child:ifscCode),


                new Padding(
                    padding: EdgeInsets.only(left:24,right: 24,top:10),
                    child:bank_branch),



                  ],
                )
           ):
                SizedBox(height: 10.0),
                new Padding(
                    padding: EdgeInsets.only(left:24,right: 24,top: 20,bottom: 20),
                    child:
                    submitButton),
            new Padding(
            padding: EdgeInsets.only(left:24,right: 24,bottom: 20),
            child:
            new Center(
                  child:
                  (_isLoading)? Align(
                      alignment: Alignment.center,
                      child: CircularProgressIndicator()): SizedBox(height: 10.0),
                ),
             )

              ]
          ),

        )
    );
  }

  void _showSnackBar(String text,Color color) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(backgroundColor:color ,content: new Text(text),duration: new Duration(milliseconds: 5000),));
  }

  void postExecution(var data ,int result_code)  {

    switch(result_code) {



      case GET_PROFILE_DETAILS_RESULT_CODE:

        setState(() {
          _isFirstTimeLoading = false;
          _isLoading = false;
        });
        try {


          if (data["responseCode"] == Helper.SUCCESS_STATUS_CODE) {
            print("got data ="+data.toString());

//            _showSnackBar(data["messages"]["messageDescription"], Colors.green);


           setState(() {


             _photo = data ["Photo"].toString();

              if(data["Name"].toString() != null && !(data["Name"].toString().compareTo("null") == 0 ))
              _name = data["Name"].toString();

              if(data["EmailId"].toString() != null && !(data["EmailId"].toString().compareTo("null") == 0 ))
              _email = data["EmailId"].toString();
              print("Email ="+_email);
              if(data["Mobile"].toString() != null && !(data["Mobile"].toString().compareTo("null") == 0 ))
              _mobile = data["Mobile"].toString();

              if(data["BankName"].toString() != null && !(data["BankName"].toString().compareTo("null") == 0 ))
              _bankName = data["BankName"].toString();

              if(data["AccountNo"].toString() != null && !(data["AccountNo"].toString().compareTo("null") == 0 ))
              _accountNo = data["AccountNo"].toString();
              if(data["IFSCCode"].toString() != null && !(data["IFSCCode"].toString().compareTo("null") == 0 ))
              _ifscCode = data["IFSCCode"].toString();
              if(data["BankBranch"].toString() != null && !(data["BankBranch"].toString().compareTo("null") == 0 ))
              _bankBranch = data["BankBranch"].toString();

           });


          } else {
            print("Error=" + data["messages"]["messageDescription"]);

            _showSnackBar(data["messages"]["messageDescription"], Colors.red);
          }
          if (data == null)
            print("Unable to get data");
        } catch (e) {
          _showSnackBar("Some problem occurred", Colors.red);
        }
        break;

      case ERROR_CODE:
        _showSnackBar("Please check your internet connection ",Colors.red);
        setState(() {
          _isLoading = false;

        });
        break;
    }
  }

  void _showDialog(String title,String msg) {
    // flutter defined function
    showDialog(

      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(title),
          content: new Text(msg),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();

              },
            ),
          ],
        );
      },
    );
  }

}