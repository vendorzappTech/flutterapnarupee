import 'dart:convert';
import 'package:apanarupee/model/ContactInfo.dart';
import 'package:apanarupee/model/LogInInfo.dart';
import 'package:apanarupee/model/PersonalInfo.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:apanarupee/model/ApiInterface.dart';
import 'package:apanarupee/model/User.dart';
import 'package:apanarupee/utils/Helper.dart';
import 'package:apanarupee/utils/HexColor.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

class References extends StatefulWidget  {
  References({Key key, this.leadId}) : super(key: key);

  final String leadId;

  @override
  ReferencesState createState() => ReferencesState(leadId);
}

class ReferencesState extends State<References> with SingleTickerProviderStateMixin implements ApiInterface  {

  String _name ="",_mobile ="",_email="",_currentCityId="",_name2 ="",_mobile2 ="",_email2="",_currentCityId2="";

  var selectedDate = new DateTime.now();
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  bool _isLoading = false;
  final formKey1 = new GlobalKey<FormState>();
  final formKey2 = new GlobalKey<FormState>();
  final formKey3 = new GlobalKey<FormState>();

  List _mycity = new List();
  List _mycity2 = new List();

  final myControllerDOB = TextEditingController();
  final myControllerFileDate = TextEditingController();
  FocusNode focuNodeDOB,focuNodeFileDate;

  final dateFormat = DateFormat("dd/MM/yyyy");
  final dateFormat1 = DateFormat("yyyy-MM-ddThh:mm:ss");
  final dateFormat2 = DateFormat("yyyy-MM-dd");
  String loadId = "";
  TabController myTabController;

  static const int  GET_CITY_RESULT_CODE =1,GET_BANK_RESULT_CODE = 2,GET_APPLOCATION_RESULT_CODE = 3,UPDATE_APPLICATION_RESULT_CODE1 = 4,UPDATE_APPLICATION_RESULT_CODE2 = 5,UPDATE_APPLICATION_RESULT_CODE3 = 6,ERROR_CODE = -1;

  ReferencesState(String leadId){
    this.loadId = leadId;
  }

  @override
  void initState() {
    // TODO: implement initState
    // call webservices to get location
    myTabController = new TabController(length: 2, vsync: this);

    String url = Helper.GET_REFERENCE_DETAIL_URL+"?LeadId="+loadId;
    print("url="+url);
    Helper.callAuthGetApi(Uri.encodeFull( url),
        json.encode({

        }), context, this, GET_APPLOCATION_RESULT_CODE);

    setState(() {
      _isLoading = true;
    });


    FirebaseMessaging _firebaseMessaging;

    _firebaseMessaging = FirebaseMessaging();

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print('on message $message');

        // show notification here
//        _showNotificationWithDefaultSound();
        var title = message['notification']['title'];
        var body = message['notification']['body'];
        // show notification here
        _showDialog(title,body);



      },
      onResume: (Map<String, dynamic> message) async {
        print('on resume $message');
      },
      onLaunch: (Map<String, dynamic> message) async {
        print('on launch $message');
      },

    );


  }


//


  void changedLocationDropDownItem(String selectedCity) {
    print("Selected city $selectedCity, we are going to refresh the UI");
    setState(() {
      _currentCityId = selectedCity;
    });
  }

  void changedLocationDropDownItem2(String selectedCity) {
    print("Selected city $selectedCity, we are going to refresh the UI");
    setState(() {
      _currentCityId2 = selectedCity;
    });
  }

  _submitRefInfo1() {
    var form = formKey1.currentState;

    if (form.validate()) {
      setState(() => _isLoading = true);
      form.save();
      print("mobile=" + _mobile);

      ContactInfo ci = new ContactInfo();
      ci.leadId = loadId;
      ci.fullName = _name;
      ci.mobileNo = _mobile;
      ci.emailId = _email;
      ci.cityId = _currentCityId;
      ci.requestType = "FirstReference";


      Helper.callAuthApi(Uri.encodeFull(Helper.UPDATE_REFERENCE_URL),
          json.encode(ci.toJson1()), context, this, UPDATE_APPLICATION_RESULT_CODE1);
    }
   }

    _submitRefInfo2() {
      var form = formKey2.currentState;

      if (form.validate()) {
        setState(() => _isLoading = true);
        form.save();
        print("mobile=" + _mobile);

        ContactInfo ci = new ContactInfo();
        ci.leadId = loadId;
        ci.fullName = _name2;
        ci.mobileNo = _mobile2;
        ci.emailId = _email2;
        ci.cityId = _currentCityId2;
        ci.requestType = "SecondReference";

        Helper.callAuthApi(Uri.encodeFull(Helper.UPDATE_REFERENCE_URL),
            json.encode(ci.toJson2()), context, this, UPDATE_APPLICATION_RESULT_CODE2);
      }
    }


    @override
    Widget build(BuildContext context) {
      var full_name = new TextFormField(

        decoration: InputDecoration(
//          border: const OutlineInputBorder(),
            hintText: "Enter Full Name",
            labelText: "Full Name",
            hintStyle: TextStyle(color: Colors.grey),
            labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor()))
        ),
        autocorrect: true,
        validator: (String str) {
          // apply email validation here
          if(str.length > 0)
             return  null;
          else
            return "Enter name";
        },
        initialValue: _name,
        onSaved: (str) => _name = str,
        keyboardType: TextInputType.text,
        enabled: true,
      );


      var email =
      new TextFormField(

        decoration: InputDecoration(
//          border: const OutlineInputBorder(),
            hintText: "Enter Email",
            labelText: "Email",
            hintStyle: TextStyle(color: Colors.grey),
            labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor()))
        ),
        initialValue: _email,
        autocorrect: true,
        validator: (String str) {
          if(str.length > 0){
          // apply email validation here
          Pattern pattern =
              r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
          RegExp regex = new RegExp(pattern);
          if (!regex.hasMatch(str))
            return 'Enter Valid Email';
          else
            return null;}
          else
            return null;

        },
        onSaved: (str) => _email = str,
        keyboardType: TextInputType.emailAddress,
        enabled: true,


      );

      var mobile =
      new TextFormField(

        decoration: InputDecoration(
//          border: const OutlineInputBorder(),
            hintText: "Enter Mobile Number",
            labelText: "Mobile",
            hintStyle: TextStyle(color: Colors.grey),
            labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor()))
        ),
        initialValue: _mobile,
        autocorrect: true,
        validator: (str) =>
        (str.length == 10)
            ? null
            : "Invalid mobile number",
        onSaved: (str) => _mobile = str,
        keyboardType: TextInputType.phone,
        enabled: true,
        inputFormatters: [
          LengthLimitingTextInputFormatter(10),
          WhitelistingTextInputFormatter.digitsOnly,
        ],

      );

      var city =
      new TextFormField(

        decoration: InputDecoration(
//          border: const OutlineInputBorder(),
            hintText: "Enter City",
            labelText: "City",
            hintStyle: TextStyle(color: Colors.grey),
            labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor()))
        ),
        initialValue: _currentCityId,
        autocorrect: true,
        validator: (str) =>
        (str.length != 0)
            ? null
            : "Enter City Name",
        onSaved: (str) => _currentCityId = str,
        keyboardType: TextInputType.text,
        enabled: true,
        inputFormatters: [


        ],

      );



//      var city =
//      (_mycity.length > 0) ?
//      new FormField(
//        builder: (FormFieldState state) {
//          return InputDecorator(
//            decoration: InputDecoration(
////            border: const OutlineInputBorder(
////
////            ),
//              labelText: "City",
//              hintStyle: TextStyle(color: Colors.grey),
//              labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor())),
//              contentPadding: const EdgeInsets.symmetric(
//                  vertical: 5.0, horizontal: 5.0),
//            ),
//            child: new DropdownButtonHideUnderline(
//
//                child:
//                new DropdownButton(
//                  value: _currentCityId,
//                  items: _mycity.map((item) {
//                    return new DropdownMenuItem<String>(
//                      child: new Text(item['Name']),
//                      value: item['CityId'].toString(),
//                    );
//                  }).toList(),
//                  onChanged: changedLocationDropDownItem,
//                )
//
//            ),
//          );
//        },
//      ) : new Text("Loading");


      var submitRefInfo =
      ButtonTheme(
          minWidth: 50.0,
          height: 50.0,
          child:
          new RaisedButton(
            color: HexColor("#fb6616"),
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(5.0)),
            splashColor: Colors.blue,
            onPressed: _submitRefInfo1,
            child: new Text("SAVE", style: TextStyle(color: Colors.white)),
          ));


      var ref1 = Center(
        child: new Form(
          key: formKey1,
          child: ListView(

              padding: EdgeInsets.only(top: 0),

              children: <Widget>[
                new Padding(
                    padding: EdgeInsets.only(left: 24, right: 24, top: 10),
                    child:
                    full_name),

                new Padding(
                    padding: EdgeInsets.only(left: 24, right: 24, top: 10),
                    child: mobile),

                new Padding(
                    padding: EdgeInsets.only(left: 24, right: 24, top: 10),
                    child: email),

                new Padding(
                    padding: EdgeInsets.only(left: 24, right: 24, top: 10),
                    child: city),

                new Padding(
                    padding: EdgeInsets.only(
                        left: 24, right: 24, top: 20, bottom: 20),
                    child:
                    submitRefInfo),

              ]
          ),
        ),
      );

      var full_name2 = new TextFormField(

        decoration: InputDecoration(
//          border: const OutlineInputBorder(),
            hintText: "Enter Full Name",
            labelText: "Full Name",
            hintStyle: TextStyle(color: Colors.grey),
            labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor()))
        ),
        autocorrect: true,
        validator: (String str) {
          // apply email validation here
          if(str.length > 0)
            return  null;
          else
            return "Enter name";
        },
        initialValue: _name2,
        onSaved: (str) => _name2 = str,
        keyboardType: TextInputType.text,
        enabled: true,
      );


      var email2 =
      new TextFormField(

        decoration: InputDecoration(
//          border: const OutlineInputBorder(),
            hintText: "Enter Email",
            labelText: "Email",
            hintStyle: TextStyle(color: Colors.grey),
            labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor()))
        ),
        initialValue: _email2,
        autocorrect: true,
        validator: (String str) {
          if(str.length > 0) {
            // apply email validation here
            Pattern pattern =
                r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
            RegExp regex = new RegExp(pattern);
            if (!regex.hasMatch(str))
              return 'Enter Valid Email';
            else
              return null;
          }else
            return null;
        },
        onSaved: (str) => _email2 = str,
        keyboardType: TextInputType.emailAddress,
        enabled: true,


      );

      var mobile2 =
      new TextFormField(

        decoration: InputDecoration(
//          border: const OutlineInputBorder(),
            hintText: "Enter Mobile Number",
            labelText: "Mobile",
            hintStyle: TextStyle(color: Colors.grey),
            labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor()))
        ),
        initialValue: _mobile2,
        autocorrect: true,
        validator: (str) =>
        (str.length == 10)
            ? null
            : "Invalid mobile number",
        onSaved: (str) => _mobile2 = str,
        keyboardType: TextInputType.phone,
        enabled: true,
        inputFormatters: [
          LengthLimitingTextInputFormatter(10),
          WhitelistingTextInputFormatter.digitsOnly,
        ],

      );

//      var city2 =
//      (_mycity.length > 0) ?
//      new FormField(
//        builder: (FormFieldState state) {
//          return InputDecorator(
//            decoration: InputDecoration(
////            border: const OutlineInputBorder(
////
////            ),
//              labelText: "City",
//              hintStyle: TextStyle(color: Colors.grey),
//              labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor())),
//              contentPadding: const EdgeInsets.symmetric(
//                  vertical: 5.0, horizontal: 5.0),
//            ),
//            child: new DropdownButtonHideUnderline(
//
//                child:
//                new DropdownButton(
//                  value: _currentCityId2,
//                  items: _mycity2.map((item) {
//                    return new DropdownMenuItem<String>(
//                      child: new Text(item['Name']),
//                      value: item['CityId'].toString(),
//                    );
//                  }).toList(),
//                  onChanged: changedLocationDropDownItem2,
//                )
//
//            ),
//          );
//        },
//      ) : new Text("Loading");

      var city2 =
      new TextFormField(

        decoration: InputDecoration(
//          border: const OutlineInputBorder(),
            hintText: "Enter City",
            labelText: "City",
            hintStyle: TextStyle(color: Colors.grey),
            labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor()))
        ),
        initialValue: _currentCityId2,
        autocorrect: true,
        validator: (str) =>
        (str.length != 0)
            ? null
            : "Enter City Name",
        onSaved: (str) => _currentCityId2 = str,
        keyboardType: TextInputType.text,
        enabled: true,
        inputFormatters: [


        ],

      );

      var submitRefInfo2 =
      ButtonTheme(
          minWidth: 50.0,
          height: 50.0,
          child:
          new RaisedButton(
            color: HexColor("#fb6616"),
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(5.0)),
            splashColor: Colors.blue,
            onPressed: _submitRefInfo2,
            child: new Text("SAVE", style: TextStyle(color: Colors.white)),
          ));


      var ref2 = Center(
        child: new Form(
          key: formKey2,
          child: ListView(

              padding: EdgeInsets.only(top: 0),

              children: <Widget>[
                new Padding(
                    padding: EdgeInsets.only(left: 24, right: 24, top: 10),
                    child:
                    full_name2),

                new Padding(
                    padding: EdgeInsets.only(left: 24, right: 24, top: 10),
                    child: mobile2),

                new Padding(
                    padding: EdgeInsets.only(left: 24, right: 24, top: 10),
                    child: email2),

                new Padding(
                    padding: EdgeInsets.only(left: 24, right: 24, top: 10),
                    child: city2),

                new Padding(
                    padding: EdgeInsets.only(
                        left: 24, right: 24, top: 20, bottom: 20),
                    child:
                    submitRefInfo2),

              ]
          ),
        ),
      );


      ScreenUtil.instance = ScreenUtil.getInstance()
        ..init(context);

      return WillPopScope(
        child: DefaultTabController(
          length: 2,
          child: Scaffold(
            key: scaffoldKey,
            appBar: AppBar(
                bottom: TabBar(
                  controller: myTabController,
                  tabs: [
                    Tab(text: "Reference 1",),
                    Tab(text: "Reference 2",)
                  ],
                ),
                title: Text('Add References Details'),
                backgroundColor: HexColor("#2b2ca3")
            ),
            body: TabBarView(
              controller: myTabController,
              children: [
                (_isLoading) ? Align(
                    alignment: Alignment.center,
                    child: CircularProgressIndicator()
                ) : ref1,
                (_isLoading) ? Align(
                    alignment: Alignment.center,
                    child: CircularProgressIndicator()
                ) : ref2,

              ],
            ),
          ),
        ),
      );
    }


    void _showSnackBar(String text, Color color) {
      scaffoldKey.currentState
          .showSnackBar(new SnackBar(backgroundColor: color,
        content: new Text(text),
        duration: new Duration(milliseconds: 5000),));
    }

    void postExecution(var data, int result_code) {

      switch (result_code) {


        case GET_APPLOCATION_RESULT_CODE:
          if (data == null)
            _showSnackBar("Unable to get application details", Colors.red);
          else {
            setState(() {

              print("Data=" + data["FirstReference"].toString());

              // get info1
              _name = data["FirstReference"][0]["FirstFullName"].toString();
              print("Name = " + _name);
              _mobile = data["FirstReference"][0]["FirstMobile"].toString();
              _email = data["FirstReference"][0]["FirstEmailId"].toString();
              if (data["FirstReference"][0]["FirstCity"] != null &&
                  data["FirstReference"][0]["FirstCity"]
                      .toString()
                      .length > 0 && !(data["FirstReference"][0]["FirstCity"] == 0))
                _currentCityId = data["FirstReference"][0]["FirstCity"].toString();

              // get info2
              _name2 = data["SecondReference"][0]["SecondFullName"].toString();
              print("Name = " + _name2);

              _mobile2 = data["SecondReference"][0]["SecondMobile"].toString();
              _email2 = data["SecondReference"][0]["SecondEmailId"].toString();
              if (data["SecondReference"][0]["SecondCity"] != null &&
                  data["SecondReference"][0]["SecondCity"]
                      .toString()
                      .length > 0 && !(data["SecondReference"][0]["SecondCity"] == 0))
                _currentCityId2 = data["SecondReference"][0]["SecondCity"].toString();

              _isLoading = false;
            });
          }

          break;

        case UPDATE_APPLICATION_RESULT_CODE1:
          try {
            setState(() => _isLoading = false);

            if (data["responseCode"] == Helper.SUCCESS_STATUS_CODE) {
              print("got data");
              _showSnackBar(
                  data["messages"]["messageDescription"], Colors.green);
              myTabController.animateTo(1);
            } else {
              print("Error=" + data["messages"]["messageDescription"]);

              _showSnackBar(data["messages"]["messageDescription"], Colors.red);
            }
            if (data == null)
              print("Unable to get data");
          } catch (e) {
            _showSnackBar("Some problem occurred", Colors.red);
          }

          break;

        case UPDATE_APPLICATION_RESULT_CODE2:
          try {
            setState(() => _isLoading = false);

            if (data["responseCode"] == Helper.SUCCESS_STATUS_CODE) {
              print("got data");
              _showSnackBar(
                  data["messages"]["messageDescription"], Colors.green);
              myTabController.animateTo(2);
            } else {
              print("Error=" + data["messages"]["messageDescription"]);

              _showSnackBar(data["messages"]["messageDescription"], Colors.red);
            }
            if (data == null)
              print("Unable to get data");
          } catch (e) {
            _showSnackBar("Some problem occurred", Colors.red);
          }

          break;


        case ERROR_CODE:
          _showSnackBar("Please check your internet connection ", Colors.red);
          setState(() {
            _isLoading = false;
          });
          break;
      }
  }

  void _showDialog(String title, String msg) {
    // flutter defined function
    showDialog(

      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(title),
          content: new Text(msg),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}