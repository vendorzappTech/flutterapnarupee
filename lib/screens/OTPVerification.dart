import 'dart:convert';
import 'dart:io';

import 'package:apanarupee/model/ApiInterface.dart';
import 'package:apanarupee/screens/Dashboard.dart';
import 'package:apanarupee/utils/Helper.dart';
import 'package:apanarupee/utils/HexColor.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:shared_preferences/shared_preferences.dart';
//import 'package:sms/sms.dart';
import 'package:firebase_messaging/firebase_messaging.dart';


class OTPVerification extends StatefulWidget {
  String mobile;

  OTPVerification({Key key ,this.mobile }) : super(key: key);

  @override
  OTPVerificationState createState() => OTPVerificationState(mobile);
}

class OTPVerificationState extends State<OTPVerification> implements ApiInterface {
  bool _isLoading = false;
  String _mobile;
  final formKey = new GlobalKey<FormState>();
  final formKey1 = new GlobalKey<FormState>();
  FirebaseMessaging _firebaseMessaging;
  var token;
  bool _isLogInClick = false;
  String otp;


  TextEditingController controller1 = new TextEditingController();
  TextEditingController controller2 = new TextEditingController();
  TextEditingController controller3 = new TextEditingController();
  TextEditingController controller4 = new TextEditingController();
  TextEditingController controller5 = new TextEditingController();
  TextEditingController controller6 = new TextEditingController();

  static const int VERIFY_OTP_RESULT_CODE = 1,ERROR_CODE=-1,RESEND_OTP_RESULT_CODE=2;

  FocusNode focusNode1,focusNode2,focusNode3,focusNode4;


  final scaffoldKey = new GlobalKey<ScaffoldState>();


  void initState() {
    super.initState();
    _firebaseMessaging = FirebaseMessaging();
    firebaseCloudMessaging_Listeners();

    focusNode1 = FocusNode();
    focusNode2 = FocusNode();
    focusNode3 = FocusNode();
    focusNode4 = FocusNode();
  }

  OTPVerificationState(String mobile)
  {
    this._mobile = mobile;
  }

  void _verifyOtp() async
  {
    _isLogInClick = true;
    if (controller1.text.isEmpty) {
      _showSnackBar("Enter 1's digit", Colors.red);

    } else if (controller2.text.isEmpty) {
      _showSnackBar("Enter 2'nd digit", Colors.red);
    } else if (controller3.text.isEmpty){
      _showSnackBar("Enter 3'rd digit", Colors.red);
    }else if(controller4.text.isEmpty){
      _showSnackBar("Enter 4'th digit", Colors.red);
    }else{
       otp = controller1.text+controller2.text+controller3.text+controller4.text;

      setState(() {
        _isLoading = true;
      });

      if((this.token  == null) || (this.token.toString().length < 0))
      {
        _isLogInClick = true;

        try {
          _firebaseMessaging = FirebaseMessaging();
          firebaseCloudMessaging_Listeners();
        }catch(e){
          print(e.toString());
        }

      } else {

        // call webservice to verify otp
      Helper.callApi(Uri.encodeFull( Helper.VERIFY_OTP_URL),
          json.encode({
            "MobileNumber": this._mobile,
            "OTP":otp,
            "token": this.token
          }), context, this, VERIFY_OTP_RESULT_CODE);
      }
    }

  }
  void _resendOtp() async
  {
    setState(() {
      _isLoading = true;
    });
    // call webservice to resend otp
    Helper.callApi(Uri.encodeFull( Helper.RESEND_OTP_URL),
        json.encode({
          "MobileNumber": this._mobile
        }), context, this, RESEND_OTP_RESULT_CODE);
  }

  void init()
  {


//    SmsReceiver receiver = new SmsReceiver();
//    receiver.onSmsReceived.listen((SmsMessage msg) {
//
//
//      print("SMS="+msg.body);
//     String sms = msg.body;
//     controller1.text= sms.substring(0,1);
//     controller2.text= sms.substring(1,2);
//     controller3.text= sms.substring(2,3);
//     controller4.text= sms.substring(3,4);
//
//
//
//    }

  //  receiver.onSmsReceived.listen((SmsMessage msg) => print(msg.body));

    //);


  }

  void _showSnackBar(String text,Color color) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(backgroundColor:color ,content: new Text(text),duration: new Duration(milliseconds: 5000),));
  }


  void postExecution(var data ,int result_code) {

    switch(result_code) {
      case VERIFY_OTP_RESULT_CODE :
        try {
          setState(() => _isLoading = false);

          if (data["responseCode"] == Helper.SUCCESS_STATUS_CODE) {
            print("got data");

            // save access token

            // save data and move to Home screen
            _savePreferences(data);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) =>   Dashboard(title:"Dashboard")),
            );

          } else {
            print("Error=" + data["messages"]["messageDescription"]);

            _showSnackBar(data["messages"]["messageDescription"], Colors.red);
            controller1.text = "";
            controller2.text = "";
            controller3.text = "";
            controller4.text = "";

          }
          if (data == null)
            print("Unable to get data");
        } catch (e) {
          _showSnackBar("Some problem occurred", Colors.red);
        }
        break;

      case RESEND_OTP_RESULT_CODE:
        setState(() => _isLoading = false);
        if (data["responseCode"] == Helper.SUCCESS_STATUS_CODE) {
          print("got data");

          _showSnackBar(data["messages"]["messageDescription"], Colors.green);

//          Future.delayed(const Duration(milliseconds: 5000), () {
//
//// Here you can write your code
//
//            setState(() {
//              // Here you can write your code for open new view
//              Navigator.of(context).pop();
//            });
//
//          });


        } else {
          print("Error=" + data["messages"]["messageDescription"]);

          _showSnackBar(data["messages"]["messageDescription"], Colors.red);
        }
        if (data == null)
          print("Unable to get data");
        break;
      case ERROR_CODE:
        _showSnackBar("Please check your internet connection ",Colors.red);

        setState(() {
          _isLoading = false;

        });
        break;
    }
  }

  @override
  Widget build(BuildContext context) {

    ScreenUtil.instance = ScreenUtil.getInstance()..init(context);
    final logo =  Hero(
        tag: 'hero',
        child:
        new Container(

            height: ScreenUtil.getInstance().setHeight(800),
//          width: 600.0,
            decoration: new BoxDecoration(
              image: new DecorationImage(image: new AssetImage("assets/loginbg.png"), fit: BoxFit.fill,
              ),
            ),

            child:
            new Column(
              children: <Widget>[
                AppBar
                  (

                    elevation: 0.0,
                    backgroundColor: Colors.transparent,
                    iconTheme: IconThemeData(
                        color: Colors.white),title:Text( "Verify OTP",style: TextStyle(fontSize: ScreenUtil.getInstance().setSp(60)),)),
                new Padding(
                  padding: EdgeInsets.only(top: ScreenUtil.getInstance().setHeight(100)),
                  child:
                  Center(
                    child: new Image.asset("assets/otp.png",width: ScreenUtil.getInstance().setWidth(350),height: ScreenUtil.getInstance().setWidth(350)),
                  ),)
              ],
            )

        )
    );
//    final mobileFocus = new FocusNode();
//    final passwordFocus = new FocusNode();

    var otpButton =
    ButtonTheme(
        minWidth: 50.0,
        height: 50.0,
        child:
        new RaisedButton(
          color: HexColor("#fb6616"),
          shape: new RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(5.0)),
          splashColor: Colors.blue,
          onPressed: _verifyOtp,
          child: new Text("VERIFY", style: TextStyle(color:Colors.white,fontSize: ScreenUtil.getInstance().setSp(50))),

        ));




    return new Scaffold(
        key: scaffoldKey,
        backgroundColor: Colors.white,

//
        body:  new Form(
            key:formKey,
            child: ListView(
          shrinkWrap: true,

                padding: EdgeInsets.only(top: 0),

                children: <Widget>[

                  logo,
                  SizedBox(height: ScreenUtil.getInstance().setHeight(72)),
                  new Center(child:new Text("Enter OTP",style: TextStyle(color: Colors.black,fontSize: ScreenUtil.getInstance().setSp(60)),)),
                  SizedBox(height: ScreenUtil.getInstance().setHeight(48)),
                  new Center(child:new Text("4 digit OTP has been sent to",style: TextStyle(color: Colors.black,fontSize: ScreenUtil.getInstance().setSp(40)),)),
                  new Center(child:new Text(_mobile,style: TextStyle(color: Colors.black,fontSize: ScreenUtil.getInstance().setSp(50),fontWeight: FontWeight.bold),)),
                  new Padding(padding: EdgeInsets.only(top: ScreenUtil.getInstance().setHeight(30),bottom: ScreenUtil.getInstance().setHeight(30),left:ScreenUtil.getInstance().setHeight(180),right: ScreenUtil.getInstance().setHeight(180)),
                 child:
                 new Center(
                   child:
                   Row(
                     children: <Widget>[
                       Expanded(
                         child:
                         Padding(
                           padding:  EdgeInsets.only(right: ScreenUtil.getInstance().setHeight(45), left: ScreenUtil.getInstance().setHeight(45)),
                           child: new Container(
                             alignment: Alignment.center,
//                             decoration: new BoxDecoration(
//                                 color: Color.fromRGBO(0, 0, 0, 0.1),
//                                 border: new Border.all(
//                                     width: 1.0,
//                                     color: Color.fromRGBO(0, 0, 0, 0.1)
//                                 ),
//                                 borderRadius: new BorderRadius.circular(4.0)
//                             ),
                             child: new TextField(
                               inputFormatters: [
                                 LengthLimitingTextInputFormatter(1),
                               ],

                               textAlign: TextAlign.center,
                               controller: controller1,
                               autofocus: false,
                               keyboardType: TextInputType.number,
                               focusNode: focusNode1,
                               onChanged: (String text){
                                 FocusScope.of(context).requestFocus(focusNode2);
                               },


                               style: TextStyle(fontSize: ScreenUtil.getInstance().setSp(50), color: Colors.black),
                             ),
                           ),
                         ),
                       ),
                       Expanded(
                         child: Padding(
                           padding:  EdgeInsets.only(right:ScreenUtil.getInstance().setHeight(45) , left: ScreenUtil.getInstance().setHeight(45)),
                           child: new Container(
                             alignment: Alignment.center,
//                             decoration: new BoxDecoration(
//                                 color: Color.fromRGBO(0, 0, 0, 0.1),
//                                 border: new Border.all(
//                                     width: 1.0,
//                                     color: Color.fromRGBO(0, 0, 0, 0.1)
//                                 ),
//                                 borderRadius: new BorderRadius.circular(4.0)
//                             ),
                             child: new TextField(
                               inputFormatters: [
                                 LengthLimitingTextInputFormatter(1),
                               ],

                               textAlign: TextAlign.center,
                               controller: controller2,
                               autofocus: false,
                               keyboardType: TextInputType.number,
                               focusNode: focusNode2,
                               onChanged: (String text){
                                 FocusScope.of(context).requestFocus(focusNode3);
                               },



                               style: TextStyle(fontSize: ScreenUtil.getInstance().setSp(50), color: Colors.black),
                             ),
                           ),
                         )
                       ),
                       Expanded(
                         child: Padding(
                           padding:  EdgeInsets.only(right: ScreenUtil.getInstance().setHeight(45), left: ScreenUtil.getInstance().setHeight(45)),
                           child: new Container(
                             alignment: Alignment.center,
//                             decoration: new BoxDecoration(
//                                 color: Color.fromRGBO(0, 0, 0, 0.1),
//                                 border: new Border.all(
//                                     width: 1.0,
//                                     color: Color.fromRGBO(0, 0, 0, 0.1)
//                                 ),
//                                 borderRadius: new BorderRadius.circular(4.0)
//                             ),
                             child: new TextField(
                               inputFormatters: [
                                 LengthLimitingTextInputFormatter(1),
                               ],

                               textAlign: TextAlign.center,
                               controller: controller3,
                               autofocus: false,
                               keyboardType: TextInputType.number,
                               focusNode: focusNode3,
                               onChanged: (String text){
                                 FocusScope.of(context).requestFocus(focusNode4);
                               },

                               style: TextStyle(fontSize: ScreenUtil.getInstance().setSp(50), color: Colors.black),
                             ),
                           ),
                         ),
                       ),
                       Expanded(
                         child: Padding(
                           padding: const EdgeInsets.only(right: 15.0, left: 15.0),
                           child: new Container(
                             alignment: Alignment.center,
//                             decoration: new BoxDecoration(
//                                 color: Color.fromRGBO(0, 0, 0, 0.1),
//                                 border: new Border.all(
//                                     width: 1.0,
//                                     color: Color.fromRGBO(0, 0, 0, 0.1)
//                                 ),
//                                 borderRadius: new BorderRadius.circular(4.0)
//                             ),
                             child: new TextField(
                               inputFormatters: [
                                 LengthLimitingTextInputFormatter(1),
                               ],

                               textAlign: TextAlign.center,
                               controller: controller4,
                               autofocus: false,
                               keyboardType: TextInputType.number,
                               focusNode: focusNode4,



                               style: TextStyle(fontSize: ScreenUtil.getInstance().setSp(50), color: Colors.black),
                             ),
                           ),
                         ),
                       ),
                     ],
                   )
                 ),),

            new Padding(
                      padding: EdgeInsets.only(left:ScreenUtil.getInstance().setHeight(72),right: ScreenUtil.getInstance().setHeight(72),top: ScreenUtil.getInstance().setHeight(96)),
                      child:
                      otpButton),
                  SizedBox(height: ScreenUtil.getInstance().setHeight(30)),
                  new Center(
                    child:
                    (_isLoading)? Align(
                        alignment: Alignment.center,
                        child: CircularProgressIndicator()
                    ): SizedBox(height: 0.0),
                  ),
                  new Padding(padding: EdgeInsets.only(top: ScreenUtil.getInstance().setHeight(72)),
                      child:
                      new Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          new GestureDetector(
                            onTap: () {
                                _resendOtp();

                            },
                            child: new Text("Resend OTP",style: TextStyle(color: HexColor("#fb6616"),fontSize: ScreenUtil.getInstance().setSp(50),fontWeight: FontWeight.bold),),
                          ),

                        ],
                      ))


                ]
            ),

        )
    );
  }

  void _savePreferences(var data) async{
    String token = data['accessToken'];
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool("isLoggedIn", true);
    prefs.setString("accessToken", token);
  }

  // firebase code
  void firebaseCloudMessaging_Listeners() {
    if (Platform.isIOS) iOS_Permission();

    if(_firebaseMessaging != null) {
      _firebaseMessaging.requestNotificationPermissions(
          const IosNotificationSettings(sound: true, badge: true, alert: true));
      _firebaseMessaging.getToken().then((token) {
        print("Token=" + token);
        //  print("id_guest="+this._id_guest);

        this.token = token;

        if ((this.token != null) && (this.token
            .toString()
            .length > 0)) {
          if (this._isLogInClick) {
            this._isLogInClick = false;
            // call webservice to verify otp
            Helper.callApi(Uri.encodeFull(Helper.VERIFY_OTP_URL),
                json.encode({
                  "MobileNumber": this._mobile,
                  "OTP": otp,
                  "token": this.token
                }), context, this, VERIFY_OTP_RESULT_CODE);
          } else {

          }
        }
      });
    }
     try{
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print('on message $message');

        // show notification here
//        _showNotificationWithDefaultSound();
        var title = message['notification']['title'];
        var body = message['notification']['body'];
        _showDialog(title,body);

      },
      onResume: (Map<String, dynamic> message) async {
        print('on resume $message');
      },
      onLaunch: (Map<String, dynamic> message) async {
        print('on launch $message');
      },

    );}
    catch(e){
      print(e.toString());
    }

  }

  void iOS_Permission() {
    _firebaseMessaging.requestNotificationPermissions(
        IosNotificationSettings(sound: true, badge: true, alert: true)
    );
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings)
    {
      print("Settings registered: $settings");
    });
  }

  void _showDialog(String title,String msg) {
    // flutter defined function
    showDialog(

      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(title),
          content: new Text(msg),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();

              },
            ),
          ],
        );
      },
    );
  }
}