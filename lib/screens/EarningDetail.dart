import 'dart:convert';

import 'package:apanarupee/model/ApiInterface.dart';
import 'package:apanarupee/screens/OTPVerification.dart';
import 'package:apanarupee/utils/Helper.dart';
import 'package:apanarupee/utils/HexColor.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

class EarningDetail extends StatefulWidget  {
  EarningDetail({Key key, this.leadId}) : super(key: key);

  final String leadId;


  @override
  EarningDetailState createState() => EarningDetailState(leadId);
}

class EarningDetailState extends State<EarningDetail> implements ApiInterface{

   String leadId;
   String name="",applOn="",typeOfLoan="",loanAmount="",bankApllicationId="",loanAccountNo="",totalDisbsAmnt="",totalLoanAmnt="",totalEarnAmnt="";
   static const int GET_EARNING_DETAIL_RESULT_CODE = 1, ERROR_CODE = -1;
   bool isLoading = true;
   List earnList = new List();
   final scaffoldKey = new GlobalKey<ScaffoldState>();
   final dateFormat1 = DateFormat("yyyy-MM-ddThh:mm:ss");
   final dateFormat2 = DateFormat("dd-MMM-yyyy");


   EarningDetailState(String leadId) {
       this.leadId = leadId;
  }

  @override
  void initState() {
    // TODO: implement initState

    super.initState();
    print("In init");
    // call web service to get data

    setState(() {
      isLoading = true;
    });

    String url = Helper.GET_EARNING_DETAILS_URL+"?LeadId="+leadId;
    Helper.callAuthGetApi(Uri.encodeFull(url ),
        json.encode({

        }), context, this, GET_EARNING_DETAIL_RESULT_CODE);


    FirebaseMessaging _firebaseMessaging;

    _firebaseMessaging = FirebaseMessaging();

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print('on message $message');

        // show notification here
//        _showNotificationWithDefaultSound();
        var title = message['notification']['title'];
        var body = message['notification']['body'];
        // show notification here
        _showDialog(title,body);



      },
      onResume: (Map<String, dynamic> message) async {
        print('on resume $message');
      },
      onLaunch: (Map<String, dynamic> message) async {
        print('on launch $message');
      },

    );


  }


  @override
  Widget build(BuildContext context) {

    ScreenUtil.instance = ScreenUtil.getInstance()..init(context);

    final logo = Hero(
        tag: 'hero',
        child:
        new Container(

            height:
            (MediaQuery.of(context).size.height >800)?
            ScreenUtil.getInstance().setHeight(750):ScreenUtil.getInstance().setHeight(950),
//          width: 600.0,
            decoration: new BoxDecoration(
              image: new DecorationImage(
                image: new AssetImage("assets/earndashboard.png"),
                fit: BoxFit.fill,
              ),
            ),

            child:
            new Column(
              children: <Widget>[
            new Container(
            height: ScreenUtil.getInstance().setHeight(200),
            child:
            AppBar
                  (
                    elevation: 0.0,
                    backgroundColor: Colors.transparent,
                    iconTheme: IconThemeData(
                        color: Colors.white),
                    title: Text("Earning Details", style: TextStyle(
                        fontSize: ScreenUtil.getInstance().setSp(60)),))),
                new Container(
                    margin: EdgeInsets.only(left: 10,right: 10),
                  //  height: ScreenUtil.getInstance().setHeight(400),
                    width: ScreenUtil.getInstance().setHeight(1100),
//                decoration:new BoxDecoration(
//                 // image: new DecorationImage(image: new AssetImage("assets/overviewbg.png"), fit: BoxFit.fill,
//                  color: Colors.white,
//                  //borderRadius: BorderRadius.circular(15),
//
//
//                  ),

                    child:
                    new Padding(padding: EdgeInsets.all(0),
                        child:
                        new Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(15.0),
                          ),
                          child: new Row(
                            children: <Widget>[

                              Expanded
                                (
                                  flex: 7,
                                  child:
                                  new Padding(padding: EdgeInsets.all(10),
                                      child:
                                      new Column(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: <Widget>[
                                          new Padding(padding: EdgeInsets.only(bottom: 0),
                                          child:
                                          new Text(name,style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black,fontSize: ScreenUtil.getInstance().setSp(50)),)),
                                          new Row(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            mainAxisAlignment: MainAxisAlignment.start,
                                            children: <Widget>[
                                              Expanded(
                                                flex: 5,
                                                child:
                                                new Text("Lead Id",style: TextStyle(color: Colors.grey),)
                                              ),
                                              Expanded(
                                                  flex: 5,
                                                  child:
                                                  new Text("Application On",style: TextStyle(color: Colors.grey),)
                                              ),

                                            ],
                                          ),
                                          new Row(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            mainAxisAlignment: MainAxisAlignment.start,

                                            children: <Widget>[

                                              Expanded(
                                                  flex: 5,
                                                  child:
                                                  new Text(leadId,style: TextStyle(color: Colors.black),)
                                              ),
                                              Expanded(
                                                  flex: 5,
                                                  child:
                                                  new Text(applOn,style: TextStyle(color: Colors.black),)
                                              ),
                                            ],
                                          ),
                                          new SizedBox(height: 5,),
                                          new Text("Type of Loan",style: TextStyle(color: Colors.grey),),
                                          new Text(typeOfLoan,style: TextStyle(color: Colors.black),),
                                          new SizedBox(height: 5,),
                                          new Text("Loan Amount",style: TextStyle(color: Colors.grey),),
                                          new Text(loanAmount,style: TextStyle(color: Colors.black),),
                                          new SizedBox(height: 5,),
                                          new Text("Bank Application Id",style: TextStyle(color: Colors.grey),),
                                          new Text(bankApllicationId,style: TextStyle(color: Colors.black),),
                                          new SizedBox(height: 5,),
                                          new Text("Loan Account no",style: TextStyle(color: Colors.grey),),
                                          new Text(loanAccountNo,style: TextStyle(color: Colors.black),),


                                        ],
                                      ))
                              ),
                              Expanded
                                (
                                  flex: 3,
                                  child:
                                  new Container(
                                    height:ScreenUtil.getInstance().setHeight(530) ,
                                  child:
                                  new Column(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    crossAxisAlignment:CrossAxisAlignment.end ,
                                    children: <Widget>[
                                      Padding(padding: EdgeInsets.only(bottom:0),
                                          child:
                                          Image(image: AssetImage("assets/earninghand.png"),fit: BoxFit.fill,))
                                    ],
                                  ))


                              )
                            ],
                          ),
                        )

                    )

                )
          ])

    ));

//    return new WebviewScaffold(
//      url: url,
//      appBar:  AppBar
//        (
//          elevation: 0.0,
//          backgroundColor: Color(HexColor.getAppBlueColor()),
//          title: Text(title, style: TextStyle(
//              fontSize: ScreenUtil.getInstance().setSp(60)),)),
//    );

    return new Scaffold(

        key: scaffoldKey,
        backgroundColor: Colors.white,

        body:
        new Form(

          child:(!isLoading)? ListView(
              
              padding: EdgeInsets.all(0),

              children: <Widget>[
                logo,
                new Container(
                  padding: EdgeInsets.all(10),
                  height:
                  (MediaQuery.of(context).size.height >800)?
                  MediaQuery
                      .of(context)
                      .size
                      .height-ScreenUtil.getInstance().setHeight(780):MediaQuery
              .of(context)
              .size
              .height-ScreenUtil.getInstance().setHeight(980),
//                  color: Colors.grey,
                  child:

                    new Column(

                      children: <Widget>[

                          new Row(
                            children: <Widget>[
                              Expanded(
                                flex: 5,
                                child: new Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    new Padding(padding: EdgeInsets.only(left: 5),
                                    child:
                                    new Text("Disbursed Amount",style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black,fontSize: ScreenUtil.getInstance().setSp(40)),),),
                                  ],
                                ),
                              ),
                              Expanded(
                                flex: 5,
                                child: new Row(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  mainAxisAlignment: MainAxisAlignment.end,
                                      children: <Widget>[
                                new Padding(padding: EdgeInsets.only(right: 5),
                                child:
                                        new Text("Earning",style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black,fontSize: ScreenUtil.getInstance().setSp(40)),),),
                                       // new Text("(5%)",style: TextStyle(fontWeight: FontWeight.bold,color: Color(HexColor.getAppOrangeColor()),fontSize: ScreenUtil.getInstance().setSp(40)),),


                                  ],
                                ),
                              ),
                            ],
                          ),

                        Expanded(
                          flex: 8,
                          child:   new ListView.builder
                            (
                              padding: EdgeInsets.only(top: 5,bottom: 5),
                              //physics: NeverScrollableScrollPhysics(),
                              //  controller: _scrollController,
                              itemCount: earnList.length,
                              itemBuilder: (BuildContext ctxt, int index) {
                                return
                                  new Card(

                                      child:

                                      new Row(
                                        children: <Widget>[
                                          Expanded(
                                              flex: 7,
                                              child:

                                              new Row(

                                                mainAxisAlignment:MainAxisAlignment.start ,
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: <Widget>[
                                                 new Column(

                                                   mainAxisAlignment:MainAxisAlignment.center ,
                                                   crossAxisAlignment: CrossAxisAlignment.center,

                                                   children: <Widget>[
                                                     new Padding(padding: EdgeInsets.all( 10),
                                                       child:

                                                       new Image(image: AssetImage("assets/earningrupee.png"),width: 35,)),
                                                   ],
                                                 ) ,



                                                  new Padding(padding: EdgeInsets.only(left: 10),
                                                      child:
                                                      new Column(
                                                        mainAxisAlignment: MainAxisAlignment.start,
                                                        crossAxisAlignment: CrossAxisAlignment.start,
                                                        children: <Widget>[
                                                          new Row(
                                                            crossAxisAlignment: CrossAxisAlignment.start,
                                                            mainAxisAlignment: MainAxisAlignment.start,
                                                            children: <Widget>[
                                                              new Padding(padding: EdgeInsets.only(right: 5,bottom: 10,top: 3),
                                                                  child:
                                                                  new Container(
                                                                    height: ScreenUtil.getInstance().setHeight(30),
                                                                    child:
                                                                    new Image.asset("assets/brupee.png"),) ),

                                                              new Padding(padding: EdgeInsets.only(right: 10,bottom: 5),
                                                                  child:
                                                                  new Text(earnList[index]["DisbursAmount"].toString(),style: TextStyle(fontSize:ScreenUtil.getInstance().setSp(45),fontWeight:FontWeight.bold,color: Color(HexColor.getAppBlueColor())),))
                                                            ],
                                                          ),

                                                          new Row(
                                                            children: <Widget>[
                                                              new Text("  Date:",style: TextStyle(color: Colors.grey,fontSize:ScreenUtil.getInstance().setSp(40) ),),
                                                              new Text(" "+dateFormat2.format(dateFormat1.parse(earnList[index]["DisbursDate"].toString())),style: TextStyle(color: Colors.black,fontSize:ScreenUtil.getInstance().setSp(40) ),),
                                                            ],
                                                          )
                                                        ],
                                                      )
                                                  ),
                                                ],
                                              )
                                          ),
                                          Expanded(
                                              flex: 5,
                                              child: new Column(
                                               children: <Widget>[
                                                 new Row(
                                                   crossAxisAlignment: CrossAxisAlignment.end,
                                                   mainAxisAlignment: MainAxisAlignment.end,
                                                   children: <Widget>[
                                                     new Padding(padding: EdgeInsets.only(right: 5,bottom: 15,top:2),
                                                         child:

                                                         new Container(
                                                           height: ScreenUtil.getInstance().setHeight(30),
                                                           child:
                                                           new Image.asset("assets/brupee.png"),) ),

                                                     new Padding(padding: EdgeInsets.only(right: 10,bottom: 10),
                                                         child:
                                                         new Text(earnList[index]["EarningAmount"].toString(),style: TextStyle(fontSize:ScreenUtil.getInstance().setSp(45),fontWeight:FontWeight.bold,color: Color(HexColor.getAppBlueColor())),))
                                                   ],
                                                 ),
                                                 SizedBox(height: 10,)

                                               ],
                                              ),

                                          )
                                        ],
                                      ));


                              }

                          ),
                        ),
                        Expanded(
                          flex: (MediaQuery.of(context).size.height >800)?2:3,
                          child:

                          new Card(
                          child:
                          new Padding(padding: EdgeInsets.all(10),
                          child:
                          new Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              new Row(
                                children: <Widget>[
                                  Expanded(
                                    flex: 5,
                                    child: new Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: <Widget>[
                                        new Text("Total Disbursed Amount:",style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black,fontSize: ScreenUtil.getInstance().setSp(40)),),
                                      ],
                                    ),
                                  ),
                                  Expanded(
                                    flex: 5,
                                    child: new Column(
                                      crossAxisAlignment: CrossAxisAlignment.end,
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: <Widget>[
                                        new Text("Total Earning:",style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black,fontSize: ScreenUtil.getInstance().setSp(40)),),

                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              new SizedBox(height: 5,),
                              new Row(
                                children: <Widget>[
                                  Expanded(
                                    flex: 5,
                                    child: new Column(
                                      crossAxisAlignment: CrossAxisAlignment.end,
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: <Widget>[
                                        new Row(
                                          children: <Widget>[
                                            new Padding(padding: EdgeInsets.only(bottom: 2),
                                            child:
                                            new Container(
                                              height: ScreenUtil.getInstance().setHeight(30),
                                              child:
                                              new Image.asset("assets/brupee.png"),padding: EdgeInsets.all(0),) ),

                                            new Text(totalDisbsAmnt,style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black,fontSize: ScreenUtil.getInstance().setSp(50)),),
                                          ],
                                        )

                                      ],
                                    ),
                                  ),
                                  Expanded(
                                    flex: 5,
                                    child: new Column(
                                      crossAxisAlignment: CrossAxisAlignment.end,
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: <Widget>[
                                        new Row(
                                          crossAxisAlignment: CrossAxisAlignment.end,
                                          mainAxisAlignment: MainAxisAlignment.end,

                                          children: <Widget>[
                                            new Padding(padding: EdgeInsets.only(bottom: 5),
                                                child:
                                                new Container(
                                                  height: ScreenUtil.getInstance().setHeight(30),
                                                  child:
                                                  new Image.asset("assets/brupee.png"),padding: EdgeInsets.all(0),) ),

                                            new Text(totalEarnAmnt,style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black,fontSize: ScreenUtil.getInstance().setSp(50)),),
                                          ],
                                        )

                                      ],
                                    ),
                                  ),
                                ],
                              )
                            ],
                          ),
                        )))

                      ],
                    )
                )


              ]
          ):Align(
              alignment: Alignment.center,
              child: CircularProgressIndicator())

        )
    );
  }

   void _showSnackBar(String text,Color color) {
     scaffoldKey.currentState
         .showSnackBar(new SnackBar(backgroundColor:color ,content: new Text(text),duration: new Duration(milliseconds: 5000),));
   }


   void postExecution(var data ,int result_code)  {

     switch(result_code) {


       case GET_EARNING_DETAIL_RESULT_CODE:
         try{
           if (data["responseCode"] == Helper.SUCCESS_STATUS_CODE) {
             print("got data");
             // _showSnackBar(data["messages"]["messageDescription"], Colors.green);
             print(data.toString());
             setState(() {
               isLoading = false;
               name = data["Name"].toString();
               applOn = dateFormat2.format(dateFormat1.parse(data["ApplyDate"].toString()));
               typeOfLoan = data["ProductName"].toString();
               loanAmount = data["LoanAmount"].toString();
               bankApllicationId = data["BankApplicationId"].toString();
               loanAccountNo = data["AccountNumber"].toString();
               totalDisbsAmnt = data["TotalDisbAmount"].toString();
               totalEarnAmnt = data["TotalEarnigAmount"].toString();

               earnList = data["DisbursedList"];
               print("Name ="+name);
               print("List size ="+earnList.length.toString());
             });


           } else {
             print("Error=" + data["messages"]["messageDescription"]);

             _showSnackBar(data["messages"]["messageDescription"], Colors.red);
           }
           if (data == null)
             print("Unable to get data");
         } catch (e) {
           _showSnackBar("Some problem occurred", Colors.red);
         }

         break;


       case ERROR_CODE:
         _showSnackBar("Please check your internet connection ",Colors.red);
         setState(() {
           //_isLoading = false;

         });
         break;
     }
   }

   void _showDialog(String title,String msg) {
     // flutter defined function
     showDialog(

       context: context,
       builder: (BuildContext context) {
         // return object of type Dialog
         return AlertDialog(
           title: new Text(title),
           content: new Text(msg),
           actions: <Widget>[
             // usually buttons at the bottom of the dialog
             new FlatButton(
               child: new Text("Close"),
               onPressed: () {
                 Navigator.of(context).pop();

               },
             ),
           ],
         );
       },
     );
   }




}
