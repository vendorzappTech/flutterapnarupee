import 'package:apanarupee/screens/Dashboard.dart';
import 'package:apanarupee/screens/LogIn.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashPage extends StatefulWidget {

  @override
  SplashPageState createState() => SplashPageState();
}

class SplashPageState extends State<SplashPage> {

// THIS FUNCTION WILL NAVIGATE FROM SPLASH SCREEN TO HOME SCREEN.    // USING NAVIGATOR CLASS.

  void navigationToNextPage() async {


    _checkLoggedIn();
  }

//  void _checkLoggedIn() async {
//
//    SharedPreferences prefs = await SharedPreferences.getInstance();
//    if(prefs.getBool("isLoggedIn") != null) {
//      if (prefs.getBool("isLoggedIn")) {
//        //  Navigator.of(context).pushReplacementNamed("/home");
//        // _navigateToHomeScreen(context);
//        print('logged in');
//        Navigator.push(
//          context,
//          MaterialPageRoute(builder: (context) =>   Dashboard(title:"Dashboard")),
//        );
//
////          Map results = await Navigator.pushReplacement(
////              context, MaterialPageRoute(builder: (context) => Dashboard(title:"Dashboard" )));
//
//      }else
//        {
//          print('not logged in');
//         // Navigator.popAndPushNamed(context, '/login');
//          Navigator.push(
//            context,
//            MaterialPageRoute(builder: (context) =>    Dashboard(title:"Dashboard")),
//          );
//
//        }
//    }
//  }

  void _checkLoggedIn() async {

    SharedPreferences prefs = await SharedPreferences.getInstance();
    if(prefs.getBool("isLoggedIn") != null) {
      if (prefs.getBool("isLoggedIn")) {
        //  Navigator.of(context).pushReplacementNamed("/home");
        // _navigateToHomeScreen(context);
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) =>   Dashboard(title:"Dashboard")),
        );
      }
    }
    else{
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) =>   LogIn()),

      );
      Navigator.pop(context);
    }
  }


  startSplashScreenTimer() async {
    var _duration = new Duration(seconds: 2);
    await new Timer(_duration, navigationToNextPage);
  }

  @override
  void initState() {

    startSplashScreenTimer();
    super.initState();

    print('hi');
    print('hello');


  }



  @override
  Widget build(BuildContext context) {

    // To make this screen full screen.
    // It will hide status bar and notch.
//    SystemChrome.setEnabledSystemUIOverlays([]);

    // full screen image for splash screen.
    return
      new Scaffold(

        backgroundColor: Colors.white,
        body:
        Container(
         width: double.infinity,
        child: new Image.asset('assets/splash.jpg'))
      );
  }
}