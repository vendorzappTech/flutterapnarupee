import 'dart:convert';
import 'dart:math';

import 'package:apanarupee/model/ApiInterface.dart';
import 'package:apanarupee/screens/OTPVerification.dart';
import 'package:apanarupee/utils/Helper.dart';
import 'package:apanarupee/utils/HexColor.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';

class Demo extends StatefulWidget  {
  @override
  DemoState createState() => DemoState();
}

class DemoState extends State<Demo> {

  int pageSize = 10;
  int pageNumber = 1;
  ScrollController controller ;
  List _mydisbursed = new List();
  final scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    // TODO: implement initState
    controller = new ScrollController()..addListener(_scrollListener);
    loadMore();
    super.initState();


  }

  void _scrollListener() {
    print(controller.position.extentAfter);

  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Demo"),
      ),
      body: LazyLoadScrollView(
        onEndOfPage:  () => loadMore(),
        child: ListView.builder(
          itemCount: _mydisbursed.length,
          itemBuilder: (context, _index) {
            return Card(
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Text(_mydisbursed[_index].toString(), style: TextStyle(fontSize: 14.0),),
              ),
            );
          },
        ),
      ),
    );
  }


  loadMore()async{


    String url = Helper.GET_ALL_LEADS_URL+"?pageNumber="+pageNumber.toString()+"&pageSize="+pageSize.toString()+"&LeadStatus=disbursed";
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString("accessToken");

    print("url="+url);
    var data = await http.get(Uri.encodeFull(url),
        headers: {"Content-Type": "application/json", "Authorization": "Basic "+token}).timeout(
        const Duration(seconds: 10));

    setState(() {
       List temp =  json.decode(data.body);
       if(temp.length >0)
       _mydisbursed.addAll(json.decode(data.body));
       else
         print("No data");
    });


    print("Data="+_mydisbursed.length.toString());

    pageNumber++;
  }


  _fetchEntry(int index) async {
    print("Future called $index");

    await Future.delayed(Duration(milliseconds: 500));
    if(index < 10) {
      return {
        'name': 'product $index',
        'price': Random().nextInt(100)
      };
    }
    if(index >= 10)
      {
        return {
          'name': 'change $index',
          'price': Random().nextInt(100)
        };
      }


  }

  _fetchPage(int pageNumber, int pageSize) async {
    await Future.delayed(Duration(seconds: 1));

    pageNumber ++;
    print("Page number ="+pageNumber.toString());

    String url = Helper.GET_ALL_LEADS_URL+"?pageNumber="+pageNumber.toString()+"&pageSize="+pageSize.toString()+"&LeadStatus=disbursed";
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString("accessToken");

    print("url="+url);
    var data = await http.get(Uri.encodeFull(url),
        headers: {"Content-Type": "application/json", "Authorization": "Basic "+token}).timeout(
        const Duration(seconds: 10));

    var _mydisbursed = json.decode(data.body);

    print("Data="+_mydisbursed.toString());

    return _mydisbursed;

//    return List.generate(pageSize, (index) {
//      return {
//        'name': 'product $index of page $pageNumber',
//        'price': Random().nextInt(100)
//      };
//    });
  }

  Widget _buildPage(List page) {
    return  ListView.builder(
                  itemCount: page.length,
                  itemBuilder: (context, _index) {
                    return Card(
                      child: Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Text(page[_index]["Name"], style: TextStyle(fontSize: 22.0),),
                      ),
                    );
                  },
                );
  }


}