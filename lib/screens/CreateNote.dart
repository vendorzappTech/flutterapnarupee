import 'dart:convert';

import 'package:apanarupee/model/ApiInterface.dart';
import 'package:apanarupee/screens/OTPVerification.dart';
import 'package:apanarupee/utils/Helper.dart';
import 'package:apanarupee/utils/HexColor.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

class CreateNote extends StatefulWidget  {
  CreateNote({Key key, this.leadId}) : super(key: key);

  final String leadId;

  @override
  CreateNoteState createState() => CreateNoteState(this.leadId);
}

class CreateNoteState extends State<CreateNote> implements ApiInterface{

  String leadId = "",_note="";
  bool _isLoading = false;
  final formKey = new GlobalKey<FormState>();
  static const int ADD_NOTES_RESULT_CODE =1,ERROR_CODE = -1;


  final scaffoldKey = new GlobalKey<ScaffoldState>();

  CreateNoteState(String leadId)
  {
    this.leadId = leadId;
  }
  void _submit() async
  {
    var form = formKey.currentState;

    if (form.validate()) {
      setState(() => _isLoading = true);
      form.save();
      Helper.callAuthApi(Uri.encodeFull(Helper.ADD_NOTES_URL),
          json.encode({
            "Notes": this._note,
            "LeadId": this.leadId,

          }), context, this, ADD_NOTES_RESULT_CODE);


    }

  }

  @override
  void initState() {
    // TODO: implement initState

    super.initState();
    print("In init");

    FirebaseMessaging _firebaseMessaging;

    _firebaseMessaging = FirebaseMessaging();

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print('on message $message');

        // show notification here
//        _showNotificationWithDefaultSound();
        var title = message['notification']['title'];
        var body = message['notification']['body'];
        // show notification here
        _showDialog(title,body);



      },
      onResume: (Map<String, dynamic> message) async {
        print('on resume $message');
      },
      onLaunch: (Map<String, dynamic> message) async {
        print('on launch $message');
      },

    );


  }


  @override
  Widget build(BuildContext context) {

    ScreenUtil.instance = ScreenUtil.getInstance()..init(context);

    final logo = Hero(
        tag: 'hero',
        child:
        new Container(

//            height: 280,
////          width: 600.0,
//            decoration: new BoxDecoration(
//              image: new DecorationImage(image: new AssetImage("assets/loginbg.png"), fit: BoxFit.fill,
//              ),
//            ),

            child:
            new Column(
              children: <Widget>[
                AppBar
                  (

                    elevation: 0.0,
                    backgroundColor:Color(HexColor.getAppBlueColor()),
                    iconTheme: IconThemeData(
                        color: Colors.white),title:Text( "Create Note",style: TextStyle(fontSize: ScreenUtil.getInstance().setSp(60)),)),

              ],
            )


        )
    );



    var note =
    new TextFormField(
      maxLines: 4,
      style: TextStyle(fontSize: 20 ,color: Colors.black),
      decoration:  InputDecoration(
        //border: const OutlineInputBorder(),
          hintText: "Enter Note",
          labelText: "Note",
          hintStyle: TextStyle(color:Colors.grey),
          labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor()),fontSize: 18)
      ),
      autocorrect: true,
      validator: (str) =>
      (str.length > 0)
          ? null
          : "Enter Note",
      onSaved: (str) => _note = str,
      keyboardType: TextInputType.multiline,
      enabled: true,


    );


    var submitButton =
    ButtonTheme(
        minWidth: 50.0,
        height: 50.0,
        child:
        new RaisedButton(
          color: HexColor("#fb6616"),
          shape: new RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(5.0)),
          splashColor: Colors.blue,
          onPressed: _submit,
          child: new Text("SAVE", style: TextStyle(color:Colors.white,fontSize: 20)),
        ));

    return new Scaffold(
        key: scaffoldKey,
        backgroundColor: Colors.white,

        body:
        new Form(
          key:formKey,
          child: ListView(

              padding: EdgeInsets.only(top: 0),

              children: <Widget>[
                logo,
//                  SizedBox(height: 14.0),
//                  new Padding(
//                      padding: EdgeInsets.only(left:24,right: 24,top: 10),
//                      child:registerAs),

                new Padding(
                    padding: EdgeInsets.only(left:24,right: 24,top:10),
                    child:note),


                new Padding(
                    padding: EdgeInsets.only(left:24,right: 24,top: 20),
                    child:
                    submitButton),
                SizedBox(height: 10.0),

                new Center(
                  child:
                  (_isLoading)? Align(
                      alignment: Alignment.center,
                      child: CircularProgressIndicator()): SizedBox(height: 0.0),
                ),


              ]
          ),

        )
    );
  }

  void _showSnackBar(String text,Color color) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(backgroundColor:color ,content: new Text(text),duration: new Duration(milliseconds: 1000),));
  }

  void postExecution(var data ,int result_code)  {

    switch(result_code) {

      case ADD_NOTES_RESULT_CODE:
        try {
          setState(() => _isLoading = false);

          if (data["responseCode"] == Helper.SUCCESS_STATUS_CODE) {
            print("got data");
            _showSnackBar(data["messages"]["messageDescription"], Colors.green);

            Future.delayed(const Duration(milliseconds: 1000), () {

// Here you can write your code

              setState(() {
                // Here you can write your code for open new view
                Navigator.pop(context);
              });

            });

          } else {
            print("Error=" + data["messages"]["messageDescription"]);

            _showSnackBar(data["messages"]["messageDescription"], Colors.red);
          }
          if (data == null)
            print("Unable to get data");
        } catch (e) {
          //_showSnackBar("Some problem occurred", Colors.red);
        }
        break;

      case ERROR_CODE:
        _showSnackBar("Please check your internet connection ",Colors.red);
        setState(() {
          _isLoading = false;

        });
        break;
    }
  }

  void _showDialog(String title,String msg) {
    // flutter defined function
    showDialog(

      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(title),
          content: new Text(msg),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();

              },
            ),
          ],
        );
      },
    );
  }

}
