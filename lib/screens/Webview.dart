import 'dart:convert';

import 'package:apanarupee/model/ApiInterface.dart';
import 'package:apanarupee/screens/OTPVerification.dart';
import 'package:apanarupee/utils/Helper.dart';
import 'package:apanarupee/utils/HexColor.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

class MyWebview extends StatefulWidget  {
  MyWebview({Key key, this.title,this.url}) : super(key: key);

  final String title,url;

  @override
  MyWebviewState createState() => MyWebviewState(this.title,this.url);
}

class MyWebviewState extends State<MyWebview> {

  String title = "",url = "";

  MyWebviewState(String title,String url) {
    this.title = title;
    this.url = url;
  }

  @override
  void initState() {
    // TODO: implement initState

    super.initState();
    print("In init");

    FirebaseMessaging _firebaseMessaging;

    _firebaseMessaging = FirebaseMessaging();

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print('on message $message');

        // show notification here
//        _showNotificationWithDefaultSound();
        var title = message['notification']['title'];
        var body = message['notification']['body'];
        // show notification here
        _showDialog(title,body);



      },
      onResume: (Map<String, dynamic> message) async {
        print('on resume $message');
      },
      onLaunch: (Map<String, dynamic> message) async {
        print('on launch $message');
      },

    );
  }


  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance = ScreenUtil.getInstance()
      ..init(context);

    final logo = Hero(
        tag: 'hero',
        child:
        new Container(

            height: MediaQuery
                .of(context)
                .size
                .height,
//          width: 600.0,
            decoration: new BoxDecoration(
              image: new DecorationImage(
                image: new AssetImage("assets/dashboardbg.png"),
                fit: BoxFit.fill,
              ),
            ),

            child:
            new Column(
              children: <Widget>[
                AppBar
                  (
                    elevation: 0.0,
                    backgroundColor: Colors.transparent,
                    iconTheme: IconThemeData(
                        color: Colors.white),
                    title: Text(title, style: TextStyle(
                        fontSize: ScreenUtil.getInstance().setSp(60)),)),
                new Container(
                    padding: EdgeInsets.all(15),
                    height: MediaQuery
                        .of(context)
                        .size
                        .height - 80,
                    decoration: new BoxDecoration(
                      color: Colors.white,
                      borderRadius: new BorderRadius.circular(25.0),
                    ),
                    child:
                    new WebviewScaffold(
                      url: "https://www.google.com",

                    ))

              ],
            )


        )
    );

    return new WebviewScaffold(
      url: url,
      appBar:  AppBar
        (
          elevation: 0.0,
          backgroundColor: Color(HexColor.getAppBlueColor()),
          title: Text(title, style: TextStyle(
              fontSize: ScreenUtil.getInstance().setSp(60)),)),
    );

//    return new Scaffold(
//
//
//        backgroundColor: Colors.white,
//
//        body:
//        new Form(
//
//          child: ListView(
//
//              padding: EdgeInsets.only(top: 0),
//
//              children: <Widget>[
//                logo,
//
//
//              ]
//          ),
//
//        )
//    );
//  }


  }

  void _showDialog(String title,String msg) {
    // flutter defined function
    showDialog(

      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(title),
          content: new Text(msg),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();

              },
            ),
          ],
        );
      },
    );
  }



}
