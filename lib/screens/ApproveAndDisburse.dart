import 'dart:convert';
import 'package:apanarupee/model/ContactInfo.dart';
import 'package:apanarupee/model/LogInInfo.dart';
import 'package:apanarupee/model/PersonalInfo.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:apanarupee/model/ApiInterface.dart';
import 'package:apanarupee/model/User.dart';
import 'package:apanarupee/utils/Helper.dart';
import 'package:apanarupee/utils/HexColor.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

class ApproveAndDisburse extends StatefulWidget  {
  ApproveAndDisburse({Key key, this.leadId,this.loanAmnt,this.leadType,this.logInInfo}) : super(key: key);

  final String leadId,loanAmnt,leadType;
  final LogInInfo logInInfo;

  @override
  ApproveAndDisburseState createState() => ApproveAndDisburseState(leadId,loanAmnt,leadType,logInInfo);
}

class ApproveAndDisburseState extends State<ApproveAndDisburse> with SingleTickerProviderStateMixin implements ApiInterface  {

  String _appliedLoanAmnt = "",_approvedLoanAmnt="",leadType="",_disbursementLoanAmnt = "";
  List disburseList;
  TextEditingController _myControllerDate = new TextEditingController();
  FocusNode _focusNodeDate;
  var selectedDate = new DateTime.now();
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  bool _isLoading = false;
  final formKey1 = new GlobalKey<FormState>();
  final formKey2 = new GlobalKey<FormState>();
  final formKey3 = new GlobalKey<FormState>();
  LogInInfo logInInfo;

  List _mycity = new List();
  List _mycity2 = new List();

  final myControllerDOB = TextEditingController();
  final myControllerFileDate = TextEditingController();
  FocusNode focuNodeDOB,focuNodeFileDate;

  final dateFormat = DateFormat("dd/MM/yyyy");
  final dateFormat1 = DateFormat("yyyy-MM-ddThh:mm:ss");
  final dateFormat2 = DateFormat("yyyy-MM-dd");
  String loadId = "",_date = "";
  TabController myTabController;

  static const int  GET_APPLOCATION_RESULT_CODE = 1,UPDATE_APPROVED_AMOUNT = 2,UPDATE_DISBURSE_AMOUNT = 3,DELETE_DISBURSE_AMOUNT =4;

  ApproveAndDisburseState(String leadId,String loanAmnt,String leadType,LogInInfo logInInfo){
    this.loadId = leadId;
    this._appliedLoanAmnt = loanAmnt;
    this.leadType = leadType;
    this.logInInfo = logInInfo;
  }

  @override
  void initState() {
    // TODO: implement initState
    // call webservices to get location

    _focusNodeDate = new FocusNode();
    _focusNodeDate.addListener(textFieldFocusDateDidChange);


    myTabController = new TabController(length: 2, vsync: this);



    if(int.parse(leadType) == 2)
      {
      //  myTabController.addListener(null);
        myTabController.addListener(() {
          setState(() {
            myTabController.index = 0;
          });
        });

      }
      if(int.parse(leadType) == 4)
        {
          myTabController.index = 1;
          myTabController.addListener(() {
            setState(() {
              myTabController.index = 1;
            });
          });
        }

    if(int.parse(leadType) == 3){
      myTabController.index = 1;
    }

    String url = Helper.GET_APPROVE_DISBURSAL_DETAIL_URL+"?LeadId="+loadId;
    print("url="+url);
    Helper.callAuthGetApi(Uri.encodeFull( url),
        json.encode({

        }), context, this, GET_APPLOCATION_RESULT_CODE);

    setState(() {
      _isLoading = true;
    });


    FirebaseMessaging _firebaseMessaging;

    _firebaseMessaging = FirebaseMessaging();

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print('on message $message');

        // show notification here
//        _showNotificationWithDefaultSound();
        var title = message['notification']['title'];
        var body = message['notification']['body'];
        // show notification here
        _showDialog(title,body);



      },
      onResume: (Map<String, dynamic> message) async {
        print('on resume $message');
      },
      onLaunch: (Map<String, dynamic> message) async {
        print('on launch $message');
      },

    );


  }

  void textFieldFocusDateDidChange() {
    if (_focusNodeDate.hasFocus) {
      // textfield was tapped
      _selectDate(context);
      _focusNodeDate.unfocus();
    }
  }

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: DateTime(DateTime.now().year, DateTime.now().month,DateTime.now().day),
        firstDate: DateTime(DateTime.now().year-5, DateTime.now().month,DateTime.now().day),
        lastDate: DateTime.now());
    if (picked != null )
      setState(() {
        _date = dateFormat.format(picked);
        _myControllerDate.text = _date;
        _date = dateFormat1.format(picked);
      });
  }


//



  _submitInfo1() {
    var form = formKey1.currentState;

    if (form.validate()) {
      setState(() => _isLoading = true);
      form.save();


      if(logInInfo != null)
      logInInfo.approvedAmouunt = _approvedLoanAmnt;

      Helper.callAuthApi(Uri.encodeFull(Helper.UPDATE_APPROVED_AMNT_URL),
          json.encode(logInInfo.toJson1()), context, this, UPDATE_APPROVED_AMOUNT);
    }
  }

  _submitInfo2() {
    var form = formKey2.currentState;

    if (form.validate()) {
      setState(() => _isLoading = true);
      form.save();
      if(logInInfo != null) {
        logInInfo.approvedAmouunt = _approvedLoanAmnt;
        logInInfo.disburseAmnt = _disbursementLoanAmnt;
        logInInfo.addedDate = _date;
      }

      Helper.callAuthApi(Uri.encodeFull(Helper.UPDATE_DISBURSED_AMNT_URL),
          json.encode(logInInfo.toJson2()), context, this, UPDATE_DISBURSE_AMOUNT);
    }
  }

  _deleteDisbursement(String id) {
      setState(() => _isLoading = true);

      if(logInInfo != null) {
        logInInfo.disbursedId = id;
        logInInfo.approvedAmouunt = _approvedLoanAmnt;
      }

        Helper.callAuthApi(Uri.encodeFull(Helper.DELETE_DISBURSEMENT_AMNT_URL),
            json.encode(logInInfo.toJson3()), context, this, DELETE_DISBURSE_AMOUNT);
      }


  @override
  Widget build(BuildContext context) {

    var appliedLoanAmnt =
    new TextFormField(

      decoration:  InputDecoration(
//          border: const OutlineInputBorder(),
          hintText: "Enter Applied Amount",
          labelText: "Applied Loan Amount",
          hintStyle: TextStyle(color:Colors.grey),
          labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor()))
      ),
      initialValue: _appliedLoanAmnt,
      readOnly: true,
      autocorrect: true,
      validator: (str) =>
      (str.length == 0)
          ? "Enter applied loan amount" :null,
      onSaved: (str) => _appliedLoanAmnt= str,
      keyboardType: TextInputType.number,
      enabled: true,
      inputFormatters: [
        //LengthLimitingTextInputFormatter(10),
        BlacklistingTextInputFormatter(new RegExp('[\\-|\\ |\\,|\\+]')),

      ],

    );

    var approvedLoanAmnt =
    new TextFormField(

      decoration:  InputDecoration(
//          border: const OutlineInputBorder(),
          hintText: "Enter Bank Approved Loan Amount",
          labelText: "Bank Approved Loan Amount",
          hintStyle: TextStyle(color:Colors.grey),
          labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor()))
      ),
      initialValue: _approvedLoanAmnt,
      autocorrect: true,
      validator: (str) =>
      (str.length == 0)
          ? "Enter bank approved loan amount" : null,

      //((double.parse(str) > double.parse(_appliedLoanAmnt))? "Enter bank approved loan amount less than or equal to aprroved loan amount" : null),
      onSaved: (str) => _approvedLoanAmnt= str,
      keyboardType: TextInputType.numberWithOptions(decimal: true),
      enabled: true,
      inputFormatters: [
        //LengthLimitingTextInputFormatter(10),
        BlacklistingTextInputFormatter(new RegExp('[\\-|\\ |\\,|\\+]')),
      ],

    );

    var disbusementLoanAmnt =
    new TextFormField(

      decoration:  InputDecoration(
//          border: const OutlineInputBorder(),
          hintText: "Enter Disbursement Loan Amount",
          labelText: "Disbusement Loan Amount",
          hintStyle: TextStyle(color:Colors.grey),
          labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor()))
      ),
      initialValue: _disbursementLoanAmnt,
      autocorrect: true,
      validator: (str) =>
      (str.length == 0)
          ? "Enter disbusement loan amount" :((double.parse(str) > double.parse(_approvedLoanAmnt))? "Enter disbusement loan amount less than or equal to aprroved loan amount" : null),
      onSaved: (str) => _disbursementLoanAmnt= str,
      keyboardType: TextInputType.number,
      enabled: true,
      inputFormatters: [
        //LengthLimitingTextInputFormatter(10),
        BlacklistingTextInputFormatter(new RegExp('[\\-|\\ |\\,|\\+]')),
      ],
    );

    var dateOfDisbursement =   new TextFormField(
        controller: _myControllerDate,
        focusNode: _focusNodeDate ,
//        initialValue: _dob,
        validator: (str) =>
        (str.length > 0)
            ? null
            : "Select date",

        decoration:  InputDecoration(
//          border: const OutlineInputBorder(),

          labelText: "Select date",
          hintStyle: TextStyle(color:Colors.grey),
          labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor())),
          prefixIcon: Padding(
            padding: EdgeInsets.all(0.0),
            child: Icon(
              Icons.date_range,
              color: Colors.grey,
            ), // icon is 48px widget.
          ),

        )
    );


    var submitRefInfo =
    ButtonTheme(
        minWidth: 50.0,
        height: 50.0,
        child:
        new RaisedButton(
          color: HexColor("#fb6616"),
          shape: new RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(5.0)),
          splashColor: Colors.blue,
          onPressed: _submitInfo1,
          child: new Text("SAVE", style: TextStyle(color: Colors.white)),
        ));


    var ref1 = Center(
      child: new Form(
        key: formKey1,
        child: ListView(

            padding: EdgeInsets.only(top: 0),

            children: <Widget>[
              new Padding(
                  padding: EdgeInsets.only(left: 24, right: 24, top: 10),
                  child:
                  appliedLoanAmnt),

              new Padding(
                  padding: EdgeInsets.only(left: 24, right: 24, top: 10),
                  child: approvedLoanAmnt),


              new Padding(
                  padding: EdgeInsets.only(
                      left: 24, right: 24, top: 20, bottom: 20),
                  child:
                  submitRefInfo),

            ]
        ),
      ),
    );

    var submitRefInfo2 =
    ButtonTheme(
        minWidth: 50.0,
        height: 50.0,
        child:
        new RaisedButton(
          color: HexColor("#fb6616"),
          shape: new RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(5.0)),
          splashColor: Colors.blue,
          onPressed: _submitInfo2,
          child: new Text("SAVE", style: TextStyle(color: Colors.white)),
        ));

    var listView =  ListView.builder
      (
        padding: EdgeInsets.only(top: 5,bottom: 5),
        //physics: NeverScrollableScrollPhysics(),
        //  controller: _scrollController,
        itemCount:(disburseList != null)? disburseList.length:0,
        itemBuilder: (BuildContext ctxt, int index) {
          return
            new Card(

                child:

                new Row(
                  children: <Widget>[
                    Expanded(
                        flex: 7,
                        child:

                        new Row(

                          mainAxisAlignment:MainAxisAlignment.center ,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            new Column(

                              mainAxisAlignment:MainAxisAlignment.center ,
                              crossAxisAlignment: CrossAxisAlignment.center,

                              children: <Widget>[
                                new Padding(padding: EdgeInsets.all( 10),
                                    child:

                                    new Image(image: AssetImage("assets/earningrupee.png"),width: 35,)),
                              ],
                            ) ,



                            new Padding(padding: EdgeInsets.only(left: 10),
                                child:
                                new Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[

                                    new Row(
                                      children: <Widget>[
                                        new Text("  Date:",style: TextStyle(color: Colors.grey,fontSize:ScreenUtil.getInstance().setSp(40) ),),
                                        new Text(" "+dateFormat2.format(dateFormat1.parse(disburseList[index]["AddedDate"].toString())),style: TextStyle(color: Colors.black,fontSize:ScreenUtil.getInstance().setSp(40) ),),
                                      ],
                                    )
                                  ],
                                )
                            ),
                          ],
                        )
                    ),
                    Expanded(
                      flex: 5,
                      child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          (disburseList[index]["Status"] == 0)?
                          new Row(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            GestureDetector(

                                onTap: () {
                              // _deleteMeeting(event.id.toString());
                              print("Hi");
                              String id = disburseList[index]["DisbursedId"].toString();
                              _deleteDisbursement(id);
                            },

                                child:  new Icon(Icons.delete_forever,color:Colors.red)
                            ),
                          ],
                          )
                              : GestureDetector(

                              onTap: () {
                                // _deleteMeeting(event.id.toString());

                              },

                              child:  new Text(""),
                          ) ,
                          new Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              new Padding(padding: EdgeInsets.only(right: 5,bottom: 15,top:2),
                                  child:

                                  new Container(
                                    height: ScreenUtil.getInstance().setHeight(30),
                                    child:
                                    new Image.asset("assets/brupee.png"),) ),

                              new Padding(padding: EdgeInsets.only(right: 10,bottom: 10),
                                  child:
                                  new Text(disburseList[index]["DisbursedAmount"].toString(),style: TextStyle(fontSize:ScreenUtil.getInstance().setSp(45),fontWeight:FontWeight.bold,color: Color(HexColor.getAppBlueColor())),))
                            ],
                          ),
                          SizedBox(height: 10,)

                        ],
                      ),

                    )
                  ],
                ));


        }

    );

    var ref2 = Center(
      child: new Form(
        key: formKey2,
        child: ListView(

            padding: EdgeInsets.only(top: 0),

            children: <Widget>[
              new Padding(
                  padding: EdgeInsets.only(left: 24, right: 24, top: 10),
                  child:
                  new Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Text('BANK APPROVED LOAN - ',style: TextStyle(fontWeight: FontWeight.bold,color:Color( HexColor.getAppBlueColor())),),
                      new Text(_approvedLoanAmnt,style: TextStyle(color:Colors.black,fontWeight: FontWeight.bold),),
                    ],
                  )),

              new Padding(
                  padding: EdgeInsets.only(left: 24, right: 24, top: 10),
                  child: disbusementLoanAmnt),

              new Padding(
                  padding: EdgeInsets.only(left: 24, right: 24, top: 10),
                  child: dateOfDisbursement),



              new Padding(
                  padding: EdgeInsets.only(
                      left: 24, right: 24, top: 20, bottom: 20),
                  child:
                  submitRefInfo2),

             new Container(
               height: 400,
               child:
             (disburseList != null && (disburseList.length > 0))?
              new Padding(
                  padding: EdgeInsets.only(left: 24, right: 24, top: 10),
                  child: listView): SizedBox(width: 0,height: 0,),
             )
            ]
        ),
      ),
    );


    ScreenUtil.instance = ScreenUtil.getInstance()
      ..init(context);

    return WillPopScope(
      child: DefaultTabController(
        length: 2,
        child: Scaffold(
          key: scaffoldKey,
          appBar: AppBar(
              bottom: TabBar(

                controller: myTabController,
                tabs: [
                  Tab(text: "Loan Approval",),
                  Tab(text: "Loan Disbursement",)
                ],
              ),
              title: Text('Approval / Disbursement Form'),
              backgroundColor: HexColor("#2b2ca3")
          ),
          body: TabBarView(
            physics: (leadType == 3)? AlwaysScrollableScrollPhysics(): NeverScrollableScrollPhysics(),
            controller: myTabController,
            children: [
              (_isLoading) ? Align(
                  alignment: Alignment.center,
                  child: CircularProgressIndicator()
              ) : ref1,
              (_isLoading) ? Align(
                  alignment: Alignment.center,
                  child: CircularProgressIndicator()
              ) : ref2,

            ],
          ),
        ),
      ),
    );
  }


  void _showSnackBar(String text, Color color) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(backgroundColor: color,
      content: new Text(text),
      duration: new Duration(milliseconds: 5000),));
  }

  void postExecution(var data, int result_code) {

    switch (result_code) {

      case GET_APPLOCATION_RESULT_CODE:
        try{
            if (data == null) {
              setState(() {
                _isLoading = false;
              });
              _showSnackBar("Unable to get application details", Colors.red);
            }
            else {
              setState(() {
                print("Data=" + data["ApproveAmt"].toString());

                _approvedLoanAmnt = data["ApproveAmt"].toString();

                disburseList = new List();

                disburseList = data["Disburse"];

                print("Disburse List size = " + disburseList.length.toString());


                _isLoading = false;
              });
            }
        }catch(e){
          print(e.toString());
        }
        break;

      case UPDATE_APPROVED_AMOUNT:

        try {
          setState(() => _isLoading = false);

          if (data["responseCode"] == Helper.SUCCESS_STATUS_CODE) {
            print("got data");
            _showSnackBar(
                data["messages"]["messageDescription"], Colors.green);

          } else {
            print("Error=" + data["messages"]["messageDescription"]);

            _showSnackBar(data["messages"]["messageDescription"], Colors.red);
          }
          if (data == null)
            print("Unable to get data");
        } catch (e) {
          _showSnackBar("Some problem occurred", Colors.red);
        }

        break;

      case UPDATE_DISBURSE_AMOUNT :

        try {
          setState(() => _isLoading = false);

          if (data["responseCode"] == Helper.SUCCESS_STATUS_CODE) {
            print("got data");
            _showSnackBar(
                data["messages"]["messageDescription"], Colors.green);




//            if(int.parse(leadType) == 3){
//              Future.delayed(const Duration(milliseconds: 3000), () {
//                setState(() {
//                  Navigator.pop(context);
//                  Navigator.pop(context);
//                });
//
//              });
//            }else{
              setState(() => _isLoading = true);
              String url = Helper.GET_APPROVE_DISBURSAL_DETAIL_URL+"?LeadId="+loadId;
              print("url="+url);
              Helper.callAuthGetApi(Uri.encodeFull( url),
                  json.encode({

                  }), context, this, GET_APPLOCATION_RESULT_CODE);


//            }


          } else {
            print("Error=" + data["messages"]["messageDescription"]);

            _showSnackBar(data["messages"]["messageDescription"], Colors.red);
          }
          if (data == null)
            print("Unable to get data");
        } catch (e) {
          _showSnackBar("Some problem occurred", Colors.red);
        }

        break;

      case DELETE_DISBURSE_AMOUNT:

        try {


          if (data["responseCode"] == Helper.SUCCESS_STATUS_CODE) {

            print("got data");
            _showSnackBar(
                data["messages"]["messageDescription"], Colors.green);

            String url = Helper.GET_APPROVE_DISBURSAL_DETAIL_URL+"?LeadId="+loadId;
            print("url="+url);
            Helper.callAuthGetApi(Uri.encodeFull( url),
                json.encode({

                }), context, this, GET_APPLOCATION_RESULT_CODE);


          } else {

            setState(() => _isLoading = false);

            print("Error=" + data["messages"]["messageDescription"]);

            _showSnackBar(data["messages"]["messageDescription"], Colors.red);
          }
          if (data == null)
            print("Unable to get data");
        } catch (e) {
          _showSnackBar("Some problem occurred", Colors.red);
        }

        break;


    }
  }

  void _showDialog(String title, String msg) {
    // flutter defined function
    showDialog(

      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(title),
          content: new Text(msg),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}