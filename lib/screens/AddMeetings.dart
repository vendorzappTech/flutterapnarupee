import 'dart:convert';

import 'package:apanarupee/model/ApiInterface.dart';
import 'package:apanarupee/screens/OTPVerification.dart';
import 'package:apanarupee/utils/Helper.dart';
import 'package:apanarupee/utils/HexColor.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

class AddMeetings extends StatefulWidget  {

  AddMeetings({Key key, this.leadId}) : super(key: key);
  final String leadId;

  @override
  AddMeetingsState createState() => AddMeetingsState(this.leadId);
}

class AddMeetingsState extends State<AddMeetings> implements ApiInterface{

  bool _isLoading = false;
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  final formKey = new GlobalKey<FormState>();
  TextEditingController _myControllerDate = new TextEditingController(),_myControllerTime = new TextEditingController();
  FocusNode _focusNodeDate,_focusNodeTime;
  String _date = "",_pickedTime = "",_address = "",leadId = "";
  var selectedDate = new DateTime.now();
  TimeOfDay selectedTime = TimeOfDay.fromDateTime(new DateTime.now());
  int selectedHr,selectedMin;

  final dateFormat = DateFormat("dd/MM/yyyy");
  final dateFormat1 = DateFormat("hh:mm");
  final dateFormat2 = DateFormat("dd/MM/yyyy hh:mm");
  final dateFormat3 = DateFormat("dd/MM/yyyyhh:mm");
  static const int ADD_MEETING_RESULT_CODE = 1,ERROR_CODE = -1;

  AddMeetingsState(String leadId)
  {
    this.leadId = leadId;
  }

  void _submit() async
  {
    var form = formKey.currentState;

    if (form.validate()) {
      setState(() => _isLoading = true);
      form.save();
      bool flag = false;


          // call webservice to add meetings
          final dateFormat2 = DateFormat("yyyy-MM-dd");
          String date = dateFormat2.format(
              dateFormat.parse(_myControllerDate.text));
      try {
          DateTime todaysDate = dateFormat.parse(DateTime
              .now()
              .day
              .toString() + "/" + DateTime
              .now()
              .month
              .toString() + "/" + DateTime
              .now()
              .year
              .toString());
          DateTime selectedDate = dateFormat.parse(_myControllerDate.text);

          if (todaysDate.compareTo(selectedDate) == 0) {
            //dd/MM/yyyy hh:mm
            //after 2 hr
            print("hi");
            int hr = DateTime
                .now()
                .hour + 2;
            int day =  DateTime
                .now().day;
            int month =  DateTime
                .now().month;
            int year =  DateTime
                .now().year;


            int hrAfter2=  DateTime
                .now()
                .hour ;
            int minAfter2 = DateTime
                .now()
                .minute+5;

            if(selectedHr >= hrAfter2)
            {
                if(selectedHr == hrAfter2){
                if(selectedMin < minAfter2){

                  _showSnackBar("Select meeting time after 5 minute from current time", Colors.red);
                  flag = true;
                  setState(() => _isLoading = false);
                }
                }
            }else
              {
                _showSnackBar("Select meeting time after 5 minute from current time", Colors.red);
                flag = true;
                setState(() => _isLoading = false);
              }

//            DateTime todays2hrDate = dateFormat3.parse( day.toString()+"/"+month.toString()+"/"+year.toString()+hr.toString() + ":" + DateTime
//                .now()
//                .minute
//                .toString());
//            print("2 hr date="+dateFormat3.format(todays2hrDate));
//
//            DateTime selDate = dateFormat1.parse( day.toString()+"/"+month.toString()+"/"+year.toString()+selectedHr.toString() + ":" + selectedMin.toString());
//            print("selected hr date="+dateFormat3.format(selDate));
//
//            if (!selectedDate.isAfter(todays2hrDate)) {
//              _showSnackBar("Select meeting time after 2 hour from current time", Colors.red);
//              flag = true;
//              setState(() => _isLoading = false);
//            }
          }
        }catch(e){
          print(e.toString());
        }
      if(!flag){
      Helper.callAuthApi(Uri.encodeFull(Helper.ADD_MEETING_URL),
          json.encode({
            "LeadId": leadId,
            "Date": date,
            "TimeSlot": _myControllerTime.text,
            "Address": _address,


          }), context, this, ADD_MEETING_RESULT_CODE);

    }}

  }

  @override
  void initState() {
    // TODO: implement initState

    super.initState();
    print("In init");

    _focusNodeDate = new FocusNode();
    _focusNodeDate.addListener(textFieldFocusDateDidChange);

    _focusNodeTime = new FocusNode();
    _focusNodeTime.addListener(textFieldFocusTimeDidChange);

    FirebaseMessaging _firebaseMessaging;

    _firebaseMessaging = FirebaseMessaging();

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print('on message $message');

        // show notification here
//        _showNotificationWithDefaultSound();
        var title = message['notification']['title'];
        var body = message['notification']['body'];
        // show notification here
        _showDialog(title,body);



      },
      onResume: (Map<String, dynamic> message) async {
        print('on resume $message');
      },
      onLaunch: (Map<String, dynamic> message) async {
        print('on launch $message');
      },

    );


  }

  void textFieldFocusDateDidChange() {
    if (_focusNodeDate.hasFocus) {
      // textfield was tapped
      _selectDate(context);
      _focusNodeDate.unfocus();
    }
  }

//
  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: DateTime(DateTime.now().year, DateTime.now().month,DateTime.now().day),
        firstDate: DateTime(DateTime.now().year, DateTime.now().month,DateTime.now().day),
        lastDate: DateTime(2101));
    if (picked != null )
      setState(() {
        _date = dateFormat.format(picked);
        _myControllerDate.text = _date;
      });
  }

  void textFieldFocusTimeDidChange() {
    if (_focusNodeTime.hasFocus) {
      // textfield was tapped
      _selectTime(context);
      _focusNodeTime.unfocus();
    }
  }

//

  Future<Null> _selectTime(BuildContext context) async {
    final TimeOfDay response = await showTimePicker(
      context: context,
     initialTime: selectedTime,
    );
    if (response != null ) {
      setState(() {
        //_pickedTime = dateFormat1.parse(response.hour+":"+response.minute);
        String append = "";
        if(response.period == DayPeriod.am)
          append = " AM";
          else
            append = " PM";
          int hr= response.hour;
         selectedHr = hr;

         if( response.hour > 12)
           hr = response.hour - 12;

         selectedMin = response.minute;

        _myControllerTime.text = dateFormat1.format(dateFormat1.parse(hr.toString()+":"+response.minute.toString()))+ append;
      });
    }
  }

  @override
  Widget build(BuildContext context) {

    var date =   new TextFormField(
        controller: _myControllerDate,
        focusNode: _focusNodeDate ,
//        initialValue: _dob,
        validator: (str) =>
        (str.length > 0)
            ? null
            : "Select date",

        decoration:  InputDecoration(
//          border: const OutlineInputBorder(),

            labelText: "Select date",
            hintStyle: TextStyle(color:Colors.grey),
            labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor())),
            prefixIcon: Padding(
              padding: EdgeInsets.all(0.0),
              child: Icon(
                Icons.date_range,
                color: Colors.grey,
              ), // icon is 48px widget.
            ),

        )
    );

    var time =   new TextFormField(
        controller: _myControllerTime,
        focusNode: _focusNodeTime ,
        validator: (str) =>
        (str.length > 0)
            ? null
            : "Select Time",

        decoration:  InputDecoration(
//          border: const OutlineInputBorder(),
            labelText: "Select Time",
            hintStyle: TextStyle(color:Colors.grey),
            labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor())),
            prefixIcon: Padding(
              padding: EdgeInsets.all(0.0),
              child: Icon(
                Icons.access_time,
                color: Colors.grey,
              ), // icon is 48px widget.
            ),
        )
    );


    var Address =
    new TextFormField(

      decoration:  InputDecoration(
        //  border: const OutlineInputBorder(),
          hintText: "Enter Address",
          labelText: "Address",
          hintStyle: TextStyle(color:Colors.grey),
          labelStyle: TextStyle(color:Color(HexColor.getAppBlueColor())),
          prefixIcon: Padding(
            padding: EdgeInsets.all(0.0),
            child: Icon(
              Icons.location_on,
              color: Colors.grey,
            ), // icon is 48px widget.
          ),

      ),
      autocorrect: true,
//      validator: (str) =>
//      (str.length > 0)
//          ? null
//          : "Enter Address",
      onSaved: (str) => _address = str,
      keyboardType: TextInputType.text,
      enabled: true,


    );


    var submitButton =
    ButtonTheme(
        minWidth: 50.0,
        height: 50.0,
        child:
        new RaisedButton(
          color: HexColor("#fb6616"),
          shape: new RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(5.0)),
          splashColor: Colors.blue,
          onPressed: _submit,
          child: new Text("SAVE", style: TextStyle(color:Colors.white)),
        ));
    ScreenUtil.instance = ScreenUtil.getInstance()..init(context);
    return new Scaffold(
        key: scaffoldKey,
        backgroundColor: Colors.white,
        appBar:
        AppBar(
          iconTheme: IconThemeData(
              color: Colors.white),title:Text( "Schedule Meeting",style: TextStyle(fontSize: ScreenUtil.getInstance().setSp(60)),),backgroundColor: HexColor("#2b2ca3"),),
        body: Center(
          child: new Form(
            key:formKey,
            child:
            (_isLoading)?Align(
                alignment: Alignment.center,
                child: CircularProgressIndicator()
            ):
            ListView(

                padding: EdgeInsets.only(top: 0),

                children: <Widget>[

                  SizedBox(height: 28.0),
                  new Padding(
                      padding: EdgeInsets.only(left:24,right: 24,top: 10),
                      child:date),

                  new Padding(
                      padding: EdgeInsets.only(left:24,right: 24,top:10),
                      child:time),

                  new Padding(
                      padding: EdgeInsets.only(left:24,right: 24,top:10),
                      child:Address),

                  new Padding(
                      padding: EdgeInsets.only(left:24,right: 24,top:40),
                      child:submitButton),

                ]
            ),
          ),
        )
    );
  }

  void _showSnackBar(String text,Color color) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(backgroundColor:color ,content: new Text(text),duration: new Duration(milliseconds: 5000),));
  }

  void postExecution(var data ,int result_code)  {

    switch(result_code) {

      case ADD_MEETING_RESULT_CODE:
        try {
          setState(() => _isLoading = false);

          if (data["responseCode"] == Helper.SUCCESS_STATUS_CODE) {
            print("got data");
            _showSnackBar(data["messages"]["messageDescription"], Colors.green);
            Navigator.pop(context);

          } else {
            print("Error=" + data["messages"]["messageDescription"]);

            _showSnackBar(data["messages"]["messageDescription"], Colors.red);
          }
          if (data == null)
            print("Unable to get data");
        } catch (e) {
          _showSnackBar("Some problem occurred", Colors.red);
        }
        break;

      case ERROR_CODE:
        _showSnackBar("Please check your internet connection ",Colors.red);
        setState(() {
          _isLoading = false;

        });
        break;
    }
  }

  void _showDialog(String title,String msg) {
    // flutter defined function
    showDialog(

      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(title),
          content: new Text(msg),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();

              },
            ),
          ],
        );
      },
    );
  }

}