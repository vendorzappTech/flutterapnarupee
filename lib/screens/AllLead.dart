import 'dart:convert';
import 'package:apanarupee/screens/LeadInfo.dart';
import 'package:apanarupee/utils/Helper.dart';
import 'package:apanarupee/utils/HexColor.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

class AllLead extends StatefulWidget  {
  AllLead({Key key, this.index}) : super(key: key);

  final int index;

  @override
  AllLeadState createState() => AllLeadState(index);
}

class AllLeadState extends State<AllLead> with SingleTickerProviderStateMixin  {
  int pageSize = 10; int pageNumber1 = 1,pageNumber2 = 1,pageNumber3 = 1,pageNumber4 = 1,pageNumber5 =1,pageNumber6=1,previousCount1 = 0,previousCount2 = 0,previousCount3 = 0,previousCount4= 0,previousCount5 = 0,previousCount6 = 0;
  ScrollController controller ;
  bool _isLoading = false;
  bool _isLoading1 = false,_isLoading2 = false,_isLoading3 = false,_isLoading4 = false,_isLoading5 = false,_isLoading6 = false;
  List _myinprogress = new List();
  List _mylogin = new List();
  List _mydisbursed = new List();
  List _myrejected = new List();
  List _myapprovedd = new List();
  List _mydockpick = new List();

  bool _isLoadMore1 = true,_isLoadMore2 = true,_isLoadMore3 = true,_isLoadMore4 = true,_isLoadMore5 = true,_isLoadMore6 = true;

  TabController myTabBarController ;

  final scaffoldKey = new GlobalKey<ScaffoldState>();

  AllLeadState(int index)
  {
    myTabBarController = new TabController(length: 6, vsync: this);
    myTabBarController.index = index;
  }

  @override
  void initState() {
    // TODO: implement initState
    _loadData();

    FirebaseMessaging _firebaseMessaging;

    _firebaseMessaging = FirebaseMessaging();

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print('on message $message');

        // show notification here
//        _showNotificationWithDefaultSound();
        var title = message['notification']['title'];
        var body = message['notification']['body'];
        // show notification here
        _showDialog(title,body);



      },
      onResume: (Map<String, dynamic> message) async {
        print('on resume $message');
      },
      onLaunch: (Map<String, dynamic> message) async {
        print('on launch $message');
      },

    );


  }

  _loadData()async
  {
    print("Load Data");
    try {
      setState(() {
        _isLoading1 = true;
        _isLoading2 = true;
        _isLoading3 = true;
        _isLoading4 = true;
        _isLoading5 = true;
        _isLoading6 = true;
      });

      loadMore11();
//      loadMore66();
//      loadMore22();
//      loadMore33();
//      loadMore44();
//      loadMore55();
    }catch(e){
      print(e.toString());
    }
  }

  _loadDataAgain()async
  {
    pageNumber1 = 1;pageNumber2 = 1;pageNumber3 = 1;pageNumber4 = 1;pageNumber5 = 1;pageNumber6 = 1 ;
    previousCount1 = 0;previousCount2 = 0;previousCount3 = 0;previousCount4 = 0;previousCount5 = 0;previousCount6 = 0;
    _isLoadMore1 = true;_isLoadMore2 = true;_isLoadMore3 = true;_isLoadMore4 = true;_isLoadMore5 = true;_isLoadMore6 = true;

    print("Load Data Again");
    _myinprogress = new List();
    _mylogin = new List();
    _mydisbursed = new List();
    _myrejected = new List();
    _myapprovedd = new List();
    _mydockpick = new List();
    _mydockpick = new List();

    try {
      setState(() {
//        _isLoading = true;
        _isLoading1 = true;
        _isLoading2 = true;
        _isLoading3 = true;
        _isLoading4 = true;
        _isLoading5 = true;
        _isLoading6 = true;
      });

      loadMore11();
//      loadMore66();
//      loadMore22();
//      loadMore33();
//      loadMore44();
//      loadMore55();
    }catch(e){
      print(e.toString());
    }
  }

  ScrollController _scrollController = new ScrollController(initialScrollOffset: 50.0);

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance = ScreenUtil.getInstance()..init(context);
    return WillPopScope(
      child: DefaultTabController(

        length: 6,
        child: Scaffold(
          key: scaffoldKey,
          appBar: AppBar(
              bottom: TabBar(
                controller: myTabBarController,
                isScrollable: true,
                tabs: [
                  Tab(text: "INPROGRESS",),
                  Tab(text: "DOCK PICK ",),
                  Tab(text: "LOGIN",),
                  Tab(text: "APPROVED",),
                  Tab(text: "DISBURSED",),
                  Tab(text: "CANCELLED",),
                ],
              ),
              title: Text('All Leads'),backgroundColor: HexColor("#2b2ca3")
          ),
          body: TabBarView(
            controller: myTabBarController,
            children: [
              (_isLoading1)?Align(
                  alignment: Alignment.center,
                  child: CircularProgressIndicator()
              ):
              (_myinprogress.length<=0)?Center(child: new Image.asset("assets/no_data.png",height: 150,width: 150)):
//                LazyLoadScrollView(
//
//                  isLoading: _isLoading1,
//                  onEndOfPage:  () => loadMore1(),
//                  child:
              ListView.builder(

                itemCount:(_myinprogress.length >= 10 && _isLoadMore1) ?_myinprogress.length+1:_myinprogress.length,
                itemBuilder: (context, _index) {
                  return (_index == _myinprogress.length && _isLoadMore1) ?
                  Container(
                    color: Color(HexColor.getAppOrangeColor()),
                    child: FlatButton(
                      child: Text("Load More",style: TextStyle(color: Colors.white)),
                      onPressed: () {
                        loadMore1();
                      },
                    ),
                  ):
                  GestureDetector(
                      onTap:(){

                      },
                      child:
                      GestureDetector(
                          onTap:(){
                            Navigator.push(
                              context,

                              MaterialPageRoute(builder: (context) =>   LeadInfo(leadId: _myinprogress[_index]["LeadId"].toString(),leadType: myTabBarController.index)),
                            ).then((value) {
                              _loadDataAgain();
                            });},
                          child:
                          Card(
                            margin: EdgeInsets.all(10),
                            child: Padding(
                                padding: const EdgeInsets.all(16.0),
                                child:
                                new Column(
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Expanded(
                                            flex: 6,
                                            child:
                                            new Column(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                mainAxisSize: MainAxisSize.max,
                                                children: <Widget>[
                                                  new Text(_myinprogress[_index]["Name"],style: TextStyle(fontWeight: FontWeight.bold,fontSize: ScreenUtil.getInstance().setSp(50))),
                                                ])
                                        ),

                                        Expanded(
                                            flex: 4,
                                            child:
                                            new Column(
                                                mainAxisAlignment: MainAxisAlignment.end,
                                                crossAxisAlignment: CrossAxisAlignment.end,
                                                mainAxisSize: MainAxisSize.max,
                                                children: <Widget>[
                                                  GestureDetector( onTap: () {
                                                    launch("tel:"+_myinprogress[_index]["MobileNo"]);
                                                  }, child: new Image.asset("assets/call.png",height: 20,) )
//                                            IconButton(
//                                              icon: new Image.asset("assets/call.png"),
//                                              onPressed: (){
//                                                launch("tel:"+_myinprogress[_index]["MobileNo"]);
//                                              },
//                                            )
                                                ])
                                        ),
                                      ],
                                    ),
                                    new Row(
                                      children: <Widget>[
                                        Expanded(
                                            flex: 6,
                                            child:
                                            new Column(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                mainAxisSize: MainAxisSize.max,
                                                children: <Widget>[
                                                  new Padding(padding: EdgeInsets.only(top:5),
                                                    child:
                                                    new Text("Lead Id: "+_myinprogress[_index]["LeadId"].toString(),style: TextStyle(color:Color(HexColor.getAppDarkGrey()),fontSize: ScreenUtil.getInstance().setSp(40))),)
                                                ])
                                        ),
                                      ],
                                    ),
                                    new Row(
                                      children: <Widget>[
                                        Expanded(
                                            flex: 6,
                                            child:
                                            new Column(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                mainAxisSize: MainAxisSize.max,
                                                children: <Widget>[
                                                  new Padding(padding: EdgeInsets.only(top:5),
                                                      child:
                                                      new Text(_myinprogress[_index]["Product"],style: TextStyle(fontSize: ScreenUtil.getInstance().setSp(40),color: Color(HexColor.getAppLightGrey())))),
                                                ])
                                        ),
                                      ],
                                    ),
                                    new Row(
                                      children: <Widget>[
                                        Expanded(
                                            flex: 6,
                                            child:
                                            new Row(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                mainAxisSize: MainAxisSize.max,
                                                children: <Widget>[
                                                  new Padding(padding: EdgeInsets.only(bottom: 0,top:7),
                                                      child:
                                                      new Container(
                                                        height: ScreenUtil.getInstance().setHeight(25),
                                                        child:
                                                        new Image.asset("assets/brupee.png"),padding: EdgeInsets.all(0),) ),
                                                  new Padding(padding: EdgeInsets.only(top:5),
                                                    child:
                                                    new Text(_myinprogress[_index]["Amount"].toString(),style: TextStyle(fontSize: ScreenUtil.getInstance().setSp(40))),),
                                                ])
                                        ),
                                      ],
                                    ),

                                    Divider(),
                                    new Row(
                                      children: <Widget>[
                                        Expanded(
                                            flex:5,
                                            child:
                                            new Row(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              mainAxisAlignment: MainAxisAlignment.start,
                                              children: <Widget>[
                                                new Text(_myinprogress[_index]["IsReferral"].toString(),style: TextStyle(color: Colors.green),)
                                              ],
                                            )
                                        ),
                                        Expanded(
                                            flex:5,
                                            child:
                                            new Row(
                                              crossAxisAlignment: CrossAxisAlignment.end,
                                              mainAxisAlignment: MainAxisAlignment.end,
                                              children: <Widget>[
                                                new Text("Created:",style: TextStyle(color: Color(HexColor.getAppLightGrey()))),
                                                new Text(_myinprogress[_index]["CreatedDays"].toString()+" days ago",style: TextStyle(color: Color(HexColor.getAppLightGrey())),)
                                              ],
                                            )
                                        )
                                      ],
                                    )


                                  ],
                                )

                            ),
                          )));
                },
              ),
//                ),
              (_isLoading2)?Align(
                  alignment: Alignment.center,
                  child: CircularProgressIndicator()
              ):
              (_mydockpick.length<=0)?Center(child: new Image.asset("assets/no_data.png",height: 150,width: 150)):
//              LazyLoadScrollView(
//                onEndOfPage:  () => loadMore6(),
//                child:
//
                ListView.builder(
                  itemCount: (_mydockpick.length >= 10 && _isLoadMore6) ?_mydockpick.length+1:_mydockpick.length,
                  itemBuilder: (context, _index) {
                    return (_index == _mydockpick.length && _isLoadMore6) ?
                    Container(
                      color: Color(HexColor.getAppOrangeColor()),
                      child: FlatButton(
                        child: Text("Load More",style: TextStyle(color: Colors.white),),
                        onPressed: () {
                          loadMore6();

                        },
                      ),
                    ):
                      GestureDetector(
                          onTap:(){

                          },
                          child:
                          GestureDetector(
                              onTap:(){
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(builder: (context) =>   LeadInfo(leadId: _mydockpick[_index]["LeadId"].toString(),leadType: myTabBarController.index)),
                                ).then((value) {
                                  _loadDataAgain();
                                });},
                              child:
                              Card(
                                margin: EdgeInsets.all(10),
                                child: Padding(
                                    padding: const EdgeInsets.all(16.0),
                                    child:
                                    new Column(
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            Expanded(
                                                flex: 6,
                                                child:
                                                new Column(
                                                    mainAxisAlignment: MainAxisAlignment.start,
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    mainAxisSize: MainAxisSize.max,
                                                    children: <Widget>[
                                                      new Text(_mydockpick[_index]["Name"],style: TextStyle(fontWeight: FontWeight.bold,fontSize: ScreenUtil.getInstance().setSp(50))),
                                                    ])
                                            ),

                                            Expanded(
                                                flex: 4,
                                                child:
                                                new Column(
                                                    mainAxisAlignment: MainAxisAlignment.end,
                                                    crossAxisAlignment: CrossAxisAlignment.end,
                                                    mainAxisSize: MainAxisSize.max,
                                                    children: <Widget>[
                                                      GestureDetector( onTap: () {
                                                        launch("tel:"+_mydockpick[_index]["MobileNo"]);
                                                      }, child: new Image.asset("assets/call.png",height: 20,) )
                                                    ])
                                            ),
                                          ],
                                        ),
                                        new Row(
                                          children: <Widget>[
                                            Expanded(
                                                flex: 6,
                                                child:
                                                new Column(
                                                    mainAxisAlignment: MainAxisAlignment.start,
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    mainAxisSize: MainAxisSize.max,
                                                    children: <Widget>[
                                                      new Padding(padding: EdgeInsets.only(top:5),
                                                        child:
                                                        new Text("Lead Id: "+_mydockpick[_index]["LeadId"].toString(),style: TextStyle(color:Color(HexColor.getAppDarkGrey()),fontSize: ScreenUtil.getInstance().setSp(40))),)
                                                    ])
                                            ),
                                          ],
                                        ),
                                        new Row(
                                          children: <Widget>[
                                            Expanded(
                                                flex: 6,
                                                child:
                                                new Row(
                                                    mainAxisAlignment: MainAxisAlignment.start,
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    mainAxisSize: MainAxisSize.max,
                                                    children: <Widget>[
                                                      new Padding(padding: EdgeInsets.only(top:5),
                                                          child:
                                                          new Text(_mydockpick[_index]["Product"],style: TextStyle(color:Color(HexColor.getAppLightGrey()),fontSize: ScreenUtil.getInstance().setSp(40),))),
                                                    ])
                                            ),
                                          ],
                                        ),
                                        new Row(
                                          children: <Widget>[
                                            Expanded(
                                                flex: 6,
                                                child:
                                                new Row(
                                                    mainAxisAlignment: MainAxisAlignment.start,
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    mainAxisSize: MainAxisSize.max,
                                                    children: <Widget>[
                                                      new Padding(padding: EdgeInsets.only(bottom: 0,top:7),
                                                          child:
                                                          new Container(
                                                            height: ScreenUtil.getInstance().setHeight(25),
                                                            child:
                                                            new Image.asset("assets/brupee.png"),padding: EdgeInsets.all(0),) ),

                                                      new Padding(padding: EdgeInsets.only(top:5),
                                                        child:
                                                        new Text(_mydockpick[_index]["Amount"].toString(),style: TextStyle(fontSize: ScreenUtil.getInstance().setSp(40))),),
                                                    ])
                                            ),
                                          ],
                                        ),          Divider(),
                                        new Row(
                                          children: <Widget>[
                                            Expanded(
                                                flex:5,
                                                child:
                                                new Row(
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                  children: <Widget>[
                                                    new Text(_mydockpick[_index]["IsReferral"].toString(),style: TextStyle(color: Colors.green),)
                                                  ],
                                                )
                                            ),
                                            Expanded(
                                                flex:5,
                                                child:
                                                new Row(
                                                  crossAxisAlignment: CrossAxisAlignment.end,
                                                  mainAxisAlignment: MainAxisAlignment.end,
                                                  children: <Widget>[
                                                    new Text("Created:",style: TextStyle(color: Color(HexColor.getAppLightGrey()))),
                                                    new Text(_mydockpick[_index]["CreatedDays"].toString()+" days ago",style: TextStyle(color: Color(HexColor.getAppLightGrey())),)
                                                  ],
                                                )
                                            )
                                          ],
                                        )


                                      ],
                                    )

                                ),
                              )));
                  },
                ),
//              ),
              (_isLoading3)?Align(
                  alignment: Alignment.center,
                  child: CircularProgressIndicator()
              ):
              (_mylogin.length<=0)?Center(child: new Image.asset("assets/no_data.png",height: 150,width: 150)):
//              LazyLoadScrollView(
//                onEndOfPage:  () => loadMore2(),
//                child:
                ListView.builder(
                  itemCount: (_mylogin.length >= 10 && _isLoading2) ?_mylogin.length+1:_mylogin.length,
                  itemBuilder: (context, _index) {
                    return (_index == _mylogin.length && _isLoading2) ?
                    Container(
                      color: Color(HexColor.getAppOrangeColor()),
                      child: FlatButton(
                        child: Text("Load More",style:TextStyle(color:Colors.white ) ,),
                        onPressed: () {
                          loadMore2();

                        },
                      ),
                    ):
                      GestureDetector(
                          onTap:(){
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) =>   LeadInfo(leadId: _mylogin[_index]["LeadId"].toString(),leadType: myTabBarController.index)),
                            ).then((value) {
                              _loadDataAgain();
                            });

                          },
                          child:
                          Card(
                            margin: EdgeInsets.all(10),
                            child: Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: new Column(
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Expanded(
                                            flex: 6,
                                            child:
                                            new Column(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                mainAxisSize: MainAxisSize.max,
                                                children: <Widget>[
                                                  new Text(_mylogin[_index]["Name"],style: TextStyle(fontWeight: FontWeight.bold,fontSize: ScreenUtil.getInstance().setSp(50))),
                                                ])
                                        ),

                                        Expanded(
                                            flex: 4,
                                            child:
                                            new Column(
                                                mainAxisAlignment: MainAxisAlignment.end,
                                                crossAxisAlignment: CrossAxisAlignment.end,
                                                mainAxisSize: MainAxisSize.max,
                                                children: <Widget>[
                                                  GestureDetector( onTap: () {
                                                    launch("tel:"+_mylogin[_index]["MobileNo"]);
                                                  }, child: new Image.asset("assets/call.png",height: 20,) )
                                                ])
                                        ),
                                      ],
                                    ),
                                    new Row(
                                      children: <Widget>[
                                        Expanded(
                                            flex: 6,
                                            child:
                                            new Column(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                mainAxisSize: MainAxisSize.max,
                                                children: <Widget>[
                                                  new Padding(padding: EdgeInsets.only(top:5),
                                                    child:
                                                    new Text("Lead Id: "+_mylogin[_index]["LeadId"].toString(),style: TextStyle(fontSize: ScreenUtil.getInstance().setSp(40),color: Color(HexColor.getAppDarkGrey()))),),
                                                  new Padding(padding: EdgeInsets.only(top:5),
                                                    child:
                                                    new Text("Application Id: "+_mylogin[_index]["BankApplicationId"].toString(),style: TextStyle(fontSize: ScreenUtil.getInstance().setSp(40),color: Color(HexColor.getAppDarkGrey()))),)
                                                ])
                                        ),
                                      ],
                                    ),
                                    new Row(
                                      children: <Widget>[
                                        Expanded(
                                            flex: 6,
                                            child:
                                            new Column(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                mainAxisSize: MainAxisSize.max,
                                                children: <Widget>[
                                                  new Padding(padding: EdgeInsets.only(top:5),
                                                      child:
                                                      new Text(_mylogin[_index]["Product"],style: TextStyle(fontSize: ScreenUtil.getInstance().setSp(40),color: Color(HexColor.getAppLightGrey())))),
                                                ])
                                        ),
                                      ],
                                    ),
                                    new Row(
                                      children: <Widget>[
                                        Expanded(
                                            flex: 6,
                                            child:
                                            new Row(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                mainAxisSize: MainAxisSize.max,
                                                children: <Widget>[
                                                  new Padding(padding: EdgeInsets.only(bottom: 0,top:7),
                                                      child:
                                                      new Container(
                                                        height: ScreenUtil.getInstance().setHeight(25),
                                                        child:
                                                        new Image.asset("assets/brupee.png"),padding: EdgeInsets.all(0),) ),
                                                  new Padding(padding: EdgeInsets.only(top:5),
                                                    child:
                                                    new Text(_mylogin[_index]["Amount"].toString(),style: TextStyle(fontSize: ScreenUtil.getInstance().setSp(40))),),
                                                ])
                                        ),
                                      ],
                                    ),
                                    Divider(),
                                    new Row(
                                      children: <Widget>[
                                        Expanded(
                                            flex:5,
                                            child:
                                            new Row(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              mainAxisAlignment: MainAxisAlignment.start,
                                              children: <Widget>[
                                                new Text(_mylogin[_index]["IsReferral"].toString(),style: TextStyle(color: Colors.green),)
                                              ],
                                            )
                                        ),
                                        Expanded(
                                            flex:5,
                                            child:
                                            new Row(
                                              crossAxisAlignment: CrossAxisAlignment.end,
                                              mainAxisAlignment: MainAxisAlignment.end,
                                              children: <Widget>[
                                                new Text("Created:",style: TextStyle(color: Color(HexColor.getAppLightGrey()),fontSize: ScreenUtil.getInstance().setSp(40))),
                                                new Text(_mylogin[_index]["CreatedDays"].toString()+" days ago",style: TextStyle(color: Color(HexColor.getAppLightGrey()),fontSize: ScreenUtil.getInstance().setSp(40)),)
                                              ],
                                            )
                                        )
                                      ],
                                    )
                                  ],
                                )
                            ),
                          ));
                  },
                ),
//              ),
              (_isLoading4)?Align(
                  alignment: Alignment.center,
                  child: CircularProgressIndicator()
              ):
              (_myapprovedd.length<=0)?Center(child: new Image.asset("assets/no_data.png",height: 150,width: 150)):
//              LazyLoadScrollView(
//                onEndOfPage:  () => loadMore3(),
//                child:
                ListView.builder(
                  itemCount: (_myapprovedd.length >= 10 && _isLoadMore3) ?_myapprovedd.length+1:_myapprovedd.length,
                  itemBuilder: (context, _index) {
                    return (_index == _myapprovedd.length && _isLoadMore3) ?
                    Container(
                      color: Color(HexColor.getAppOrangeColor()),
                      child: FlatButton(
                        child: Text("Load More",style: TextStyle(color: Colors.white),),
                        onPressed: () {
                          loadMore3();
                        },
                      ),
                    ):
                      GestureDetector(
                          onTap:(){
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) =>   LeadInfo(leadId: _myapprovedd[_index]["LeadId"].toString(),leadType: myTabBarController.index)),
                            ).then((value) {
                              _loadDataAgain();
                            });

                          },
                          child:

                          Card(
                            margin: EdgeInsets.all(10),
                            child: Padding(
                                padding: const EdgeInsets.all(16.0),
                                child:
                                new Column(
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Expanded(
                                            flex: 6,
                                            child:
                                            new Column(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                mainAxisSize: MainAxisSize.max,
                                                children: <Widget>[
                                                  new Text(_myapprovedd[_index]["Name"],style: TextStyle(fontWeight: FontWeight.bold,fontSize: ScreenUtil.getInstance().setSp(50))),

                                                ])
                                        ),

                                        Expanded(
                                            flex: 4,
                                            child:
                                            new Column(
                                                mainAxisAlignment: MainAxisAlignment.end,
                                                crossAxisAlignment: CrossAxisAlignment.end,
                                                mainAxisSize: MainAxisSize.max,
                                                children: <Widget>[
                                                  GestureDetector( onTap: () {
                                                    launch("tel:"+_myapprovedd[_index]["MobileNo"]);
                                                  }, child: new Image.asset("assets/call.png",height: 20,) )
                                                ])
                                        ),
                                      ],
                                    ),
                                    new Row(
                                      children: <Widget>[
                                        Expanded(
                                            flex: 6,
                                            child:
                                            new Column(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                mainAxisSize: MainAxisSize.max,
                                                children: <Widget>[
                                                  new Padding(padding: EdgeInsets.only(top:5),
                                                    child:
                                                    new Text("Lead Id: "+_myapprovedd[_index]["LeadId"].toString(),style: TextStyle(fontSize: ScreenUtil.getInstance().setSp(40),color: Color(HexColor.getAppDarkGrey()))),),
                                                  new Padding(padding: EdgeInsets.only(top:5),
                                                    child:
                                                    new Text("Application Id: "+_myapprovedd[_index]["BankApplicationId"].toString(),style: TextStyle(fontSize: ScreenUtil.getInstance().setSp(40),color: Color(HexColor.getAppDarkGrey()))),)
                                                ])
                                        ),
                                      ],
                                    ),
                                    new Row(
                                      children: <Widget>[
                                        Expanded(
                                            flex: 6,
                                            child:
                                            new Column(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                mainAxisSize: MainAxisSize.max,
                                                children: <Widget>[
                                                  new Padding(padding: EdgeInsets.only(top:5),
                                                      child:
                                                      new Text(_myapprovedd[_index]["Product"],style: TextStyle(fontSize: ScreenUtil.getInstance().setSp(40),color: Color(HexColor.getAppLightGrey())))),
                                                ])
                                        ),
                                      ],
                                    ),
                                    new Row(
                                      children: <Widget>[
                                        Expanded(
                                            flex: 6,
                                            child:
                                            new Row(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                mainAxisSize: MainAxisSize.max,
                                                children: <Widget>[
                                                  new Padding(padding: EdgeInsets.only(bottom: 0,top:7),
                                                      child:
                                                      new Container(
                                                        height: ScreenUtil.getInstance().setHeight(25),
                                                        child:
                                                        new Image.asset("assets/brupee.png"),padding: EdgeInsets.all(0),) ),
                                                  new Padding(padding: EdgeInsets.only(top:5),
                                                    child:
                                                    new Text(_myapprovedd[_index]["Amount"].toString(),style: TextStyle(fontSize: ScreenUtil.getInstance().setSp(40))),),
                                                ])
                                        ),
                                      ],
                                    ),
                                    Divider(),
                                    new Row(
                                      children: <Widget>[
                                        Expanded(
                                            flex:5,
                                            child:
                                            new Row(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              mainAxisAlignment: MainAxisAlignment.start,
                                              children: <Widget>[
                                                new Text(_myapprovedd[_index]["IsReferral"].toString(),style: TextStyle(color: Colors.green),)
                                              ],
                                            )
                                        ),
                                        Expanded(
                                            flex:5,
                                            child:
                                            new Row(
                                              crossAxisAlignment: CrossAxisAlignment.end,
                                              mainAxisAlignment: MainAxisAlignment.end,
                                              children: <Widget>[
                                                new Text("Created:",style: TextStyle(color: Color(HexColor.getAppLightGrey()),fontSize: ScreenUtil.getInstance().setSp(40))),
                                                new Text(_myapprovedd[_index]["CreatedDays"].toString()+" days ago",style: TextStyle(color: Color(HexColor.getAppLightGrey()),fontSize: ScreenUtil.getInstance().setSp(40)),)
                                              ],
                                            )
                                        )
                                      ],
                                    )
                                  ],
                                )

                            ),
                          ));
                  },
                ),
//              ),
              (_isLoading5)?Align(
                  alignment: Alignment.center,
                  child: CircularProgressIndicator()
              ):
              (_mydisbursed.length<=0)?Center(child: new Image.asset("assets/no_data.png",height: 150,width: 150)):
//              LazyLoadScrollView(
//                onEndOfPage:  () => loadMore4(),
//                child:
                ListView.builder(
                  itemCount: (_mydisbursed.length >= 10 && _isLoadMore4) ?_mydisbursed.length+1:_mydisbursed.length,
                  itemBuilder:  (context, _index) {
                    return
                      (_index == _mydisbursed.length && _isLoadMore4) ?
                      Container(
                        color: Color(HexColor.getAppOrangeColor()),
                        child: FlatButton(
                          child: Text("Load More",style: TextStyle(color: Colors.white),),
                          onPressed: () {
                            loadMore4();
                          },
                        ),
                      ):
                      new Container(
                          padding: EdgeInsets.all(10),
//                            decoration: new BoxDecoration(boxShadow: [
//                              new BoxShadow(
//                                color: Colors.grey,
//
////                                offset: Offset(1.0, 6.0),
//                                blurRadius: 80.0,
//                              ),
//                            ]),
                          child:
                          GestureDetector(
                              onTap:(){
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(builder: (context) =>   LeadInfo(leadId: _mydisbursed[_index]["LeadId"].toString(),leadType: myTabBarController.index,)),
                                ).then((value) {
                                  _loadDataAgain();
                                });

                              },
                              child:
                              Card(
                                margin: EdgeInsets.all(10),
                                child: Padding(
                                    padding: const EdgeInsets.all(16.0),
                                    child:
                                    new Column(
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            Expanded(
                                                flex: 6,
                                                child:
                                                new Column(
                                                    mainAxisAlignment: MainAxisAlignment.start,
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    mainAxisSize: MainAxisSize.max,
                                                    children: <Widget>[
                                                      new Text(_mydisbursed[_index]["Name"],style: TextStyle(fontWeight: FontWeight.bold,fontSize: ScreenUtil.getInstance().setSp(50))),
                                                    ])
                                            ),

                                            Expanded(
                                                flex: 4,
                                                child:
                                                new Column(
                                                    mainAxisAlignment: MainAxisAlignment.end,
                                                    crossAxisAlignment: CrossAxisAlignment.end,
                                                    mainAxisSize: MainAxisSize.max,
                                                    children: <Widget>[

                                                      GestureDetector( onTap: () {
                                                      }, child: new Image.asset("assets/done.png",height: 30,width: 30,) )
                                                    ])
                                            ),
                                          ],
                                        ),
                                        new Row(
                                          children: <Widget>[
                                            Expanded(
                                                flex: 6,
                                                child:
                                                new Column(
                                                    mainAxisAlignment: MainAxisAlignment.start,
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    mainAxisSize: MainAxisSize.max,
                                                    children: <Widget>[
                                                      new Padding(padding: EdgeInsets.only(top:5),
                                                        child:
                                                        new Text("Lead Id: "+_mydisbursed[_index]["LeadId"].toString(),style: TextStyle(color:Color(HexColor.getAppDarkGrey()),fontSize: ScreenUtil.getInstance().setSp(40))),),
                                                      new Padding(padding: EdgeInsets.only(top:5),
                                                        child:
                                                        new Text("Application Id: "+_mydisbursed[_index]["BankApplicationId"].toString(),style: TextStyle(fontSize: ScreenUtil.getInstance().setSp(40),color: Color(HexColor.getAppDarkGrey()))),)

                                                    ])
                                            ),
                                          ],
                                        ),
                                        new Row(
                                          children: <Widget>[
                                            Expanded(
                                                flex: 6,
                                                child:
                                                new Column(
                                                    mainAxisAlignment: MainAxisAlignment.start,
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    mainAxisSize: MainAxisSize.max,
                                                    children: <Widget>[
                                                      new Padding(padding: EdgeInsets.only(top:5),
                                                          child:
                                                          new Text(_mydisbursed[_index]["Product"],style: TextStyle(color:Color(HexColor.getAppLightGrey()),fontSize: ScreenUtil.getInstance().setSp(40)))),
                                                    ])
                                            ),
                                          ],
                                        ),
                                        new Row(
                                          children: <Widget>[
                                            Expanded(
                                                flex: 6,
                                                child:
                                                new Row(
                                                    mainAxisAlignment: MainAxisAlignment.start,
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    mainAxisSize: MainAxisSize.max,
                                                    children: <Widget>[
                                                      new Padding(padding: EdgeInsets.only(bottom: 0,top:7),
                                                          child:
                                                          new Container(
                                                            height: ScreenUtil.getInstance().setHeight(25),
                                                            child:
                                                            new Image.asset("assets/brupee.png"),padding: EdgeInsets.all(0),) ),
                                                      new Padding(padding: EdgeInsets.only(top:5),
                                                        child:
                                                        new Text(_mydisbursed[_index]["Amount"].toString(),style: TextStyle(fontSize: ScreenUtil.getInstance().setSp(40))),),
                                                    ])
                                            ),
                                          ],
                                        ),
                                        Divider(),
                                        new Row(
                                          children: <Widget>[
                                            Expanded(
                                                flex:5,
                                                child:
                                                new Row(
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                  children: <Widget>[
                                                    new Text(_mydisbursed[_index]["IsReferral"].toString(),style: TextStyle(color: Colors.green),)
                                                  ],
                                                )
                                            ),
                                            Expanded(
                                                flex:5,
                                                child:
                                                new Row(
                                                  crossAxisAlignment: CrossAxisAlignment.end,
                                                  mainAxisAlignment: MainAxisAlignment.end,
                                                  children: <Widget>[
                                                    new Text("Created:",style: TextStyle(fontSize: ScreenUtil.getInstance().setSp(40),color: Color(HexColor.getAppLightGrey(),))),
                                                    new Text(_mydisbursed[_index]["CreatedDays"].toString()+" days ago",style: TextStyle(fontSize: ScreenUtil.getInstance().setSp(40),color: Color(HexColor.getAppLightGrey())),)
                                                  ],
                                                )
                                            )
                                          ],
                                        )
                                      ],
                                    )

                                ),
                              )));
                  },
//                ),
              ),
              (_isLoading6)?Align(
                  alignment: Alignment.center,
                  child: CircularProgressIndicator()
              ):
              (_myrejected.length<=0)?Center(child: new Image.asset("assets/no_data.png",height: 150,width: 150)):
//              LazyLoadScrollView(
//                onEndOfPage:  () => loadMore5(),
//                child:
                ListView.builder(
                  itemCount: (_myrejected.length >= 10 && _isLoadMore5) ?_myrejected.length+1:_myrejected.length,
                  itemBuilder: (context, _index) {
                    return  (_index == _myrejected.length && _isLoadMore5 ) ?
                    Container(
                      color: Color(HexColor.getAppOrangeColor()),
                      child: FlatButton(
                        child: Text("Load More",style: TextStyle(color: Colors.white),),
                        onPressed: () {
                          loadMore5();
                        },
                      ),
                    ):

                    GestureDetector(
                          onTap:(){
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) =>   LeadInfo(leadId: _myrejected[_index]["LeadId"].toString(),leadType: myTabBarController.index)),
                            ).then((value) {
                              _loadDataAgain();
                            });

                          },
                          child:

                          Card(
                            margin: EdgeInsets.all(10),
                            child: Padding(
                                padding: const EdgeInsets.all(16.0),
                                child:
                                new Column(
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Expanded(
                                            flex: 6,
                                            child:
                                            new Column(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                mainAxisSize: MainAxisSize.max,
                                                children: <Widget>[
                                                  new Text(_myrejected[_index]["Name"],style: TextStyle(fontWeight: FontWeight.bold,fontSize: ScreenUtil.getInstance().setSp(50)),),
                                                ])
                                        ),

                                        Expanded(
                                            flex: 4,
                                            child:
                                            new Column(
                                                mainAxisAlignment: MainAxisAlignment.end,
                                                crossAxisAlignment: CrossAxisAlignment.end,
                                                mainAxisSize: MainAxisSize.max,
                                                children: <Widget>[
//                                              IconButton(
//                                                icon: Icon(Icons.close,color: Colors.red,),
//                                                onPressed: (){
//
//                                                },
//                                              ).

                                                  GestureDetector( onTap: () {
                                                  }, child: new Icon(Icons.close,color: Colors.red,) )
                                                ])
                                        ),
                                      ],
                                    ),
                                    new Row(
                                      children: <Widget>[
                                        Expanded(
                                            flex: 6,
                                            child:
                                            new Column(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                mainAxisSize: MainAxisSize.max,
                                                children: <Widget>[
                                                  new Padding(padding: EdgeInsets.only(top:5),
                                                    child:
                                                    new Text("Lead Id: "+_myrejected[_index]["LeadId"].toString(),style: TextStyle(fontSize: ScreenUtil.getInstance().setSp(40),color: Color(HexColor.getAppDarkGrey()))),)
                                                ])
                                        ),
                                      ],
                                    ),
                                    new Row(
                                      children: <Widget>[
                                        Expanded(
                                            flex: 6,
                                            child:
                                            new Column(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                mainAxisSize: MainAxisSize.max,
                                                children: <Widget>[
                                                  new Padding(padding: EdgeInsets.only(top:5),
                                                      child:
                                                      new Text(_myrejected[_index]["Product"],style: TextStyle(fontSize: ScreenUtil.getInstance().setSp(40),color: Color(HexColor.getAppLightGrey())))),
                                                ])
                                        ),
                                      ],
                                    ),
                                    new Row(
                                      children: <Widget>[
                                        Expanded(
                                            flex: 6,
                                            child:
                                            new Row(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                mainAxisSize: MainAxisSize.max,
                                                children: <Widget>[
                                                  new Padding(padding: EdgeInsets.only(bottom: 0,top:7),
                                                      child:
                                                      new Container(
                                                        height: ScreenUtil.getInstance().setHeight(25),
                                                        child:
                                                        new Image.asset("assets/brupee.png"),padding: EdgeInsets.all(0),) ),
                                                  new Padding(padding: EdgeInsets.only(top:5),
                                                    child:
                                                    new Text(_myrejected[_index]["Amount"].toString(),style: TextStyle(fontSize: ScreenUtil.getInstance().setSp(40))),),
                                                ])
                                        ),
                                      ],
                                    ),
                                    Divider(),
                                    new Row(
                                      children: <Widget>[
                                        Expanded(
                                            flex:5,
                                            child:
                                            new Row(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              mainAxisAlignment: MainAxisAlignment.start,
                                              children: <Widget>[
                                                new Text(_myrejected[_index]["IsReferral"].toString(),style: TextStyle(color: Colors.green),)
                                              ],
                                            )
                                        ),
                                        Expanded(
                                            flex:5,
                                            child:
                                            new Row(
                                              crossAxisAlignment: CrossAxisAlignment.end,
                                              mainAxisAlignment: MainAxisAlignment.end,
                                              children: <Widget>[
                                                new Text("Created:",style: TextStyle(color: Color(HexColor.getAppLightGrey()),fontSize: ScreenUtil.getInstance().setSp(40))),
                                                new Text(_myrejected[_index]["CreatedDays"].toString()+" days ago",style: TextStyle(color: Color(HexColor.getAppLightGrey()),fontSize: ScreenUtil.getInstance().setSp(40)),)
                                              ],
                                            )
                                        )
                                      ],
                                    )
                                  ],
                                )

                            ),
                          ));
                  },
                ),
//              ),
            ]
          ),
        ),
      ),
    );
  }


  void _showSnackBar(String text,Color color) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(backgroundColor:color ,content: new Text(text),duration: new Duration(milliseconds: 5000),));
  }

  loadMore1()async{


    if(previousCount1 != pageNumber1) {
      previousCount1 = pageNumber1;

      setState(() {
//        _isLoading1 = true;
      });


      String url = Helper.GET_ALL_LEADS_URL + "?pageNumber=" +
          pageNumber1.toString() + "&pageSize=" + pageSize.toString() +
          "&LeadStatus=InProgress";
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String token = prefs.getString("accessToken");

      print("url=" + url);
      print("token=" + token);
      try {
        var data = await http.get(Uri.encodeFull(url),
            headers: {
              "Content-Type": "application/json",
              "Authorization": "Basic " + token
            }).timeout(
            const Duration(seconds: 10));


        if (data.statusCode == 200) {
          setState(() {
            _isLoading1 = false;
            try {
              List temp = json.decode(data.body);
              if (temp.length > 0){
                _myinprogress.addAll(json.decode(data.body));

              }
              else{
                print("No data");
                _isLoadMore1 = false;
              }
              if (temp.length > 0)
                pageNumber1++;
            } catch (e) {
              print(e.toString());
            }
          });
        } else {
          SharedPreferences prefs = await SharedPreferences.getInstance();
          prefs.setBool("isLoggedIn", false);
          prefs.setString("username", null);
          prefs.setString("mobile", null);
          Navigator.of(context).pop();
          Navigator.of(context).pushReplacementNamed("/login");
        }
      } catch (e) {
        print(e.toString());
      }
    }
  }

  loadMore2()async{

    if(previousCount2 != pageNumber2) {
      previousCount2 = pageNumber2;

      setState(() {
//        _isLoading2 = true;
      });
      String url = Helper.GET_ALL_LEADS_URL + "?pageNumber=" +
          pageNumber2.toString() + "&pageSize=" + pageSize.toString() +
          "&LeadStatus=login";
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String token = prefs.getString("accessToken");

      print("url=" + url);
      try {
        var data = await http.get(Uri.encodeFull(url),
            headers: {
              "Content-Type": "application/json",
              "Authorization": "Basic " + token
            }).timeout(
            const Duration(seconds: 10));
        if (data.statusCode == 200) {
          setState(() {
            _isLoading2 = false;
            List temp = json.decode(data.body);
            if (temp.length > 0)
              _mylogin.addAll(json.decode(data.body));
            else {
              print("No data");
              _isLoadMore2 = false;
            }

            if (temp.length > 0)
              pageNumber2++;
          });
        } else {
          SharedPreferences prefs = await SharedPreferences.getInstance();
          prefs.setBool("isLoggedIn", false);
          prefs.setString("username", null);
          prefs.setString("mobile", null);
          Navigator.of(context).pop();
          Navigator.of(context).pushReplacementNamed("/login");
        }


        print("Data=" + _mylogin.length.toString());
      } catch (e) {
        print(e.toString());
      }
    }
  }

  loadMore3()async{

    if(previousCount3 != pageNumber3) {
      previousCount3 = pageNumber3;

      setState(() {
//        _isLoading3 = true;
      });

      String url = Helper.GET_ALL_LEADS_URL + "?pageNumber=" +
          pageNumber3.toString() + "&pageSize=" + pageSize.toString() +
          "&LeadStatus=approved";
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String token = prefs.getString("accessToken");

      print("url=" + url);
      try {
        var data = await http.get(Uri.encodeFull(url),
            headers: {
              "Content-Type": "application/json",
              "Authorization": "Basic " + token
            }).timeout(
            const Duration(seconds: 10));
        if (data.statusCode == 200) {
          setState(() {
            _isLoading3 = false;

            List temp = json.decode(data.body);
            if (temp.length > 0) {
              _mydisbursed.addAll(json.decode(data.body));
              print("Data123=" + data.toString());
            }
            else {
              print("No data");
              _isLoadMore3 = false;
            }
            if (temp.length > 0)
              pageNumber3++;
          });
        } else {
          SharedPreferences prefs = await SharedPreferences.getInstance();
          prefs.setBool("isLoggedIn", false);
          prefs.setString("username", null);
          prefs.setString("mobile", null);
          Navigator.of(context).pop();
          Navigator.of(context).pushReplacementNamed("/login");
        }

        print("Data=" + _mydisbursed.length.toString());
      } catch (e) {
        print(e.toString());
      }
    }
  }

  loadMore4()async{

    if(previousCount4 != pageNumber4) {
      previousCount4 = pageNumber4;


      setState(() {
//        _isLoading4 = true;
      });

      String url = Helper.GET_ALL_LEADS_URL + "?pageNumber=" +
          pageNumber4.toString() + "&pageSize=" + pageSize.toString() +
          "&LeadStatus=disbursed";
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String token = prefs.getString("accessToken");

      print("url=" + url);
      try {
        var data = await http.get(Uri.encodeFull(url),
            headers: {
              "Content-Type": "application/json",
              "Authorization": "Basic " + token
            }).timeout(
            const Duration(seconds: 10));
        if (data.statusCode == 200) {
          setState(() {
            _isLoading4 = false;

            List temp = json.decode(data.body);
            if (temp.length > 0) {
              _mydisbursed.addAll(json.decode(data.body));
              print("Data123=" + data.toString());
              pageNumber4++;
            }
            else{
              _isLoadMore4 = false;
              print("No data");}
          });
          print("Data=" + _mydisbursed.length.toString());
        } else {
          SharedPreferences prefs = await SharedPreferences.getInstance();
          prefs.setBool("isLoggedIn", false);
          prefs.setString("username", null);
          prefs.setString("mobile", null);
          Navigator.of(context).pop();
          Navigator.of(context).pushReplacementNamed("/login");
        }
      } catch (e) {
        print(e.toString());
      }
    }

  }

  loadMore5()async{

    if(previousCount5 != pageNumber5) {
      previousCount5 = pageNumber5;


      setState(() {
//        _isLoading5 = true;
      });

      String url = Helper.GET_ALL_LEADS_URL + "?pageNumber=" +
          pageNumber5.toString() + "&pageSize=" + pageSize.toString() +
          "&LeadStatus=cancelled";
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String token = prefs.getString("accessToken");

      try {
        print("url=" + url);
        var data = await http.get(Uri.encodeFull(url),
            headers: {
              "Content-Type": "application/json",
              "Authorization": "Basic " + token
            }).timeout(
            const Duration(seconds: 10));

        if (data.statusCode == 200) {
          setState(() {
            _isLoading5 = false;

            List temp = json.decode(data.body);
            if (temp.length > 0)
              _myrejected.addAll(json.decode(data.body));
            else{
              _isLoadMore5 = false;
              print("No data");}
            if (temp.length > 0)
              pageNumber5++;
          });


          print("Data=" + _mydisbursed.length.toString());
        } else {
          SharedPreferences prefs = await SharedPreferences.getInstance();
          prefs.setBool("isLoggedIn", false);
          prefs.setString("username", null);
          prefs.setString("mobile", null);
          Navigator.of(context).pop();
          Navigator.of(context).pushReplacementNamed("/login");
        }
      } catch (e) {
        print(e.toString());
      }
    }
  }

  loadMore6()async{

    if(previousCount6 != pageNumber6) {
      previousCount6 = pageNumber6;

      setState(() {
//        _isLoading6 = true;
      });

      String url = Helper.GET_ALL_LEADS_URL + "?pageNumber=" +
          pageNumber6.toString() + "&pageSize=" + pageSize.toString() +
          "&LeadStatus=docpick";
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String token = prefs.getString("accessToken");
      try {
        print("url=" + url);
        var data = await http.get(Uri.encodeFull(url),
            headers: {
              "Content-Type": "application/json",
              "Authorization": "Basic " + token
            }).timeout(
            const Duration(seconds: 10));
        if (data.statusCode == 200) {
          setState(() {
            _isLoading6 = false;

            List temp = json.decode(data.body);
            if (temp.length > 0)
              _mydockpick.addAll(json.decode(data.body));
            else{
              print("No data");
              _isLoadMore6 = false;
            }

            if (temp.length > 0)
              pageNumber6++;
          });


          print("Data=" + _mydockpick.length.toString());
        } else {
          SharedPreferences prefs = await SharedPreferences.getInstance();
          prefs.setBool("isLoggedIn", false);
          prefs.setString("username", null);
          prefs.setString("mobile", null);
          Navigator.of(context).pop();
          Navigator.of(context).pushReplacementNamed("/login");
        }
      } catch (e) {
        print(e.toString());
      }
    }

  }


  loadMore11()async{
    try{
      setState(() {
        _isLoading1 = true;
      });

      String url = Helper.GET_ALL_LEADS_URL+"?pageNumber="+pageNumber1.toString()+"&pageSize="+pageSize.toString()+"&LeadStatus=InProgress";
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String token = prefs.getString("accessToken");

      print("url="+url);
      print("token="+token);
      var data = await http.get(Uri.encodeFull(url),
          headers: {"Content-Type": "application/json", "Authorization": "Basic "+token}).timeout(
          const Duration(seconds: 10));

      if (data.statusCode == 200) {

        setState(() {

          _isLoading1 = false;
          try {
            List temp = json.decode(data.body);
            print(data.body);
            if (temp.length > 0)
              _myinprogress.addAll(json.decode(data.body));
            else
              print("No data");
            if (temp.length > 0)
              pageNumber1++;
          }catch(e)
          {
            print(e.toString());
          }
        });

        print("Data="+_myinprogress.length.toString());

        loadMore66();

      }else
      {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setBool("isLoggedIn", false);
        prefs.setString("username", null);
        prefs.setString("mobile", null);
        Navigator.of(context).pop();
        Navigator.of(context).pushReplacementNamed("/login");
      }


    }catch(e){print(e.toString());}
  }

  loadMore22()async{

    try {
      setState(() {
        _isLoading2 = true;
      });
      String url = Helper.GET_ALL_LEADS_URL + "?pageNumber=" +
          pageNumber2.toString() + "&pageSize=" + pageSize.toString() +
          "&LeadStatus=login";
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String token = prefs.getString("accessToken");

      print("url=" + url);
      var data = await http.get(Uri.encodeFull(url),
          headers: {
            "Content-Type": "application/json",
            "Authorization": "Basic " + token
          }).timeout(
          const Duration(seconds: 10));
      if (data.statusCode == 200) {
        setState(() {
          try {
            _isLoading2 = false;
            List temp = json.decode(data.body);
            print("Data = "+data.body);
            if (temp.length > 0)
              _mylogin.addAll(json.decode(data.body));
            else
              print("No data");

            if (temp.length > 0)
              pageNumber2++;

          } catch (e) {
            print(e.toString());
          }
        });
        print("Data=" + _mylogin.length.toString());


        loadMore33();
      }else
      {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setBool("isLoggedIn", false);
        prefs.setString("username", null);
        prefs.setString("mobile", null);
        Navigator.of(context).pop();
        Navigator.of(context).pushReplacementNamed("/login");
      }

    }catch(e){print(e.toString());}
  }

  loadMore33()async{

    try{
      setState(() {
        _isLoading3 = true;
      });

      String url = Helper.GET_ALL_LEADS_URL+"?pageNumber="+pageNumber3.toString()+"&pageSize="+pageSize.toString()+"&LeadStatus=approved";
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String token = prefs.getString("accessToken");

      print("url="+url);
      var data = await http.get(Uri.encodeFull(url),
          headers: {"Content-Type": "application/json", "Authorization": "Basic "+token}).timeout(
          const Duration(seconds: 10));
      if (data.statusCode == 200) {
        setState(() {
          _isLoading3 = false;
          try{
            List temp =  json.decode(data.body);
            if(temp.length >0) {
              pageNumber3++;
              _myapprovedd.addAll(json.decode(data.body));
              print("Data123="+data.toString());
            }
            else
              print("No data");
          }catch(e){
            print(e.toString());
          }
        });


        print("Data="+_myapprovedd.length.toString());


        loadMore44();
      }else
      {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setBool("isLoggedIn", false);
        prefs.setString("username", null);
        prefs.setString("mobile", null);
        Navigator.of(context).pop();
        Navigator.of(context).pushReplacementNamed("/login");
      }

    }catch(e){print(e.toString());}
  }



  loadMore44()async{

    try{
      setState(() {
        _isLoading4 = true;
      });

      String url = Helper.GET_ALL_LEADS_URL+"?pageNumber="+pageNumber4.toString()+"&pageSize="+pageSize.toString()+"&LeadStatus=disbursed";
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String token = prefs.getString("accessToken");

      print("url="+url);
      var data = await http.get(Uri.encodeFull(url),
          headers: {"Content-Type": "application/json", "Authorization": "Basic "+token}).timeout(
          const Duration(seconds: 10));
      if (data.statusCode == 200) {
        setState(() {
          _isLoading4 = false;
          try{
            List temp =  json.decode(data.body);
            if(temp.length >0) {
              _mydisbursed.addAll(json.decode(data.body));
              print("Data123="+data.toString());
              pageNumber4++;
            }
            else
              print("No data");
          }catch(e){
            print(e.toString());
          }
        });


        print("Data="+_mydisbursed.length.toString());


        loadMore55();
      }else
      {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setBool("isLoggedIn", false);
        prefs.setString("username", null);
        prefs.setString("mobile", null);
        Navigator.of(context).pop();
        Navigator.of(context).pushReplacementNamed("/login");
      }

    }catch(e){print(e.toString());}
  }

  loadMore55()async{
    try
    {
      setState(() {
        _isLoading5 = true;
      });

      String url = Helper.GET_ALL_LEADS_URL+"?pageNumber="+pageNumber5.toString()+"&pageSize="+pageSize.toString()+"&LeadStatus=cancelled";
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String token = prefs.getString("accessToken");

      print("url="+url);
      var data = await http.get(Uri.encodeFull(url),
          headers: {"Content-Type": "application/json", "Authorization": "Basic "+token}).timeout(
          const Duration(seconds: 10));

      if (data.statusCode == 200) {
        setState(() {
          _isLoading5 = false;
          try {
            List temp = json.decode(data.body);
            if (temp.length > 0)
              _myrejected.addAll(json.decode(data.body));
            else
              print("No data");

            if (temp.length > 0)
              pageNumber5++;

          } catch(e){
            print(e.toString());
          }
        });


        print("Data="+_mydisbursed.length.toString());


        setState(() {
          _isLoading = false;
        });

      }else
      {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setBool("isLoggedIn", false);
        prefs.setString("username", null);
        prefs.setString("mobile", null);
        Navigator.of(context).pop();
        Navigator.of(context).pushReplacementNamed("/login");
      }



    }catch(e){print(e.toString());}
  }

  loadMore66()async{
    try
    {
      setState(() {
        _isLoading6 = true;
      });

      String url = Helper.GET_ALL_LEADS_URL+"?pageNumber="+pageNumber6.toString()+"&pageSize="+pageSize.toString()+"&LeadStatus=docpick";
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String token = prefs.getString("accessToken");

      print("url="+url);
      var data = await http.get(Uri.encodeFull(url),
          headers: {"Content-Type": "application/json", "Authorization": "Basic "+token}).timeout(
          const Duration(seconds: 10));

      if (data.statusCode == 200) {
        setState(() {
          _isLoading6 = false;
          try {
            List temp = json.decode(data.body);
            if (temp.length > 0)
              _mydockpick.addAll(json.decode(data.body));
            else
              print("No data");

            if (temp.length > 0)
              pageNumber6++;

          } catch(e){
            print(e.toString());
          }
        });


        print("Data="+_mydockpick.length.toString());

        loadMore22();

      }else
      {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setBool("isLoggedIn", false);
        prefs.setString("username", null);
        prefs.setString("mobile", null);
        Navigator.of(context).pop();
        Navigator.of(context).pushReplacementNamed("/login");
      }

    }catch(e){print(e.toString());}
  }

  void _showDialog(String title,String msg) {
    // flutter defined function
    showDialog(

      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(title),
          content: new Text(msg),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();

              },
            ),
          ],
        );
      },
    );
  }


}