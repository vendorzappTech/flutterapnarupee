import 'dart:convert';

import 'package:apanarupee/model/ApiInterface.dart';
import 'package:apanarupee/model/FABBottomAppBarItem.dart';
import 'package:apanarupee/model/fab_with_icons.dart';
import 'package:apanarupee/model/layout.dart';
import 'package:apanarupee/screens/AllLead.dart';
import 'package:apanarupee/screens/ApplicationFill.dart';
import 'package:apanarupee/screens/CreateLead.dart';
import 'package:apanarupee/screens/Demo.dart';
import 'package:apanarupee/screens/LeadInfo.dart';
import 'package:apanarupee/screens/OTPVerification.dart';
import 'package:apanarupee/screens/Profile.dart';
import 'package:apanarupee/utils/Helper.dart';
import 'package:apanarupee/utils/HexColor.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'EarningDetail.dart';
import 'Meetings.dart';
import 'Notifications.dart';
import 'TopPerformers.dart';
import 'Webview.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

class Dashboard extends StatefulWidget  {

  Dashboard({Key key, this.title}) : super(key: key);

  final String title;
  @override
  DashboardState createState() => DashboardState();
}

class DashboardState extends State<Dashboard> with TickerProviderStateMixin implements ApiInterface {

  String _lastSelected = '';
  int currentTab = 0;
  static const int GET_PROFILE_DETAILS_RESULT_CODE = 1,GET_BANNER_RESULT_CODE = 2, GET_OVERVIEW_RESULT_CODE = 3,GET_EARNINGS_RESULT_CODE =4,GET_PRODUCTS_RESULT_CODE=5,ERROR_CODE =-1;
  String _name = "",_email = "",_photo = "",_empId = "";
  List topList = new List();
  List earnList = new List();
  List productList = new List();
  bool _isLoading = true;
  String title = "Dashboard",_currentMonthId="All";

  String totalInProgress = "0",totalLogIn = "0",totalDockPick = "0", totalApproved = "0", totalDisbursed = "0",totalCanceled = "0", toataEarning = "0",_totalEarnAmount = "0";

  final scaffoldKey = new GlobalKey<ScaffoldState>();


  final dateFormat1 = DateFormat("yyyy-MM-ddThh:mm:ss");
  final dateFormat2 = DateFormat("dd-MMM-yyyy");

  bool showEarningMonth = false;

  List<String> bannerList = new List<String>();

  @override
  void initState() {
    // TODO: implement initState
    //  call get method to get profile details
    Helper.callAuthGetApi(Uri.encodeFull( Helper.GET_PROFILE_DETAILS),
        json.encode({

        }), context, this, GET_PROFILE_DETAILS_RESULT_CODE);


    FirebaseMessaging _firebaseMessaging;

    _firebaseMessaging = FirebaseMessaging();

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print('on message $message');

        // show notification here
//        _showNotificationWithDefaultSound();
        var title = message['notification']['title'];
        var body = message['notification']['body'];
        // show notification here
        _showDialog(title,body);



      },
      onResume: (Map<String, dynamic> message) async {
        print('on resume $message');
      },
      onLaunch: (Map<String, dynamic> message) async {
        print('on launch $message');
      },

    );


    super.initState();
  }

  void changedMonthDropDownItem(String selectedMonth) {
    print("Selected gender $selectedMonth, we are going to refresh the UI");
    setState(() {
      _currentMonthId = selectedMonth;
      _isLoading = true;
      if(selectedMonth=="All"){
        String url = Helper.GET_EARNINGS_URL ;
        Helper.callAuthGetApi(Uri.encodeFull(url),
            json.encode({
            }), context, this, GET_EARNINGS_RESULT_CODE);
      }else {
        String url = Helper.GET_MONTH_EARNINGS_URL + "?Month=" + selectedMonth;
        Helper.callAuthGetApi(Uri.encodeFull(url),
            json.encode({
            }), context, this, GET_EARNINGS_RESULT_CODE);
      }
    });

  }



  _loadData()async{

    // call overview method webservice
    String url = Helper.GET_OVERVIEW_URL+"?RequestType&ParamType";
    Helper.callAuthGetApi(Uri.encodeFull(url ),
        json.encode({

        }), context, this, GET_OVERVIEW_RESULT_CODE);

    setState(() {
      currentTab = 0;
      title = "Dashboard";

    });

  }



  void _selectedTab(int index) {


    print("selectedd index = " +index.toString());
    setState(() {

      _lastSelected = '';

    switch(index)
    {
      case 0:

        title = "Dashboard";
        _loadData();
        break;
      case 1 :
        title = "All Leads";
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) =>   AllLead(index:0)),
        ).then((value) {
          _loadData();
        });

        //Navigator.pushNamed(context, '/allleads');
        break;
      case 2:
        title = "Earnings";
        _isLoading = true;
        showEarningMonth = false;
        _currentMonthId = "All";
        String url = Helper.GET_EARNINGS_URL;
        Helper.callAuthGetApi(Uri.encodeFull(url ),
            json.encode({

            }), context, this, GET_EARNINGS_RESULT_CODE);


        break;
      case 3:
        title = "Products";
        _isLoading = true;

        String url = Helper.GET_PRODUCTS_URL;
        Helper.callGETApi(Uri.encodeFull(url ),
            json.encode({

            }), context, this, GET_PRODUCTS_RESULT_CODE);

        break;

    }
    });
  }

  void _selectedFab(int index) {
    setState(() {
      _lastSelected = '';
    });
  }

 Future<bool> _onBackPressed()
  {
    if(currentTab == 0)
      {
        SystemNavigator.pop();
        SystemNavigator.pop();
      }else
        {
          setState(() {
            currentTab = 0;
            title = "Dashboard";
          });
        }

  }


//  @override
//  void didChangeAppLifecycleState(AppLifecycleState state) {
//
//    print("State = "+state.index.toString());
//    setState(() {
//      switch(state.index){
//        case 0:// resumed
//          setState(() {
//            currentTab = 0;
//            _selectedTab(0);
//          });
//
//          break;
//        case 1:// inactive
//
//          break;
//        case 2:// paused
//
//          break;
//      }
//
//    });
//  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance = ScreenUtil.getInstance()..init(context);
    final logo = Hero(
        tag: 'hero',
        child:
        new Container(

            height:
            (MediaQuery.of(context).size.height >800)?
            ((currentTab == 0)?ScreenUtil.getInstance().setHeight(700):((currentTab == 2)?ScreenUtil.getInstance().setHeight(700):MediaQuery.of(context).size.height)):
            ((currentTab == 0)?ScreenUtil.getInstance().setHeight(800):((currentTab == 2)?ScreenUtil.getInstance().setHeight(800):MediaQuery.of(context).size.height)),
//          width: 600.0,
            decoration: new BoxDecoration(
              image:(currentTab == 0)? new DecorationImage(image: new AssetImage("assets/dashboardbg.png"), fit: BoxFit.fill,
              ):new DecorationImage(image: new AssetImage("assets/earndashboard.png"), fit: BoxFit.fill,
              )
            ),

            child:
            new Column(
              children: <Widget>[
                new Container(
                  height: ScreenUtil.getInstance().setHeight(180),
                  child:
                  AppBar
                    (
                      elevation: 0.0,
                      backgroundColor: Colors.transparent,
                      iconTheme: IconThemeData(
                          color: Colors.white),title:Text( title,style: TextStyle(fontSize: ScreenUtil.getInstance().setSp(60)),)),

                ),
             new SingleChildScrollView(
                 child:
                 new Padding(padding: EdgeInsets.only(left: 14,right: 14,top: 5,bottom: 0),
                 child:
                 (currentTab == 0)?
                 Center(
                   child:

                   new Column(
                       children: <Widget>[
                   new Center( child: new Text("TOTAL EARNING",style: TextStyle(color: Colors.white,fontSize: ScreenUtil.getInstance().setSp(40)),),),
                   new Center( child:

                   new Row(
                     crossAxisAlignment: CrossAxisAlignment.center,
                     mainAxisAlignment: MainAxisAlignment.center,
                     children: <Widget>[
                     new Container(
                       height: ScreenUtil.getInstance().setHeight(40),
                     child:
                     new Image.asset("assets/rupee.png"),padding: EdgeInsets.all(0),) ,
                     new Padding(padding: EdgeInsets.all(5),
                     child:
                     new Center(child:new Text(toataEarning,style: TextStyle(color: Colors.white,fontSize: ScreenUtil.getInstance().setSp(80)),)))
                     ],
                   ),),

                   new Container(
                    // height: ScreenUtil.getInstance().setHeight(270),
                     width: ScreenUtil.getInstance().setWidth(1200),
                     decoration:new BoxDecoration(
                       image: new DecorationImage(image: new AssetImage("assets/overviewbg.png"), fit: BoxFit.fill,
                       ),
                     ),
                     child:
                         new Padding(padding: EdgeInsets.all(10),
                     child:
                     new Column(
                       mainAxisAlignment: MainAxisAlignment.start,
                       crossAxisAlignment: CrossAxisAlignment.start,
                       children: <Widget>[

                         new Padding(padding: EdgeInsets.only(top: 10,left: 5,bottom: 5),
                         child:
                             new Text("Performance Overview",style: TextStyle(color:Colors.white,fontWeight: FontWeight.bold ,fontSize:ScreenUtil.getInstance().setSp(50) ),),
                         ),
                         new Container(
                           width: ScreenUtil.getInstance().setWidth(900),
                           height: ScreenUtil.getInstance().setHeight(200),
                           child:
                         SingleChildScrollView(
                             scrollDirection: Axis.horizontal,
                             child:
                         new Row(

                           children: <Widget>[
                             new GestureDetector(
                               onTap: (){
                                 Navigator.push(
                                   context,
                                   MaterialPageRoute(builder: (context) =>   AllLead(index: 0,)),
                                 ).then((value) {
                                   _loadData();
                                 });

                               },child:

                               new Column(
                                 children: <Widget>[
                                 Expanded(
                                 flex:1,
                                 child:
                                 new Padding(padding: EdgeInsets.all(5),
                                  child:
                                   new Text(totalInProgress,style: TextStyle(color: Colors.white,fontSize: ScreenUtil.getInstance().setSp(50)),),
                                 )),
                               Expanded(
                                 flex:1,
                                 child:

                                 new Padding(padding: EdgeInsets.only(top:5,left: 10,right: 10,bottom: 10),
                                   child:
                                   new Text("InProgress",style: TextStyle(color: Colors.white,),),
                                 ))
                                ],
                               ),),
                             new GestureDetector(
                               onTap: (){
                                 Navigator.push(
                                   context,
                                   MaterialPageRoute(builder: (context) =>   AllLead(index: 1,)),
                                 ).then((value) {
                                   _loadData();
                                 });

                               },child:

                             new Column(
                                 children: <Widget>[
                               Expanded(
                               flex:1,
                               child:

                               new Padding(padding: EdgeInsets.all(5),
                                     child:
                                     new Text(totalDockPick,style: TextStyle(color: Colors.white,fontSize: ScreenUtil.getInstance().setSp(50)),),
                                   )),
                                 Expanded(
                                   flex:1,
                                   child:

                                   new Padding(padding: EdgeInsets.only(top:5,left: 10,right: 10,bottom: 10),
                                     child:
                                     new Text("Dock Pick",style: TextStyle(color: Colors.white,),),
                                   ))
                                 ],
                               )),
                             new GestureDetector(
                               onTap: (){
                                 Navigator.push(
                                   context,
                                   MaterialPageRoute(builder: (context) =>   AllLead(index: 2,)),
                                 ).then((value) {
                                   _loadData();
                                 });

                               },child:

                             new Column(
                                 children: <Widget>[
                               Expanded(
                               flex:1,
                               child:

                               new Padding(padding: EdgeInsets.all(5),
                                     child:
                                     new Text(totalLogIn,style: TextStyle(color: Colors.white,fontSize: ScreenUtil.getInstance().setSp(50)),),
                                   )),
                                 Expanded(
                                   flex:1,
                                   child:

                                   new Padding(padding: EdgeInsets.only(top:5,left: 10,right: 10,bottom: 10),
                                     child:
                                     new Text("Login",style: TextStyle(color: Colors.white),),
                                   ))
                                 ],
                               )),

                             new GestureDetector(
                               onTap: (){
                                 Navigator.push(
                                   context,
                                   MaterialPageRoute(builder: (context) =>   AllLead(index: 3,)),
                                 ).then((value) {
                                   _loadData();
                                 });

                               },child:

                             new Column(
                                 children: <Widget>[
                               Expanded(
                               flex:1,
                               child:

                               new Padding(padding: EdgeInsets.all(5),
                                     child:
                                     new Text(totalApproved,style: TextStyle(color: Colors.white,fontSize:ScreenUtil.getInstance().setSp(50)),),
                                   )),
                               Expanded(
                                 flex:1,
                                 child:

                                 new Padding(padding: EdgeInsets.only(top:5,left: 10,right: 10,bottom: 10),
                                     child:
                                     new Text("Approved",style: TextStyle(color: Colors.white),),
                                   ))
                                 ],
                               ),),
                             new GestureDetector(
                               onTap: (){
                                 Navigator.push(
                                   context,
                                   MaterialPageRoute(builder: (context) =>   AllLead(index: 4,)),
                                 ).then((value) {
                                   _loadData();
                                 });

                               },child:

                             new Column(
                                 children: <Widget>[
                               Expanded(
                               flex:1,
                               child:

                               new Padding(padding: EdgeInsets.all(5),
                                     child:
                                     new Text(totalDisbursed,style: TextStyle(color: Colors.white),),
                                   )),
                                 Expanded(
                                   flex:1,
                                   child:

                                   new Padding(padding: EdgeInsets.only(top:5,left: 10,right: 10,bottom: 10),
                                     child:
                                     new Text("Disbursed",style: TextStyle(color: Colors.white),),
                                   ))
                                 ],
                             )),
                             new GestureDetector(
                               onTap: (){
                                 Navigator.push(
                                   context,
                                   MaterialPageRoute(builder: (context) =>   AllLead(index: 5,)),
                                 ).then((value) {
                                   _loadData();
                                 });

                               },child:

                             new Column(
                                 children: <Widget>[
                               Expanded(
                               flex:1,
                               child:

                               new Padding(padding: EdgeInsets.all(5),
                                     child:
                                     new Text(totalCanceled,style: TextStyle(color: Colors.white),),
                                   )),
                                 Expanded(
                                   flex:1,
                                   child:

                                   new Padding(padding: EdgeInsets.only(top:5,left: 10,right: 10,bottom: 10),
                                     child:
                                     new Text("Cancelled",style: TextStyle(color: Colors.white),),
                                   ))
                                 ],
                               )),

                           ],
                         )))

                       ],
                     )
                         )

                   )
                 ])

                 ): SizedBox(width: 0,height: 0,),

                 ) ),

            // earning design
            (currentTab == 2)?new Container(
                margin: EdgeInsets.only(left: 10,right: 10,top: 24),
             //   height: ScreenUtil.getInstance().setHeight(400),
                width: ScreenUtil.getInstance().setHeight(1200),
//                decoration:new BoxDecoration(
//                 // image: new DecorationImage(image: new AssetImage("assets/overviewbg.png"), fit: BoxFit.fill,
//                  color: Colors.white,
//                  //borderRadius: BorderRadius.circular(15),
//
//
//                  ),

                child:
                new Padding(padding: EdgeInsets.all(0),
                    child:
                    new Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0),
                      ),
                      child: new Row(
                        children: <Widget>[
                          Expanded
                          (
                            flex: 2,
                            child:
                              Padding(padding: EdgeInsets.all(10),
                              child:
                              Image(image: AssetImage("assets/pocket.png")))

                          ),
                          Expanded
                            (
                              flex: 7,
                              child:
                              new Padding(padding: EdgeInsets.only(left: 10,right: 10,top: 5,bottom: 5),
                              child:
                              new Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  (showEarningMonth)?
                                  new Padding(padding: EdgeInsets.all(0),
                                      child:
                                      new FormField(
                                        builder: (FormFieldState state) {
                                          return InputDecorator(
                                            decoration:  InputDecoration(
//                  border: const OutlineInputBorder(),
                                              labelStyle: TextStyle(fontWeight:FontWeight.bold,fontSize:ScreenUtil.getInstance().setSp(50),color: Color(HexColor.getAppOrangeColor())),
                                              labelText: "Month",
                                              hintStyle: TextStyle(color:Colors.grey),
                                              contentPadding: const EdgeInsets.symmetric(vertical: 0.0,horizontal: 2.0),
                                            ),
                                            child: new DropdownButtonHideUnderline(
                                                child:
                                                new DropdownButton(
                                                  value: _currentMonthId,
                                                  items: <String>['All','January', 'February','March','April','May','June','July','August','September','October','November','December'].map((String value) {
                                                    return new DropdownMenuItem<String>(
                                                      value: value,
                                                      child: new Text(value),
                                                    );
                                                  }).toList(),
                                                  onChanged: changedMonthDropDownItem,

                                                )

                                            ),
                                          );
                                        },
                                      )):new SizedBox(height: 0,width: 0,),
                                  new Padding(padding: EdgeInsets.only(top: 10),
                                  child:
                                  new Text("Total Earning Amount" , style: TextStyle(color: Color(HexColor.getAppOrangeColor(),),fontWeight: FontWeight.bold,fontSize:ScreenUtil.getInstance().setSp(40) ),)),
                                  new Row(
                                    children: <Widget>[
                                      new Container(
                                        height: ScreenUtil.getInstance().setHeight(60),
                                        child:
                                        new Image.asset("assets/brupee.png"),padding: EdgeInsets.all(0),) ,

                                      new Padding(padding: EdgeInsets.all(5),
                                    child:
                                    new Center(child:new Text(_totalEarnAmount,style: TextStyle(color: Colors.black,fontSize: ScreenUtil.getInstance().setSp(80)),)))
                                        ],
                                  ),
//                                  new Text("* on successful disbursal of loans / set up of card than 5% loan amount in your account ",style: TextStyle(color: Colors.black,fontSize:ScreenUtil.getInstance().setSp(30) ),)

                                ],
                              ))
                          ),
                        ],
                      ),
                    )

                )

            ) :SizedBox(height: 0,width: 0,),
                (currentTab == 3)?
                    new Container(
                    height: MediaQuery.of(context).size.height - 150,
                      decoration: new BoxDecoration(
                        color:Colors.white,
                        borderRadius: new BorderRadius.circular(25.0),
                      ),
                        padding:EdgeInsets.only(top: 5) ,

                      child:(!_isLoading)?
                      new ListView.builder
                        (

                          //physics: NeverScrollableScrollPhysics(),
                          //  controller: _scrollController,
                          itemCount: productList.length,
                          itemBuilder: (BuildContext ctxt, int index) {
                            return
                              new Padding(padding: EdgeInsets.all(5),
                              child:
                              new Card(
                                  child:
                              new Row(
                              children: <Widget>[
                                Expanded(
                                    flex: 7,
                                    child:
                                    new Row(
                                      mainAxisAlignment:MainAxisAlignment.start ,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[


                                        new Padding(padding: EdgeInsets.all(10),
                                            child:
                                            new Column(
                                              mainAxisAlignment: MainAxisAlignment.start,
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: <Widget>[
                                            new Container(
                                                width: ScreenUtil.getInstance().setWidth(600),
                                               child:
                                               SingleChildScrollView(
                                                 child:
                                                new Text(productList[index]["ProductName"].toString(),style: TextStyle(color: Color(HexColor.getAppOrangeColor()),fontSize: ScreenUtil.getInstance().setSp(50),fontWeight: FontWeight.bold)),
                                               )),
                                                new Container(
                                                    width: ScreenUtil.getInstance().setWidth(600),
                                                  child:
                                                  SingleChildScrollView(
                                                  child:
                                                new Text(productList[index]["ProdDescription"].toString(),style: TextStyle(color: Colors.black,fontSize:ScreenUtil.getInstance().setSp(40) ),maxLines: 4,     textAlign: TextAlign.justify,
                                                  overflow: TextOverflow.ellipsis,
                                                )
                                                ),
                                                )
                                              ],
                                            )
                                        ),
                                      ],
                                    )
                                ),
                                Expanded(
                                    flex: 3,
                                    child:
                                    new Row(
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: <Widget>[
                                        Align(
                                          alignment: Alignment.center,
                                          child:
                                        new Padding(padding: EdgeInsets.only(left: 10,right: 10,top:5,bottom: 5),
                                          child:
//
                                          new Image(image: NetworkImage(productList[index]["ProductPic"].toString()),width: 70,height: 70,),

                                        ),),

                                      ],
                                    )

                                )
                              ],
                            )));


                          }

                      ):Align(
                    alignment: Alignment.center,
                    child: CircularProgressIndicator()),

                    )
                    :SizedBox(height: 0,width: 0,),

              ],
            )


        )
    );


    return
      new WillPopScope(
        onWillPop: _onBackPressed,
        child:
      Scaffold(
      key : scaffoldKey,

//      appBar: AppBar(
//        title: Text(widget.title),
//        backgroundColor: HexColor("#2b2ca3"),
//      ),
      drawer: _buildDrawer(context),
      body:
      (currentTab == 0)?(
      (!_isLoading) ?
       new Column(
         mainAxisAlignment: MainAxisAlignment.start,
         crossAxisAlignment: CrossAxisAlignment.start,
         children: <Widget>[
           logo,


           (bannerList.length > 0) ?
           new Padding(padding: EdgeInsets.only(top: 10,bottom: 10),
           child:
           new CarouselSlider(
               items: bannerList.map((url) {
                 return new Builder(
                   builder: (BuildContext context) {
                     return new

                     Container(
                       margin: new EdgeInsets.symmetric(horizontal: 5.0),
                       decoration: new BoxDecoration(
                           borderRadius: new BorderRadius.all(new Radius.circular(5.0)),
                           image:
                           new DecorationImage(
                               image: new NetworkImage(url),
                               fit: BoxFit.fill
                           )
                       ),

                     );

                   },
                 );
               }).toList(),
//               height: 400.0,
               height:  (MediaQuery.of(context).size.height > 800)?
            MediaQuery.of(context).size.height - ScreenUtil.getInstance().setHeight(800)-ScreenUtil.getInstance().setHeight(200):
            MediaQuery.of(context).size.height - ScreenUtil.getInstance().setHeight(900)-ScreenUtil.getInstance().setHeight(250),
               autoPlay: true
           ))

               :Align(
               alignment: Alignment.center,
               child: CircularProgressIndicator()),





//           new Row(
//              children: <Widget>[
//                Expanded(
//                  flex: 5,
//                  child:
//                    new Column(
//                      mainAxisAlignment: MainAxisAlignment.start,
//                      crossAxisAlignment: CrossAxisAlignment.start,
//                      children: <Widget>[
//                        new Padding(padding: EdgeInsets.only(left: 10),
//                          child: new Text("Top Performers",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: ScreenUtil.getInstance().setSp(50)),),
//                        ),
//                      ],
//                    )
//                ),
//                Expanded(
//                    flex: 5,
//                    child:
//                    new Column(
//                      mainAxisAlignment: MainAxisAlignment.end,
//                      crossAxisAlignment: CrossAxisAlignment.end,
//                      children: <Widget>[
//                        new Padding(padding: EdgeInsets.only(right: 5),
//                          child:
//                          new FlatButton(onPressed: (){
//                            Navigator.push(
//                              context,
//                              MaterialPageRoute(builder: (context) =>   TopPerformers()),
//                            ).then((value) {
//                              _loadData();
//                            });
//                          }, child:
//                          new Text("View All",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: ScreenUtil.getInstance().setSp(50)),)),
//                        ),
//                      ],
//                    )
//                ),
//              ],
//
//           ),

//           new Container(
//             height:
//            (MediaQuery.of(context).size.height > 800)?
//            MediaQuery.of(context).size.height - ScreenUtil.getInstance().setHeight(800)-ScreenUtil.getInstance().setHeight(200):
//            MediaQuery.of(context).size.height - ScreenUtil.getInstance().setHeight(900)-ScreenUtil.getInstance().setHeight(250),
//             child:
//
//           SingleChildScrollView(
//
//
//             child:
//               new Column(
//                 mainAxisAlignment: MainAxisAlignment.start,
//                 crossAxisAlignment: CrossAxisAlignment.start,
//                 children: <Widget>[
//
//                   new Container(
//                     height: topList.length * 50.0,
//                     child:
//
//                   new ListView.builder
//                    (
//                     padding: EdgeInsets.only(top: 10),
//                     physics: NeverScrollableScrollPhysics(),
//                    //  controller: _scrollController,
//                      itemCount: topList.length,
//                      itemBuilder: (BuildContext ctxt, int index) {
//                        return
//
//                          new Row(
//
//                          children: <Widget>[
//                            Expanded(
//                                flex: 7,
//                                child:
//                                new Row(
//                                  mainAxisAlignment:MainAxisAlignment.start ,
//                                  crossAxisAlignment: CrossAxisAlignment.start,
//                                  children: <Widget>[
//
//
//                                    new Padding(padding: EdgeInsets.only(left: 10,right: 10,bottom: 10),
//                                        child:(topList[index]["PhotoPath"] != null || !(topList[index]["PhotoPath"].toString().compareTo("null")==0 ) || (topList[index]["PhotoPath"].toString().length > 4))?
//                                        new CircleAvatar(
//                                          radius: 20,
//                                          backgroundImage: NetworkImage(topList[index]["PhotoPath"].toString()),
//                                        ): new CircleAvatar(
//                                          radius: 20,
//                                          backgroundColor: Colors.white,
//                                          backgroundImage: AssetImage("assets/profile.png"),
//                                        )
//                                    ),
//                                    new Padding(padding: EdgeInsets.all(5),
//                                        child:
//                                        new Column(
//                                          mainAxisAlignment: MainAxisAlignment.start,
//                                          crossAxisAlignment: CrossAxisAlignment.start,
//                                          children: <Widget>[
//                                            new Text(topList[index]["Name"].toString(),style: TextStyle(color: Colors.black,fontSize: ScreenUtil.getInstance().setSp(50))),
//                                            new Text(topList[index]["City"].toString(),style: TextStyle(color: Colors.grey),),
//                                          ],
//                                        )
//                                    ),
//                                  ],
//                                )
//                            ),
//                            Expanded(
//                                flex: 3,
//                                child:
//                                new Row(
//                                  crossAxisAlignment: CrossAxisAlignment.end,
//                                  mainAxisAlignment: MainAxisAlignment.end,
//                                  children: <Widget>[
//                                    new Padding(padding: EdgeInsets.only(right: 10,top:5,bottom: 5),
//                                        child:
//                                        new Text(topList[index]["TotalDisbursals"].toString()+" Disbursals",style: TextStyle(color: Color(HexColor.getAppBlueColor())),))
//                                  ],
//                                )
//
//                            )
//                          ],
//                        );
//
//
//                      }
//
//
//                  )),
//                 ],
//               )
//           ))
         ],
       )

        :Align(
          alignment: Alignment.center,
          child: CircularProgressIndicator())):
      // earning design page
      ((currentTab == 2)?
        (! _isLoading)?
        new ListView(
          padding: EdgeInsets.all(0),
          physics: NeverScrollableScrollPhysics(),
          children: <Widget>[
            logo,

            //SizedBox(height: 10,),
            (earnList.length > 0)?
            new Row(
              children: <Widget>[
                Expanded(
                  flex: 5,
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      new Padding(padding: EdgeInsets.only(left: 10),
                        child: new Text("Lead Id",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: ScreenUtil.getInstance().setSp(50)),),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  flex: 5,
                  child:new Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      new Padding(padding: EdgeInsets.only(right: 10),
                        child: new Text("Earning",style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold,fontSize: ScreenUtil.getInstance().setSp(50)),),
                      ),
                    ],
                  ),
                ),
              ],
            ):
            Align(
              alignment: Alignment.center,
              child:
              new Image.asset("assets/no_data.png",height: 150,width: 150)),

            new Container(
                padding:EdgeInsets.all(0) ,
                height:  (MediaQuery.of(context).size.height > 800)?
                MediaQuery.of(context).size.height - ScreenUtil.getInstance().setHeight(700)-ScreenUtil.getInstance().setHeight(200):
                MediaQuery.of(context).size.height - ScreenUtil.getInstance().setHeight(800)-ScreenUtil.getInstance().setHeight(250),

                child:

                new ListView.builder
                  (
                    padding: EdgeInsets.all(0),
                    //physics: NeverScrollableScrollPhysics(),
                    //  controller: _scrollController,
                    itemCount: earnList.length ,
                    itemBuilder: (BuildContext ctxt, int index) {
                      return
                        new GestureDetector(
                          onTap: (){
                            // call listview details page
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) =>   EarningDetail(leadId:earnList[index]["LeadId"].toString() ,)),
                            ).then((value) {
                              _loadData();
                              setState(() {
                                title = "Earnings";
                                currentTab = 2;
                                _isLoading = true;
                                showEarningMonth = false;
                                _currentMonthId = "All";
                                String url = Helper.GET_EARNINGS_URL;
                                Helper.callAuthGetApi(Uri.encodeFull(url ),
                                    json.encode({

                                    }), context, this, GET_EARNINGS_RESULT_CODE);


                              });

                            });


                          },
                      child:
                        new Card(
                        child:
                        new Row(
                        children: <Widget>[
                          Expanded(
                              flex: 7,
                              child:

                              new Row(
                                mainAxisAlignment:MainAxisAlignment.start ,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[

                                 new Column(
                                   mainAxisAlignment:MainAxisAlignment.center ,
                                   crossAxisAlignment: CrossAxisAlignment.center,

                                   children: <Widget>[
                                     new Padding(padding: EdgeInsets.only(left: 10,right: 10,top: 10,bottom: 10),
                                       child:
//                                      (
//                                          topList[index]["PhotoPath"] != null || !(topList[index]["PhotoPath"].toString().compareTo("null")==0 ) || (topList[index]["PhotoPath"].toString().length > 4))?
//                                      new CircleAvatar(
//                                        radius: 20,
//                                        backgroundImage: NetworkImage(topList[index]["PhotoPath"].toString()),
//                                      ):
                                       new Image(image: AssetImage("assets/earningrupee.png"),width: 40),

                                     ),

                                   ],
                                 ),
                                  new Padding(padding: EdgeInsets.all(5),
                                      child:
                                      new Column(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: <Widget>[
                                      new Container(
                                      width: ScreenUtil.getInstance().setWidth(400),
//                                        color: Colors.red,
                                        child:
                                          new SingleChildScrollView(
                                          child:
                                          new Text(earnList[index]["Name"].toString(),style: TextStyle(color: Colors.black,fontSize: ScreenUtil.getInstance().setSp(50))),
                                          ),

                                       ),
                                          new Row(
                                            children: <Widget>[
                                              new Text("Id:",style: TextStyle(color: Colors.grey,fontSize:ScreenUtil.getInstance().setSp(40) ),),
                                              new Text(" "+earnList[index]["LeadId"].toString(),style: TextStyle(color: Colors.black,fontSize:ScreenUtil.getInstance().setSp(40) ),),

                                            ],
                                          ),
                                          new Row(
                                            children: <Widget>[

                                              new Text("Date:",style: TextStyle(color: Colors.grey,fontSize:ScreenUtil.getInstance().setSp(40) ),),
                                                        new Text(" "+dateFormat2.format(dateFormat1.parse(earnList[index]["DisbursDate"].toString())),style: TextStyle(color: Colors.black,fontSize:ScreenUtil.getInstance().setSp(40) ),),
                                            ],
                                          )
                                        ],
                                      )
                                  ),
                                ],
                              )
                          ),
                          Expanded(
                              flex: 4,
                              child:
                              new Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[


                                new Padding(padding: EdgeInsets.only(right: 5,bottom: 10),
                                child:
                                  new Container(
                                    height: ScreenUtil.getInstance().setHeight(30),
                                    child:
                                    new Image.asset("assets/brupee.png"),) ),

                                  new Padding(padding: EdgeInsets.only(right: 10,bottom: 5),
                                      child:
                                      new Text(earnList[index]["EarningAmount"].toString(),style: TextStyle(fontSize:ScreenUtil.getInstance().setSp(45),fontWeight:FontWeight.bold,color: Color(HexColor.getAppBlueColor())),))
                                ],
                              )

                          )
                        ],
                      )));


                    }

                )),


          ],
        )
            :Align(
            alignment: Alignment.center,
            child: CircularProgressIndicator())
          :
      (currentTab == 3)?(
          new ListView(
            physics: (currentTab == 3)? NeverScrollableScrollPhysics():ScrollPhysics(),
            padding: EdgeInsets.all(0),
            children: <Widget>[
              logo,


            ],
          )

      ):
      new SizedBox(
           height: 0,
          width: 0,
          )),




//      bottomNavigationBar: FABBottomAppBar(
//        centerItemText: '',
//        color: Colors.grey,
//        selectedColor: HexColor("#fb6616"),
//        notchedShape: CircularNotchedRectangle(),
//
//        onTabSelected: _selectedTab,
//        items: [
//          FABBottomAppBarItem( img:
//              "assets/home1.png", text: 'Dashboard'),
//          FABBottomAppBarItem(img: "assets/list1.png", text: 'Leads'),
//          FABBottomAppBarItem(img: "assets/earning1.png", text: 'Earnings'),
//          FABBottomAppBarItem(img: "assets/product1.png", text: 'Product'),
//        ],
//      ),
    bottomNavigationBar: BottomNavigationBar(

      type: BottomNavigationBarType.fixed,
      currentIndex: currentTab,
      onTap: (int index) {
        setState(() {
          currentTab = index;
          _selectedTab(index);
        });
      },


      items: <BottomNavigationBarItem>[
        BottomNavigationBarItem(
            icon: ImageIcon(
              AssetImage("assets/home1.png"),
              color: currentTab == 0
                  ? Color(HexColor.getAppOrangeColor())
                  : Colors.black,
            ),
            title: Text('DASHBOARD',
              style: TextStyle(
                  fontSize: 10.0,
                  color: currentTab == 0
                      ? Color(HexColor.getAppOrangeColor())
                      : Colors.black),
            )
        ),
        BottomNavigationBarItem(

            icon:
             new Padding(padding: EdgeInsets.only(right: 30),child:
            ImageIcon(
              AssetImage("assets/list1.png"),
              color: currentTab == 1
                  ? Color(HexColor.getAppOrangeColor())
                  : Colors.black,
            )),
            title:
            new Padding(padding: EdgeInsets.only(right: 30),child:
             Text('VIEW LEADS',
              style: TextStyle(
                  fontSize: 10.0,
                  color: currentTab == 1
                      ? Color(HexColor.getAppOrangeColor())
                      : Colors.black),))
        ),

        BottomNavigationBarItem(

            icon:
            new Padding(padding: EdgeInsets.only(left: 30),child:
            ImageIcon(
              AssetImage("assets/earning1.png"),
              color: currentTab == 2
                  ? Color(HexColor.getAppOrangeColor())
                  : Colors.black,
            )),
            title:
            new Padding(padding: EdgeInsets.only(left: 30),child:
            Text('EARNINGS',
              style: TextStyle(
                  fontSize: 10.0,
                  color: currentTab == 2
                      ? Color(HexColor.getAppOrangeColor())
                      : Colors.black),))
        ),
        BottomNavigationBarItem(
            icon: ImageIcon(
              AssetImage("assets/product1.png"),
              color: currentTab == 3
                  ? Color(HexColor.getAppOrangeColor())
                  : Colors.black,
            ),
            title: Text('PRODUCTS',
              style: TextStyle(
                  fontSize: 10.0,
                  color: currentTab == 3
                      ?Color(HexColor.getAppOrangeColor())
                      : Colors.black),)
        )
      ],
    ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: _buildFab(
          context), // This trailing comma makes auto-formatting nicer for build methods.
    ));
  }

  Widget _buildFab(BuildContext context) {
    final icons = [ Icons.sms, Icons.mail, Icons.phone ];
    return
      Padding(
        padding: EdgeInsets.only(bottom: 5),
      child:
      FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) =>   CreateLead()),
          ).then((value) {
            _loadData();
          });

        },
        tooltip: 'Increment',
        child: Icon(Icons.add),
        elevation: 2.0,
        backgroundColor: HexColor("#fb6616"),


      ));

  }

  Widget _buildDrawer(BuildContext context)
  {
    return new Drawer(

  child: new ListView(
    padding: EdgeInsets.only(top: 0),
  children: <Widget>[
  new UserAccountsDrawerHeader(
    decoration: BoxDecoration( color:Color(HexColor.getAppBlueColor())),

  accountEmail:new Row(
  children: <Widget>[
  Expanded(
  flex: 8,
  child:new Column(
  crossAxisAlignment: CrossAxisAlignment.start,
  mainAxisAlignment: MainAxisAlignment.start,
  children: <Widget>[
  new Text(_name),
  new Text(_email,style: TextStyle(fontSize: ScreenUtil.getInstance().setSp(35)),),
  new Padding(padding:EdgeInsets.all(1),

  child:
  new Container(
  padding: EdgeInsets.all(1),
  color: Colors.white,
    child:
  new Text(_empId ,style: TextStyle(color:HexColor("00008b"),backgroundColor: Colors.white),),))
  ],
  )
  ),
  Expanded(
  flex: 2,
  child: new Column(
  crossAxisAlignment: CrossAxisAlignment.center,
  mainAxisAlignment: MainAxisAlignment.center,
  children: <Widget>[
  IconButton(
  icon: Icon(Icons.edit,color: Colors.white,),
  tooltip: 'Edit profile',
  onPressed: () {
  setState(() {

  Navigator.push(
  context,
  MaterialPageRoute(builder: (context) =>   Profile()),
  ).then((value) {
  Helper.callAuthGetApi(Uri.encodeFull( Helper.GET_PROFILE_DETAILS),
  json.encode({

  }), context, this, GET_PROFILE_DETAILS_RESULT_CODE);

  });

  });
  },
  ),
  ],
  ),
  )
  ],
  ),

  currentAccountPicture: CircleAvatar(
    maxRadius: 100,
   minRadius: 100,
  backgroundColor:
  Theme.of(context).platform == TargetPlatform.iOS
  ? Colors.blue
      : Colors.white,
  backgroundImage:

  (_photo.toString().length > 0)?
    NetworkImage(
    _photo,
    ):
  AssetImage("assets/profile.png"),
  ),
  ),
  new ListTile(
  title: new Text("My Leads"),
  leading: new Image.asset("assets/mlist.png",height: 20,width: 20,),
  onTap: () {
  print("my Leads");
  title = "All Leads";
  Navigator.pop(context);
  Navigator.push(
    context,
    MaterialPageRoute(builder: (context) =>   AllLead(index:0)),
  ).then((value) {
    _loadData();
  });
 // Navigator.pop(context);
  },
  ),
  new ListTile(
  title: new Text("Notification"),
  leading: new Image.asset("assets/mnotification.png",height: 20,width: 20,),
  onTap: () {
  print("Notification");
  Navigator.pop(context);
  Navigator.push(
    context,
    MaterialPageRoute(builder: (context) =>   Notifications()),
  ).then((value) {
    _loadData();
  });

  },
  ),
  new ListTile(
  title: new Text("Loan Calculator"),
  leading: new Image.asset("assets/mcalculator.png",height: 20,width: 20,),
  onTap: () {
  print("Calculator");
  Navigator.pop(context);
  Navigator.push(
    context,
    MaterialPageRoute(builder: (context) =>   MyWebview(title: "Loan Calculator",url: "https://apnarupee.com/emicalcalator.html?mobile",)),
  );

  },
  ),

    new ListTile(
      title: new Text("Eligibility Calculator"),
      leading: new Image.asset("assets/mcalculator.png",height: 20,width: 20,),
      onTap: () {
        print("Calculator");
        Navigator.pop(context);
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) =>   MyWebview(title: "Eligibility Calculator",url: "http://apnarupee.in/webview/loanEligibiliyCalculator.html",)),
        );

      },
    ),
  new ListTile(
  title: new Text("Products"),
  leading: new Image.asset("assets/mgift.png",height: 20,width: 20,),
  onTap: () {
    Navigator.pop(context);
  print("Products");
  title = "Products";
  _isLoading = true;
  setState(() {
    currentTab = 3;
  });


  String url = Helper.GET_PRODUCTS_URL;
  Helper.callGETApi(Uri.encodeFull(url ),
      json.encode({

      }), context, this, GET_PRODUCTS_RESULT_CODE);


  },
  ),
  new ListTile(
  title: new Text("Meetings"),
  leading: new Image.asset("assets/meeting.png",height: 20,width: 20,),
  onTap: () {
    Navigator.pop(context);
  print("Meetings");
  Navigator.push(
    context,
    MaterialPageRoute(builder: (context) =>   Meetings(title:"Meetings")),
  );

  }),
  new ListTile(
  title: new Text("Earnings"),
  leading: new Image.asset("assets/mrupee.png",height: 20,width: 20,),
  onTap: () {
    Navigator.pop(context);
  print("Earnings");
  title = "Earnings";
  _isLoading = true;
  setState(() {
    currentTab = 2;
    showEarningMonth = true;
    List<String> months =<String>['All','January', 'February','March','April','May','June','July','August','September','October','November','December'];
    int current_month = new DateTime.now().month;

    print("Current month ="+current_month.toString());
      _currentMonthId = months[current_month];

      print("currentMonthId="+_currentMonthId);
    String url = Helper.GET_MONTH_EARNINGS_URL + "?Month=" + _currentMonthId;
    Helper.callAuthGetApi(Uri.encodeFull(url),
        json.encode({
        }), context, this, GET_EARNINGS_RESULT_CODE);

  });

  String url = Helper.GET_EARNINGS_URL;
  Helper.callAuthGetApi(Uri.encodeFull(url ),
      json.encode({

      }), context, this, GET_EARNINGS_RESULT_CODE);


  },
  ),
  new ListTile(
    title: new Text("Top Performance"),
    leading: new Image.asset("assets/top_performance.png",height: 20,width: 20,),
    onTap: () {
      Navigator.pop(context);
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) =>   TopPerformers()),
      ).then((value) {
        _loadData();
      });

    },
  ),

  Divider(),
  new ListTile(
  title: new Text("About Us"),
  leading: new Image.asset("assets/maboutus.png",height: 20,width: 20,),
  onTap: () {
    Navigator.pop(context);
  print("About Us");
  Navigator.push(
    context,
    MaterialPageRoute(builder: (context) =>   MyWebview(title: "About Us",url: "https://apnarupee.com/about.html?mobile",)),
  );

  },
  ),
  new ListTile(
  title: new Text("Contact Us"),
  leading: new Image.asset("assets/mphone.png",height: 20,width: 20,),
  onTap: () {
    Navigator.pop(context);
  print("Contact Us");
  Navigator.push(
    context,
    MaterialPageRoute(builder: (context) =>   MyWebview(title: "Contact Us",url: "https://apnarupee.com/contact-us.html",)),
  );

  },
  ),

  new ListTile(
  title: new Text("Log Out"),
  leading: new Image.asset("assets/mlogout.png",height: 20,width: 20,),
  onTap: () async {
  print("Log Out");

  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setBool("isLoggedIn", false);
  prefs.setString("username", null);
  prefs.setString("mobile", null);
  Navigator.of(context).pop();
  Navigator.of(context).pushReplacementNamed("/login");

  },
  ),

  ],
  ),);

  }

  void _showSnackBar(String text,Color color) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(backgroundColor:color ,content: new Text(text),duration: new Duration(milliseconds: 5000),));
  }


  void postExecution(var data ,int result_code)  {

    switch(result_code) {

      case GET_PROFILE_DETAILS_RESULT_CODE:

        try {

          if (data["responseCode"] == Helper.SUCCESS_STATUS_CODE) {
            print("got data");
           // _showSnackBar(data["messages"]["messageDescription"], Colors.green);

            print(data.toString());
            setState(() {
              _name = data["Name"].toString();
              _email = data ["EmailId"].toString();
              _empId = data ["EmpId"].toString();
              if(data ["Photo"].toString() != null)
              _photo = data ["Photo"].toString();
              _loadData();
            });


          } else {
            print("Error=" + data["messages"]["messageDescription"]);

            _showSnackBar(data["messages"]["messageDescription"], Colors.red);
          }
          if (data == null)
            print("Unable to get data");
        } catch (e) {
         // _showSnackBar("Some problem occurred", Colors.red);
          print(e);
        }

        break;

      case GET_OVERVIEW_RESULT_CODE:


        try{
        if (data["responseCode"] == Helper.SUCCESS_STATUS_CODE) {
          print("got data");
          // _showSnackBar(data["messages"]["messageDescription"], Colors.green);
          print(data.toString());
          setState(() {
            totalInProgress = data["InProgress"].toString();
            totalLogIn = data["Login"].toString();
            totalApproved = data["Aprroved"].toString();
            totalDisbursed = data["Disbursed"].toString();
            totalDockPick = data["DocPick"].toString();
            totalCanceled = data["Cancelled"].toString();
            toataEarning = data["TotalEarning"].toString();
          });

//          String url = Helper.GET_TOP_PERFORMER_URL+"?RequestType&ParamType";
//          Helper.callAuthGetApi(Uri.encodeFull(url ),
//              json.encode({
//
//              }), context, this, GET_TOP_PERFORMER_RESULT_CODE);

          String url = Helper.GET_BANNER_URL+"?RequestType&ParamType";
          Helper.callAuthGetApi(Uri.encodeFull(url ),
              json.encode({

              }), context, this, GET_BANNER_RESULT_CODE);


        } else {
          print("Error=" + data["messages"]["messageDescription"]);

          _showSnackBar(data["messages"]["messageDescription"], Colors.red);
        }
        if (data == null)
          print("Unable to get data");
        } catch (e) {
        //_showSnackBar("Some problem occurred", Colors.red);
        }


    break;

      case GET_BANNER_RESULT_CODE:

        try{

          setState(() {
            _isLoading = false;
            topList = data["Banners"];

            for(final item in topList)
            {
              bannerList.add(item["DashBanner"]);
            }

          });

        }catch(e){
          print(e.toString());
        }

        break;

      case GET_EARNINGS_RESULT_CODE:
        try{
          if (data["responseCode"] == Helper.SUCCESS_STATUS_CODE) {
            print("got data");
            // _showSnackBar(data["messages"]["messageDescription"], Colors.green);
            print(data.toString());
            setState(() {
              _isLoading = false;
               _totalEarnAmount = data["TotalEarnigAmount"].toString();
               earnList = data["EarningList"];
               print("Total earning ="+_totalEarnAmount);
               print("List size ="+earnList.length.toString());
            });


          } else {
            print("Error=" + data["messages"]["messageDescription"]);

            _showSnackBar(data["messages"]["messageDescription"], Colors.red);
          }
          if (data == null)
            print("Unable to get data");
        } catch (e) {
         // _showSnackBar("Some problem occurred", Colors.red);
        }

        break;

      case GET_PRODUCTS_RESULT_CODE:
        setState(() {
          _isLoading = false;
        });
        productList = data;
        print("product size="+productList.length.toString());
        break;

      case ERROR_CODE:
        _showSnackBar("Please check your internet connection ",Colors.red);
        setState(() {
          //_isLoading = false;

        });
        break;
    }
  }

  void _showDialog(String title,String msg) {
    // flutter defined function
    showDialog(

      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(title),
          content: new Text(msg),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

}