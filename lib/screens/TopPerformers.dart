import 'dart:convert';

import 'package:apanarupee/model/ApiInterface.dart';
import 'package:apanarupee/utils/Helper.dart';
import 'package:apanarupee/utils/HexColor.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:apanarupee/screens/AddMeetings.dart';
import 'package:apanarupee/screens/ApplicationFill.dart';
import 'package:apanarupee/screens/ChangeStatus.dart';
import 'package:apanarupee/screens/DocumentUpload.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'CreateNote.dart';

class TopPerformers extends StatefulWidget  {



  @override
  TopPerformersState createState() => TopPerformersState();
}

class TopPerformersState extends State<TopPerformers> implements ApiInterface {

  final formKey = new GlobalKey<FormState>();
  final scaffoldKey = new GlobalKey<ScaffoldState>();

  static const int GET_TOP_PERFORMER_RESULT_CODE = 1;
  bool _isLoading = false;
  List topList = new List();
  List monthList = new List();
  ScrollController _scrollController = new ScrollController();


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // call webservice to get top performers

    setState(() {
      _isLoading = true;
    });

    String url = Helper.GET_TOP_PERFORMER_BY_MONTH_URL+"?RequestType&ParamType";
    Helper.callAuthGetApi(Uri.encodeFull(url ),
        json.encode({

        }), context, this, GET_TOP_PERFORMER_RESULT_CODE);

    FirebaseMessaging _firebaseMessaging;

    _firebaseMessaging = FirebaseMessaging();

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print('on message $message');

        // show notification here
//        _showNotificationWithDefaultSound();
        var title = message['notification']['title'];
        var body = message['notification']['body'];
        // show notification here
        _showDialog(title,body);



      },
      onResume: (Map<String, dynamic> message) async {
        print('on resume $message');
      },
      onLaunch: (Map<String, dynamic> message) async {
        print('on launch $message');
      },

    );


  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance = ScreenUtil.getInstance()
      ..init(context);

    final logo = Hero(
        tag: 'hero',
        child:
        new Container(

            height: MediaQuery
                .of(context)
                .size
                .height,
//          width: 600.0,
            decoration: new BoxDecoration(
              image: new DecorationImage(
                image: new AssetImage("assets/earndashboard.png"),
                fit: BoxFit.fill,
              ),
              color: Colors.white
            ),

            child:
            new Column(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child:
                AppBar
                  (
                    elevation: 0.0,
                    backgroundColor: Colors.transparent,
                    iconTheme: IconThemeData(
                        color: Colors.white),
                    title: Text("Top Performers", style: TextStyle(
                        fontSize: ScreenUtil.getInstance().setSp(60)),)),
                ),
                Expanded(
                  flex: 9,
                child:
                new Padding(padding: EdgeInsets.only(top:5),
                child:
                (!_isLoading)?
                new Container(
                    padding: EdgeInsets.only(top:10),
                    width: MediaQuery
                        .of(context)
                        .size
                        .width,
                    height: MediaQuery
                        .of(context)
                        .size
                        .height-100,

                    decoration: new BoxDecoration(
                      color: Colors.white,
                      borderRadius: new BorderRadius.circular(25.0),
                    ),
                    child:
                    new Form(
                              key:formKey,
                             child:

                             SingleChildScrollView(
                                child:
                                new Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                    (!_isLoading)?
                                      new Padding(padding: EdgeInsets.only(top: 0,left: 10,right: 10,bottom: 10),
                                          child:
                                          (monthList != null || monthList.length > 0)?
                                          new Container(
//                                             color: Colors.green,
                                              height: MediaQuery
                                                  .of(context)
                                                  .size
                                                  .height-120,
                                              child:

                                              new ListView.builder
                                                (

                                          //        physics: NeverScrollableScrollPhysics(),
                                                  controller: _scrollController,
                                                  itemCount: monthList.length,
                                                  itemBuilder: (BuildContext ctxt, int index) {
                                                    return
                                                      new Padding(padding: EdgeInsets.only(bottom: 10),
                                                          child:
                                                               new Row(
                                                                children: <Widget>[
                                                                  Expanded(
                                                                    flex: 8,
                                                                    child: new Column(
                                                                      mainAxisAlignment: MainAxisAlignment.start,
                                                                      crossAxisAlignment: CrossAxisAlignment.start
                                                                      ,
                                                                      children: <Widget>[
                                                                        new Padding(padding: EdgeInsets.only(left: 5),
                                                                            child:
                                                                            Container(

                                                                                child:
                                                                                SingleChildScrollView(
                                                                                    child:
                                                                                    new Text(monthList[index]['MonthName'].toString(),style: TextStyle(color: Colors.black,fontWeight:FontWeight.bold,fontSize: 16),),

                                                                                ))),
                                                                                    Divider(),
    new Container(
    height: (getTopListSize(monthList[index]['MonthName'])*60).toDouble(),
    child:

    new ListView.builder
    (

    physics: NeverScrollableScrollPhysics(),
//    controller: _scrollController,
    itemCount: topList.length,
    itemBuilder: (BuildContext ctxt, int index1)
                                                    {
                                                      return (topList[index1]['MonthName'].toString() ==  topList[index]['MonthName'].toString())?
                                                      new Card(
                                                        child:
                                                        new Row(

                                                          children: <Widget>[
                                                            Expanded(
                                                                flex: 7,
                                                                child:
                                                                new Row(
                                                                  mainAxisAlignment:MainAxisAlignment.start ,
                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                                  children: <Widget>[


                                                                    new Padding(padding: EdgeInsets.only(left: 5,right: 5,bottom: 5,top: 5),
                                                                        child:(topList[index1]["PhotoPath"] != null || !(topList[index1]["PhotoPath"].toString().compareTo("null")==0 ) || (topList[index1]["PhotoPath"].toString().length > 4))?
                                                                        new CircleAvatar(
                                                                          radius: 20,
                                                                          backgroundImage: NetworkImage(topList[index1]["PhotoPath"].toString()),
                                                                        ): new CircleAvatar(
                                                                          radius: 20,
                                                                          backgroundColor: Colors.white,
                                                                          backgroundImage: AssetImage("assets/profile.png"),
                                                                        )
                                                                    ),
                                                                    new Padding(padding: EdgeInsets.all(5),
                                                                        child:
                                                                        new Column(
                                                                          mainAxisAlignment: MainAxisAlignment.start,
                                                                          crossAxisAlignment: CrossAxisAlignment.start,
                                                                          children: <Widget>[
                                                                            new Text(topList[index1]["Name"].toString(),style: TextStyle(color: Colors.black,fontSize: ScreenUtil.getInstance().setSp(50))),
                                                                            new Text(topList[index1]["City"].toString(),style: TextStyle(color: Colors.grey),),
                                                                          ],
                                                                        )
                                                                    ),
                                                                  ],
                                                                )
                                                            ),
                                                            Expanded(
                                                                flex: 3,
                                                                child:
                                                                new Row(
                                                                  crossAxisAlignment: CrossAxisAlignment.end,
                                                                  mainAxisAlignment: MainAxisAlignment.end,
                                                                  children: <Widget>[
                                                                    new Padding(padding: EdgeInsets.only(right: 5,top:5,bottom: 5),
                                                                        child:
                                                                        new Text(topList[index1]["TotalDisbursals"].toString()+" Disbursals",style: TextStyle(color: Color(HexColor.getAppBlueColor())),))
                                                                  ],
                                                                )

                                                            )
                                                          ],
                                                        )
                                                      ):SizedBox(height: 0,width: 0,);
                                                    }))

    ],
                                                                    ),
                                                                  ),

                                                                ],
                                                              ));
                                                    //new Text(_noteList[0]['Notes'].toString(),style: TextStyle(color: Colors.black),);
                                                  }
                                              )
                                          )
                                              :
                                          SizedBox(height: 0,width: 0,)
                                      )
                                    :Align(
                                                      alignment: Alignment.center,
                                                      child: CircularProgressIndicator()
                                                  ),

                                    ]
                                ),
                            ))):
                  new Container(
                      height: MediaQuery
                          .of(context)
                          .size
                          .height-120,
                      decoration: new BoxDecoration(
                        color: Colors.white,
                        borderRadius: new BorderRadius.circular(25.0),
                      ),
                  child:
                Align(
                    alignment: Alignment.center,
                    child: CircularProgressIndicator()
                )),

//                        )
//                    )

//               )
        ))

              ],
            )


        )
    );

    return new Scaffold(

        key: scaffoldKey,
        backgroundColor: Colors.white,

        body:
        new Form(

          child: SingleChildScrollView(
                child:logo,
//              padding: EdgeInsets.only(top: 0),
//
//              children: <Widget>[
//                logo,


//              ]
          ),

        )
    );
  }

  void _showSnackBar(String text,Color color) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(backgroundColor:color ,content: new Text(text),duration: new Duration(milliseconds: 5000),));
  }

  int getTopListSize(String month)
  {
    int length = 0;
    for(int i = 0;i<topList.length;i++)
    {
      if(topList[i]["MonthName"]==month)
        {
          length++;
        }
    }
    return length;
  }

  void postExecution(var data ,int result_code)  {

    switch(result_code) {

      case GET_TOP_PERFORMER_RESULT_CODE:

        try{

          setState(() {
            _isLoading = false;
          });
          setState(() {

            topList = data["ObjPerformer"];
            monthList = data["ObjPermonth"];
            print("Toplist Length="+topList.length.toString());
            print("MonthList length="+monthList.length.toString());

          });

        }catch(e){
          print(e.toString());
        }

        break;


    }
  }

  void _showDialog(String title,String msg) {
    // flutter defined function
    showDialog(

      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(title),
          content: new Text(msg),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();

              },
            ),
          ],
        );
      },
    );
  }


}

