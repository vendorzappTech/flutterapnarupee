import 'dart:convert';

import 'package:apanarupee/model/ApiInterface.dart';
import 'package:apanarupee/utils/Helper.dart';
import 'package:apanarupee/utils/HexColor.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:intl/intl.dart';

class Notifications extends StatefulWidget  {



  @override
  NotificationsState createState() => NotificationsState();
}

class NotificationsState extends State<Notifications> implements ApiInterface {

  final formKey = new GlobalKey<FormState>();
  final scaffoldKey = new GlobalKey<ScaffoldState>();

  static const int GET_NOTIFICATIONS_RESULT_CODE = 1;
  bool _isLoading = false;
  List notificationList = new List();

  ScrollController _scrollController = new ScrollController();

  final dateFormat1 = DateFormat("yyyy-MM-ddThh:mm:ss");
  final dateFormat2 = DateFormat("dd-MMM-yyyy");


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // call webservice to get top performers
    setState(() {
      _isLoading = true;
    });
    String url = Helper.GET_NOTIFICATIONS_URL+"?RequestType&ParamType";
    Helper.callAuthGetApi(Uri.encodeFull(url ),
        json.encode({

        }), context, this, GET_NOTIFICATIONS_RESULT_CODE);

    FirebaseMessaging _firebaseMessaging;

    _firebaseMessaging = FirebaseMessaging();

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print('on message $message');

        // show notification here
//        _showNotificationWithDefaultSound();
        var title = message['notification']['title'];
        var body = message['notification']['body'];
        // show notification here
        _showDialog(title,body);



      },
      onResume: (Map<String, dynamic> message) async {
        print('on resume $message');
      },
      onLaunch: (Map<String, dynamic> message) async {
        print('on launch $message');
      },

    );


  }

  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance = ScreenUtil.getInstance()
      ..init(context);

    final logo = Hero(
        tag: 'hero',
        child:
        new Container(

            height: MediaQuery
                .of(context)
                .size
                .height,
//          width: 600.0,
            decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: new AssetImage("assets/earndashboard.png"),
                  fit: BoxFit.fill,
                ),
                color: Colors.white
            ),

            child:
            new Column(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child:
                  AppBar
                    (
                      elevation: 0.0,
                      backgroundColor: Colors.transparent,
                      iconTheme: IconThemeData(
                          color: Colors.white),
                      title: Text("Notifications", style: TextStyle(
                          fontSize: ScreenUtil.getInstance().setSp(60)),)),
                ),
                (!_isLoading)?
                Expanded(
                    flex: 9,
                    child:
                    new Padding(padding: EdgeInsets.only(top:5),
                      child:

                      new Container(

                          padding: EdgeInsets.only(top:10),
                          width: MediaQuery
                              .of(context)
                              .size
                              .width,
                          height: MediaQuery
                              .of(context)
                              .size
                              .height-100,

                          decoration: new BoxDecoration(
                            color: Colors.white,
                            borderRadius: new BorderRadius.circular(25.0),
                          ),
                          child:
                          new Form(
                              key:formKey,
                              child: SingleChildScrollView(
                                child:
                                new Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      (!_isLoading)?
                                      new Padding(padding: EdgeInsets.only(top: 0,left: 10,right: 10,bottom: 10),
                                          child:
                                          (notificationList != null || notificationList.length > 0)?
                                          new Container(
//                                             color: Colors.green,
                                              height: MediaQuery
                                                  .of(context)
                                                  .size
                                                  .height-120,
                                              child:

                                              new ListView.builder
                                                (

                                                //        physics: NeverScrollableScrollPhysics(),
                                                  controller: _scrollController,
                                                  itemCount: notificationList.length,
                                                  itemBuilder: (BuildContext ctxt, int index) {
                                                    return
                                                      new Card(
                                                          child:
                                                          new Row(

                                                            children: <Widget>[
                                                              Expanded(
                                                                  flex: 10,
                                                                  child:
                                                                  new Row(
                                                                    mainAxisAlignment:MainAxisAlignment.center ,
                                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                                    children: <Widget>[

                                                                      Expanded(
                                                                      flex: 2,
                                                                      child:
                                                                      new Padding(padding: EdgeInsets.only(left: 5,right: 5,bottom: 5,top: 5),
                                                                          child:
                                                                          (notificationList[index]["Image"] != null || !(notificationList[index]["Image"].toString().compareTo("null")==0 ) || (notificationList[index]["Image"].toString().length > 4))?
//                                                                          new CircleAvatar(
//                                                                            radius: 30,
//                                                                            backgroundColor: Colors.transparent,
//                                                                            backgroundImage:
                                                                            Image.network(
                                                                              notificationList[index]["Image"].toString(),
                                                                            fit: BoxFit.fitHeight,
                                                                            width: MediaQuery.of(context).size.width,
                                                                            height: 50,
//                                                                          ),
                                                                            //NetworkImage(notificationList[index]["Image"].toString(),fit: BoxFit.fitWidth,),

                                                                          ):
                                                                          new CircleAvatar(
                                                                            radius: 30,
                                                                            backgroundColor: Colors.white,
                                                                            backgroundImage: AssetImage("assets/profile.png"),
                                                                          )
                                                                      ),

//                                                                      new Padding(padding: EdgeInsets.only(left: 10,right: 10,bottom: 10,top: 10),
//                                                                          child:(notificationList[index]["Image"] != null || !(notificationList[index]["Image"].toString().compareTo("null")==0 ) || (notificationList[index]["Image"].toString().length > 4))?
//                                                                          new CircleAvatar(
//                                                                            radius: 30,
//                                                                            backgroundImage: NetworkImage(notificationList[index]["Image"].toString()),
//                                                                          ): new CircleAvatar(
//                                                                            radius: 30,
//                                                                            backgroundColor: Colors.white,
//                                                                           // backgroundImage: AssetImage("assets/profile.png"),
//                                                                          )
//                                                                      ),
 ),
                                                                  Expanded(
                                                                    flex: 8,
                                                                    child:

                                                                      new Padding(padding: EdgeInsets.all(5),
                                                                          child:
                                                                          new Column(
                                                                            mainAxisAlignment: MainAxisAlignment.start,
                                                                            crossAxisAlignment: CrossAxisAlignment.start,
                                                                            children: <Widget>[
                                                                              new Text(notificationList[index]["Title"].toString(),style: TextStyle(color: Colors.black,fontSize: ScreenUtil.getInstance().setSp(50))),
                                                                              new Text(notificationList[index]["TextMessage"].toString(),style: TextStyle(color: Color(HexColor.getAppDarkGrey()),fontSize: ScreenUtil.getInstance().setSp(40)),),

                                                                              new Text(dateFormat2.format(dateFormat1.parse(notificationList[index]["NotificationDate"].toString())),style: TextStyle(color: Color(HexColor.getAppLightGrey()),fontSize:ScreenUtil.getInstance().setSp(35) ),),
                                                                            ],
                                                                          )
                                                                      ),),
                                                                    ],
                                                                  )
                                                              ),

                                                            ],
                                                          )
                                                      );
                                                    //new Text(_noteList[0]['Notes'].toString(),style: TextStyle(color: Colors.black),);
                                                  }
                                              )
                                          )
                                              :
                                          SizedBox(height: 0,width: 0,)
                                      )
                                          :Align(
                                          alignment: Alignment.center,
                                          child: CircularProgressIndicator()
                                      ),

                                    ]
                                ),
                              )))

//                        )
//                    )

//               )
                    )): Expanded(
                  flex: 9,
                  child:
                  Container(
                    padding: EdgeInsets.only(top:10),
                    width: MediaQuery
                        .of(context)
                        .size
                        .width,
                    height: MediaQuery
                        .of(context)
                        .size
                        .height-100,

                    decoration: new BoxDecoration(
                      color: Colors.white,
                      borderRadius: new BorderRadius.circular(25.0),
                    ),
                    child:Align(
                        alignment: Alignment.center,
                        child: CircularProgressIndicator()
                    ) ,
                  )
                  ),

              ],
            )


        )
    );

    return new Scaffold(

        key: scaffoldKey,
        backgroundColor: Colors.white,

        body:
        new Form(

          child: SingleChildScrollView(
            child:logo,
//              padding: EdgeInsets.only(top: 0),
//
//              children: <Widget>[
//                logo,


//              ]
          ),

        )
    );
  }

  void _showSnackBar(String text,Color color) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(backgroundColor:color ,content: new Text(text),duration: new Duration(milliseconds: 5000),));
  }


  void postExecution(var data ,int result_code)  {

    switch(result_code) {

      case GET_NOTIFICATIONS_RESULT_CODE:

        try{

          setState(() {
            _isLoading = false;
          });
          setState(() {

            notificationList = data["NotificationList"];

            print("Notification length="+notificationList.length.toString());

          });

        }catch(e){
          print(e.toString());
        }

        break;


    }
  }

  void _showDialog(String title,String msg) {
    // flutter defined function
    showDialog(

      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(title),
          content: new Text(msg),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();

              },
            ),
          ],
        );
      },
    );
  }


}

