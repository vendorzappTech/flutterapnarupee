import 'dart:convert';

import 'package:apanarupee/model/ApiInterface.dart';
import 'package:apanarupee/screens/OTPVerification.dart';
import 'package:apanarupee/utils/Helper.dart';
import 'package:apanarupee/utils/HexColor.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

class ChangeStatus extends StatefulWidget  {
  ChangeStatus({Key key, this.leadId}) : super(key: key);

  final String leadId;

  @override
  ChangeStatusState createState() => ChangeStatusState(this.leadId);
}

class ChangeStatusState extends State<ChangeStatus> implements ApiInterface{


  bool _isLoading = false;
  String leadId = "",_comment = "",_currentReasonId ;
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  final formKey = new GlobalKey<FormState>();
  static const int GET_REASONS_RESULT_CODE = 1,CHANGE_STATUS_RESULT_CODE =2,ERROR_CODE = -1;
  List _myreasons = new List();

  ChangeStatusState(String leadId)
  {
    this.leadId = leadId;
  }

  void changedReasonDropDownItem(String selectedReason) {
    print("Selected city $selectedReason, we are going to refresh the UI");
    setState(() {
      _currentReasonId = selectedReason;
      //_currentReasonId = selectedReason;
    });
  }


  void _submit() async
  {
    var form = formKey.currentState;

    if (form.validate()) {

      form.save();

      // call webservice to register user

      if((_currentReasonId!=null)) {
        setState(() => _isLoading = true);
        Helper.callAuthApi(Uri.encodeFull(Helper.CHANGE_STATUS_URL),
            json.encode({
              "LeadId": leadId,
              "LeadReason": _currentReasonId,
              "LeadComments": _comment,
              "LeadStatus": "cancel",


            }), context, this, CHANGE_STATUS_RESULT_CODE);
      }else{
        _showSnackBar("Select reason", Colors.red);
      }
    }


  }

  @override
  void initState() {
    // TODO: implement initState

    super.initState();
    print("In init");

    // GET ALL REASONS
    Helper.callGETApi(Uri.encodeFull( Helper.GET_REASONS_URL),
        json.encode({

        }), context, this, GET_REASONS_RESULT_CODE);

    FirebaseMessaging _firebaseMessaging;

    _firebaseMessaging = FirebaseMessaging();

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print('on message $message');

        // show notification here
//        _showNotificationWithDefaultSound();
        var title = message['notification']['title'];
        var body = message['notification']['body'];
        // show notification here
        _showDialog(title,body);



      },
      onResume: (Map<String, dynamic> message) async {
        print('on resume $message');
      },
      onLaunch: (Map<String, dynamic> message) async {
        print('on launch $message');
      },

    );




  }


  @override
  Widget build(BuildContext context) {

    var reason =
    (_myreasons.length > 0)?
    new FormField(

      builder: (FormFieldState state) {
        return InputDecorator(
          decoration:  InputDecoration(
//              border: const OutlineInputBorder(
//
//              ),
            labelText: "Reason",
            hintStyle: TextStyle(color:Colors.grey),
            labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor())),
            contentPadding: const EdgeInsets.symmetric(vertical: 5.0,horizontal: 5.0),
          ),

          child: new DropdownButtonHideUnderline(

              child:
              new DropdownButton(
                value: _currentReasonId,
                items: _myreasons.map((item) {
                  return new DropdownMenuItem<String>(
                    child: new Text(item.toString()),
                    value: item.toString(),
                  );
                }).toList(),
                onChanged: changedReasonDropDownItem,

              )

          ),
        );
      },
    ): new Text("Loading");
    ;

    var comment =
    new TextFormField(

      decoration:  InputDecoration(
        //  border: const OutlineInputBorder(),
          hintText: "Enter Comment",
          labelText: "Comment",
          hintStyle: TextStyle(color:Colors.grey),
          labelStyle: TextStyle(color:Color(HexColor.getAppBlueColor()))
      ),
      autocorrect: true,

      validator: (str) =>
      (str.length > 0)
          ? null
          : "Enter comment",
      onSaved: (str) => _comment = str,
      keyboardType: TextInputType.text,
      enabled: true,

    );


    var submitButton =
    ButtonTheme(
        minWidth: 50.0,
        height: 50.0,
        child:
        new RaisedButton(
          color: HexColor("#fb6616"),
          shape: new RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(5.0)),
          splashColor: Colors.blue,
          onPressed: _submit,
          child: new Text("UPDATE STATUS", style: TextStyle(color:Colors.white)),
        ));

    return new Scaffold(
        key: scaffoldKey,
        backgroundColor: Colors.white,
        appBar:
        AppBar(
          iconTheme: IconThemeData(
              color: Colors.white),title:Text( "Change Status"),backgroundColor: HexColor("#2b2ca3"),),
        body: Center(
          child: new Form(
            key:formKey,
            child:
            (_isLoading)?Align(
                alignment: Alignment.center,
                child: CircularProgressIndicator()
            ):
            ListView(

                padding: EdgeInsets.only(top: 0),

                children: <Widget>[

                  SizedBox(height: 28.0),

            new Padding(
                padding: EdgeInsets.only(left:100,right: 100,top:10),
                child:
                  ButtonTheme(
                      minWidth: 50.0,
                      height: 50.0,
                      child:
                      new RaisedButton(
                        color: Colors.red,
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(20.0)),
                        splashColor: Colors.blue,
                        onPressed: _submit,
                        child: new Text("CANCEL", style: TextStyle(color:Colors.white)),
                      ))),

                  new Padding(
                      padding: EdgeInsets.only(left:24,right: 24,top:10),
                      child:reason),

                  new Padding(
                      padding: EdgeInsets.only(left:24,right: 24,top:10),
                      child:comment),

                  new Padding(
                      padding: EdgeInsets.only(left:24,right: 24,top:40),
                      child:submitButton),




                ]
            ),
          ),
        )
    );
  }

  void _showSnackBar(String text,Color color) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(backgroundColor:color ,content: new Text(text),duration: new Duration(milliseconds: 5000),));
  }

  void postExecution(var data ,int result_code)  {

    switch(result_code) {
      case GET_REASONS_RESULT_CODE:
        if(data == null)
          _showSnackBar("Unable to get location", Colors.red);
        else {
          setState(() {
            _myreasons = data;
         //   _currentReasonId = data[0].toString();
          });
        }
        break;

      case CHANGE_STATUS_RESULT_CODE:
        try {
          setState(() => _isLoading = false);

          if (data["responseCode"] == Helper.SUCCESS_STATUS_CODE) {
            print("got data");
            _showSnackBar(data["messages"]["messageDescription"], Colors.green);

            Future.delayed(const Duration(milliseconds: 3000), () {
             setState(() {
                Navigator.pop(context);
                Navigator.pop(context);
              });

            });



          } else {
            print("Error=" + data["messages"]["messageDescription"]);

            _showSnackBar(data["messages"]["messageDescription"], Colors.red);
          }
          if (data == null)
            print("Unable to get data");
        } catch (e) {
          _showSnackBar("Some problem occurred", Colors.red);
        }
        break;

      case ERROR_CODE:
        _showSnackBar("Please check your internet connection ",Colors.red);
        setState(() {
          _isLoading = false;

        });
        break;
    }
  }

  void _showDialog(String title,String msg) {
    // flutter defined function
    showDialog(

      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(title),
          content: new Text(msg),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();

              },
            ),
          ],
        );
      },
    );
  }



}