import 'dart:convert';

import 'package:apanarupee/model/ApiInterface.dart';
import 'package:apanarupee/screens/OTPVerification.dart';
import 'package:apanarupee/utils/Helper.dart';
import 'package:apanarupee/utils/HexColor.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

class Registration extends StatefulWidget  {
  Registration({Key key, this.mobile}) : super(key: key);

  final String mobile;

  @override
  RegistrationState createState() => RegistrationState(this.mobile);
}

class RegistrationState extends State<Registration> implements ApiInterface{


  static const int GET_CITY_RESULT_CODE = 1,REGISTRATION_RESULT_CODE=2,GET_BRANCH_RESULT_CODE=3,ERROR_CODE=-1;
  String _currentCityId,_currentBranchId,_name,_email,_mobile,_currentRegidsterId="Employee",_branchResult="Loading";
  List _mycity = new List();
  List _mybranch = new List();
  List _myrole = new List();
  bool _isLoading = false;

  final formKey = new GlobalKey<FormState>();
  final formKey1 = new GlobalKey<FormState>();

  final scaffoldKey = new GlobalKey<ScaffoldState>();

  RegistrationState(String mobile)
  {
      this._mobile = mobile;
  }
  void _submit() async
  {
    var form = formKey.currentState;

    if (form.validate()) {
      setState(() => _isLoading = true);
      form.save();
      print("mobile=" + _mobile);

      // call webservice to register user

      Helper.callApi(Uri.encodeFull(Helper.REGISTRATION_URL),
          json.encode({
            "BranchId": this._currentBranchId,
            "Name": this._name,
            "MobileNumber": this._mobile,
            "Email": this._email,
            "RegisterAs": this._currentRegidsterId

          }), context, this, REGISTRATION_RESULT_CODE);

    }


  }

  @override
  void initState() {
    // TODO: implement initState

    super.initState();
    print("In init");


    // call webservices to get location
    Helper.callGETApi(Uri.encodeFull( Helper.GET_CITY_URL),
        json.encode({

        }), context, this, GET_CITY_RESULT_CODE);

    FirebaseMessaging _firebaseMessaging;

    _firebaseMessaging = FirebaseMessaging();

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print('on message $message');

        // show notification here
//        _showNotificationWithDefaultSound();
        var title = message['notification']['title'];
        var body = message['notification']['body'];
        // show notification here
        _showDialog(title,body);



      },
      onResume: (Map<String, dynamic> message) async {
        print('on resume $message');
      },
      onLaunch: (Map<String, dynamic> message) async {
        print('on launch $message');
      },

    );

  }




//  void init()
//  {
//
//  }

  void changedRegisterDropDownItem(String selectedRole) {
    print("Selected role $selectedRole, we are going to refresh the UI");
    setState(() {
      _currentRegidsterId = selectedRole;
    });
  }
  void changedLocationDropDownItem(String selectedCity) {
    print("Selected city $selectedCity, we are going to refresh the UI");
    setState(() {
      _currentCityId = selectedCity;
      _isLoading = true;

      String uri = Helper.GET_BRANCH_URL +"?cityId="+_currentCityId;
      print("uri="+uri);
      Helper.callGETApi(Uri.encodeFull( uri),
          json.encode({
            "cityId":_currentCityId,
          }), context, this, GET_BRANCH_RESULT_CODE);
    });
  }
  void changedBranchDropDownItem(String selectedBranch) {
    print("Selected city $selectedBranch, we are going to refresh the UI");
    setState(() {
      _currentBranchId = selectedBranch;
    });
  }



  @override
  Widget build(BuildContext context) {

    ScreenUtil.instance = ScreenUtil.getInstance()..init(context);

    final logo = Hero(
        tag: 'hero',
        child:
        new Container(

            height: ScreenUtil.getInstance().setHeight(800),
//          width: 600.0,
            decoration: new BoxDecoration(
              image: new DecorationImage(image: new AssetImage("assets/loginbg.png"), fit: BoxFit.fill,
              ),
            ),

            child:
            new Column(
              children: <Widget>[
                AppBar
                  (

                    elevation: 0.0,
                    backgroundColor: Colors.transparent,
                    iconTheme: IconThemeData(
                        color: Colors.white),title:Text( "Register",style: TextStyle(fontSize: ScreenUtil.getInstance().setSp(60)),)),
                new Padding(
                  padding: EdgeInsets.only(top: ScreenUtil.getInstance().setHeight(100)),
                  child:
                  Center(
                    child: new Image.asset("assets/register.png",width: ScreenUtil.getInstance().setWidth(350),height: ScreenUtil.getInstance().setWidth(350)),
                  ),)
              ],
            )


        )
    );


    var registerAs =

    new FormField(
      builder: (FormFieldState state) {
        return InputDecorator(
          decoration:  InputDecoration(
              border: const OutlineInputBorder(),
              labelText: "Register As",
              hintStyle: TextStyle(color:Colors.grey),
              labelStyle: TextStyle(color:Colors.grey),
              contentPadding: const EdgeInsets.symmetric(vertical: 5.0,horizontal: 5.0),
          ),
          child: new DropdownButtonHideUnderline(
              child:
              new DropdownButton(
                value: _currentRegidsterId,
                items: <String>['Employee', 'Partner'].map((String value) {
                  return new DropdownMenuItem<String>(
                    value: value,
                    child: new Text(value),
                  );
                }).toList(),
                onChanged: changedRegisterDropDownItem,

              )

          ),
        );
      },
    );

    var name =
    new TextFormField(
      style: TextStyle(fontSize: ScreenUtil.getInstance().setSp(50) ,color: Colors.black),
      decoration:  InputDecoration(
          //border: const OutlineInputBorder(),
          hintText: "Enter Name",
          labelText: "Name",
          hintStyle: TextStyle(color:Colors.grey,fontSize: ScreenUtil.getInstance().setSp(40)),
          labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor()),fontSize: ScreenUtil.getInstance().setSp(40))
      ),
      autocorrect: true,
      validator: (str) =>
      (str.length >0)
          ? null
          : "Enter Name",


      onSaved: (str) => _name = str,
      keyboardType: TextInputType.text,
      enabled: true,


    );

    var email =
    new TextFormField(
      style: TextStyle(fontSize: ScreenUtil.getInstance().setSp(50) ,color: Colors.black),
      decoration:  InputDecoration(
          //border: const OutlineInputBorder(),
          hintText: "Enter Email",
          labelText: "Email",
          hintStyle: TextStyle(color:Colors.grey,fontSize:ScreenUtil.getInstance().setSp(40) ),
          labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor()),fontSize: ScreenUtil.getInstance().setSp(40))
      ),
      autocorrect: true,
      validator: ( String str) {
        // apply email validation here
        Pattern pattern =
            r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
        RegExp regex = new RegExp(pattern);
        if (!regex.hasMatch(str))
          return 'Enter Valid Email';
        else
          return null;
      },
      onSaved: (str) => _email = str,
      keyboardType: TextInputType.emailAddress,
      enabled: true,

    );

    var mobile =
    new TextFormField(
      style: TextStyle(fontSize: ScreenUtil.getInstance().setSp(50) ,color: Colors.black),
      decoration:  InputDecoration(
         // border: const OutlineInputBorder(),
          hintText: "Enter Mobile Number",
          labelText: "Mobile",
          hintStyle: TextStyle(color:Colors.grey,fontSize: ScreenUtil.getInstance().setSp(40)),
          labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor()),fontSize: ScreenUtil.getInstance().setSp(40))
      ),
      autocorrect: true,
      initialValue: _mobile,
      validator: (str) =>
      (str.length == 10)
          ? null
          : "Enter 10 digits valid mobile number",
      onSaved: (str) => _mobile = str,
      keyboardType: TextInputType.phone,
      enabled: true,
      inputFormatters: [
        LengthLimitingTextInputFormatter(10),
        WhitelistingTextInputFormatter.digitsOnly,
      ],

    );

    var city =
    (_mycity.length > 0)?
    new FormField(
      builder: (FormFieldState state) {
        return InputDecorator(
          decoration:  InputDecoration(
//              border: const OutlineInputBorder(
//
//              ),
              labelText: "City",
              hintStyle: TextStyle(color:Colors.grey),
            labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor())),
              contentPadding: const EdgeInsets.symmetric(vertical: 5.0,horizontal: 5.0),
          ),
          child: new DropdownButtonHideUnderline(

              child:
              new DropdownButton(
                value: _currentCityId,
                items: _mycity.map((item) {
                  return new DropdownMenuItem(
                    child: new Text(item['Name'],style: TextStyle(fontSize: ScreenUtil.getInstance().setSp(40)),),
                    value: item['CityId'].toString(),
                  );
                }).toList(),
                onChanged: changedLocationDropDownItem,
              )

          ),
        );
      },
    ): new Text("Loading");

    var branch =
    (_mybranch.length > 0)?
    new FormField(
      builder: (FormFieldState state) {
        return InputDecorator(
          decoration:  InputDecoration(
          //    border: const OutlineInputBorder(),
              labelText: "Branch",
              hintStyle: TextStyle(color:Colors.grey),
            labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor())),
              contentPadding: const EdgeInsets.symmetric(vertical: 5.0,horizontal: 5.0),

          ),
          child: new DropdownButtonHideUnderline(
              child:
              new DropdownButton(
                value: _currentBranchId,
                items: _mybranch.map((item) {
                  return new DropdownMenuItem(
                    child: new Text(item['Name'],style: TextStyle(fontSize: ScreenUtil.getInstance().setSp(40))),
                    value: item['BranchId'].toString(),
                  );
                }).toList(),
                onChanged: changedBranchDropDownItem,
              )

          ),
        );
      },
    ):new Center(
      child:
      new Text(_branchResult,style: TextStyle(fontSize:ScreenUtil.getInstance().setSp(40) ),));


    var submitButton =
    ButtonTheme(
        minWidth: 50.0,
        height: 50.0,
        child:
        new RaisedButton(
          color: HexColor("#fb6616"),
          shape: new RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(5.0)),
          splashColor: Colors.blue,
        //  onPressed: _submit,
          onPressed:!(_mybranch.length > 0)? null : () => _submit(),
          child: new Text("REGISTER", style: TextStyle(color:Colors.white,fontSize: ScreenUtil.getInstance().setSp(50))),
        ));

    return new Scaffold(

        key: scaffoldKey,
        backgroundColor: Colors.white,

        body:
           new Form(
            key:formKey,
            child: ListView(

                padding: EdgeInsets.only(top: 0),

                children: <Widget>[
                  logo,
//                  SizedBox(height: 14.0),
//                  new Padding(
//                      padding: EdgeInsets.only(left:24,right: 24,top: 10),
//                      child:registerAs),

                  new Padding(
                      padding: EdgeInsets.only(left:24,right: 24,top:10),
                      child:name),

                  new Padding(
                      padding: EdgeInsets.only(left:24,right: 24,top:10),
                      child:email),

                  new Padding(
                      padding: EdgeInsets.only(left:24,right: 24,top:10),
                      child:mobile),

                  new Padding(
                      padding: EdgeInsets.only(left:24,right: 24,top:10),
                      child:city),
                  new Padding(
                      padding: EdgeInsets.only(left:24,right: 24,top:10),
                      child:branch),


                  new Padding(
                      padding: EdgeInsets.only(left:24,right: 24,top: ScreenUtil.getInstance().setSp(72)),
                      child:
                      submitButton),
                  SizedBox(height: 10.0),
                  new Center(
                    child:
                    (_isLoading)? Align(
                        alignment: Alignment.center,
                        child: CircularProgressIndicator()): SizedBox(height: 0.0),
                  ),
                  new Padding(padding: EdgeInsets.only(top: 10),
                      child:
                      new Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          new GestureDetector(
                            onTap: () {

                            },
                            child: new Text("Already have an acount ",style: TextStyle(color: Colors.black,fontSize: ScreenUtil.getInstance().setSp(50)),),
                          ),
                          new Padding(padding: EdgeInsets.only(top: 10,bottom: 10),

                              child:
                              new GestureDetector(
                                onTap: () {
                                      Navigator.pop(context);
                                },
                                child: new Text("Login Now",style: TextStyle(color: HexColor("#fb6616"),fontSize: ScreenUtil.getInstance().setSp(40),fontWeight: FontWeight.bold),),
                              ))
                        ],
                      ))


                ]
            ),

        )
    );
  }

  void _showSnackBar(String text,Color color) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(backgroundColor:color ,content: new Text(text),duration: new Duration(milliseconds: 5000),));
  }

  void postExecution(var data ,int result_code)  {

    switch(result_code) {
      case GET_CITY_RESULT_CODE:
        if(data == null)
          _showSnackBar("Unable to get location", Colors.red);
        else {
          setState(() {
            _mycity = data;
            print(data[0]["CityId"]);
            int id = data[0]["CityId"];
            _currentCityId = id.toString();
          });

          String uri = Helper.GET_BRANCH_URL +"?cityId="+_currentCityId;
          print("uri="+uri);
          Helper.callGETApi(Uri.encodeFull( uri),
              json.encode({
               "cityId":_currentCityId,
              }), context, this, GET_BRANCH_RESULT_CODE);
        }
        break;
      case GET_BRANCH_RESULT_CODE:

        setState(() {
          _isLoading = false;
        });
        if(data == null){
           setState(() {
             _branchResult = "branch not available";
           });

          _showSnackBar("Unable to get branch", Colors.red);}
        else {
          setState(() {
            _mybranch = data;
            if(_mybranch.length <= 0)
              {
                setState(() {
                  _branchResult = "branch not available";
                });

              }else{
              int id = data[0]["BranchId"];
              _currentBranchId = id.toString();
              }

          });

        }
        break;
      case REGISTRATION_RESULT_CODE:
        try {
          setState(() => _isLoading = false);

          if (data["responseCode"] == Helper.SUCCESS_STATUS_CODE) {
            print("got data");
            _showSnackBar(data["messages"]["messageDescription"], Colors.green);

            Future.delayed(const Duration(milliseconds: 5000), () {

// Here you can write your code

              setState(() {
                // Here you can write your code for open new view

                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) =>   OTPVerification(mobile: _mobile)),
                );


              });

            });



          } else {
            print("Error=" + data["messages"]["messageDescription"]);

            _showSnackBar(data["messages"]["messageDescription"], Colors.red);
          }
          if (data == null)
            print("Unable to get data");
        } catch (e) {
          _showSnackBar("Some problem occurred", Colors.red);
        }
        break;

      case ERROR_CODE:
        _showSnackBar("Please check your internet connection ",Colors.red);
        setState(() {
          _isLoading = false;

        });
        break;
    }
  }

  void _showDialog(String title,String msg) {
    // flutter defined function
    showDialog(

      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(title),
          content: new Text(msg),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();

              },
            ),
          ],
        );
      },
    );
  }


}