import 'dart:convert';

import 'package:apanarupee/model/ApiInterface.dart';
import 'package:apanarupee/screens/ApplicationFill.dart';
import 'package:apanarupee/screens/OTPVerification.dart';
import 'package:apanarupee/utils/Helper.dart';
import 'package:apanarupee/utils/HexColor.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

import 'AllLead.dart';

class CreateLead extends StatefulWidget  {
  @override
  CreateLeadState createState() => CreateLeadState();
}

class CreateLeadState extends State<CreateLead> implements ApiInterface {
  int _index = 0;
  bool stepActve1 = true,stepActve2 = false,stepActve3 = false,lastReach = false,_isLoading = false,isSubType = false,_isLoading1 = false;
  String _branchResult = "Loading";
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  final formKey = new  GlobalKey<FormState>();
  String _currentProductTypeId,_currentProductId,_currentCityId,_currentBranchId,_loan_amount,_name="",_email="",_mobile="",_alternateMobile="",_productName="";
  String _loanId = "0";
  List _productType = new List();
  List _product = new List(); var product_data;
  List _mycity = new List();
  List _mybranch = new List();


  static const int GET_PRODUCT_TYPE_RESULT_CODE = 1,GET_PRODUCT_RESULT_CODE = 2,GET_CITY_RESULT_CODE = 3,CREATE_LEAD_RESULT_CODE=4,GET_BRANCH_RESULT_CODE=5,ERROR_CODE = -1;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // call webservice to fetch product type
    _isLoading = true;
    String uri = Helper.GET_PRODUCT_TYPE_URL +"?RequestType=ProductType&ParamType=1";
    print("uri="+uri);
    Helper.callGETApi(Uri.encodeFull( uri),
        json.encode({
        }), context, this, GET_PRODUCT_TYPE_RESULT_CODE);

    FirebaseMessaging _firebaseMessaging;

    _firebaseMessaging = FirebaseMessaging();

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print('on message $message');

        // show notification here
//        _showNotificationWithDefaultSound();
        var title = message['notification']['title'];
        var body = message['notification']['body'];
        // show notification here
        _showDialog(title,body);



      },
      onResume: (Map<String, dynamic> message) async {
        print('on resume $message');
      },
      onLaunch: (Map<String, dynamic> message) async {
        print('on launch $message');
      },

    );


  }


  void changedProductTypeDropDownItem(String selectedId) {
    print("Selected role $selectedId, we are going to refresh the UI");
    setState(() {
      isSubType = false;
      _currentProductTypeId = selectedId;
      // call web service to get product
   //   _isLoading = true;
      String uri = Helper.GET_PRODUCT_TYPE_URL +"?RequestType=Product&ParamType="+_currentProductTypeId;
      print("uri="+uri);
      _product.clear();
      Helper.callGETApi(Uri.encodeFull( uri),
          json.encode({

          }), context, this, GET_PRODUCT_RESULT_CODE);

    });
  }

  void changedProductDropDownItem(String selectedId) {
    print("Selected role $selectedId, we are going to refresh the UI");
    setState(() {
      _currentProductId = selectedId;
    });
  }

  void changedCityDropDownItem(String selectedCity) {
    print("Selected city $selectedCity, we are going to refresh the UI");
    setState(() {
      print("Selected city $selectedCity, we are going to refresh the UI");

        _currentCityId = selectedCity;
      });
  }

  void changedBranchDropDownItem(String selectedBranch) {
    print("Selected city $selectedBranch, we are going to refresh the UI");
    setState(() {
      _currentBranchId = selectedBranch;
    });
  }


  void _showSnackBar(String text,Color color) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(backgroundColor:color ,content: new Text(text),duration: new Duration(milliseconds: 5000),));
  }

  void postExecution(var data ,int result_code) {
    try {
      switch (result_code) {
        case GET_PRODUCT_TYPE_RESULT_CODE:
          if (data == null)
            _showSnackBar("Unable to get location", Colors.red);
          else {
            setState(() {
              _productType = data;
              print(data[0]["id"]);
              int id = data[0]["id"];
              _currentProductTypeId = id.toString();
            });

            // call webservice to get product data

            String uri = Helper.GET_PRODUCT_TYPE_URL +
                "?RequestType=Product&ParamType=" + _currentProductTypeId;
            print("uri=" + uri);
            Helper.callGETApi(Uri.encodeFull(uri),
                json.encode({
                }), context, this, GET_PRODUCT_RESULT_CODE);
          }

          break;
        case GET_PRODUCT_RESULT_CODE:

          if (data == null)
            _showSnackBar("Unable to get location", Colors.red);
          else {
            setState(() {
              product_data = data;
              _product = data;
              print(data[0]["id"]);
              int id = data[0]["id"];
              _currentProductId = id.toString();
              _isLoading = false;
              isSubType = true;
              // call webservice to get city
              Helper.callGETApi(Uri.encodeFull(Helper.GET_CITY_URL),
                  json.encode({
                  }), context, this, GET_CITY_RESULT_CODE);
            });
          }

          break;

        case GET_CITY_RESULT_CODE:
          if (data == null)
            _showSnackBar("Unable to get location", Colors.red);
          else {
            setState(() {
              _mycity = data;
              print(data[0]["CityId"]);
              int id = data[0]["CityId"];
              _currentCityId = id.toString();
            });

          String uri = Helper.GET_BRANCH_URL +"?cityId="+_currentCityId;
          print("uri="+uri);
//          Helper.callGETApi(Uri.encodeFull( uri),
//              json.encode({
//                "cityId":_currentCityId,
//              }), context, this, GET_BRANCH_RESULT_CODE);
          }


          break;

        case GET_BRANCH_RESULT_CODE:
          setState(() {
            _isLoading = false;
          });
          if (data == null) {
            setState(() {
              _branchResult = "branch not available";
            });

            _showSnackBar("Unable to get branch", Colors.red);
          }
          else {
            setState(() {
              _mybranch = data;

              if (_mybranch.length <= 0) {
                setState(() {
                  _branchResult = "branch not available";
                });
              } else {
                int id = data[0]["BranchId"];
                _currentBranchId = id.toString();
              }
            });
          }
          break;

        case CREATE_LEAD_RESULT_CODE:
          try {
            setState(() => _isLoading1 = false);
            setState(() => _isLoading = false);

            if (data["responseCode"] == Helper.SUCCESS_STATUS_CODE) {
              print("got data");
              _showSnackBar(
                  data["messages"]["messageDescription"], Colors.green);
                setState(() {
                  int id =data["Id"];
                  print("id="+id.toString());
                  _loanId = id.toString();
                   _index = 3;
                });


            } else {
              print("Error=" + data["messages"]["messageDescription"]);
              _index = 1;
              _showSnackBar(data["messages"]["messageDescription"], Colors.red);
            }
            if (data == null)
              print("Unable to get data");
          } catch (e) {
            _showSnackBar("Some problem occurred", Colors.red);
          }
          break;

        case ERROR_CODE:
          _showSnackBar("Please check your internet connection ", Colors.red);

          setState(() {
            _isLoading = false;
          });
          break;
      }
    }
     catch (e) {
      print(e.toString());
  //  _showSnackBar("Some problem occurred", Colors.red);
    }

  }


  _nextClick()
  {
    print("Index = "+_index.toString());
    var form = formKey.currentState;
    if (form.validate()) {
      setState(() => _isLoading = true);
      form.save();
      print("mobile=" + _mobile);


             if(_index != 2)
              _index++;
                          if(_index == 2)
                          {
                             // call webservice to create lead

                            setState(() => _isLoading1 = true);
                                      Helper.callAuthApi(Uri.encodeFull(
                                          Helper.CREATE_LEAD_URL),
                                          json.encode({
                                            "ProductId": this._currentProductId,
                                            "Name": this._name,
                                            "MobileNo": this._mobile,
                                            "alternatemobile":this._alternateMobile,
                                            "EmailId": this._email,
                                            "CityId": this._currentCityId,
                                            "LoanAmount": this._loan_amount
                                          }), context, this,
                                          CREATE_LEAD_RESULT_CODE);
                                   }
                                   else{

                                     int num =int.parse(_currentProductId);
                                     for(int i =0 ;i< _product.length;i++){
                                       if(num == _product[i]["id"]){
                                         _productName = product_data[i]["name"];
                                         break;
                                       }
                                     }
//
                                   }
      if(_index == 4)
      {

        Navigator.pop(context);
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) =>   AllLead(index:0)),
        );
      }

    }

  }


  @override
  Widget build(BuildContext context) {
    // product type
    var productType =
    (_productType.length > 0) ?
    new FormField(
      builder: (FormFieldState state) {
        return InputDecorator(
          decoration: InputDecoration(
//            border: const OutlineInputBorder(
//
//            ),
            labelText: "Product Type",
            hintStyle: TextStyle(color: Colors.grey),
            labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor())),
            contentPadding: const EdgeInsets.symmetric(
                vertical: 5.0, horizontal: 5.0),
          ),
          child: new DropdownButtonHideUnderline(

              child:
              new DropdownButton(
                value: _currentProductTypeId,
                items: _productType.map((item) {
                  return new DropdownMenuItem(
                    child:
                    new Container(
                        width: 250,
                        child:
                        new Text(item['name'])),
                    value: item['id'].toString(),
                  );
                }).toList(),
                onChanged: changedProductTypeDropDownItem,
              )

          ),
        );
      },
    ) : new Text("Loading");

    //product
    var product =
    (_product.length >= 1 ) ?
    new FormField(
      builder: (FormFieldState state) {
        return InputDecorator(
          decoration: InputDecoration(
//            border: const OutlineInputBorder(
//
//            ),
            labelText: "Product",
            hintStyle: TextStyle(color: Colors.grey),
            labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor())),
            contentPadding: const EdgeInsets.symmetric(
                vertical: 5.0, horizontal: 5.0),
          ),
          child: new DropdownButtonHideUnderline(

              child:
              new DropdownButton(
                value: _currentProductId,
                items: _product.map((item) {
                  return new DropdownMenuItem(
                    child:
                      new Container(
                      width: 250,
                      child:
                    new Text(item['name'])),
                    value: item['id'].toString(),
                  );
                }).toList(),
                onChanged: changedProductDropDownItem,
              )

          ),
        );
      },
    ) : new Text("Product Not Available");


    // first step
    Widget _getStep1(BuildContext context) {
      return
        SingleChildScrollView(
          child:
        Padding(
            padding: EdgeInsets.all(5),
            child:
            (_isLoading)?Align(
                alignment: Alignment.center,
                child: CircularProgressIndicator()
            ):
            new Column(
              children: <Widget>[

                new Padding(padding: EdgeInsets.all(5),
                child:productType,
                ),
                new Padding(padding: EdgeInsets.all(5),
                  child:product,
                ),
              ],
            )
        ));
    }

    // second step

    var name =
    new TextFormField(

      decoration:  InputDecoration(
         // border: const OutlineInputBorder(),
          hintText: "Enter Name",
          labelText: "Name",
          hintStyle: TextStyle(color:Colors.grey),
          labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor())),
      ),
      autocorrect: true,
      validator: (str) =>
      (str.length >0)
          ? null
          : "Enter Name",
      onSaved: (str) => _name = str,
      keyboardType: TextInputType.text,
      enabled: true,
      initialValue: _name,


    );

    var email =
    new TextFormField(

      decoration:  InputDecoration(
         // border: const OutlineInputBorder(),
          hintText: "Enter Email",
          labelText: "Email",
          hintStyle: TextStyle(color:Colors.grey),
          labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor()))
      ),
      autocorrect: true,
      initialValue: _email,
      validator: ( String str) {
        Pattern pattern =
            r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
        RegExp regex = new RegExp(pattern);

        return  (str.length <= 0)
            ? null
            : ((!regex.hasMatch(str))?'Enter Valid Email':null);

        // apply email validation here
        if (!regex.hasMatch(str))
          return 'Enter Valid Email';
        else
          return null;
      },
      onSaved: (str) => _email = str,
      keyboardType: TextInputType.emailAddress,
      enabled: true,


    );

    var alternateMobile =
    new TextFormField(

      decoration:  InputDecoration(
        //  border: const OutlineInputBorder(),
          hintText: "Enter Alternate Number",
          labelText: "Alternate Mobile",
          hintStyle: TextStyle(color:Colors.grey),
          labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor()))
      ),
      autocorrect: true,
      initialValue: _alternateMobile,
      validator: (str) =>
      (str.length >0)?
      (str.length == 10)
          ? null
          : "Enter 10 digits valid mobile number":null,
      onSaved: (str) => _alternateMobile = str,
      keyboardType: TextInputType.phone,
      enabled: true,
      inputFormatters: [
        LengthLimitingTextInputFormatter(10),
        WhitelistingTextInputFormatter.digitsOnly,
      ],

    );

    var mobile =
    new TextFormField(

      decoration:  InputDecoration(
        //  border: const OutlineInputBorder(),
          hintText: "Enter Mobile Number",
          labelText: "Mobile",
          hintStyle: TextStyle(color:Colors.grey),
          labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor()))
      ),
      autocorrect: true,
      initialValue: _mobile,
      validator: (str) =>
      (str.length == 10)
          ? null
          : "Enter 10 digits valid mobile number",
      onSaved: (str) => _mobile = str,
      keyboardType: TextInputType.phone,
      enabled: true,
      inputFormatters: [
        LengthLimitingTextInputFormatter(10),
        WhitelistingTextInputFormatter.digitsOnly,
      ],

    );


    var city =
    (_mycity.length > 0)?
    new FormField(
      builder: (FormFieldState state) {
        return InputDecorator(
          decoration:  InputDecoration(
//            border: const OutlineInputBorder(
//
//            ),
            labelText: "City",
            hintStyle: TextStyle(color:Colors.grey),
            labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor())),
            contentPadding: const EdgeInsets.symmetric(vertical: 5.0,horizontal: 5.0),
          ),
          child: new DropdownButtonHideUnderline(

              child:
              new DropdownButton(
                value: _currentCityId,
                items: _mycity.map((item) {
                  return new DropdownMenuItem(
                    child: new Text(item['Name']),
                    value: item['CityId'].toString(),
                  );
                }).toList(),
                onChanged: changedCityDropDownItem,
              )

          ),
        );
      },
    ): new Text("Loading");

    var branch =
    (_mybranch == null)? new SizedBox(height:0):
    (_mybranch.length > 0)?
    new FormField(
      builder: (FormFieldState state) {
        return InputDecorator(
          decoration:  InputDecoration(
           // border: const OutlineInputBorder(),
            labelText: "Branch",
            hintStyle: TextStyle(color:Colors.grey),
            labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor())),
            contentPadding: const EdgeInsets.symmetric(vertical: 5.0,horizontal: 5.0),

          ),
          child: new DropdownButtonHideUnderline(
              child:
              new DropdownButton(
                value: _currentBranchId,
                items: _mybranch.map((item) {
                  return new DropdownMenuItem(
                    child: new Text(item['Name']),
                    value: item['BranchId'].toString(),
                  );
                }).toList(),
                onChanged: changedBranchDropDownItem,
              )

          ),
        );
      },
    ): new SizedBox(height: 0,);


    var loan_amount =
    new TextFormField(

      decoration:  InputDecoration(
         // border: const OutlineInputBorder(),
          hintText: "Enter Loan Amount",
          labelText: "Loan Amount",
          hintStyle: TextStyle(color:Colors.grey),
          labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor()))
      ),
      autocorrect: true,
      validator: (str) =>
      (str.length != 0)
          ? null
          : "Enter Loan Amount",

      initialValue: _loan_amount,
      onSaved: (str) => _loan_amount = str,
      keyboardType: TextInputType.number,
      enabled: true,

    );



    Widget _getStep2(BuildContext context) {
      return
        SingleChildScrollView(
          child:

          Padding(
            padding: EdgeInsets.all(5),
            child:
            new Column(
              children: <Widget>[
                new Padding(padding: EdgeInsets.all(5),
                  child:name,
                ),
                new Padding(padding: EdgeInsets.all(5),
                  child:mobile,
                ),
                new Padding(padding: EdgeInsets.all(5),
                  child:alternateMobile,
                ),
                new Padding(padding: EdgeInsets.all(5),
                  child:email,
                ),
                new Padding(padding: EdgeInsets.all(5),
                  child:city,
                ),
                (_isLoading)?Align(
                    alignment: Alignment.center,
                    child: CircularProgressIndicator()
                ):

                new Padding(padding: EdgeInsets.all(5),
                  child:branch,
                ),
                new Padding(padding: EdgeInsets.all(5),
                  child:loan_amount,
                ),
              ],
            )
        ));
    }


    // step 3
//    // application fill button
//
    var successBox =
                   Container(

                     decoration:BoxDecoration(color:Color( HexColor.getAppOrangeColor()),
                      borderRadius:new BorderRadius.circular(15.0),
                       gradient: new LinearGradient(
                           colors: [HexColor("#fb6616"),HexColor("#f1854b")],
                           begin: Alignment.centerRight,
                           end: new Alignment(0.50, 0.50),
                           tileMode: TileMode.clamp
                       ),
                     ),
                     child: new Column(
                       crossAxisAlignment: CrossAxisAlignment.center,
                       mainAxisAlignment: MainAxisAlignment.center,
                       children: <Widget>[
                         new Padding(
                           padding: EdgeInsets.all( 10),
                           child:
                           new Center(child:new Text(_productName.toString(),style: TextStyle(color: Colors.white,fontWeight:FontWeight.bold,fontSize: ScreenUtil.getInstance().setSp(50)),)),
                         ),
                         new Padding(
                           padding: EdgeInsets.all( 5),
                           child:
                           new Center(child:new Text(_name.toString(),style: TextStyle(color: Colors.white,fontWeight:FontWeight.bold,fontSize: ScreenUtil.getInstance().setSp(40)),)),
                         ),

                         new Padding(
                         padding: EdgeInsets.only(bottom: 10),
                         child:
                         new Center(child:new Text("Loan ID:"+_loanId.toString(),style: TextStyle(color: Colors.white,fontSize: ScreenUtil.getInstance().setSp(35)),)),
                        )


                       ],
                     ),
                   );

    var application_fill = ButtonTheme(
        height: 50.0,
        minWidth: 300,
        child:
        new RaisedButton(
          color: HexColor("#ffffff"),
          shape: new RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(5.0)),
          splashColor: Colors.blue,
          onPressed: () {
            setState(() {

              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) =>   ApplicationFill(leadId:_loanId)),
              );


            });
          },
          child:  new Text("APPLICATION FILL",
              style: TextStyle(color: HexColor("fb6616"),fontSize: ScreenUtil.getInstance().setSp(50))),


        ));

    Widget _getStep3(BuildContext context) {
      return
        (_isLoading)?Align(
            alignment: Alignment.center,
            child: CircularProgressIndicator()
        ):
        Padding(
            padding: EdgeInsets.only(top: 5,bottom: 5),
            child:
            new Column(
              children: <Widget>[
//                new Padding(padding: EdgeInsets.all(5),
//                  child:application_fill,
//                ),

                 application_fill,
              ],
            )
        );
    }


   Widget  _getStep(BuildContext context){
      setState(() {

        switch(_index){
          case 0:
            return _getStep1(context);
            break;

          case 1:
            return _getStep2(context);
            break;

          case 2:
            return _getStep3(context);
            break;

        }

      });
    }


    List<Step> steps = [
      Step(

        title: Text(''),
        content:_getStep1(context),
        isActive: stepActve1,

      ),
      Step(
        title: Text(''),
        content:  _getStep2(context),
        isActive: stepActve2,
      ),
      Step(
        title: Text(''),
        content: _getStep3(context),
        isActive: stepActve3,
      ),

    ];




//
//    return Scaffold(
//        key: scaffoldKey ,
//        appBar: AppBar(
//          title: Text("Create Lead"),
//          backgroundColor: HexColor("#2b2ca3"),
//        ),
////         backgroundColor: Color(HexColor.getAppBlueColor()),
//
//        // commented code for stepper
//
//        body:
//        Theme(
//
//          data: ThemeData(
//            primaryColor: HexColor("#fb6616"),
//            accentColor: HexColor("#fb6616"),
//
//
//
//          ),
//          child:
//          new SingleChildScrollView(
//
//          child:
//          new Form (
//
//            key:formKey,
//            child:
//
//           Column(
//
//
//              children: <Widget>[
//                //our code.
//                Container(
//
//                  margin: EdgeInsets.only(top: 10),
//                  constraints: BoxConstraints.expand(height: 550 ),
//                  color: Colors.white,
//
//                  child: Stepper(
//                    type: StepperType.horizontal,
//
//
//                    controlsBuilder: (BuildContext context,
//                        {VoidCallback onStepContinue, VoidCallback onStepCancel}) =>
//                        ButtonTheme(
//                            minWidth: 50.0,
//                            height: 50.0,
//                            child:
//                            new Padding(
//                              padding: EdgeInsets.only(top: 20,bottom: 20),
//                            child:
//                            new RaisedButton(
//                              color: HexColor("#fb6616"),
//                              shape: new RoundedRectangleBorder(
//                                  borderRadius: new BorderRadius.circular(5.0)),
//                              splashColor: Colors.blue,
//                              onPressed: () {
//                                if(_index == 2){
//                                  Navigator.pop(context);
//                                }
//
//                                setState(() {
//
//                                  print("indx=$_index");
//                                  if(_index == 1){
//
//                                     int num =int.parse(_currentProductId);
//                                     for(int i =0 ;i< _product.length;i++){
//                                       if(num == _product[i]["id"]){
//                                         _productName = product_data[i]["name"];
//                                         break;
//                                       }
//                                     }
//
//
//                                     // do form valiadation here
//                                    var form = formKey.currentState;
//                                    if (form.validate()) {
//                                      setState(() => _isLoading = true);
//                                      form.save();
//                                      print("mobile=" + _mobile);
//
//
//                                      Helper.callAuthApi(Uri.encodeFull(
//                                          Helper.CREATE_LEAD_URL),
//                                          json.encode({
//                                            "ProductId": this._currentProductId,
//                                            "Name": this._name,
//                                            "MobileNo": this._mobile,
//                                            "EmailId": this._email,
//                                            "BranchId": this._currentBranchId,
//                                            "LoanAmount": this._loan_amount
//                                          }), context, this,
//                                          CREATE_LEAD_RESULT_CODE);
//
//                                      if (_index < steps.length - 1) {
//                                        _index = _index + 1;
//                                        if (_index == (steps.length - 2)) {
//                                          lastReach = true;
//                                        }
//                                      }
//                                      switch (_index) {
//                                        case 0:
//                                          stepActve1 = true;
//                                          break;
//                                        case 1:
//                                          stepActve2 = true;
//                                          break;
//                                        case 2:
//                                          stepActve3 = true;
//                                          break;
//                                      }
//
//                                    }
//                                    }else {
//                                    if (_index < steps.length - 1) {
//                                      _index = _index + 1;
//                                      if (_index == (steps.length - 2)) {
//                                        lastReach = true;
//                                      }
//                                    }
//                                    switch (_index) {
//                                      case 0:
//                                        stepActve1 = true;
//                                        break;
//                                      case 1:
//                                        stepActve2 = true;
//                                        break;
//                                      case 2:
//                                        stepActve3 = true;
//                                        break;
//                                    }
//                                  }
//
//                                });
//                              },
//                              child: (_index >= 2)
//                                  ? new Text(
//                                  "DONE", style: TextStyle(color: Colors.white))
//                                  : new Text("CONTINUE",
//                                  style: TextStyle(color: Colors.white)),
//
//
//                            ))),
//
//                    steps: steps,
//
//
//                    currentStep: this._index,
//                    onStepTapped: (index) {
//                      setState(() {
//                        if(index != 2) {
//                          if (lastReach) {
//                            _index = index;
//                            print("You are clicking the tap button.$index");
//                            print("Current index.$_index");
//                            switch (index) {
//                              case 0:
//                                stepActve1 = true;
//                                break;
//                              case 1:
//                                stepActve2 = true;
//                                break;
//                              case 2:
//                                stepActve3 = true;
//                                break;
//                            }
//                          }
//                        }
//                      });
//                    },
//                    onStepCancel: () {
//                      print("You are clicking the cancel button.");
//                      setState(() {
//                        if (_index > 0) {
//                          _index = _index - 1;
//                          lastReach = false;
//                        } else {
//                          _index = 0;
//                          lastReach = false;
//                        }
//                        switch (_index) {
//                          case 0:
//                            stepActve1 = true;
//                            stepActve2 = false;
//                            stepActve3 = false;
//                            break;
//                          case 1:
//                            stepActve2 = true;
//                            stepActve1 = true;
//                            stepActve3 = false;
//                            break;
//                          case 2:
//                            stepActve3 = true;
//
//                            break;
//                        }
//                      });
//                    },
//                    onStepContinue: () {
//                      setState(() {
//                        if (_index < steps.length - 1) {
//                          _index = _index + 1;
//                          if (_index == (steps.length - 1)) {
//                            lastReach = true;
//                          }
//                        } else {
//                          _index = 0;
//                        }
//                        switch (_index) {
//                          case 0:
//                            stepActve1 = true;
//                            break;
//                          case 1:
//                            stepActve2 = true;
//                            break;
//                          case 2:
//                            stepActve3 = true;
//                            break;
//                        }
//                      });
//                      print("You are clicking the continue button.$_index");
//                    },
//
//
//                  ),
//                )
//
//
//              ],
//            ),
//
//          ),
//        )));


    var nextButton =
    ButtonTheme(
        minWidth: MediaQuery.of(context).size.width,
        height: 50.0,
        child:
        new RaisedButton(

          color: HexColor("#fb6616"),
          shape: new RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(5.0)),
          splashColor: Colors.blue,
          onPressed: !isSubType ? null : () => _nextClick(),
          child: (_index >= 3 )?new Text("DONE", style: TextStyle(color:Colors.white,fontSize: ScreenUtil.getInstance().setSp(50))):new Text("NEXT", style: TextStyle(color:Colors.white,fontSize: ScreenUtil.getInstance().setSp(50))),
        ));


    final logo = Hero(
        tag: 'hero',
        child:
        new Container(

            height: MediaQuery.of(context).size.height + 40,
//          width: 600.0,

            decoration: new BoxDecoration(
              image: new DecorationImage(image: new AssetImage("assets/demo.png"), fit: BoxFit.fill,
              ),
            ),

            child:
            new Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[

                AppBar
                  (

                    elevation: 0.0,
                    backgroundColor: Colors.transparent,
                    iconTheme: IconThemeData(
                        color: Colors.white),title:Text( "Create Lead",style: TextStyle(fontSize: ScreenUtil.getInstance().setSp(60)),),
                ),
                (_index == 3)?new Padding(
                  padding: EdgeInsets.only(top: ScreenUtil.getInstance().setHeight(100)),
                  child:
                  Center(
                    child: new Image.asset("assets/goal.png",width: ScreenUtil.getInstance().setWidth(250),height: ScreenUtil.getInstance().setWidth(250)),
                  ),): SizedBox(height: 0,width: 0,),
                (_index == 3)? new Padding(padding: EdgeInsets.all(5),
                    child:
                    new Center(child:new Text("Congrats",style: TextStyle(color: Colors.white,fontSize: ScreenUtil.getInstance().setSp(60),fontWeight: FontWeight.bold))))
                 :SizedBox(height: 0,width: 0,),
                (_index == 3)? new Padding(padding: EdgeInsets.all(5),
                    child:
                    new Center(child:new Text("Loan against "+_productName+" successfully submitted",style: TextStyle(color:Colors.white,fontSize: ScreenUtil.getInstance().setSp(40)),)))
                 :SizedBox(height: 0,width: 0,),
                (_index == 3)?Padding(
                  padding: EdgeInsets.only(left:45,right: 45,top:30),
                  child:
                  successBox)
                    :SizedBox(height: 0,width: 0,),


                Padding(
                  padding: (_index == 3)?EdgeInsets.all(20):EdgeInsets.all(0),
                  child:


                new Container(
                   // height: MediaQuery.of(context).size.height-80,
                  height: (_index == 3)?MediaQuery.of(context).size.height-450:(_index == 1 || _index == 2)?(MediaQuery.of(context).size.height-90):(MediaQuery.of(context).size.height-120),
                  decoration: new BoxDecoration(
                      color:(_index == 3 )?Colors.transparent:Colors.white,
                      borderRadius: new BorderRadius.circular(25.0),
                  ),
                

                  child:
                      new ListView(
                        physics: (_index == 3)? NeverScrollableScrollPhysics():ScrollPhysics(),
                        children: <Widget>[


                (_index == 0) ? Padding(
                    padding: EdgeInsets.only(left:24,right: 24,top:10),
                    child:
                    (_isLoading)?Align(
                        alignment: Alignment.center,
                        child: CircularProgressIndicator()
                    ):
                    new Column(
                      children: <Widget>[

                        new Padding(padding: EdgeInsets.only(bottom: 5,top: 5),
                          child:productType,
                        ),
                        new Padding(padding: EdgeInsets.only(bottom: 5,top: 5),
                          child:product,
                        ),
                      ],
                    )
                ): new Text(""),
                (_index == 1 || _index == 2) ?
                Padding(
                    padding: EdgeInsets.only(left:24,right: 24),
                    child:
                    new Column(
                      children: <Widget>[
                        new Padding(padding: EdgeInsets.all(5),
                          child:name,
                        ),
                        new Padding(padding: EdgeInsets.all(5),
                          child:mobile,
                        ),
                        new Padding(padding: EdgeInsets.all(5),
                          child:alternateMobile,
                        ),
                        new Padding(padding: EdgeInsets.all(5),
                          child:email,
                        ),
                        new Padding(padding: EdgeInsets.all(5),
                          child:city,
                        ),

                        new Padding(padding: EdgeInsets.all(5),
                          child:loan_amount,
                        ),
                        (_isLoading1)?Align(
                            alignment: Alignment.center,
                            child: CircularProgressIndicator()
                        ):SizedBox(height: 0,width: 0)
                      ],
                    )
                )
                    :new SizedBox(width: 0,height: 0,),

                (_index == 3)?
                (_isLoading)?Align(
                    alignment: Alignment.center,
                    child: CircularProgressIndicator()
                ):
                Padding(
                    padding: EdgeInsets.only(left:24,right: 24),
                    child:
                    new Column(
                      children: <Widget>[
//                new Padding(padding: EdgeInsets.all(5),
//                  child:application_fill,
//                ),
                    //    successBox,
                        application_fill,
                      ],
                    )
                )
                    :new Text(""),

                new Padding(
                  padding: EdgeInsets.only(left:24,right: 24,top:10,bottom: 10),
                  child:nextButton,

                ),
                 ])
                )
                )

              ],
            )


        )
    );

    ScreenUtil.instance = ScreenUtil.getInstance()..init(context);
    return new Scaffold(
        key: scaffoldKey,
        backgroundColor: Colors.white,

//        appBar: AppBar(
//          title: Text("Create Lead"),
//          backgroundColor: HexColor("#2b2ca3"),
//        ),

        body:
        new Form(
          key:formKey,
          child: ListView(
             padding: EdgeInsets.all(0),
              //padding: EdgeInsets.only(left:24,right: 24,top:10),
              children: <Widget>[
                logo,
//               (_index == 0) ? Padding(
//                   padding: EdgeInsets.only(left:24,right: 24,top:10),
//                   child:
//                   (_isLoading)?Align(
//                       alignment: Alignment.center,
//                       child: CircularProgressIndicator()
//                   ):
//                   new Column(
//                     children: <Widget>[
//
//                       new Padding(padding: EdgeInsets.only(bottom: 5,top: 5),
//                         child:productType,
//                       ),
//                       new Padding(padding: EdgeInsets.only(bottom: 5,top: 5),
//                         child:product,
//                       ),
//                     ],
//                   )
//               ): new Text(""),
//               (_index == 1) ?
//               Padding(
//                   padding: EdgeInsets.only(left:24,right: 24,top:10),
//                   child:
//                   new Column(
//                     children: <Widget>[
//                       new Padding(padding: EdgeInsets.all(5),
//                         child:name,
//                       ),
//                       new Padding(padding: EdgeInsets.all(5),
//                         child:mobile,
//                       ),
//                       new Padding(padding: EdgeInsets.all(5),
//                         child:email,
//                       ),
//                       new Padding(padding: EdgeInsets.all(5),
//                         child:city,
//                       ),
//
//                       new Padding(padding: EdgeInsets.all(5),
//                         child:loan_amount,
//                       ),
//                     ],
//                   )
//               )
//                   :new Text(""),
//
//               (_index == 2)?
//               (_isLoading)?Align(
//                   alignment: Alignment.center,
//                   child: CircularProgressIndicator()
//               ):
//               Padding(
//                   padding: EdgeInsets.only(left:24,right: 24,top:10),
//                   child:
//                   new Column(
//                     children: <Widget>[
////                new Padding(padding: EdgeInsets.all(5),
////                  child:application_fill,
////                ),
//                       successBox,
//                       application_fill,
//                     ],
//                   )
//               )
//                   :new Text(""),
//
//               new Padding(
//                 padding: EdgeInsets.only(left:24,right: 24,top:20,bottom: 20),
//                 child:nextButton,
//
//               ),

              ]
          ),

        )
    );


  }

  void _showDialog(String title,String msg) {
    // flutter defined function
    showDialog(

      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(title),
          content: new Text(msg),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();

              },
            ),
          ],
        );
      },
    );
  }


}
