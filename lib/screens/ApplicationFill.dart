import 'dart:convert';
import 'package:apanarupee/model/ContactInfo.dart';
import 'package:apanarupee/model/LogInInfo.dart';
import 'package:apanarupee/model/PersonalInfo.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:apanarupee/model/ApiInterface.dart';
import 'package:apanarupee/model/User.dart';
import 'package:apanarupee/utils/Helper.dart';
import 'package:apanarupee/utils/HexColor.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

class ApplicationFill extends StatefulWidget  {
  ApplicationFill({Key key, this.leadId}) : super(key: key);

  final String leadId;

  @override
  ApplicationFillState createState() => ApplicationFillState(leadId);
}

class ApplicationFillState extends State<ApplicationFill> with SingleTickerProviderStateMixin implements ApiInterface  {

  String _name ="",_mobile ="",_email="",_currentCityId="",_loanAmnt="",_tenure="",_currentGenderId,_dob="",_pancard="",_adharcard="",_address="",_pincode="";
  String _fileLogInBank = "",_fileLogInDate = "",_bankApplicationId = "",_bankRMName = "",_bankRMPhoneNumber = "",_bankBranch = "",_documentStatus ,_currentBankId ;
  var selectedDate = new DateTime.now();
  final scaffoldKey = new GlobalKey<ScaffoldState>();
  bool _isLoading = false;
  final formKey1 = new GlobalKey<FormState>();
  final formKey2 = new GlobalKey<FormState>();
  final formKey3 = new GlobalKey<FormState>();

  List _mycity = new List();
  List _mybank = new List();
  List _myApplData = new List();
  static const int  GET_CITY_RESULT_CODE =1,GET_BANK_RESULT_CODE = 2,GET_APPLOCATION_RESULT_CODE = 3,UPDATE_APPLICATION_RESULT_CODE1 = 4,UPDATE_APPLICATION_RESULT_CODE2 = 5,UPDATE_APPLICATION_RESULT_CODE3 = 6,ERROR_CODE = -1;

  final myControllerDOB = TextEditingController();
  final myControllerFileDate = TextEditingController();
  FocusNode focuNodeDOB,focuNodeFileDate;

  final dateFormat = DateFormat("dd/MM/yyyy");
  final dateFormat1 = DateFormat("yyyy-MM-ddThh:mm:ss");
  final dateFormat2 = DateFormat("yyyy-MM-dd");
  String loadId = "";
  TabController myTabController;

  ApplicationFillState(String leadId){
      this.loadId = leadId;
  }

  @override
  void initState() {
    // TODO: implement initState
    // call webservices to get location
    myTabController = new TabController(length: 3, vsync: this);
    Helper.callGETApi(Uri.encodeFull( Helper.GET_CITY_URL),
        json.encode({

        }), context, this, GET_CITY_RESULT_CODE);

    focuNodeDOB = new FocusNode();
    focuNodeDOB.addListener(textFieldFocusDOBDidChange);

    focuNodeFileDate = new FocusNode();
    focuNodeFileDate.addListener(textFieldFocusFileDateDidChange);
    _isLoading = true;

    FirebaseMessaging _firebaseMessaging;

    _firebaseMessaging = FirebaseMessaging();

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print('on message $message');

        // show notification here
//        _showNotificationWithDefaultSound();
        var title = message['notification']['title'];
        var body = message['notification']['body'];
        // show notification here
        _showDialog(title,body);



      },
      onResume: (Map<String, dynamic> message) async {
        print('on resume $message');
      },
      onLaunch: (Map<String, dynamic> message) async {
        print('on launch $message');
      },

    );


  }

  void textFieldFocusDOBDidChange() {
    if (focuNodeDOB.hasFocus) {
      // textfield was tapped
      _selectDate(context);
      focuNodeDOB.unfocus();
    }
  }

//
  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: DateTime(new DateTime.now().year - 70, 1, 1),
        firstDate: DateTime(new DateTime.now().year - 70, 1, 1),
        lastDate: DateTime(new DateTime.now().year - 18, 1, 1));
    if (picked != null && picked != selectedDate)
      setState(() {
        _dob = dateFormat.format(picked);
        myControllerDOB.text = _dob;
      });
  }

  void textFieldFocusFileDateDidChange() {
    if (focuNodeFileDate.hasFocus) {
      // textfield was tapped
      _selectFileDate(context);
      focuNodeFileDate.unfocus();
    }
  }

//
  Future<Null> _selectFileDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(2015,1),
        lastDate: DateTime.now());
    if (picked != null && picked != selectedDate)
      setState(() {
        _fileLogInDate = dateFormat.format(picked);
        myControllerFileDate.text = _fileLogInDate;
      });
  }


  void changedLocationDropDownItem(String selectedCity) {
    print("Selected city $selectedCity, we are going to refresh the UI");
    setState(() {
      _currentCityId = selectedCity;
    });
  }

  void changedGenderDropDownItem(String selectedGender) {
      print("Selected gender $selectedGender, we are going to refresh the UI");
      setState(() {
        _currentGenderId = selectedGender;
      });
  }

  void changedBankDropDownItem(String selectedCity) {
    print("Selected city $selectedCity, we are going to refresh the UI");
    setState(() {
      _currentBankId = selectedCity;
    });
  }


  void changedDocumentStatusDropDownItem(String selected) {
    print("Selected  $selected, we are going to refresh the UI");
    setState(() {
      _documentStatus = selected;
    });
  }


  _submitLeadInfo(){
    var form = formKey1.currentState;

    if (form.validate()) {
      setState(() => _isLoading = true);
      form.save();
      print("mobile=" + _mobile);

      ContactInfo  ci = new ContactInfo();
      ci.leadId = loadId;
      ci.fullName = _name;
      ci.mobileNo = _mobile;
      ci.emailId = _email;
      ci.cityId = _currentCityId;
      ci.loanAmntReq = _loanAmnt;
      ci.tenure = _tenure;
      ci.requestType = "ContactInfo";



      Helper.callAuthApi(Uri.encodeFull(Helper.UPDATE_APPLICATION_URL),
          json.encode(ci), context, this, UPDATE_APPLICATION_RESULT_CODE1);

    }


  }


  _submitPersonalInfo(){
    var form = formKey2.currentState;

    if (form.validate()) {
      setState(() => _isLoading = true);
      form.save();
      print("mobile=" + _mobile);

      PersonalInfo  pi = new PersonalInfo();
      pi.leadId = loadId;
      pi.dob = dateFormat2.format(dateFormat.parse(myControllerDOB.text));
      pi.gender = _currentGenderId;
      pi.pancard = _pancard;
      pi.aadharcard = _adharcard;
      pi.address = _address;
      pi.pincode = _pincode;
      pi.requestType = "PersonalInfo";

      Helper.callAuthApi(Uri.encodeFull(Helper.UPDATE_APPLICATION_URL),
          json.encode(pi), context, this, UPDATE_APPLICATION_RESULT_CODE2);

    }

  }

  _submitLogInInfo(){

    var form = formKey3.currentState;

    if (form.validate()) {
      setState(() => _isLoading = true);
      form.save();
      print("mobile=" + _mobile);

      LogInInfo  pi = new LogInInfo();
      pi.leadId = loadId;
      pi.bankId = _currentBankId;
      pi.bankRMName = _bankRMName;
      pi.loginDate = dateFormat2.format(dateFormat.parse(myControllerFileDate.text));
      pi.bankApplId = _bankApplicationId;
      pi.bankRMNo = _bankRMPhoneNumber;
      pi.bankBranch = _bankBranch;
      pi.documentStatus = _documentStatus;
      pi.requestType = "LoginInfo";

      Helper.callAuthApi(Uri.encodeFull(Helper.UPDATE_APPLICATION_URL),
          json.encode(pi), context, this, UPDATE_APPLICATION_RESULT_CODE3);

    }

  }


  @override
  Widget build(BuildContext context) {

    var full_name = new TextFormField(

      decoration:  InputDecoration(
//          border: const OutlineInputBorder(),
          hintText: "Enter Full Name",
          labelText: "Full Name",
          hintStyle: TextStyle(color:Colors.grey),
          labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor()))
      ),
      autocorrect: true,
      validator: ( String str) {
        // apply email validation here
        null;
      },
      initialValue: _name,
      onSaved: (str) => _name = str,
      keyboardType: TextInputType.text,
      enabled: true,
    );


    var email =
    new TextFormField(

      decoration:  InputDecoration(
//          border: const OutlineInputBorder(),
          hintText: "Enter Email",
          labelText: "Email",
          hintStyle: TextStyle(color:Colors.grey),
          labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor()))
      ),
      initialValue: _email,
      autocorrect: true,
      validator: ( String str) {
        // apply email validation here
        Pattern pattern =
            r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
        RegExp regex = new RegExp(pattern);
        if (!regex.hasMatch(str))
          return 'Enter Valid Email';
        else
          return null;
      },
      onSaved: (str) => _email = str,
      keyboardType: TextInputType.emailAddress,
      enabled: true,


    );

    var mobile =
    new TextFormField(

      decoration:  InputDecoration(
//          border: const OutlineInputBorder(),
          hintText: "Enter Mobile Number",
          labelText: "Mobile",
          hintStyle: TextStyle(color:Colors.grey),
          labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor()))
      ),
      initialValue: _mobile,
      autocorrect: true,
      validator: (str) =>
      (str.length == 10)
          ? null
          : "Invalid mobile number",
      onSaved: (str) => _mobile = str,
      keyboardType: TextInputType.phone,
      enabled: true,
      inputFormatters: [
        LengthLimitingTextInputFormatter(10),
        WhitelistingTextInputFormatter.digitsOnly,
      ],

    );

    var city =
    (_mycity.length > 0)?
    new FormField(
      builder: (FormFieldState state) {
        return InputDecorator(
          decoration:  InputDecoration(
//            border: const OutlineInputBorder(
//
//            ),
            labelText: "City",
            hintStyle: TextStyle(color:Colors.grey),
            labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor())),
            contentPadding: const EdgeInsets.symmetric(vertical: 5.0,horizontal: 5.0),
          ),
          child: new DropdownButtonHideUnderline(

              child:
              new DropdownButton(
                value: _currentCityId,
                items: _mycity.map((item) {
                  return new DropdownMenuItem<String>(
                    child: new Text(item['Name']),
                    value: item['CityId'].toString(),
                  );
                }).toList(),
                onChanged: changedLocationDropDownItem,
              )

          ),
        );
      },
    ): new Text("Loading");


    var loanAmnt =
    new TextFormField(

      decoration:  InputDecoration(
//          border: const OutlineInputBorder(),
          hintText: "Enter Loan Amount",
          labelText: "Loan Amount Required*",
          hintStyle: TextStyle(color:Colors.grey),
          labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor()))
      ),
      initialValue: _loanAmnt,
      autocorrect: true,
      validator: (str) =>
      (str.length == 0)
          ? "Enter loan amount" :null,
      onSaved: (str) => _loanAmnt= str,
      keyboardType: TextInputType.number,
      enabled: true,
      inputFormatters: [
        LengthLimitingTextInputFormatter(10),
      ],

    );

    var tenure =
    new TextFormField(

      decoration:  InputDecoration(
//          border: const OutlineInputBorder(),
          hintText: "Enter Tenure",
          labelText: "Tenure(In months)*",
          hintStyle: TextStyle(color:Colors.grey),
          labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor()))
      ),
      autocorrect: true,
      initialValue: _tenure,
      validator: (str) =>
      (str.length >= 0)
          ? null
          : "Enter tenure",
      onSaved: (str) => _tenure= str,
      keyboardType: TextInputType.number,
      enabled: true,
      inputFormatters: [
        LengthLimitingTextInputFormatter(10),
      ],

    );

    var submitLeadInfo =
    ButtonTheme(
        minWidth: 50.0,
        height: 50.0,
        child:
        new RaisedButton(
          color: HexColor("#fb6616"),
          shape: new RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(5.0)),
          splashColor: Colors.blue,
          onPressed: _submitLeadInfo,
          child: new Text("SAVE", style: TextStyle(color:Colors.white)),
        ));



    var leadInfo =  Center(
      child: new Form(
        key:formKey1,
        child: ListView(

            padding: EdgeInsets.only(top: 0),

            children: <Widget>[
              new Padding(
                  padding: EdgeInsets.only(left:24,right: 24,top: 10),
                  child:
                  full_name),

              new Padding(
                  padding: EdgeInsets.only(left:24,right: 24,top:10),
                  child:mobile),

              new Padding(
                  padding: EdgeInsets.only(left:24,right: 24,top:10),
                  child:email),

              new Padding(
                  padding: EdgeInsets.only(left:24,right: 24,top:10),
                  child:city),

              new Padding(
                  padding: EdgeInsets.only(left:24,right: 24,top:10),
                  child:loanAmnt),
              new Padding(
                  padding: EdgeInsets.only(left:24,right: 24,top:10),
                  child:tenure),
              new Padding(
                  padding: EdgeInsets.only(left:24,right: 24,top: 20,bottom: 20),
                  child:
                  submitLeadInfo),

            ]
        ),
      ),
    );

    /// personal info tab

    var gender =
          new FormField(
            builder: (FormFieldState state) {
              return InputDecorator(
                decoration:  InputDecoration(
//                  border: const OutlineInputBorder(),
                  labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor())),
                  labelText: "Gender*",
                  hintStyle: TextStyle(color:Colors.grey),
                  contentPadding: const EdgeInsets.symmetric(vertical: 5.0,horizontal: 5.0),
                ),
                child: new DropdownButtonHideUnderline(
                    child:
                    new DropdownButton(
                      value: _currentGenderId,
                      items: <String>['Male', 'Female'].map((String value) {
                        return new DropdownMenuItem<String>(
                          value: value,
                          child: new Text(value),
                        );
                      }).toList(),
                      onChanged: changedGenderDropDownItem,

                    )

                ),
              );
            },
          );

    var dob =   new TextFormField(
      controller: myControllerDOB,
      focusNode: focuNodeDOB ,
//        initialValue: _dob,
        validator: (str) =>
        (str.length > 0)
            ? null
            : "Select Dob",

        decoration:  InputDecoration(
//          border: const OutlineInputBorder(),

            labelText: "Date of Birth*",
            hintStyle: TextStyle(color:Colors.grey),
            labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor()))
        )
    );

    var pancard = new TextFormField(

      decoration:  InputDecoration(
//          border: const OutlineInputBorder(),
          hintText: "Enter Pancard No",
          labelText: "Pancard*",
          hintStyle: TextStyle(color:Colors.grey),
          labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor()))
      ),
      autocorrect: true,
      initialValue: _pancard,
      validator: (str) =>
      (str.length > 0)
          ? null
          :"Enter Pancard",
      onSaved: (str) => _pancard = str,
      keyboardType: TextInputType.text,
      textCapitalization: TextCapitalization.characters,
      enabled: true,
      inputFormatters: [
        LengthLimitingTextInputFormatter(10),
      ],

    );

    var aadharcard = new TextFormField(

      decoration:  InputDecoration(
//          border: const OutlineInputBorder(),
          hintText: "Enter Aadhar No",
          labelText: "Aadhar*",
          hintStyle: TextStyle(color:Colors.grey),
          labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor()))
      ),
      initialValue: _adharcard,
      inputFormatters: [
        LengthLimitingTextInputFormatter(12),
      ],

      autocorrect: true,
      validator: (str) =>
      (str.length > 0)
          ? null
          : "Enter Aadhar No",
      onSaved: (str) => _adharcard = str,
      keyboardType: TextInputType.number,
      enabled: true,
    );

    var address = new TextFormField(

      decoration:  InputDecoration(
//          border: const OutlineInputBorder(),
          hintText: "Enter Address",
          labelText: "Address*",
          hintStyle: TextStyle(color:Colors.grey),
          labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor()))
      ),
      initialValue: _address,
      autocorrect: true,
      validator: (str) =>
      (str.length > 0)
          ? null
          : "Enter Address",
      onSaved: (str) => _address = str,
      keyboardType: TextInputType.text,
      enabled: true,
    );

    var pincode = new TextFormField(

      decoration:  InputDecoration(
//          border: const OutlineInputBorder(),
          hintText: "Enter Pincode",
          labelText: "Pincode*",
          hintStyle: TextStyle(color:Colors.grey),
          labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor()))
      ),
      initialValue: _pincode,
      autocorrect: true,
      validator: (str) =>
      (str.length > 0)
          ? null
          : "Enter Pincode",
      onSaved: (str) => _pincode = str,
      keyboardType: TextInputType.number,
      enabled: true,
      inputFormatters: [
        LengthLimitingTextInputFormatter(6),
      ],

    );

    var submitPersonalInfo =
    ButtonTheme(
        minWidth: 50.0,
        height: 50.0,
        child:
        new RaisedButton(
          color: HexColor("#fb6616"),
          shape: new RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(5.0)),
          splashColor: Colors.blue,
          onPressed: _submitPersonalInfo,
          child: new Text("SAVE", style: TextStyle(color:Colors.white)),
        ));




    var personalInfo =  Center(
      child: new Form(
        key:formKey2,
        child: ListView(

            padding: EdgeInsets.only(top: 0),

            children: <Widget>[
              new Padding(
                  padding: EdgeInsets.only(left:24,right: 24,top: 10),
                  child:
                  gender),

              new Padding(
                  padding: EdgeInsets.only(left:24,right: 24,top:10),
                  child:dob),

              new Padding(
                  padding: EdgeInsets.only(left:24,right: 24,top:10),
                  child:pancard),

              new Padding(
                  padding: EdgeInsets.only(left:24,right: 24,top:10),
                  child:aadharcard),

              new Padding(
                  padding: EdgeInsets.only(left:24,right: 24,top:10),
                  child:address),
              new Padding(
                  padding: EdgeInsets.only(left:24,right: 24,top:10),
                  child:pincode),
              new Padding(
                  padding: EdgeInsets.only(left:24,right: 24,top: 20,bottom: 20),
                  child:
                  submitPersonalInfo),

            ]
        ),
      ),
    );

    var file_login_bank  =
    (_mybank.length > 0)?
    new FormField(
      builder: (FormFieldState state) {
        return InputDecorator(
          decoration:  InputDecoration(
//            border: const OutlineInputBorder(
//
//            ),
            labelText: "File Bank Name",
            hintStyle: TextStyle(color:Colors.grey),
            labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor())),
            contentPadding: const EdgeInsets.symmetric(vertical: 5.0,horizontal: 5.0),
          ),
          child: new DropdownButtonHideUnderline(

              child:
              new DropdownButton(
                value: _currentBankId,
                items: _mybank.map((item) {
                  return new DropdownMenuItem<String>(
                    child: new Text(item['Name']),
                    value: item['BankId'].toString(),
                  );
                }).toList(),
                onChanged: changedBankDropDownItem,
              )

          ),
        );
      },
    ): new Text("Loading");


    var file_login_date  =   new TextFormField(
        controller: myControllerFileDate,
        focusNode: focuNodeFileDate ,
//        initialValue: _dob,
        validator: (str) =>
        (str.length > 0)
            ? null
            : "Enter file login date",

        decoration:  InputDecoration(
//          border: const OutlineInputBorder(),
            labelText: "Login File Date*",
            hintStyle: TextStyle(color:Colors.grey),
            labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor()))
        )
    );




    var bank_application_id = new TextFormField(

      decoration:  InputDecoration(
//          border: const OutlineInputBorder(),
          hintText: "Enter bank application Id",
          labelText: "Bank Application Id",
          hintStyle: TextStyle(color:Colors.grey),
          labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor()))
      ),
      initialValue: _bankApplicationId,
      autocorrect: true,
      validator: (str) =>
    (str.length > 0)
        ? null
        : "Enter bank application Id",
      onSaved: (str) => _bankApplicationId = str,
      keyboardType: TextInputType.text,
      enabled: true,
    );

    var bank_rm_name = new TextFormField(

      decoration:  InputDecoration(
//          border: const OutlineInputBorder(),
          hintText: "Enter Bank RM Name",
          labelText: "Bank RM Name",
          hintStyle: TextStyle(color:Colors.grey),
          labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor()))
      ),
      initialValue: _bankRMName,
      autocorrect: true,
//      validator: (str) =>
//      (str.length >0)
//          ? null
//          : "Enter Bank RM Name",
      onSaved: (str) => _bankRMName = str,
      keyboardType: TextInputType.text,

      enabled: true,
    );


    var bank_rm_phone_no = new TextFormField(

      decoration:  InputDecoration(
//          border: const OutlineInputBorder(),
          hintText: "Enter Bank RM Phone Number",
          labelText: "Bank RM Phone Number",
          hintStyle: TextStyle(color:Colors.grey),
          labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor()))
      ),
      initialValue: _bankRMPhoneNumber,
      autocorrect: true,
//      validator: (str) =>
//      (str.length >= 0)
//          ? null
//          : "Enter Bank RM Phone Number",
      onSaved: (str) => _bankRMPhoneNumber = str,
      keyboardType: TextInputType.number,
      enabled: true,
      inputFormatters: [
        LengthLimitingTextInputFormatter(13),
        WhitelistingTextInputFormatter.digitsOnly,
      ],

    );

    var bank_branch= new TextFormField(

      decoration:  InputDecoration(
//          border: const OutlineInputBorder(),
          hintText: "Enter Bank Branch",
          labelText: "Bank branch",
          hintStyle: TextStyle(color:Colors.grey),
          labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor()))
      ),
      initialValue: _bankBranch,
      autocorrect: true,
      validator: (str) =>
      (str.length > 0)
          ? null
          : "Enter bank branch",
      onSaved: (str) => _bankBranch = str,
      keyboardType: TextInputType.text,
      enabled: true,
    );

    var document_status = new TextFormField(

      decoration:  InputDecoration(
//          border: const OutlineInputBorder(),
          hintText: "Enter Document Status",
          labelText: "Document Status",
          hintStyle: TextStyle(color:Colors.grey),
          labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor()))
      ),
      initialValue: _documentStatus,
      autocorrect: true,
//      validator: (str) =>
//      (str.length > 0)
//          ? null
//          : "Enter doument status",
      onSaved: (str) => _documentStatus = str,
      keyboardType: TextInputType.text,
      enabled: true,
    );


//    var document_status= new FormField(
//      builder: (FormFieldState state) {
//        return InputDecorator(
//          decoration:  InputDecoration(
////                  border: const OutlineInputBorder(),
//            labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor())),
//            labelText: "Document Status",
//            hintStyle: TextStyle(color:Colors.grey),
//            contentPadding: const EdgeInsets.symmetric(vertical: 5.0,horizontal: 5.0),
//          ),
//          child: new DropdownButtonHideUnderline(
//              child:
//              new DropdownButton(
//                value: _documentStatus,
//                items: <String>['Pending', 'Done'].map((String value) {
//                  return new DropdownMenuItem<String>(
//                    value: value,
//                    child: new Text(value),
//                  );
//                }).toList(),
//                onChanged: changedDocumentStatusDropDownItem,
//
//              )
//
//          ),
//        );
//      },
//    );


    var submitLogInnfo =
    ButtonTheme(
        minWidth: 50.0,
        height: 50.0,
        child:
        new RaisedButton(
          color: HexColor("#fb6616"),
          shape: new RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(5.0)),
          splashColor: Colors.blue,
          onPressed: _submitLogInInfo,
          child: new Text("SAVE", style: TextStyle(color:Colors.white)),
        ));



    var loginInfo =  Center(
      child: new Form(
        key:formKey3,
        child: ListView(

            padding: EdgeInsets.only(top: 0),

            children: <Widget>[
              new Padding(
                  padding: EdgeInsets.only(left:24,right: 24,top: 10),
                  child:
                  file_login_bank),

              new Padding(
                  padding: EdgeInsets.only(left:24,right: 24,top:10),
                  child:file_login_date),


              new Padding(
                  padding: EdgeInsets.only(left:24,right: 24,top:10),
                  child:bank_application_id),



              new Padding(
                  padding: EdgeInsets.only(left:24,right: 24,top:10),
                  child:bank_rm_name),

              new Padding(
                  padding: EdgeInsets.only(left:24,right: 24,top:10),
                  child:bank_rm_phone_no),
              new Padding(
                  padding: EdgeInsets.only(left:24,right: 24,top:10),
                  child:bank_branch),
              new Padding(
                  padding: EdgeInsets.only(left:24,right: 24,top: 20),
                  child:
                  document_status),

              new Padding(
                  padding: EdgeInsets.only(left:24,right: 24,top: 20,bottom: 20),
                  child:
                  submitLogInnfo),

            ]
        ),
      ),
    );




    ScreenUtil.instance = ScreenUtil.getInstance()..init(context);

    return WillPopScope(
      child: DefaultTabController(
        length: 3,
        child: Scaffold(
          key: scaffoldKey,
          appBar: AppBar(
              bottom: TabBar(
                controller: myTabController,
                tabs: [
                  Tab(text: "Contact Info",),
                  Tab(text: "Personal Info",),
                  Tab(text: "Login Info",),
                ],
              ),
              title: Text('Application Fill'),backgroundColor: HexColor("#2b2ca3")
          ),
          body: TabBarView(
            controller: myTabController,
            children: [
              (_isLoading)?Align(
                  alignment: Alignment.center,
                  child: CircularProgressIndicator()
              ):leadInfo,
              (_isLoading)?Align(
                  alignment: Alignment.center,
                  child: CircularProgressIndicator()
              ):personalInfo,
              (_isLoading)?Align(
                  alignment: Alignment.center,
                  child: CircularProgressIndicator()
              ):loginInfo,

            ],
          ),
        ),
      ),
    );
  }


  void _showSnackBar(String text,Color color) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(backgroundColor:color ,content: new Text(text),duration: new Duration(milliseconds: 5000),));
  }

  void postExecution(var data ,int result_code)  {

    switch(result_code) {

      case GET_CITY_RESULT_CODE:
        if(data == null)
          _showSnackBar("Unable to get cities", Colors.red);
        else {
          setState(() {
            _mycity = data;
            print(data[0]["CityId"]);
            int id = data[0]["CityId"];
            _currentCityId = id.toString();
          });

          Helper.callGETApi(Uri.encodeFull( Helper.GET_BANK_URL),
              json.encode({

              }), context, this, GET_BANK_RESULT_CODE);

        }
        break;

      case GET_BANK_RESULT_CODE:
        if(data == null)
          _showSnackBar("Unable to get banks", Colors.red);
        else {
          setState(() {
            _mybank = data;
            print(data[0]["BankId"]);
            int id = data[0]["BankId"];
            _currentBankId = id.toString();
          });

          // get application details
          if(loadId.length > 0)
            {
              String url = Helper.GET_APPLICATION_DETAILS_URL+"?LeadId="+loadId;
              print("url="+url);
              Helper.callAuthGetApi(Uri.encodeFull( url),
                  json.encode({

                  }), context, this, GET_APPLOCATION_RESULT_CODE);

            }

        }
        break;

      case GET_APPLOCATION_RESULT_CODE:
        if(data == null)
          _showSnackBar("Unable to get application details", Colors.red);
        else {
          setState(() {
            _name = "Meera";

            print("Data="+data["ContactInfo"].toString());
            print("Data="+data["PersonalInfo"].toString());
            print("Data="+data["LoginInfo"].toString());

            // get contact info
            _name = data["ContactInfo"][0]["FullName"].toString();
            print("Name = "+ _name);
            _mobile = data["ContactInfo"][0]["MobileNo"].toString();
            _email = data["ContactInfo"][0]["EmailId"].toString();
            if(data["ContactInfo"][0]["CityId"] != null && data["ContactInfo"][0]["CityId"].toString().length > 0 &&!(data["ContactInfo"][0]["CityId"]==0) )
            _currentCityId = data["ContactInfo"][0]["CityId"].toString();
            _loanAmnt = data["ContactInfo"][0]["LoanAmountRequired"].toString();
            _tenure = data["ContactInfo"][0]["Tenure"].toString();

            // get personal info
            if(!data["PersonalInfo"][0]["Dob"].toString().contains("0001-01-01"))
            myControllerDOB.text = dateFormat.format(dateFormat1.parse(data["PersonalInfo"][0]["Dob"].toString()));
             if(data["PersonalInfo"][0]["Gender"].toString().length > 0)
            _currentGenderId = data["PersonalInfo"][0]["Gender"].toString();
            _pancard = data["PersonalInfo"][0]["PancardNo"].toString();
            _adharcard = data["PersonalInfo"][0]["AadharCardNo"].toString();
            _address = data["PersonalInfo"][0]["Address"].toString();
            _pincode = data["PersonalInfo"][0]["PinCode"].toString();

            print("bank id ="+ data["LoginInfo"][0]["BankId"].toString());
            print("DocumentStatus ="+ data["LoginInfo"][0]["DocumentStatus"].toString());
            // get login info
            if((data["LoginInfo"][0]["BankId"] != null)&&(!(data["LoginInfo"][0]["BankId"].toString().compareTo("0")== 0))){
              print("Hi");
              // _currentBankId = data["LoginInfo"][0]["BankId"].toString();
              var  _id = data["LoginInfo"][0]["BankId"].toString();
              bool flag = false;
              for(int i = 0;i< _mybank.length;i++)
              {
                if(_mybank[i]['BankId'].toString().compareTo(data["LoginInfo"][0]["BankId"].toString()) == 0)
                {
                  flag = true;
                  break;
                }
              }

              if(flag)
                _currentBankId = data["LoginInfo"][0]["BankId"].toString();

              print("Current bank id ="+_currentBankId);
            }

            if(!data["LoginInfo"][0]["LoginDate"].toString().contains("0001-01-01"))
             myControllerFileDate.text = dateFormat.format(dateFormat1.parse(data["LoginInfo"][0]["LoginDate"].toString()));
            _bankApplicationId = data["LoginInfo"][0]["BankApplicationId"].toString();
            _bankRMName = data["LoginInfo"][0]["BankRMName"].toString();
            _bankRMPhoneNumber = data["LoginInfo"][0]["BankRmPhoneNo"].toString();
            _bankBranch = data["LoginInfo"][0]["BankBranch"].toString();
            if(data["LoginInfo"][0]["DocumentStatus"]!=null && data["LoginInfo"][0]["DocumentStatus"].toString().length > 0)
            _documentStatus = data["LoginInfo"][0]["DocumentStatus"].toString();
             print("bank id ="+_currentBankId);
             if(_documentStatus != null)
            print("DocumentStatus ="+ _documentStatus);

             if (_currentBankId .compareTo("0")==0)
               {
                 _currentBankId = null;
               }
            _isLoading = false;

          });
        }

        break;

      case UPDATE_APPLICATION_RESULT_CODE1:

        try {
          setState(() => _isLoading = false);

          if (data["responseCode"] == Helper.SUCCESS_STATUS_CODE) {
            print("got data");
            _showSnackBar(data["messages"]["messageDescription"], Colors.green);
            myTabController.animateTo(1);
          } else {
            print("Error=" + data["messages"]["messageDescription"]);

            _showSnackBar(data["messages"]["messageDescription"], Colors.red);
          }
          if (data == null)
            print("Unable to get data");
        } catch (e) {
          _showSnackBar("Some problem occurred", Colors.red);
        }

        break;

      case UPDATE_APPLICATION_RESULT_CODE2:

        try {
          setState(() => _isLoading = false);

          if (data["responseCode"] == Helper.SUCCESS_STATUS_CODE) {
            print("got data");
            _showSnackBar(data["messages"]["messageDescription"], Colors.green);
            myTabController.animateTo(2);

          } else {
            print("Error=" + data["messages"]["messageDescription"]);

            _showSnackBar(data["messages"]["messageDescription"], Colors.red);
          }
          if (data == null)
            print("Unable to get data");
        } catch (e) {
          _showSnackBar("Some problem occurred", Colors.red);

        }

        break;

      case UPDATE_APPLICATION_RESULT_CODE3:

        try {
          setState(() => _isLoading = false);

          if (data["responseCode"] == Helper.SUCCESS_STATUS_CODE) {
            print("got data");
            _showSnackBar(data["messages"]["messageDescription"], Colors.green);

            Future.delayed(const Duration(milliseconds: 1000), () {
              setState(() {
                Navigator.pop(context);
              });

            });


          } else {
            print("Error=" + data["messages"]["messageDescription"]);

            _showSnackBar(data["messages"]["messageDescription"], Colors.red);
          }
          if (data == null)
            print("Unable to get data");
        } catch (e) {
          _showSnackBar("Some problem occurred", Colors.red);
        }

        break;



      case ERROR_CODE:
        _showSnackBar("Please check your internet connection ",Colors.red);
        setState(() {
          _isLoading = false;

        });
        break;
    }
  }

  void _showDialog(String title,String msg) {
    // flutter defined function
    showDialog(

      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(title),
          content: new Text(msg),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();

              },
            ),
          ],
        );
      },
    );
  }


}