import 'dart:convert';

import 'package:apanarupee/model/ApiInterface.dart';
import 'package:apanarupee/model/MyEvent.dart';
import 'package:apanarupee/utils/Helper.dart';
import 'package:apanarupee/utils/HexColor.dart';
import 'package:date_utils/date_utils.dart';
import 'package:flutter/material.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
// Example holidays
final Map<DateTime, List> _holidays = {
  DateTime(2019, 1, 1): ['New Year\'s Day'],
  DateTime(2019, 1, 6): ['Epiphany'],
  DateTime(2019, 2, 14): ['Valentine\'s Day'],
  DateTime(2019, 4, 21): ['Easter Sunday'],
  DateTime(2019, 4, 22): ['Easter Monday'],
};

final dateFormat = DateFormat("yyyy/MM/dd");

class Meetings extends StatefulWidget {
  Meetings({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MeetingsState createState() => _MeetingsState();
}

class _MeetingsState extends State<Meetings> with TickerProviderStateMixin implements ApiInterface{
  DateTime _selectedDay;
 // Map<DateTime, List> _events;
  Map<DateTime, List<Event>> _events;
  Map<DateTime, List> _visibleEvents;
  Map<DateTime, List> _visibleHolidays;
  List _selectedEvents;
  AnimationController _controller;
  bool _isLoading = true;
  final scaffoldKey = new GlobalKey<ScaffoldState>();

  static const int GET_MEETINGS_RESULT_CODE = 1,DELETE_MEETINGS_RESULT_CODE = 2,ERROR_CODE = -1;

  @override
  void initState() {
    super.initState();

    // call web service to get meetings data
    _loadData();
    _selectedDay = DateTime.now();

    FirebaseMessaging _firebaseMessaging;

    _firebaseMessaging = FirebaseMessaging();

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print('on message $message');

        // show notification here
//        _showNotificationWithDefaultSound();
        var title = message['notification']['title'];
        var body = message['notification']['body'];
        // show notification here
        _showDialog(title,body);



      },
      onResume: (Map<String, dynamic> message) async {
        print('on resume $message');
      },
      onLaunch: (Map<String, dynamic> message) async {
        print('on launch $message');
      },

    );

  }

  _loadData()async{
    String url = Helper.GET_ALL_MEETINGS_URL+"?Date=";
    print("url="+url);
    Helper.callAuthGetApi(Uri.encodeFull( url),
        json.encode({

        }), context, this, GET_MEETINGS_RESULT_CODE);

  }
  void _onDaySelected(DateTime day, List events) {
    setState(() {
      _selectedDay = day;
      _selectedEvents = events;
    });
  }

  _deleteMeeting(String leadMeetingId)async{

    String url = Helper.GET_DELETE_MEETING+"?LeadMeetingId="+leadMeetingId;
    print("url="+url);
    Helper.callAuthGetApi(Uri.encodeFull( url),
        json.encode({

        }), context, this, DELETE_MEETINGS_RESULT_CODE);


  }


  void _onVisibleDaysChanged(DateTime first, DateTime last, CalendarFormat format) {
    setState(() {
      _visibleEvents = Map.fromEntries(
        _events.entries.where(
              (entry) =>
          entry.key.isAfter(first.subtract(const Duration(days: 1))) &&
              entry.key.isBefore(last.add(const Duration(days: 1))),
        ),
      );

      _visibleHolidays = Map.fromEntries(
        _holidays.entries.where(
              (entry) =>
          entry.key.isAfter(first.subtract(const Duration(days: 1))) &&
              entry.key.isBefore(last.add(const Duration(days: 1))),
        ),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        backgroundColor: Color(HexColor.getAppBlueColor()),
        title: Text(widget.title,style: TextStyle(color: Colors.white),),
      ),
      body: (_isLoading)?Align(
          alignment: Alignment.center,
          child: CircularProgressIndicator()
      ): Column(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          // Switch out 2 lines below to play with TableCalendar's settings
          //-----------------------
          _buildTableCalendar(),
          // _buildTableCalendarWithBuilders(),
          const SizedBox(height: 8.0),
          Expanded(child: _buildEventList()),
        ],
      ),
    );
  }



  // Simple TableCalendar configuration (using Styles)
  Widget _buildTableCalendar() {
    return TableCalendar(

      locale: 'en_US',
      events: _visibleEvents,
//      holidays: _visibleHolidays,
      initialCalendarFormat: CalendarFormat.week,
      formatAnimation: FormatAnimation.slide,
      startingDayOfWeek: StartingDayOfWeek.monday,
      availableGestures: AvailableGestures.all,
      availableCalendarFormats: const {
        CalendarFormat.week: 'Week',
      },
      calendarStyle: CalendarStyle(
        selectedColor: Colors.deepOrange[400],
        todayColor: Colors.deepOrange[200],
        markersColor: Colors.brown[700],
      ),
      headerStyle: HeaderStyle(
        formatButtonTextStyle: TextStyle().copyWith(color: Colors.white, fontSize: 15.0),
        formatButtonDecoration: BoxDecoration(
          color: Colors.deepOrange[400],
          borderRadius: BorderRadius.circular(16.0),
        ),
      ),
      onDaySelected: _onDaySelected,
      onVisibleDaysChanged: _onVisibleDaysChanged,
    );
  }

  // More advanced TableCalendar configuration (using Builders & Styles)
  Widget _buildTableCalendarWithBuilders() {
    return TableCalendar(
      locale: 'pl_PL',
      events: _visibleEvents,
      holidays: _visibleHolidays,
      initialCalendarFormat: CalendarFormat.month,
      formatAnimation: FormatAnimation.slide,
      startingDayOfWeek: StartingDayOfWeek.sunday,
      availableGestures: AvailableGestures.all,
      availableCalendarFormats: const {
        CalendarFormat.month: '',
        CalendarFormat.week: '',
      },
      calendarStyle: CalendarStyle(
        outsideDaysVisible: false,
        weekendStyle: TextStyle().copyWith(color: Colors.blue[800]),
        holidayStyle: TextStyle().copyWith(color: Colors.blue[800]),
      ),
      daysOfWeekStyle: DaysOfWeekStyle(
        weekendStyle: TextStyle().copyWith(color: Colors.blue[600]),
      ),
      headerStyle: HeaderStyle(
        centerHeaderTitle: true,
        formatButtonVisible: false,
      ),
      builders: CalendarBuilders(
        selectedDayBuilder: (context, date, _) {
          return FadeTransition(
            opacity: Tween(begin: 0.0, end: 1.0).animate(_controller),
            child: Container(
              margin: const EdgeInsets.all(4.0),
              padding: const EdgeInsets.only(top: 5.0, left: 6.0),
              color: Colors.deepOrange[300],
              width: 100,
              height: 100,
              child: Text(
                '${date.day}',
                style: TextStyle().copyWith(fontSize: 16.0),
              ),
            ),
          );
        },
        todayDayBuilder: (context, date, _) {
          return Container(
            margin: const EdgeInsets.all(4.0),
            padding: const EdgeInsets.only(top: 5.0, left: 6.0),
            color: Colors.amber[400],
            width: 100,
            height: 100,
            child: Text(
              '${date.day}',
              style: TextStyle().copyWith(fontSize: 16.0),
            ),
          );
        },
        markersBuilder: (context, date, events, holidays) {
          final children = <Widget>[];

          if (events != null) {
            children.add(
              Positioned(
                right: 1,
                bottom: 1,
                child: _buildEventsMarker(date, events),
              ),
            );
          }

          if (holidays != null) {
            children.add(
              Positioned(
                right: -2,
                top: -2,
                child: _buildHolidaysMarker(),
              ),
            );
          }

          return children;
        },
      ),
      onDaySelected: (date, events) {
        _onDaySelected(date, events);
        _controller.forward(from: 0.0);
      },
      onVisibleDaysChanged: _onVisibleDaysChanged,
    );
  }

  Widget _buildEventsMarker(DateTime date, List events) {

    return AnimatedContainer(
      duration: const Duration(milliseconds: 300),
      decoration: BoxDecoration(
        shape: BoxShape.rectangle,
        color: Utils.isSameDay(date, _selectedDay)
            ? Colors.brown[500]
            : Utils.isSameDay(date, DateTime.now()) ? Colors.brown[300] : Colors.blue[400],
      ),
      width: 16.0,
      height: 16.0,
      child: Center(
        child: Text(
          '${events.length}',
          style: TextStyle().copyWith(
            color: Colors.white,
            fontSize: 12.0,
          ),
        ),
      ),
    );
  }

  Widget _buildHolidaysMarker() {
    return Icon(
      Icons.add_box,
      size: 20.0,
      color: Colors.blueGrey[800],
    );
  }

  Widget _buildEventList() {
    return ListView(
      children: _selectedEvents
          .map((event) => Container(
//        decoration: BoxDecoration(
//          border: Border.all(width: 0.8),
//          borderRadius: BorderRadius.circular(10.0),
//        ),
        margin: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
        child: ListTile(
          title:
              new Card(
                child:
              new Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Padding(
                    padding: EdgeInsets.all(5),
                    child:
                    new Row(
                      children: <Widget>[
                        Expanded(
                          flex:1,
                          child:  new Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Text(event.name.toString(),style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black),)
                            ],
                          ) ,
                        ),
                        Expanded(
                          flex:1 ,
                          child:  new Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: <Widget>[
                              GestureDetector( onTap: () {
                                _deleteMeeting(event.id.toString());
                              }, child:  new Icon(Icons.delete_forever,color:Colors.red)
                              ) ,
                            ],
                          ) ,
                        )
                      ],
                    ),
                  ),

                  new Padding(
                    padding: EdgeInsets.all(5),
                    child:
                    new Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Text("Lead Id: "+event.leadId.toString(),style: TextStyle(color: Color(HexColor.getAppDarkGrey())),)
                      ],
                    ) ,

                  ),
                  new Padding(
                    padding: EdgeInsets.all(5),
                    child:
                    new Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Text("Location: "+event.address.toString(),style: TextStyle(color: Color(HexColor.getAppLightGrey())),)
                      ],
                    ) ,

                  ),
                  Divider(),
                  new Padding(
                    padding: EdgeInsets.all(5),
                    child:
                    new Row(
                      children: <Widget>[
                        Expanded(
                          flex:1,
                          child:  new Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              new Row(
                                children: <Widget>[
                                  new Icon(Icons.access_time,color:Color(HexColor.getAppOrangeColor())),
                                  new Text(" "+event.time.toString(),style: TextStyle(color: Colors.grey),)
                                ],
                              )
                            ],
                          ) ,
                        ),
                        Expanded(
                          flex:1 ,
                          child:  new Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: <Widget>[
                              GestureDetector( onTap: () {
                                launch("tel:"+event.mobile.toString());
                              }, child: new Image.asset("assets/call.png",height: 30,) ) ,
                            ],
                          ) ,
                        )
                      ],
                    ),
                  ),


                ],
              ),),
            //  Text("Hi"+event.name.toString()),
          onTap: () => print('$event tapped!'),
        ),
      ))
          .toList(),
    );
  }



  void _showSnackBar(String text,Color color) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(backgroundColor:color ,content: new Text(text),duration: new Duration(milliseconds: 5000),));
  }

  void postExecution(var data ,int result_code)  {

    switch(result_code) {

      case GET_MEETINGS_RESULT_CODE :
        try {
          setState(() => _isLoading = false);

          if (data != null) {
            print("got data");
            //_showSnackBar(data["messages"]["messageDescription"], Colors.green);
            // now get event data here

            List meetingList = data;

            List<String> dates = new List();

            // get all unique dates from list
            for(int i =0;i< meetingList.length;i++)
              {
                bool flag = true;
                for(int j = 0 ; j< dates.length;j++) {
                  if (meetingList[i]["MeetingDate"].toString()== dates[j])
                    {
                      flag = false;
                      break;
                    }
                }
                if(flag)
                {
                  dates.add(meetingList[i]["MeetingDate"].toString());
                }

              }

              for(int j = 0 ; j< dates.length;j++) {
                print ("date = "+ dates[j].toString());
              }

              List<MyEvent> myEvents = new List();

              for(int k = 0;k<dates.length;k++)
                {
                  MyEvent event = new MyEvent();
                  event.date = dates[k].toString();
                  List<Event> eventList = new List();
                  for(int i=0;i<meetingList.length;i++)
                    {
                       if(meetingList[i]['MeetingDate'].toString() == dates[k].toString() ) {
                         String id = meetingList[i]['LeadMeetingId'].toString();
                         String leadId = meetingList[i]['LeadId'].toString();
                         String name = meetingList[i]['Name'].toString();
                         String mobile = meetingList[i]['MobileNo'].toString();
                         String time = meetingList[i]['TimeSlot'].toString();
                         String address = meetingList[i]['Address'].toString();
                         Event event = new Event(
                             id, leadId, name, mobile, time, address);
                         eventList.add(event);
                       }


                    }
                  event.eventsList = eventList;
                  myEvents.add(event);
                }

                print("MyEvent size ="+myEvents.length.toString());

                for(int i = 0 ; i< myEvents.length; i++)
                  {
                    print("Date ="+myEvents[i].date+" event size ="+myEvents[i].eventsList.length.toString());
                  }


//            final Map<DateTime, List> _holidays = {
//              DateTime(2019, 1, 1): ['New Year\'s Day'],
//              DateTime(2019, 1, 6): ['Epiphany'],
//              DateTime(2019, 2, 14): ['Valentine\'s Day'],
//              DateTime(2019, 4, 21): ['Easter Sunday'],
//              DateTime(2019, 4, 22): ['Easter Monday'],
//            };

             _events = new Map();

            for(int i=0;i<myEvents.length;i++)
              {
                Map<DateTime, List<Event>> stuff = {dateFormat.parse(myEvents[i].date): myEvents[i].eventsList};
                _events.addAll(stuff);
              }

            _selectedEvents = _events[_selectedDay] ?? [];
            _visibleEvents = _events;
            _visibleHolidays = _holidays;

            _controller = AnimationController(
              vsync: this,
              duration: const Duration(milliseconds: 400),
            );

            _controller.forward();



          } else {
            print("Error=" + data["messages"]["messageDescription"]);

            _showSnackBar(data["messages"]["messageDescription"], Colors.red);

          }
          if (data == null)
            print("Unable to get data");
        } catch (e) {
          //_showSnackBar("Some problem occurred", Colors.red);
          print("Exception ="+e.toString())  ;    }
        break;
      case DELETE_MEETINGS_RESULT_CODE:
        try {
          setState(() => _isLoading = false);

          if (data["responseCode"] == Helper.SUCCESS_STATUS_CODE) {
            print("got data");
            _showSnackBar(data["messages"]["messageDescription"], Colors.green);
            _loadData();

          } else {
            print("Error=" + data["messages"]["messageDescription"]);

            _showSnackBar(data["messages"]["messageDescription"], Colors.red);
          }
          if (data == null)
            print("Unable to get data");
        } catch (e) {
          _showSnackBar("Some problem occurred", Colors.red);
        }
        break;
      case ERROR_CODE:
        _showSnackBar("Please check your internet connection ",Colors.red);
        setState(() {
          _isLoading = false;

        });
        break;
    }
  }

  void _showDialog(String title,String msg) {
    // flutter defined function
    showDialog(

      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(title),
          content: new Text(msg),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();

              },
            ),
          ],
        );
      },
    );
  }

}

