import 'dart:convert';
import 'dart:io';

import 'package:apanarupee/model/ApiInterface.dart';
import 'package:apanarupee/screens/Dashboard.dart';
import 'package:apanarupee/screens/OTPVerification.dart';
import 'package:apanarupee/screens/Registration.dart';
import 'package:apanarupee/utils/Helper.dart';
import 'package:apanarupee/utils/HexColor.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

class LogIn extends StatefulWidget  {
  @override
  LogInState createState() => LogInState();
}

class LogInState extends State<LogIn> implements ApiInterface{


  static const int SEND_OTP_RESULT_CODE = 1,ERROR_CODE=-1;

  bool _isLoading = false,_checkedLogIn = false;
  String _mobile;
  final formKey = new GlobalKey<FormState>();
  final formKey1 = new GlobalKey<FormState>();

  final scaffoldKey = new GlobalKey<ScaffoldState>();

  void _sendOtp() async
  {
    var form = formKey.currentState;

    if (form.validate()) {
      setState(() => _isLoading = true);
      form.save();
      print("mobile=" + _mobile);

      // call webservice to send otp

      Helper.callApi(Uri.encodeFull(Helper.SEND_OTP_URL),
          json.encode({
            "MobileNumber": this._mobile
          }), context, this, SEND_OTP_RESULT_CODE);

    }

    FirebaseMessaging _firebaseMessaging;

    _firebaseMessaging = FirebaseMessaging();

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print('on message $message');

        // show notification here
//        _showNotificationWithDefaultSound();
        var title = message['notification']['title'];
        var body = message['notification']['body'];
        // show notification here
        _showDialog(title,body);



      },
      onResume: (Map<String, dynamic> message) async {
        print('on resume $message');
      },
      onLaunch: (Map<String, dynamic> message) async {
        print('on launch $message');
      },

    );


  }

  void _checkLoggedIn() async {

    SharedPreferences prefs = await SharedPreferences.getInstance();
    if(prefs.getBool("isLoggedIn") != null) {
      if (prefs.getBool("isLoggedIn")) {
        //  Navigator.of(context).pushReplacementNamed("/home");
       // _navigateToHomeScreen(context);
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) =>   Dashboard(title:"Dashboard")),
        );

      }
    }

    Future.delayed(const Duration(milliseconds: 1000), () {

// Here you can write your code

      setState(() {
        // Here you can write your code for open new view

        setState(() {
          _checkedLogIn = true;
        });

      });

    });


  }

  @override
  void initState() {
    // TODO: implement initState
    _checkLoggedIn();
    super.initState();



  }


  @override
  Widget build(BuildContext context) {

    ScreenUtil.instance = ScreenUtil.getInstance()..init(context);

    final logo = Hero(
        tag: 'hero',
        child:
        new Container(

          height: ScreenUtil.getInstance().setHeight(800),
//          width: 600.0,
          decoration: new BoxDecoration(
            image: new DecorationImage(image: new AssetImage("assets/loginbg.png"), fit: BoxFit.fill,
            ),
          ),
          
          child:
          new Padding(
            padding: EdgeInsets.only(top: ScreenUtil.getInstance().setHeight(100)),
              child:
          Center(
            child: new Image.asset("assets/login.png",width: ScreenUtil.getInstance().setWidth(350),height: ScreenUtil.getInstance().setHeight(350)),
          ),)
        )
    );

//    final mobileFocus = new FocusNode();
//    final passwordFocus = new FocusNode();

    var mobile =

    new TextFormField(

      style: TextStyle(fontSize:ScreenUtil.getInstance().setSp(50) ,color: Colors.black),
      decoration:  InputDecoration(

//        border: const OutlineInputBorder(),
        hintText: "Enter Mobile Number",
        labelText: "Mobile",
        hintStyle: TextStyle(color:Colors.grey,fontSize: ScreenUtil.getInstance().setSp(40)),
        labelStyle: TextStyle(color: Color(HexColor.getAppBlueColor()),fontSize:ScreenUtil.getInstance().setSp(40) )

      ),
//      autocorrect: true,
      validator: (str) =>
      (str.length == 10)
          ? null
          : "Invalid mobile number",
      onSaved: (str) => _mobile = str,
      keyboardType: TextInputType.phone,
      enabled: true,
      inputFormatters: [
        LengthLimitingTextInputFormatter(10),
      ],


    );


    var otpButton =
    ButtonTheme(
        minWidth: 50.0,
        height: 50.0,
        child:

        new RaisedButton(

          color: HexColor("#fb6616"),
          shape: new RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(5.0)),
          splashColor: Colors.blue,
          onPressed: _sendOtp,
          child: new Text("GET OTP", style: TextStyle(color:Colors.white,fontSize: ScreenUtil.getInstance().setSp(50))),


        ));




    return new Scaffold(

        key: scaffoldKey,
        backgroundColor: Colors.white,
        body: new Form(
        key:formKey,
        child:
        (!_checkedLogIn)?
        Align(
            alignment: Alignment.center,
            child: CircularProgressIndicator()
        )
            :
        ListView(

//          shrinkWrap: true,

        padding: EdgeInsets.only(top: 0),

    children: <Widget>[

    logo,
    SizedBox(height: ScreenUtil.getInstance().setHeight(80)),
    new Center(child:new Text("Welcome",style: TextStyle(color: Colors.black,fontSize: ScreenUtil.getInstance().setSp(70)),)),
    new Padding(padding: EdgeInsets.only(top: ScreenUtil.getInstance().setHeight(10)),
    child:
    new Center(
        child:new Text("Log in to access your acount",style: TextStyle(color: Colors.black,fontSize: ScreenUtil.getInstance().setSp(40)),))),

    SizedBox(height: 28.0),
    new Padding(
    padding: EdgeInsets.only(left:ScreenUtil.getInstance().setHeight(72),right: ScreenUtil.getInstance().setHeight(72)),
    child:mobile),
    new Padding(
        padding: EdgeInsets.only(left:ScreenUtil.getInstance().setHeight(72),right: ScreenUtil.getInstance().setHeight(72),top: 2),
        child:new Text("(OTP will be sent on this number)",style: TextStyle(color: Colors.blue,fontSize: ScreenUtil.getInstance().setSp(40))),
    ),
    new Padding(
    padding: EdgeInsets.only(left:ScreenUtil.getInstance().setHeight(72),right: ScreenUtil.getInstance().setHeight(72),top: ScreenUtil.getInstance().setHeight(96)),
    child:
    otpButton),
    SizedBox(height: ScreenUtil.getInstance().setHeight(20)),
            new Center(
              child:
              (_isLoading)?Align(
                  alignment: Alignment.center,
                  child: CircularProgressIndicator()
              ): SizedBox(height: 0.0),
            ),
            new Padding(padding: EdgeInsets.only(top: ScreenUtil.getInstance().setHeight(72)),
                child:
                new Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    new GestureDetector(
                      onTap: () {

                      },
                      child: new Text("Don't have an acount ",style: TextStyle(color: Colors.black,fontSize: ScreenUtil.getInstance().setSp(50)),),
                    ),
                    new Padding(padding: EdgeInsets.only(top: ScreenUtil.getInstance().setHeight(20),bottom: ScreenUtil.getInstance().setHeight(20)),

                    child:
                    new GestureDetector(
                      onTap: () {
                        //Navigator.of(context).popAndPushNamed('/register');
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) =>   Registration()),
                        );

                      },
                      child: new Text("Register Now",style: TextStyle(color: HexColor("#fb6616"),fontSize: ScreenUtil.getInstance().setSp(40),fontWeight: FontWeight.bold),),
                    ))
                  ],
                )),

    ]
    ),

    )
    );
  }

  void _showSnackBar(String text,Color color) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(backgroundColor:color ,content: new Text(text),duration: new Duration(milliseconds: 5000),));
  }

  void postExecution(var data ,int result_code) {

    switch(result_code) {
      case SEND_OTP_RESULT_CODE:
        try {
          setState(() => _isLoading = false);

          if (data["responseCode"] == Helper.SUCCESS_STATUS_CODE) {
            print("got data");

            // send to otp receive screen
             Navigator.push(
             context,
             MaterialPageRoute(builder: (context) =>   OTPVerification(mobile: _mobile)),
             );

          } else {
            print("Error=" + data["messages"]["messageDescription"]);

            _showSnackBar(data["messages"]["messageDescription"], Colors.red);

            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) =>   Registration(mobile: _mobile,)),
            );

          }
          if (data == null)
            print("Unable to get data");
        } catch (e) {
          _showSnackBar("Some problem occurred", Colors.red);
        }
        break;
      case ERROR_CODE:
        _showSnackBar("Please check your internet connection ",Colors.red);

        setState(() {
          _isLoading = false;

        });
        break;
    }
  }

  void _showDialog(String title,String msg) {
    // flutter defined function
    showDialog(

      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(title),
          content: new Text(msg),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();

              },
            ),
          ],
        );
      },
    );
  }



}