import 'dart:convert';

import 'package:apanarupee/model/ApiInterface.dart';
import 'package:apanarupee/model/LogInInfo.dart';
import 'package:apanarupee/utils/Helper.dart';
import 'package:apanarupee/utils/HexColor.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:apanarupee/screens/AddMeetings.dart';
import 'package:apanarupee/screens/ApplicationFill.dart';
import 'package:apanarupee/screens/ChangeStatus.dart';
import 'package:apanarupee/screens/DocumentUpload.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';

import 'ApproveAndDisburse.dart';
import 'CreateNote.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

import 'References.dart';

class LeadInfo extends StatefulWidget  {

  LeadInfo({Key key, this.leadId,this.leadType}) : super(key: key);

  final String leadId;
  final int leadType;


  @override
  LeadInfoState createState() => LeadInfoState(leadId,leadType);
}

class LeadInfoState extends State<LeadInfo> implements ApiInterface {


  static const int GET_LEAD_DETAILS_RESULT_CODE = 1,ADD_NOTES_RESULT_CODE =2,CHANGE_DOC_PICK_RESULT_CODE =3,SEND_SMS_RESULT_CODE=4,ERROR_CODE=-1;
  bool invalidSMS = false;
  int leadType=0;
  String _leadId = "",_name = "",_dayCnt = "",_estEarning = "",_cityName = "",_productName = "",_loanAmnt = "",_mobile = "",_alternateMobile = "",_docFillStatus="0",_bankId = "",_employeeId = "",_productTypeId = "",_bankApplicationId = "";
  bool _isLoading = false,_showNotes = true,isReverse = false;
  bool isSwitched = false;
  FocusNode focusNode;
  ScrollController _scrollController = new ScrollController();
  final dateFormat1 = DateFormat("yyyy-MM-ddThh:mm:ss");
  final dateFormat = DateFormat("dd/MM/yyyy");

  final formKey = new GlobalKey<FormState>();
  final formKey1 = new GlobalKey<FormState>();

  final scaffoldKey = new GlobalKey<ScaffoldState>();

  List _noteList = new List();
  TextEditingController myController1;

  LeadInfoState(String leadId,int leadType){
    this._leadId = leadId;
    this.leadType = leadType;
    print("Lead Id = "+leadId);
    print("Lead Type = "+leadType.toString());
  }

  String getFormmatedDate(String data)
  {
    return dateFormat.format(dateFormat1.parse(data));
  }

  _sendSMS()
  {
    // call webservice to send sms
    Helper.callAuthApi(Uri.encodeFull(Helper.SEND_SMS_URL),
        json.encode({
          "MobileNo": _mobile,
          "Message": _mySmsController.text,


        }), context, this, SEND_SMS_RESULT_CODE);


  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    focusNode = new FocusNode();

    myController1 = new TextEditingController();
    print("Lead Info ="+_leadId);
    _loadData();

    FirebaseMessaging _firebaseMessaging;

    _firebaseMessaging = FirebaseMessaging();

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print('on message $message');

        // show notification here
//        _showNotificationWithDefaultSound();
        var title = message['notification']['title'];
        var body = message['notification']['body'];
        // show notification here
        _showDialog(title,body);



      },
      onResume: (Map<String, dynamic> message) async {
        print('on resume $message');
      },
      onLaunch: (Map<String, dynamic> message) async {
        print('on launch $message');
      },

    );

  }

  _loadData()async
  {
    // call webservice to get lead Info
    if(_leadId.length > 0)
    {
      String url = Helper.GET_LEAD_DETAILS_URL+"?LeadId="+_leadId;
      print("url="+url);
      Helper.callAuthGetApi(Uri.encodeFull( url),
          json.encode({

          }), context, this, GET_LEAD_DETAILS_RESULT_CODE);

    }

  }

  _uploadDocument(){
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) =>   DocumentUpload(leadId: _leadId,productName: _productName,name: _name,)),
    );

  }

  _approvalAndDisbursed(){

    LogInInfo loginInfo = new LogInInfo();
    loginInfo.leadId = _leadId;
    loginInfo.employeeId = _employeeId;
    loginInfo.bankId = _bankId;
    loginInfo.productTypeId = _productTypeId;
    loginInfo.bankApplId = _bankApplicationId;
    loginInfo.loanAmount = _loanAmnt;

    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) =>   ApproveAndDisburse(leadId: _leadId,loanAmnt:_loanAmnt,leadType: leadType.toString(),logInInfo: loginInfo)),
    ).then((value) {
      _loadData();
    });
  }

  _changeDocPickStatus(){
    // call webserive to change doc fill status
    String url = Helper.CHANGE_DOCK_PICK_STATUS_URL+ "?LeadId="+_leadId;
    Helper.callAuthGetApi(Uri.encodeFull(url),
        json.encode({
          "LeadId": _leadId,


        }), context, this, CHANGE_DOC_PICK_RESULT_CODE);

  }

  _editApplication(){
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) =>   ApplicationFill(leadId: _leadId,)),
    ).then((value) {
      _loadData();
    });
  }

  _addReference(){
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) =>   References(leadId: _leadId,)),
    ).then((value) {
      _loadData();
    });
  }

  _scheduleMeetings(){
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) =>   AddMeetings(leadId: _leadId,)),
    );

  }
  _changeStatus(){

    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) =>   ChangeStatus(leadId: _leadId,)),
    );




  }

  _addNotes()
  {
    // call webservice to add notes
    Helper.callAuthApi(Uri.encodeFull(Helper.ADD_NOTES_URL),
        json.encode({
          "Notes": myController1.text.toString(),
          "LeadId": this._leadId,

        }), context, this, ADD_NOTES_RESULT_CODE);

  }


  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance = ScreenUtil.getInstance()
      ..init(context);

    final logo = Hero(
        tag: 'hero',
        child:
        new Container(

            height: MediaQuery
                .of(context)
                .size
                .height,
//          width: 600.0,
            decoration: new BoxDecoration(
              image: new DecorationImage(
                image: new AssetImage("assets/earndashboard.png"),
                fit: BoxFit.fill,
              ),
            ),

            child:
            new Column(
              children: <Widget>[
                AppBar
                  (
                    elevation: 0.0,
                    backgroundColor: Colors.transparent,
                    iconTheme: IconThemeData(
                        color: Colors.white),
                    title: Text("Lead Info", style: TextStyle(
                        fontSize: ScreenUtil.getInstance().setSp(60)),)),
                new Padding(padding: EdgeInsets.only(top:5),
                child:
                new Container(
                    padding: EdgeInsets.only(top:10),
                    height: MediaQuery
                        .of(context)
                        .size
                        .height-100,
                    decoration: new BoxDecoration(
                      color: Colors.white,
                      borderRadius: new BorderRadius.circular(25.0),
                    ),
                    child:
                   // Center(
//                        child:
//                        Card(
//                          child:
                    new Form(
                              key:formKey,
                              child: SingleChildScrollView(
                                child:
                                new Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      new Padding(
                                          padding: EdgeInsets.only(left:24,right: 24,top:5),
                                          child:
                                          new Row(
                                            children: <Widget>[
                                              Expanded(
                                                  flex: 8,
                                                  child:
                                                  new Column(
                                                      mainAxisAlignment: MainAxisAlignment.start,
                                                      crossAxisAlignment: CrossAxisAlignment.start,

                                                      children: <Widget>[
                                                        new Text(_name,style: TextStyle(fontWeight: FontWeight.bold,fontSize: 18),),
                                                        new Padding(padding: EdgeInsets.only(top: 5),
                                                            child:
                                                            new Text("Lead Id: "+_leadId,style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16),)),
                                                  new Padding(padding: EdgeInsets.only(top: 5),
                                                      child:
                                                        new Row(
                                                          children: <Widget>[
                                                            new Text("Loan Amount: ",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16),),
                                                            new Padding(padding: EdgeInsets.only(bottom: 4,top: 3),
                                                                child:
                                                                new Container(
                                                                  height: ScreenUtil.getInstance().setHeight(32),
                                                                  child:
                                                                  new Image.asset("assets/brupee.png"),padding: EdgeInsets.all(0),) ),
                                                            new Text(_loanAmnt,style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16),),
                                                          ],
                                                        ),),
                                                        (_alternateMobile.length > 0)?
                                                        new Padding(padding: EdgeInsets.only(top: 5),
                                                            child:
                                                            new Text("Alternate No: "+_alternateMobile,style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16),))
                                                        :SizedBox(height: 5,),
                                                      ])
                                              ),

                                              Expanded(
                                                  flex: 2,
                                                  child:
                                                  new Column(
                                                      children: <Widget>[
                                                        GestureDetector( onTap: () {
                                                          launch("tel:"+_mobile);
                                                        }, child: new Image.asset("assets/call.png",height: 30,) ) ,
                                                        GestureDetector( onTap: () {



                                                          showDialog(
                                                              context: context,
                                                              builder: (_) => new AlertDialog(

                                                                title:
                                                                new Column(
                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                                  children: <Widget>[
                                                                    new Text("Send SMS To"),
                                                                    Text(_mobile,style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold, fontSize: ScreenUtil.getInstance().setSp(40) ),),
                                                                  ],
                                                                ) ,

                                                                content:
                                                                new Container(

                                                                  height: 100,
                                                                  child:

                                                                  new Column(
                                                                    mainAxisAlignment: MainAxisAlignment.start,
                                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                                    children: <Widget>[

                                                                      TextField(
                                                                        controller: _mySmsController,
                                                                        decoration: InputDecoration(hintText: "SMS text"),

                                                                      ),
//                                                                      (invalidSMS)?
//                                                                      new Padding(padding: EdgeInsets.only(top: 5),
//                                                                      child:
//                                                                      new Text("Enter SMS text",style: TextStyle(color: Colors.red,fontSize: ScreenUtil.getInstance().setSp(40)),)):new SizedBox(height: 0,),
                                                                    ],
                                                                  ),
                                                                ),
                                                                actions: <Widget>[
                                                                  new Padding(padding: EdgeInsets.all(5),
                                                                      child:
                                                                      new FlatButton(
                                                                        child: new Text('CANCEL'),
                                                                        onPressed: () {
                                                                          Navigator.of(context).pop();
                                                                          setState(() {
                                                                            invalidSMS = false;
                                                                          });
                                                                        },
                                                                      )
                                                                  ),
                                                                  new Padding(padding: EdgeInsets.all(5),
                                                                      child:
                                                                      new FlatButton(
                                                                        child: new Text('SEND'),
                                                                        onPressed: () {
                                                                          print("SMS text size ="+_mySmsController.text.toString().length.toString());
                                                                          if(_mySmsController.text.toString().length > 0) {
                                                                             _sendSMS();
                                                                          }
                                                                          else
                                                                          {
                                                                           _showSnackBar("Enter sms text", Colors.red);
                                                                          }
                                                                        },
                                                                      )
                                                                  )
                                                                ],
                                                              )
                                                          );

                                                        }, child: Padding(padding: EdgeInsets.only(top: 7),child:new Image.asset("assets/sms.png",height: 30,)) ) ,
                                                        SizedBox(height: 15,),
                                          (_alternateMobile.length > 0)?
                                                        GestureDetector( onTap: () {
                                                          launch("tel:"+_alternateMobile);
                                                        }, child: new Image.asset("assets/call.png",height: 30,) )
                                                       : SizedBox(height: 5,),
                                                      ])
                                              ),
                                            ],

                                          )
                                      ),

                                      Divider(color: Colors.grey,),

                                      new Padding(
                                        padding: EdgeInsets.only(left:24,right: 24,top:10),
                                        child:new Text("Product Name",style: TextStyle(color:Colors.grey,fontWeight: FontWeight.bold,fontSize: 14),),
                                      ),

                                      new Padding(
                                        padding: EdgeInsets.only(left:24,right: 24,top:5),
                                        child:new Text(_productName,style: TextStyle(color:Colors.black,fontWeight: FontWeight.bold,fontSize: 16),),
                                      ),


                                      new Padding(
                                          padding: EdgeInsets.only(left:24,right: 24),
                                          child:
                                          new Row(
                                            children: <Widget>[
                                              Expanded(
                                                  flex: 6,
                                                  child:
                                                  new Column(
                                                      mainAxisAlignment: MainAxisAlignment.start,
                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                      mainAxisSize: MainAxisSize.max,
                                                      children: <Widget>[
                                                        new Padding(
                                                          padding: EdgeInsets.only(top:10),
                                                          child:new Text("City Name",style: TextStyle(color:Colors.grey,fontWeight: FontWeight.bold,fontSize: 14),),
                                                        ),

                                                        new Padding(
                                                          padding: EdgeInsets.only(top:5),
                                                          child:new Text(_cityName,style: TextStyle(color:Colors.black,fontWeight: FontWeight.bold,fontSize: 16),),
                                                        ),

                                                      ])
                                              ),
                                              Expanded(
                                                  flex: 4,
                                                  child:
                                                  new Column(
                                                      mainAxisAlignment: MainAxisAlignment.end,
                                                      crossAxisAlignment: CrossAxisAlignment.end,
                                                      mainAxisSize: MainAxisSize.max,
                                                      children: <Widget>[
                                                        new Padding(
                                                          padding: EdgeInsets.only(top:10),
                                                          child:new Text("Created",style: TextStyle(color:Colors.grey,fontWeight: FontWeight.bold,fontSize: 14),),
                                                        ),

                                                        new Padding(
                                                          padding: EdgeInsets.only(top:5),
                                                          child:new Text(_dayCnt+" days ago",style: TextStyle(color:Colors.black,fontWeight: FontWeight.bold,fontSize: 16),),
                                                        ),

                                                      ])
                                              )

                                            ],

                                          )
                                      ),
                                      Padding(
                                          padding: EdgeInsets.only(left:24,right:24,top: 5),
                                          child:new Row(
                                            children: <Widget>[
                                              Expanded(
                                                flex: 1,
                                               child:
                                               new Row(
                                                 children: <Widget>[


                                              (_docFillStatus.toString().compareTo("1") == 0)?
                                              new Text ("Doc Pick Up",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16),):SizedBox(height: 0,),
                                              new Container(
                                                  width: 80,
                                                  child:
                                                  ((_docFillStatus.toString().compareTo("1") == 0)?
                                                  Switch(
                                                    value: isSwitched,
                                                    onChanged: (value) {
                                                      setState(() {
                                                        isSwitched = value;

                                                        if(value)
                                                          _changeDocPickStatus();
                                                      });
                                                    },
                                                    activeTrackColor: Colors.lightGreenAccent,
                                                    activeColor: Colors.green,
                                                  ):
                                                  SizedBox(height: 0,)
                                                  )
                                              )
                                                 ],
                                               )
                                              ),
                                              Expanded(
                                                flex: 1,
                                                child:
                                                Padding(padding: EdgeInsets.only(right: 10,left: 10),
                                                    child:
                                                    ButtonTheme(
                                                        minWidth: 30.0,
                                                        height: 30.0,
                                                        padding: EdgeInsets.only(right: 10,left: 10),
                                                        child:
                                                        (leadType == 5)?
                                                        new SizedBox(height: 0,width: 0):
                                                        new RaisedButton(
                                                          color:Color( HexColor.getAppBlueColor()),
                                                          shape: new RoundedRectangleBorder(
                                                              borderRadius: new BorderRadius.circular(1.0)),
                                                          splashColor: Colors.blue,
                                                          onPressed: _addReference,
                                                          child: new Text("Add References", style: TextStyle(color:Colors.white)),
                                                        )))

                                              )

                                            ],
                                          )
                                      )
                                      ,
                                      Divider(color: Colors.grey,),
                                      new Padding(padding: EdgeInsets.only(left: 24,right: 24,top:5),
                                          child:
                                          new Row(
                                            children: <Widget>[
                                              Expanded(
                                                  flex: 5,
                                                  child:
                                                  Padding(padding: EdgeInsets.only(right: 10,left: 10),
                                                      child:
                                                      ButtonTheme(
                                                          minWidth: 30.0,
                                                          height: 30.0,
                                                          padding: EdgeInsets.only(right: 10,left: 10),
                                                          child:
                                                          (leadType == 5)?
                                                          new SizedBox(height: 0,width: 0):
                                                          new RaisedButton(
                                                            color:Color( HexColor.getAppBlueColor()),
                                                            shape: new RoundedRectangleBorder(
                                                                borderRadius: new BorderRadius.circular(1.0)),
                                                            splashColor: Colors.blue,
                                                            onPressed: _uploadDocument,
                                                            child: new Text("Upload Document", style: TextStyle(color:Colors.white)),
                                                          )))
                                              ),
                                              Expanded(
                                                  flex: 5,
                                                  child:
                                                  Padding(padding: EdgeInsets.only(right: 10,left: 10),
                                                      child:
                                                      ButtonTheme(
                                                          minWidth: 30.0,
                                                          height: 30.0,
                                                          padding: EdgeInsets.only(right: 10,left: 10),
                                                          child:
                                                          ((leadType == 3)||(leadType == 4) || (leadType == 5))?
                                                          new SizedBox(height: 0,width: 0):
                                                          new RaisedButton(
                                                            color:Color( HexColor.getAppBlueColor()),
                                                            shape: new RoundedRectangleBorder(
                                                                borderRadius: new BorderRadius.circular(1.0)),
                                                            splashColor: Colors.blue,
                                                            onPressed: _editApplication,
                                                            child: new Text("Edit Application", style: TextStyle(color:Colors.white)),
                                                          )))
                                              ),

                                            ],
                                          )),
                                      ((leadType == 2)|| (leadType == 3) || (leadType == 4))?
                                       new Padding(padding: EdgeInsets.only(left: 24,right: 24,top:5),
                                          child:
                                          new Row(
                                            children: <Widget>[
                                              Expanded(
                                                  flex: 5,
                                                  child:
                                                  Padding(padding: EdgeInsets.only(right: 10,left: 10),
                                                      child:
                                                      ButtonTheme(
                                                          minWidth: 30.0,
                                                          height: 30.0,
                                                          padding: EdgeInsets.only(right: 10,left: 10),
                                                          child:
                                                          new RaisedButton(
                                                            color:Color( HexColor.getAppBlueColor()),
                                                            shape: new RoundedRectangleBorder(
                                                                borderRadius: new BorderRadius.circular(1.0)),
                                                            splashColor: Colors.blue,
                                                            onPressed: _approvalAndDisbursed,
                                                            child: new Text("Approval & Disbursement", style: TextStyle(color:Colors.white)),
                                                          )))
                                              )

                                            ],
                                          )):new SizedBox(height: 0,width: 0),
                                      ((leadType == 3)||(leadType == 4)||(leadType == 5))?
                                      new SizedBox(height: 0,width: 0):
                                      new Padding(
                                          padding: EdgeInsets.only(left: 14,right: 14,top: 5),
                                          child:
                                          new Container(
                                              color: Colors.grey,
                                              child:
                                              new FlatButton(onPressed: (){
                                                _changeStatus();
                                              },
                                                  child:
                                                  new Row(
                                                    children: <Widget>[
                                                      Expanded(
                                                          flex: 8,
                                                          child:
                                                          new Row(
                                                              mainAxisAlignment: MainAxisAlignment.start,
                                                              crossAxisAlignment: CrossAxisAlignment.start,

                                                              children: <Widget>[
                                                                Padding(
                                                                    padding: EdgeInsets.only(left: 10),
                                                                    child:
                                                                    new Icon(Icons.settings,color: Colors.white,) ),
                                                                Padding(
                                                                    padding: EdgeInsets.only(left: 10),
                                                                    child:
                                                                    new Text("Change Status",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 18,color: Colors.white))),
                                                              ])
                                                      ),
                                                      Expanded(
                                                          flex: 2,
                                                          child:
                                                          new Row(
                                                              mainAxisAlignment: MainAxisAlignment.end,
                                                              crossAxisAlignment: CrossAxisAlignment.end,

                                                              children: <Widget>[
                                                                new Icon(Icons.keyboard_arrow_right,color: Colors.white,) ,
                                                              ])
                                                      ),
                                                    ],
                                                  )))
                                      ),
                                      (leadType == 5)?
                                      new SizedBox(height: 0,width: 0):
                                      new Padding(
                                          padding: EdgeInsets.only(left: 14,right: 14,top: 5),
                                          child:
                                          new Container(
                                              color: Colors.grey,
                                              child:
                                              new FlatButton(onPressed: (){

                                                setState(() {
                                                  _scheduleMeetings();
                                                });

                                              }, child:

                                              new Row(
                                                children: <Widget>[
                                                  Expanded(
                                                      flex: 8,
                                                      child:
                                                      new Row(
                                                          mainAxisAlignment: MainAxisAlignment.start,
                                                          crossAxisAlignment: CrossAxisAlignment.start,
//                                  mainAxisSize: MainAxisSize.max,
                                                          children: <Widget>[
                                                            Padding(
                                                                padding: EdgeInsets.only(left: 10),
                                                                child:

                                                                new Icon(Icons.av_timer,color: Colors.white,) ),
                                                            Padding(
                                                                padding: EdgeInsets.only(left: 10),
                                                                child:
                                                                new Text("Schedule Meeting",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 18,color: Colors.white),)),
                                                          ])
                                                  ),
                                                  Expanded(
                                                      flex: 2,
                                                      child:
                                                      new Row(
                                                          mainAxisAlignment: MainAxisAlignment.end,
                                                          crossAxisAlignment: CrossAxisAlignment.end,
//                                  mainAxisSize: MainAxisSize.max,
                                                          children: <Widget>[
                                                            new Icon(Icons.keyboard_arrow_right,color: Colors.white,) ,
                                                          ])
                                                  ),
                                                ],
                                              )))
                                      ),
                                      (leadType == 5)?
                                      new SizedBox(height: 0,width: 0):
                                      new Padding(
                                          padding: EdgeInsets.only(left: 14,right: 14,top:5,bottom: 0),
                                          child:
                                          new Container(
                                              color: Colors.grey,
                                              child:

                                              new FlatButton(onPressed: (){

                                                setState(() {
                                                  print("Hi");
                                                  if(_showNotes){
                                                    _showNotes = false;}
                                                  else {
                                                    _showNotes = true;
                                                  }
                                                });

                                              }, child:
                                              new Row(
                                                children: <Widget>[
                                                  Expanded(
                                                      flex: 8,
                                                      child:
                                                      new Row(
                                                          mainAxisAlignment: MainAxisAlignment.start,
                                                          crossAxisAlignment: CrossAxisAlignment.start,

                                                          children: <Widget>[
                                                            Padding(
                                                                padding: EdgeInsets.only(left: 10),
                                                                child:

                                                                new Icon(Icons.note,color: Colors.white,) ),
                                                            Padding(
                                                                padding: EdgeInsets.only(left: 10),
                                                                child:
                                                                new Text("Notes",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 18,color: Colors.white),)),
                                                          ])
                                                  ),
                                                  Expanded(
                                                      flex: 2,
                                                      child:
                                                      new Row(
                                                          mainAxisAlignment: MainAxisAlignment.end,
                                                          crossAxisAlignment: CrossAxisAlignment.end,

                                                          children: <Widget>[
                                                            (_showNotes)?new Icon(Icons.keyboard_arrow_down,color: Colors.white,):new Icon(Icons.keyboard_arrow_right,color: Colors.white,) ,
                                                          ])
                                                  ),
                                                ],
                                              )))
                                      ),
                                      (leadType == 5)?
                                      new SizedBox(height: 0,width: 0):
                                      (_showNotes)?
                                      new Padding(padding: EdgeInsets.only(left: 24,right: 24,bottom: 0),
                                          child:
                                          new FlatButton(onPressed: (){
                                            Navigator.push(
                                              context,
                                              MaterialPageRoute(builder: (context) =>  CreateNote(leadId: _leadId,)),
                                            ).then((value) {
                                              _loadData();
                                            });

                                          },
                                              child:
                                              new Center(child:
                                              new Row(

                                                children: <Widget>[
//                                                  new Padding(padding: EdgeInsets.all(5),
//                                                      child:
                                                  new Icon(Icons.add,color:Color(HexColor.getAppBlueColor())),
//                                                      )),
//                                                  new Padding(padding: EdgeInsets.all(5),
//                                                    child:

                                                  new Text("Add Note",style: TextStyle(color:Color(HexColor.getAppBlueColor()) ),),
//                                                  ),


                                                ],
                                              )
                                              )

                                          )):new SizedBox(height: 40),
                                      (leadType == 5)?
                                      new SizedBox(height: 0,width: 0):
                                      (_showNotes)?
                                      new Padding(padding: EdgeInsets.only(top: 0,left: 24,right: 24,bottom: 5),
                                          child:
                                          (_noteList != null || _noteList.length > 0)?
                                          new Container(
                                              height: (_noteList.length*85).toDouble(),
                                              child:

                                              new ListView.builder
                                                (

                                                  physics: NeverScrollableScrollPhysics(),
                                                  controller: _scrollController,
                                                  itemCount: _noteList.length,
                                                  itemBuilder: (BuildContext ctxt, int index) {
                                                    return
                                                      new Padding(padding: EdgeInsets.only(bottom: 10),
                                                          child:
                                                          new Card(
                                                              child:
                                                              new Row(
                                                                children: <Widget>[
                                                                  Expanded(
                                                                    flex: 8,
                                                                    child: new Column(
                                                                      mainAxisAlignment: MainAxisAlignment.start,
                                                                      crossAxisAlignment: CrossAxisAlignment.start
                                                                      ,
                                                                      children: <Widget>[
                                                                        new Padding(padding: EdgeInsets.only(top: 5,left: 5),
                                                                            child:
                                                                            Container(

                                                                                child:
                                                                                SingleChildScrollView(
                                                                                    child:
                                                                                    new Text(_noteList[index]['Notes'].toString(),style: TextStyle(color: Colors.black,fontSize: 16),)
                                                                                ))),
                                                                        new Padding(padding: EdgeInsets.only(top: 5,bottom: 10,left: 5),
                                                                            child:
                                                                            new Text(getFormmatedDate(_noteList[index]['CreatedDateTime'].toString()),style: TextStyle(color: Colors.grey,fontSize: 12),)
                                                                        ),

                                                                      ],
                                                                    ),
                                                                  ),

                                                                ],
                                                              )));
                                                    //new Text(_noteList[0]['Notes'].toString(),style: TextStyle(color: Colors.black),);
                                                  }
                                              )):
                                          SizedBox(height: 0,width: 0,)
                                      ):SizedBox(height: 0,width: 0,),

                                    ]
                                ),
                              )),
//                        )
//                    )

               ))

              ],
            )


        )
    );

//    return new WebviewScaffold(
//      url: url,
//      appBar:  AppBar
//        (
//          elevation: 0.0,
//          backgroundColor: Color(HexColor.getAppBlueColor()),
//          title: Text(title, style: TextStyle(
//              fontSize: ScreenUtil.getInstance().setSp(60)),)),
//    );

    return new Scaffold(

        key: scaffoldKey,
        backgroundColor: Colors.white,

        body:
        new Form(

          child: ListView(

              padding: EdgeInsets.only(top: 0),

              children: <Widget>[
                logo,


              ]
          ),

        )
    );
  }

  void _showSnackBar(String text,Color color) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(backgroundColor:color ,content: new Text(text),duration: new Duration(milliseconds: 5000),));
  }

  void postExecution(var data ,int result_code)  {

    switch(result_code) {

      case GET_LEAD_DETAILS_RESULT_CODE:
        if(data == null)
          _showSnackBar("Unable to get application details", Colors.red);
        else {
          setState(() {
            print(data.toString());
            _name = data["Name"].toString();
            _mobile = data["MobileNo"].toString();
            _alternateMobile = data["alternatemobile"].toString();
            _loanAmnt = data["LoanAmount"].toString();
            _productName = data["ProductName"].toString();
            _cityName = data["City"].toString();
            _estEarning = data["CommissionAmount"].toString();
            _bankId = data["BankId"].toString();
            _employeeId = data["EmployeeId"].toString();
            _productTypeId = data["ProductTypeId"].toString();
            _bankApplicationId = data["BankApplicationId"].toString();

            _dayCnt = data["daysCnt"].toString();
            _docFillStatus = data["DocFillStatus"].toString();

            _noteList = data["ArrayOfNotes"];
            print("Note size="+_noteList.length.toString());
            // print("Note size="+_noteList[0]['Notes'].toString());
            _isLoading = false;

          });
        }


        break;
      case ADD_NOTES_RESULT_CODE:
        try{
          setState(() => _isLoading = false);

          if (data["responseCode"] == Helper.SUCCESS_STATUS_CODE) {
            print("got data");
            //_showSnackBar(data["messages"]["messageDescription"], Colors.green);

            // call webservice to get lead Info
            if(_leadId.length > 0)
            {
              String url = Helper.GET_LEAD_DETAILS_URL+"?LeadId="+_leadId;
              print("url="+url);
              Helper.callAuthGetApi(Uri.encodeFull( url),
                  json.encode({

                  }), context, this, GET_LEAD_DETAILS_RESULT_CODE);
            }
          } else {
            print("Error=" + data["messages"]["messageDescription"]);

            _showSnackBar(data["messages"]["messageDescription"], Colors.red);
          }
          if (data == null)
            print("Unable to get data");
        } catch (e) {
          _showSnackBar("Some problem occurred", Colors.red);
        }
        break;
      case SEND_SMS_RESULT_CODE:

        setState(() {
          invalidSMS = false;
        });

        Navigator.of(context).pop();
        try {
          print(data.toString());
          setState(() => _isLoading = false);

          if (data["responseCode"] == Helper.SUCCESS_STATUS_CODE) {
            print("got data");
          //  Navigator.pop(context);
            _showSnackBar(data["messages"]["messageDescription"], Colors.green);

          } else {
            print("Error=" + data["messages"]["messageDescription"]);

            _showSnackBar(data["messages"]["messageDescription"], Colors.red);
          }
          if (data == null)
            print("Unable to get data");
        } catch (e) {
          _showSnackBar("Some problem occurred", Colors.red);
        }
        break;
      case CHANGE_DOC_PICK_RESULT_CODE:
        {
          try {
            print(data.toString());
            setState(() => _isLoading = false);

            if (data["responseCode"] == Helper.SUCCESS_STATUS_CODE) {
              print("got data");
              _showSnackBar(data["messages"]["messageDescription"], Colors.green);
              if(_leadId.length > 0)
              {
                String url = Helper.GET_LEAD_DETAILS_URL+"?LeadId="+_leadId;
                print("url="+url);
                Helper.callAuthGetApi(Uri.encodeFull( url),
                    json.encode({

                    }), context, this, GET_LEAD_DETAILS_RESULT_CODE);

              }


            } else {
              print("Error=" + data["messages"]["messageDescription"]);

              _showSnackBar(data["messages"]["messageDescription"], Colors.red);
            }
            if (data == null)
              print("Unable to get data");
          } catch (e) {
            _showSnackBar("Some problem occurred", Colors.red);
          }
        }
        break;
      case ERROR_CODE:
        _showSnackBar("Please check your internet connection ",Colors.red);
        setState(() {
          _isLoading = false;

        });
        break;
    }
  }

  TextEditingController _mySmsController = new TextEditingController();

  _displaySMSDialog(BuildContext context) async {

    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Send SMS'),
            content:
            new Column(
              children: <Widget>[
                Text("("+_mobile+")",style: TextStyle(color: Colors.black),),
                TextField(
                  controller: _mySmsController,
                  decoration: InputDecoration(hintText: "SMS text"),
                ),
                (invalidSMS)? new Text("Enter SMS text"):new SizedBox(height: 0,),
              ],
            ),

            actions: <Widget>[
              new Row(children: <Widget>[
                Expanded(
                    flex: 1,
                    child:
                    new Padding(padding: EdgeInsets.all(5),
                        child:
                        new FlatButton(
                          child: new Text('CANCEL'),
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                        )
                    )
                ),
                Expanded(
                    flex: 1,
                    child:
                    new Padding(padding: EdgeInsets.all(5),
                        child:
                        new FlatButton(
                          child: new Text('SEND'),
                          onPressed: () {
                            print("SMS text size ="+_mySmsController.text.toString().length.toString());
                            if(_mySmsController.text.toString().length > 0) {
                             // _sendSMS();
                            }
                            else
                              {
                                setState(() {
                                  invalidSMS = true;
                                });

                              }
                          },
                        )
                    )
                )
              ],)

            ],
          );
        });
  }


  void _showDialog(String title,String msg) {
    // flutter defined function
    showDialog(

      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(title),
          content: new Text(msg),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();

              },
            ),
          ],
        );
      },
    );
  }



}
