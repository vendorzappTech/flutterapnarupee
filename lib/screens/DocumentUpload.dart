import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:apanarupee/model/UploadFile.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:intl/intl.dart';
import 'package:apanarupee/model/ApiInterface.dart';
import 'package:apanarupee/model/User.dart';
import 'package:apanarupee/utils/Helper.dart';
import 'package:apanarupee/utils/HexColor.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:image_picker/image_picker.dart';
import 'package:dio/dio.dart';
import 'package:permission_handler/permission_handler.dart';
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:flutter/foundation.dart';
//import 'package:open_file/open_file.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:progress_dialog/progress_dialog.dart';

class DocumentUpload extends StatefulWidget  {

  DocumentUpload({Key key, this.leadId,this.productName,this.name}) : super(key: key);

  final String leadId,productName,name;

  @override
  DocumentUploadState createState() => DocumentUploadState(this.leadId,this.productName,this.name);
}

class DocumentUploadState extends State<DocumentUpload> implements ApiInterface {

  final scaffoldKey = new GlobalKey<ScaffoldState>();
  bool _isLoading = false;
  bool isDownLoadingFile = false;
  String _productName = "",
      _name = "",
      _currentPickUpStatus = "PENDING",
      leadId = "";
  static const DELETE_FILE_RESULT_CODE = 1,
      GET_DOCUMENTS_RESULT_CODE = 2,
      ERROR_CODE = -1;
  File file;
  List<UploadFile> poiList = new List();

  List<UploadFile> poaList = new List();
  double progress = 0;


  DocumentUploadState(String leadId, String productName, String name) {
    this.leadId = leadId;
    this._productName = productName;
    this._name = name;
  }

  @override
  void initState() {
    // TODO: implement initState
    // call webservices to get location

    // call webservice  to get document details
    print("in upload document");
    setState(() {
      _isLoading = true;
    });

    _loadData();

    FirebaseMessaging _firebaseMessaging;

    _firebaseMessaging = FirebaseMessaging();

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print('on message $message');

        // show notification here
//        _showNotificationWithDefaultSound();
        var title = message['notification']['title'];
        var body = message['notification']['body'];
        // show notification here
        _showDialog(title,body);



      },
      onResume: (Map<String, dynamic> message) async {
        print('on resume $message');
      },
      onLaunch: (Map<String, dynamic> message) async {
        print('on launch $message');
      },

    );


  }

  _loadData() async
  {
    Dio dio = new Dio();

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString("accessToken");
    String url = Helper.GET_DOCUMENTS_URL + "?LeadId=" + leadId;
    Response response = await dio.get(url, options: Options(
        method: 'GET',
        responseType: ResponseType.json,
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/json",
          "Authorization": "Basic " + token
        }
    ));

    try {
      var data = json.decode(response.toString());
//      setState(() => _isLoading = false);

      if (data["responseCode"] == Helper.SUCCESS_STATUS_CODE) {
        print("got data=" + data.toString());
        //_showSnackBar(data["messages"]["messageDescription"], Colors.green);

        if (data['DocumentDetails'] != null) {
          var list = data['DocumentDetails'] as List;
          List<UploadFile> uploadFileList = list.map((i) =>
              UploadFile.fromJson(i)).toList();
          print("SIZE=" + uploadFileList.length.toString());

          poiList.clear();
          poaList.clear();
          for (int i = 0; i < uploadFileList.length; i++) {
            if (uploadFileList[i].catName.toString() == "KYC Documents") {
              poaList.add(uploadFileList[i]);
            }
            if (uploadFileList[i].catName.toString() == "Proof of identity") {
              poiList.add(uploadFileList[i]);
            }
          }

          print("SIZE1=" + poaList.length.toString());
          print("SIZE2=" + poiList.length.toString());
        }
      } else {
        print("Error=" + data["messages"]["messageDescription"]);

        _showSnackBar(data["messages"]["messageDescription"], Colors.red);
      }
      if (data == null)
        print("Unable to get data");
    } catch (e) {
      print(e.toString());
    }
    setState(() => _isLoading = false);
//    if((objectpr != null)&&(objectpr.isShowing()))
//      objectpr.hide();
  }

  _uploadFileFromGallery(String docId) async {
    file = await ImagePicker.pickImage(source: ImageSource.gallery);
    upload(docId);
  }

  _uploadFileFromCamera(String docId) async {
    file = await ImagePicker.pickImage(source: ImageSource.camera);
    upload(docId);
  }

 // ProgressDialog objectpr;
  upload(String docId) async
  {
//    objectpr = new ProgressDialog(context,ProgressDialogType.Normal);
//
//    objectpr.setMessage("Uploading File Please Wait.....");
//
//    objectpr.show();
    _showProgressDialog("Uploading...","Uploading");

    setState(() {
//      _isLoading = true;
    });




    if (file == null) return;
    String base64Image = base64Encode(file.readAsBytesSync());
    String fileName = file.path
        .split("/")
        .last;

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString("accessToken");


    Dio dio = new Dio();
    FormData formdata = new FormData(); // just like JS
    formdata.add("file", new UploadFileInfo(file, fileName));
    formdata.add('LeadId', leadId);
    formdata.add('DocId', docId);



    Response response = await dio.post(
        Helper.UPLOAD_DOCUMENT_URL, data: formdata, options: Options(
        method: 'POST',
        responseType: ResponseType.json,
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/json",
          "Authorization": "Basic " + token
        }
    ));

    try {
      var data = json.decode(response.toString());
      setState(() => _isLoading = false);

      Navigator.of(context).pop();

     // objectpr.update(message: "Almost done");
      if (data["responseCode"] == Helper.SUCCESS_STATUS_CODE) {
        print("got data");
        _showSnackBar(data["messages"]["messageDescription"], Colors.green);
        setState(() {
          _isLoading = true;
        });

        _loadData();
        //objectpr.hide();
      } else {
        print("Error=" + data["messages"]["messageDescription"]);

        _showSnackBar(data["messages"]["messageDescription"], Colors.red);
      }
      if (data == null)
        print("Unable to get data");
    } catch (e) {
      print(e.toString());
    }
  }

  deleteFile(String docId) {
    String url = Helper.DELETE_FILE_URL + "?DocumentId=" + docId;
    print(url);
    Helper.callGETApi(Uri.encodeFull(url), null
        , context, this, DELETE_FILE_RESULT_CODE);
  }

  downloadFile(String fileUrl) async
  {

//    final taskId = await FlutterDownloader.enqueue(
//      url:fileUrl,
//      savedDir: 'the path of directory where you want to save downloaded files',
//      showNotification: true, // show download progress in status bar (for Android)
//      openFileFromNotification: true, // click on notification to open downloaded file (for Android)
//    );
    //String dir = (await getApplicationDocumentsDirectory()).path;



    _checkPermission().then((hasGranted) {
      setState(() {
        finalDownloadFile(fileUrl);
      });
    });

  }

  finalDownloadFile(String fileUrl)async{
    _showProgressDialog("Downloading...","Downloading");
    String _localPath = (await _findLocalPath()) + '/Download';
    print("dir="+_localPath);
    String filename = fileUrl.split("/").last;
    try {
      var dio = new Dio();
      dio.interceptors.add(LogInterceptor());
      await dio.download(
        fileUrl,
        _localPath+'/'+filename ,
        onReceiveProgress: showDownloadProgress,
      );
    } catch (e) {
      print(e);
    }

    Navigator.of(context).pop();

  }

  Future<bool> _checkPermission() async {
    final platform = Theme.of(context).platform;
    if (platform == TargetPlatform.android) {
      PermissionStatus permission = await PermissionHandler()
          .checkPermissionStatus(PermissionGroup.storage);
      if (permission != PermissionStatus.granted) {
        Map<PermissionGroup, PermissionStatus> permissions =
        await PermissionHandler()
            .requestPermissions([PermissionGroup.storage]);
        if (permissions[PermissionGroup.storage] == PermissionStatus.granted) {
          return true;
        }
      } else {
        return true;
      }
    } else {
      return true;
    }
    return false;
  }



  void showDownloadProgress(received, total) {

    if (total != -1) {
      print((received / total * 100).toStringAsFixed(0) + "%");
      setState(() {
        isDownLoadingFile = true;

        progress = double.parse((received / total * 100).toStringAsFixed(0));
        if(progress==100) {
          isDownLoadingFile = false;
          _showSnackBar("File downloaded successfully", Colors.green);
        }
      });
    }
  }

  Future<String> _findLocalPath() async {
    final platform = Theme.of(context).platform;
    final directory = platform == TargetPlatform.android
        ? await getExternalStorageDirectory()
        : await getApplicationDocumentsDirectory();
    return directory.path;
  }

  void changedPickUpDropDownItem(String selectedRole) {
    print("Selected role $selectedRole, we are going to refresh the UI");
    setState(() {
      _currentPickUpStatus = selectedRole;
    });
  }


  @override
  Widget build(BuildContext context) {
    ScreenUtil.instance = ScreenUtil.getInstance()
      ..init(context);

    var POIList = (poiList.length > 0) ? new Expanded(
        child: new ListView.builder
          (
            itemCount: poiList.length,
            itemBuilder: (BuildContext ctxt, int index) {
              return new Row(
                children: <Widget>[
                  Expanded(
                      flex: 8,
                      child:
                      new Row(
                        children: <Widget>[
                          (poiList[index].fileFullUrl
                              .toString()
                              .length > 0) ?
                          Image.network(
                            poiList[index].fileFullUrl.toString(), width: 100,
                            height: 100,
                            fit: BoxFit.fill,
                          ) :
                          new Icon(Icons.image, size: 100,
                              color: Color(HexColor.getAppBlueColor())),

                          (poiList[index].fileName
                              .toString()
                              .length > 0) ?
                          new Text(poiList[index].fileName.toString(),
                            style: TextStyle(color: Colors.black),) :
                          new Text(poiList[index].docName.toString(),
                            style: TextStyle(color: Colors.black),)

                        ],
                      )
                  ),
                  Expanded(
                    flex: 1,
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        new RaisedButton(onPressed: () {
                          // _displayFileChooseDialog(context,poiList[index].docId.toString());

                          _settingModalBottomSheet(
                              context, poiList[index].docId.toString());
                        },
                            child:
                            new Icon(Icons.file_upload,
                                color: Color(HexColor.getAppBlueColor()))),
                        new RaisedButton(onPressed: () {
                          downloadFile(poiList[index].fileFullUrl.toString());
                        },
                            child:
                            new Icon(Icons.file_download,
                                color: Color(HexColor.getAppBlueColor()))),
                        new RaisedButton(onPressed: () {
                          deleteFile(poiList[index].docId.toString());
                        },
                            child:
                            new Icon(Icons.delete_forever,
                                color: Color(HexColor.getAppBlueColor()))),
                      ],
                    ),
                  ),
                ],
              );
            }
        )
    ) : new Text(" ");

    var POAList = (poaList.length > 0) ? new Expanded(
        child: new ListView.builder
          (
            itemCount: poiList.length,
            itemBuilder: (BuildContext ctxt, int index) {
              return new Row(
                children: <Widget>[
                  Expanded(
                      flex: 8,
                      child:
                      new Row(
                        children: <Widget>[
                          (poaList[index].fileFullUrl
                              .toString()
                              .length > 0) ?
                          Image.network(
                            poaList[index].fileFullUrl.toString(), width: 100,
                            height: 100,
                          ) :
                          new Image.asset(
                            "assets/addimg.png", width: 100, height: 100,),

                          (poaList[index].fileName
                              .toString()
                              .length > 0) ?
                          new Text(poaList[index].fileName.toString(),
                            style: TextStyle(color: Colors.black),) :
                          new Text(poaList[index].docName.toString(),
                            style: TextStyle(color: Colors.black),)

                        ],
                      )
                  ),
                  Expanded(
                    flex: 2,
                    child: new Row(
                      children: <Widget>[
                        new RaisedButton(onPressed: () {
                          //  _uploadFile(poaList[index].docId.toString());
                          // _displayFileChooseDialog(context, poaList[index].docId.toString());
                          _settingModalBottomSheet(
                              context, poaList[index].docId.toString());
                        },
                            child:
                            new Icon(Icons.file_upload,
                                color: Color(HexColor.getAppBlueColor()))),
                        new RaisedButton(onPressed: () {
                          downloadFile(poaList[index].fileFullUrl.toString());
                        },
                            child:
                            new Icon(Icons.file_download,
                                color: Color(HexColor.getAppBlueColor()))),
                        new RaisedButton(onPressed: () {
                          deleteFile(poaList[index].docId.toString());
                        },
                            child:
                            new Icon(Icons.delete_forever,
                                color: Color(HexColor.getAppBlueColor()))),
                      ],
                    ),
                  ),
                ],
              );
            }
        )
    ) : new Text("");

    var uploadDocument =
    new Padding(padding: EdgeInsets.only(left: 5, right: 0, top: 20),
        child:
        new ListView(
//      mainAxisAlignment: MainAxisAlignment.start,
//      crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            isDownLoadingFile
                ?
            new LinearProgressIndicator(
              backgroundColor: Colors.red,
              // valueColor: AlwaysStoppedAnimation<Color>(Colors.amber,),
              value: progress/100,
            )
                :SizedBox(width: 0,height: 0,),
            new Padding(
              padding: EdgeInsets.all(5),
              child:
              new Text("1. KYC Documents: : ", style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: ScreenUtil.getInstance().setSp(55)),),
            ),
            Divider(),
//         new Text("Driving license, Ration card, Passport, PAN card, Voter's ID, Employee ID, Bank passbook(Any Two) ",style: TextStyle(color: Colors.blue),),
//         new Row(
//           children: <Widget>[
//             Expanded(
//                flex: 1,
//                child: new Column(                  crossAxisAlignment: CrossAxisAlignment.start,
//                  mainAxisAlignment: MainAxisAlignment.start,
//                  children: <Widget>[
//                    _inputPOI,
//                  ],
//                ),
//             ),
//             Expanded(
//                flex: 1,
//                 child: new Column(
//                   crossAxisAlignment: CrossAxisAlignment.end,
//                   mainAxisAlignment: MainAxisAlignment.end,
//                   children: <Widget>[
//                     new RaisedButton(onPressed:(){
//                       _uploadPOI();
//                     } ,
//                     child:
//                     new Icon(Icons.add,color:Color(HexColor.getAppBlueColor())),
//                     )
//                   ],
//                 )
//             )
//           ],
//         ),
//         Divider(),


            (_isLoading) ? Align(
                alignment: Alignment.center,
                child: CircularProgressIndicator()
            ) : new Container(
                height: poaList.length * 60.toDouble(),
                child: new ListView.builder
                  (
                    itemCount: poaList.length,
                    itemBuilder: (BuildContext ctxt, int index) {
                      return new Row(
                        children: <Widget>[
                          Expanded(
                              flex: 9,
                              child:
                              new Row(
                                children: <Widget>[

                                  new FlatButton(onPressed: () {
                                    //_uploadFile(poaList[index].docId.toString());
                                    // _displayFileChooseDialog(context, poaList[index].docId.toString());
                                    _settingModalBottomSheet(context,
                                        poaList[index].docId.toString());
                                  },
                                    child:
                                    new Padding(padding: EdgeInsets.all(5),
                                      child:
                                      (poaList[index].fileFullUrl
                                          .toString()
                                          .length > 0) ?
                                      Image.network(
                                          poaList[index].fileFullUrl.toString(),
                                          width: 50,
                                          height: 50,
                                          fit: BoxFit.fill
                                      ) :

                                      new Image.asset(
                                        "assets/addimg.png", width: 50,
                                        height: 50,),

                                    ),),
//                               (poaList[index].fileName.toString().length > 0)?
//                               new Text(poaList[index].fileName.toString().substring(0,10),style: TextStyle(color: Colors.black),):
                                  new Text(poaList[index].docName.toString(),
                                    style: TextStyle(color: Colors.black),)

                                ],
                              )
                          ),
                          Expanded(
                            flex: 4,
                            child: new Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
//                             new Container(
//                               padding: EdgeInsets.all(5),
//                               width : 40,
//                               child:
//                               new FlatButton(onPressed: (){
//
//                               },
//
//                                   child:
//                                   new Center(child:
//                                   new Icon(Icons.file_upload,color:Color(HexColor.getAppBlueColor())))),
//                             ),
                                (poaList[index].fileFullUrl
                                    .toString()
                                    .length > 0) ?
                                new Container(
                                  padding: EdgeInsets.all(5),
                                  width: 40,
                                  child:
                                  IconButton(
                                    icon:   Icon(Icons.file_download,
                                        color: Color(
                                            HexColor.getAppBlueColor())),
                                    tooltip: 'Download File',
                                    onPressed: () {
                                      setState(() {
                                        downloadFile(
                                            poaList[index].fileFullUrl.toString());

                                      });
                                    },
                                  ),

//                                  new FlatButton(
//
//                                      onPressed: () {
//                                    downloadFile(
//                                        poaList[index].fileFullUrl.toString());
//                                  },
//                                      child:
//                                      new Center(child:
//                                      new Icon(Icons.file_download,
//                                          color: Color(
//                                 Color             HexColor.getAppBlueColor())))),
                                ) : SizedBox(height: 0,),
                                (poaList[index].fileFullUrl
                                    .toString()
                                    .length > 0)
                                    ?
                                new Container(
                                  padding: EdgeInsets.all(5),
                                  width: 40,
                                  child:
                                  IconButton(
                                    icon:   Icon(Icons.delete_forever,
                                        color: Color(
                                            HexColor.getAppBlueColor())),
                                    tooltip: 'Remove File',
                                    onPressed: () {
                                      setState(() {
                                        deleteFile(
                                            poaList[index].leadDocId.toString());

                                      });
                                    },
                                  ),
//                                    new FlatButton(onPressed: () {
//                                      deleteFile(
//                                          poaList[index].leadDocId.toString());
//                                    },
//                                        child:
//                                        new Center(child:
//                                        new Icon(Icons.delete_forever,
//                                            color: Color(
//                                                HexColor.getAppBlueColor()))))
                                )
                                    :
                                new SizedBox(height: 0,)
                              ],
                            ),
                          ),
                        ],
                      );
                    }
                ))
            ,
            //  Divider(),
            new Padding(
              padding: EdgeInsets.all(5),
              child:
              new Text("2. Proof of identity : ", style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: ScreenUtil.getInstance().setSp(55)),),
            ),
            Divider(),
            // new Text("Bank passbook or Bank account statement, Voter's ID, Ration card, Passport, LIC policy/receipt,Form16, IT returns",style: TextStyle(color: Colors.blue),),
//         new Row(
//           children: <Widget>[
//             Expanded(
//               flex: 1,
//               child: new Column(
//                 crossAxisAlignment: CrossAxisAlignment.start,
//                 mainAxisAlignment: MainAxisAlignment.start,
//                 children: <Widget>[
//                   _inputPOA,
//                 ],
//               ),
//             ),
//             Expanded(
//                 flex: 1,
//                 child: new Column(
//                   crossAxisAlignment: CrossAxisAlignment.end,
//                   mainAxisAlignment: MainAxisAlignment.end,
//                   children: <Widget>[
//                     new RaisedButton(onPressed:(){
//                          _uploadPOA();
//                     } ,
//                       child:
//                       new Icon(Icons.add,color:Color(HexColor.getAppBlueColor())),
//                     )
//
//                   ],
//                 )
//             )
//           ],
//         ),

            (_isLoading) ? Align(
                alignment: Alignment.center,
                child: CircularProgressIndicator()
            ) : new Container(
                height: poiList.length * 60.toDouble(),
                child: new ListView.builder
                  (
                    itemCount: poiList.length,
                    itemBuilder: (BuildContext ctxt, int index) {
                      return new Row(
                        children: <Widget>[
                          Expanded(
                              flex: 9,
                              child:
                              new Row(
                                children: <Widget>[
                                  new FlatButton(
                                      onPressed: () {
                                        //_uploadFile(poiList[index].docId.toString());
                                        //  _displayFileChooseDialog(context, poiList[index].docId.toString());
                                        _settingModalBottomSheet(context,
                                            poiList[index].docId.toString());
                                      },
                                      child:
                                      new Padding(padding: EdgeInsets.all(5),
                                        child:
                                        (poiList[index].fileFullUrl
                                            .toString()
                                            .length > 0) ?
                                        Image.network(
                                          poiList[index].fileFullUrl.toString(),
                                          width: 50,
                                          height: 50,
                                          fit: BoxFit.fill,
                                        ) :
                                        new Image.asset(
                                          "assets/addimg.png", width: 50,
                                          height: 50,),

                                      )),
//                               (poiList[index].fileName.toString().length > 0)?
//                               new Text(poiList[index].fileName.toString().substring(0,10),style: TextStyle(color: Colors.black),):
                                  new Text(poiList[index].docName.toString(),
                                    style: TextStyle(color: Colors.black),)

                                ],
                              )
                          ),
                          Expanded(
                            flex: 4,
                            child: new Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: <Widget>[
//                             new Container(
//                               padding: EdgeInsets.all(5),
//                               width : 40,
//                               child:
//
//                             new FlatButton(onPressed: (){
//                               _uploadFile(poiList[index].docId.toString());
//                             },
//
//                                 child:
//                                 new Center(child:
//                                 new Icon(Icons.file_upload,color:Color(HexColor.getAppBlueColor()))),
//                             )),
                                (poiList[index].fileFullUrl
                                    .toString()
                                    .length > 0) ?
                                new Container(
                                  padding: EdgeInsets.all(5),
                                  width: 40,

                                  child:
                                  IconButton(
                                    icon:   Icon(Icons.file_download,
                                        color: Color(
                                            HexColor.getAppBlueColor())),
                                    tooltip: 'Download File',
                                    onPressed: () {
                                      setState(() {
                                        downloadFile(
                                            poiList[index].fileFullUrl.toString());

                                      });
                                    },
                                  ),

//                                  new FlatButton(
//
//                                      onPressed: () {
//                                    downloadFile(
//                                        poiList[index].fileFullUrl.toString());
//                                  },
//                                      child:
//                                      new Center(child:
//                                      new Icon(Icons.file_download,
//                                          color: Color(
//                                              HexColor.getAppBlueColor())))),
                                ) : SizedBox(height: 0,),
                                (poiList[index].fileFullUrl
                                    .toString()
                                    .length > 0)
                                    ?
                                new Container(
                                  padding: EdgeInsets.all(5),
                                  width: 40,
                                  child:

                                  IconButton(
                                    icon:   Icon(Icons.delete_forever,
                                        color: Color(
                                            HexColor.getAppBlueColor())),
                                    tooltip: 'Remove File',
                                    onPressed: () {
                                      setState(() {
                                        deleteFile(
                                            poiList[index].leadDocId.toString());

                                      });
                                    },
                                  ),
//                                    new FlatButton(onPressed: () {
//                                      deleteFile(
//                                          poiList[index].leadDocId.toString());
//                                    },
//                                        child:
//                                        new Center(child:
//                                        new Icon(Icons.delete_forever,
//                                            color: Color(
//                                                HexColor.getAppBlueColor()))))
                                )
                                    :
                                SizedBox(height: 0,),
                              ],
                            ),
                          ),
                        ],
                      );
                    }
                )),
//            new Container(
//              height: 50,
//                child:

//            ),
          ],
        ));
    var pickUp =
    new Padding(padding: EdgeInsets.only(left: 24, right: 24, top: 20),
        child:
        new ListView(
          children: <Widget>[
            new FormField(
              builder: (FormFieldState state) {
                return InputDecorator(
                  decoration: InputDecoration(
//                border: const OutlineInputBorder(),
                    labelText: "",
                    hintStyle: TextStyle(color: Colors.grey),
                    labelStyle: TextStyle(color: Colors.grey),
                    contentPadding: const EdgeInsets.symmetric(
                        vertical: 5.0, horizontal: 5.0),
                  ),
                  child: new DropdownButtonHideUnderline(
                      child:
                      new DropdownButton(
                        value: _currentPickUpStatus,
                        items: <String>["PICKED", "PENDING"].map((
                            String value) {
                          return new DropdownMenuItem<String>(
                            value: value,
                            child: new Text(value),
                          );
                        }).toList(),
                        onChanged: changedPickUpDropDownItem,

                      )

                  ),
                );
              },
            ),
            new Padding(padding: EdgeInsets.only(top: 20),
                child:
                ButtonTheme(
                    minWidth: 50.0,
                    height: 50.0,
                    child:
                    new RaisedButton(
                      color: HexColor("#fb6616"),
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                      splashColor: Colors.blue,
                      //onPressed: _savePickUpStatus,
                      child: new Text(
                          "SAVE", style: TextStyle(color: Colors.white)),
                    )))
          ],
        ));
//    return WillPopScope(
//      child: DefaultTabController(
//        length: 1,
//        child: Scaffold(
//          key: scaffoldKey,
//          appBar: AppBar(
////              bottom: TabBar(
////                tabs: [
////                  Tab(text: "Document",),
//////                  Tab(text: "Pick up",)
////
////                ],
////              ),
//              title: Text('Document Upload'),backgroundColor: HexColor("#2b2ca3")
//          ),
//          body:ListView(
//
//            children: <Widget>[
//              (_isLoading)?Align(
//                  alignment: Alignment.center,
//                  child: CircularProgressIndicator()
//              ):
//
//              uploadDocument
//
//            ],
//
//          )
//
////          TabBarView(
////            children: [
////              (_isLoading)?Align(
////                  alignment: Alignment.center,
////                  child: CircularProgressIndicator()
////              ):
////
////              uploadDocument,
//////              (_isLoading)?Align(
//////                  alignment: Alignment.center,
//////                  child: CircularProgressIndicator()
//////              ):pickUp,
////
////            ],
////          ),
//        ),
//      ),
//    );
    _onBackPressed()
    {
      Navigator.of(context).pop();
    }

    return
      new WillPopScope(
        onWillPop: _onBackPressed,
        child:
      new Scaffold(
        key: scaffoldKey,
        backgroundColor: Colors.white,

        appBar: AppBar(
          backgroundColor: Color(HexColor.getAppBlueColor()),
          title: Text("Document Upload", style: TextStyle(color: Colors.white),),
        ),
        body:


        (_isLoading) ? Align(
            alignment: Alignment.center,
            child: CircularProgressIndicator()
        ) :

        new Padding(
          padding: EdgeInsets.all(5),
          child:
          uploadDocument,
        )


//          child: ListView(
//
//             // padding: EdgeInsets.only(top: 0),
//
//              children: <Widget>[
//              (_isLoading)?Align(
//                  alignment: Alignment.center,
//                  child: CircularProgressIndicator()
//              ):
//
//              uploadDocument,
//
//              ]
//          ),

      //)
    ));
  }

  void _settingModalBottomSheet(context, String docId) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: new Wrap(
              children: <Widget>[
                new ListTile(
                    leading: new Icon(Icons.image),
                    title: new Text('Gallery'),
                    onTap: () {
                      _uploadFileFromGallery(docId);
                      Navigator.pop(context);
                    }
                ),
                new ListTile(
                  leading: new Icon(Icons.camera),
                  title: new Text('Camera'),
                  onTap: () {
                    _uploadFileFromCamera(docId);
                    Navigator.pop(context);
                  },
                ),
              ],
            ),
          );
        }
    );
  }


//  _displayFileChooseDialog(BuildContext context,String docId) async {
//
//    return showDialog(
//        context: context,
//        builder: (context) {
//          return AlertDialog(
//            title: Text('Send File'),
//            content:
//            new Container(
//              height: 100,
//            child:
//            new Column(
//              children: <Widget>[
//                new FlatButton(onPressed:(){
//                      _uploadFileFromGallery(docId);
//                },
//                  child:
//                  new Text("From Gallery"),
//                ),
//                new FlatButton(onPressed:(){
//                  _uploadFileFromCamera(docId);
//                },
//                  child:
//                  new Text("From Camera"),
//                )
//              ],
//            ),
//            )
//
////             actions: <Widget>[
////               new Row(children: <Widget>[
////                 Expanded(
////                     flex: 1,
////                     child:
////                     new Padding(padding: EdgeInsets.all(5),
////                         child:
////                         new FlatButton(
////                           child: new Text('CANCEL'),
////                           onPressed: () {
////                             Navigator.of(context).pop();
////                           },
////                         )
////                     )
////                 ),
////                 Expanded(
////                     flex: 1,
////                     child:
////                     new Padding(padding: EdgeInsets.all(5),
////                         child:
////                         new FlatButton(
////                           child: new Text('SEND'),
////                           onPressed: () {
////                             _sendSMS();
////                           },
////                         )
////                     )
////                 )
////               ],)
////
////             ],
//          );
//        });
//  }


  void _showSnackBar(String text, Color color) {
    scaffoldKey.currentState
        .showSnackBar(new SnackBar(backgroundColor: color,
      content: new Text(text),
      duration: new Duration(milliseconds: 5000),));
  }

  void postExecution(var data, int result_code) {
    //print("got data ="+data);
    switch (result_code) {
      case GET_DOCUMENTS_RESULT_CODE:
      // setState(() => _isLoading = false);
      //   print("got data ="+data);
      //            for (int i =0;i<mainDocList.length;i++)
//            {
//              if(mainDocList[i]["CategoryName"].toString()== "KYC Documents"){
//                String leadDocId = mainDocList[i]["LeadDocId"].toString();
//                String docId = mainDocList[i]["DocumentId"].toString();
//                String catName = mainDocList[i]["CategoryName"].toString();
//                String docName = mainDocList[i]["DocumentName"].toString();
//                String fileName = mainDocList[i]["FileName"].toString();
//                String fileFullUrl = mainDocList[i]["FileFullUrl"].toString();
//                UploadFile uploadFile = new UploadFile(leadDocId, docId, catName, docName, fileName, fileFullUrl);
//                poaList.add(uploadFile);
//              }
//              if(mainDocList[i]["CategoryName"].toString()== "Proof of identity"){
//                String leadDocId = mainDocList[i]["LeadDocId"].toString();
//                String docId = mainDocList[i]["DocumentId"].toString();
//                String catName = mainDocList[i]["CategoryName"].toString();
//                String docName = mainDocList[i]["DocumentName"].toString();
//                String fileName = mainDocList[i]["FileName"].toString();
//                String fileFullUrl = mainDocList[i]["FileFullUrl"].toString();
//                UploadFile uploadFile = new UploadFile(leadDocId, docId, catName, docName, fileName, fileFullUrl);
//                poiList.add(uploadFile);
//              }
//            }

        break;

      case DELETE_FILE_RESULT_CODE :
        try {
          setState(() => _isLoading = false);

          if (data["responseCode"] == Helper.SUCCESS_STATUS_CODE) {
            print("got data");
            _showSnackBar(data["messages"]["messageDescription"], Colors.green);

            setState(() {
              _isLoading = true;
            });

            _loadData();
          } else {
            print("Error=" + data["messages"]["messageDescription"]);

            _showSnackBar(data["messages"]["messageDescription"], Colors.red);
          }
          if (data == null)
            print("Unable to get data");
        } catch (e) {
          _showSnackBar("Some problem occurred", Colors.red);
        }
        break;

      case ERROR_CODE:
        _showSnackBar("Please check your internet connection ", Colors.red);
        setState(() {
          _isLoading = false;
        });
        break;
    }
  }

  void _showDialog(String title,String msg) {
    // flutter defined function
    showDialog(

      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(title),
          content: new Text(msg),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();

              },
            ),
          ],
        );
      },
    );
  }


  void _showProgressDialog(String title,String msg) {
    // flutter defined function
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(title),
          content:
          new Container(
            height: 100,
            child:
              Align(
                  alignment: Alignment.center,
                  child: CircularProgressIndicator()
              ) ),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
//            new FlatButton(
//              child: new Text("Close"),
//              onPressed: () {
//                Navigator.of(context).pop();
//              },
//            ),
          ],
        );
      },
    );
  }


}
class MyFiles{
  List<UploadFile> uploadFileList;


  MyFiles({this.uploadFileList});




  factory MyFiles.fromJson(Map<String, dynamic> json) {
    return new MyFiles(
        uploadFileList: json['DocumentDetails'].map((value) => new UploadFile.fromJson(value)).toList()
    );


  }

}