import 'package:apanarupee/screens/AllLead.dart';
import 'package:apanarupee/screens/Dashboard.dart';
import 'package:apanarupee/screens/Registration.dart';
import 'package:apanarupee/screens/SplashPage.dart';
import 'package:apanarupee/screens/LogIn.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';

//void main() => runApp(new MyApp());
void main() {
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(new MyApp());
  });
}
class MyApp extends StatelessWidget{

  @override
  Widget build(BuildContext context) {

    // Fixing App Orientation.
//    SystemChrome.setPreferredOrientations(
//        [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);

    return new MaterialApp(

      debugShowCheckedModeBanner: false,
      title: 'Apanarupee',
      theme: new ThemeData(

          primarySwatch: Colors.blue,
          hintColor: Colors.grey,
//          inputDecorationTheme: new InputDecorationTheme(
//              labelStyle: new TextStyle(color: Colors.white),
//              border: OutlineInputBorder(
//                  borderRadius: BorderRadius.circular(4.0)))),
      ),
      home:new LogIn(),
      routes: <String, WidgetBuilder>{
        '/login': (BuildContext context) => new LogIn(),
        '/splashPage': (BuildContext context) => new SplashPage(),
        '/register': (BuildContext context) => new Registration(),
        '/dashboard': (BuildContext context) => new Dashboard(),
        '/allleads': (BuildContext context) => new AllLead()

      },
    );
  }
}