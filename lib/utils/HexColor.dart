import 'package:flutter/material.dart';

class HexColor extends Color {

   static  int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }

   static  int getAppBlueColor() {
     String hexColor = "#2b2ca3";
     hexColor = hexColor.toUpperCase().replaceAll("#", "");
     if (hexColor.length == 6) {
       hexColor = "FF" + hexColor;
     }
     return int.parse(hexColor, radix: 16);
   }


   static  int getAppOrangeColor() {
     String hexColor = "#fb6616";
     hexColor = hexColor.toUpperCase().replaceAll("#", "");
     if (hexColor.length == 6) {
       hexColor = "FF" + hexColor;
     }
     return int.parse(hexColor, radix: 16);
   }

   static  int getAppLightGrey() {
     String hexColor = "#A9A9A9";
     hexColor = hexColor.toUpperCase().replaceAll("#", "");
     if (hexColor.length == 6) {
       hexColor = "FF" + hexColor;
     }
     return int.parse(hexColor, radix: 16);
   }

   static  int getAppDarkGrey() {
     String hexColor = "#808080";
     hexColor = hexColor.toUpperCase().replaceAll("#", "");
     if (hexColor.length == 6) {
       hexColor = "FF" + hexColor;
     }
     return int.parse(hexColor, radix: 16);
   }

   HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}