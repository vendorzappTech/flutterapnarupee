import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:url_launcher/url_launcher.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:apanarupee/model/ApiInterface.dart';
import 'package:package_info/package_info.dart';
import 'dart:io';

class Helper{

   static final int SUCCESS_STATUS_CODE = 0;
   static final int NO_DATA_STATUS_CODE = 1;
   static final String VERSION_CHANGE_STATUS_CODE = "3";
   static final String INACTIVE_STATUS_CODE = "2";


   static final String MAIN_URL = "http://api.apnarupee.in/api/";
   //static final String MAIN_URL = "http://dattashram.vendorzapp.in/service/index.php/";

   static final String SEND_OTP_URL = MAIN_URL + "user/PostsendLoginSms";
   static final String VERIFY_OTP_URL = MAIN_URL + "user/PostVerifyLogin";
   static final String RESEND_OTP_URL = MAIN_URL + "user/PostsendLoginResendSms";
   static final String GET_CITY_URL = MAIN_URL + "user/GetAllCity";
   static final String GET_BRANCH_URL = MAIN_URL + "user/GetBranchByCity";
   static final String REGISTRATION_URL = MAIN_URL + "user/PostRegisterUser";
   static final String GET_PRODUCT_TYPE_URL = MAIN_URL + "user/GetAllProduct/";
   static final String CREATE_LEAD_URL = MAIN_URL + "Auth/PostCreateLead/";
   static final String GET_ALL_LEADS_URL = MAIN_URL + "Auth/GetMyLeads/";
   static final String GET_BANK_URL = MAIN_URL + "User/GetAllBanks/";
   static final String GET_APPLICATION_DETAILS_URL = MAIN_URL + "Auth/GetLeadAndApplicationDetails/";
   static final String UPDATE_APPLICATION_URL = MAIN_URL + "Auth/PostLeadInfo/";
   static final String GET_LEAD_DETAILS_URL = MAIN_URL + "Auth/GetLeadDetails/";
   static final String UPLOAD_DOCUMENT_URL = MAIN_URL + "Auth/PostDocument";
   static final String GET_REASONS_URL = MAIN_URL + "User/GetAllReasons/";
   static final String GET_PROFILE_DETAILS = MAIN_URL + "Auth/GetUserProfileDetails/";
   static final String UPDATE_PROFILE_URL = MAIN_URL + "Auth/UpdateUserProgile/";
   static final String ADD_NOTES_URL = MAIN_URL + "Auth/AddLeadNote/";
   static final String CHANGE_STATUS_URL = MAIN_URL + "Auth/UpdateLeadStatus/";
   static final String ADD_MEETING_URL = MAIN_URL + "Auth/AddMeeting/";
   static final String CHANGE_DOCK_PICK_STATUS_URL = MAIN_URL + "Auth/ChangeStatusToDocPick/";
   static final String SEND_SMS_URL = MAIN_URL + "Auth/SendMessageToLead/";
   static final String GET_ALL_MEETINGS_URL = MAIN_URL + "Auth/GetLeadMeetings/";
   static final String DELETE_FILE_URL = MAIN_URL + "User/DeleteDocument/";
   static final String GET_DOCUMENTS_URL = MAIN_URL + "Auth/GetLeadDocumentDetails/";
   static final String GET_DELETE_MEETING = MAIN_URL + "Auth/DeleteMeeting/";
   static final String GET_TOP_PERFORMER_URL = MAIN_URL + "Auth/GetTopPerformers/";
   static final String GET_OVERVIEW_URL = MAIN_URL + "Auth/UserPerformanceOverview/";
   static final String GET_EARNINGS_URL = MAIN_URL + "Auth/GetEarningDetails/";
   static final String GET_PRODUCTS_URL = MAIN_URL + "User/GetProductDyanamically/";
   static final String GET_EARNING_DETAILS_URL = MAIN_URL + "Auth/GetEarningDetailsbyLeadId/";
   static final String GET_MONTH_EARNINGS_URL = MAIN_URL + "Auth/GetUserEarningByMonth/";
   static final String GET_TOP_PERFORMER_BY_MONTH_URL = MAIN_URL + "Auth/GetAllPerformersmonthWise/";
   static final String GET_NOTIFICATIONS_URL = MAIN_URL + "Auth/GetNotificationPartnerApp/";
   static final String GET_BANNER_URL = MAIN_URL + "Auth/GetDashboardBanner/";
   static final String GET_REFERENCE_DETAIL_URL = MAIN_URL + "Auth/GetLeadReferenceByLeadID/";
   static final String UPDATE_REFERENCE_URL = MAIN_URL + "Auth/UpdateReferenceLead/";
   static final String GET_APPROVE_DISBURSAL_DETAIL_URL = MAIN_URL + "Auth/GetDisbursedAmountByLeadID/";
   static final String UPDATE_APPROVED_AMNT_URL = MAIN_URL + "Auth/PostApprovedAmount/";
   static final String UPDATE_DISBURSED_AMNT_URL = MAIN_URL + "Auth/PostDisbursedAmount/";
   static final String DELETE_DISBURSEMENT_AMNT_URL = MAIN_URL + "Auth/PostDeleteDisbursedAmountByID/";



   static ApiInterface _apiInterface;

   static const APP_STORE_URL =
       'https://phobos.apple.com/WebObjects/MZStore.woa/wa/viewSoftwareUpdate?id=';
   static const PLAY_STORE_URL =
       'https://play.google.com/store/apps/details?id=';

   static launchURL() async {
      String url = "";

      if(!Platform.isAndroid)
      {
         // for ios
         String appId = "";
         url= APP_STORE_URL + appId + "&mt=8";

      }
      else{
         //for android
         String appId = "com.vendorzapp.dattashram";
         url= PLAY_STORE_URL + appId ;

      }

      if (await canLaunch(url)) {
         await launch(url);
      } else {
         throw 'Could not launch $url';
      }
   }


   static void callApi(String uri , String body ,BuildContext context,ApiInterface apiInterface,int result_code) async
   {
      bool isConnected = true;
      _apiInterface = apiInterface;
      // check internet connection
      try {
         final result = await InternetAddress.lookup('google.com');
         if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            print('connected');
         }
      } on SocketException catch (_) {
         print('not connected');
         isConnected = false;
         //_apiInterface.postExecution(null, -1);
      }
      if(isConnected) {
         try {
            // get App version number here

            PackageInfo packageInfo = await PackageInfo.fromPlatform();

            String version = packageInfo.version;
            print("project code = " + version);


            var jsonData = json.decode(body);

            jsonData["app_version"] = version;

            // get os
            if (!Platform.isAndroid) {
               jsonData["requested_from"] = "ios";
            } else {
               jsonData["requested_from"] = "android";
            }


            print("JSON Data = " + jsonData.toString());

            var data = await http.post(uri,
                body: json.encode(jsonData),
                headers: {"Content-Type": "application/json"}).timeout(
                const Duration(seconds: 10));


            if (data.statusCode == 200) {
               jsonData = json.decode(data.body);

               print("JSON body =" + data.body);

               if ((jsonData["responseCode"] == Helper.SUCCESS_STATUS_CODE) ||
                   (jsonData["responseCode"] == Helper.NO_DATA_STATUS_CODE)) {
                  print("success");
                  // call interface method here
                  _apiInterface.postExecution(jsonData, result_code);
                  // _showDialog( context);
               }

               if (jsonData["responseCode"] == Helper.INACTIVE_STATUS_CODE) {
                  // do log out here
                  print("Inactive status");

                  SharedPreferences prefs = await SharedPreferences
                      .getInstance();
                  prefs.setBool("isLoggedIn", false);
                  prefs.setString("username", null);
                  prefs.setString("mobile", null);
                  Navigator.of(context).pop();
                  Navigator.of(context).pushReplacementNamed("/login");
               }

               if (jsonData["responseCode"] ==
                   Helper.VERSION_CHANGE_STATUS_CODE) {
                  // show downlod new version app
                  print("Version change");
                  _showDialog(context);
               }
            }else
               {
                  print("Inactive status");

                  SharedPreferences prefs = await SharedPreferences
                      .getInstance();
                  prefs.setBool("isLoggedIn", false);
                  prefs.setString("username", null);
                  prefs.setString("mobile", null);
                  Navigator.of(context).pop();
                  Navigator.of(context).pushReplacementNamed("/login");
               }
         } catch (e) {
            print("Exception occurred="+e.toString());
            //_apiInterface.postExecution(null, -1);
         }
      }else{
         _apiInterface.postExecution(null, -1);
      }

   }

   static void callAuthApi(String uri , String body ,BuildContext context,ApiInterface apiInterface,int result_code) async
   {
      bool isConnected = true;
      _apiInterface = apiInterface;
      // check internet connection
      try {
         final result = await InternetAddress.lookup('google.com');
         if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            print('connected');
         }
      } on SocketException catch (_) {
         print('not connected');
         isConnected = false;
         //_apiInterface.postExecution(null, -1);
      }
      if(isConnected) {
         try {
            // get App version number here

            PackageInfo packageInfo = await PackageInfo.fromPlatform();

            String version = packageInfo.version;
            print("project code = " + version);


            var jsonData = json.decode(body);

            jsonData["app_version"] = version;

            // get os
            if (!Platform.isAndroid) {
               jsonData["requested_from"] = "ios";
            } else {
               jsonData["requested_from"] = "android";
            }


            print("JSON Data = " + jsonData.toString());
            SharedPreferences prefs = await SharedPreferences.getInstance();
            String token = prefs.getString("accessToken");

            print("Token="+token);
            var data = await http.post(uri,
                body: json.encode(jsonData),
                encoding: Encoding.getByName("utf-8"),
                headers: {"Accept": "application/json","Content-Type": "application/json", "Authorization": "Basic "+token}).timeout(
                const Duration(seconds: 10));


            if (data.statusCode == 200) {
               jsonData = json.decode(data.body);

               print("JSON body =" + data.body);

               if ((jsonData["responseCode"] == Helper.SUCCESS_STATUS_CODE) ||
                   (jsonData["responseCode"] == Helper.NO_DATA_STATUS_CODE)) {
                  print("success");
                  // call interface method here
                  _apiInterface.postExecution(jsonData, result_code);
                  // _showDialog( context);
               }

               if (jsonData["responseCode"] == Helper.INACTIVE_STATUS_CODE) {
                  // do log out here
                  print("Inactive status");

                  SharedPreferences prefs = await SharedPreferences
                      .getInstance();
                  prefs.setBool("isLoggedIn", false);
                  prefs.setString("username", null);
                  prefs.setString("mobile", null);
                  Navigator.of(context).pop();
                  Navigator.of(context).pushReplacementNamed("/login");
               }

               if (jsonData["responseCode"] ==
                   Helper.VERSION_CHANGE_STATUS_CODE) {
                  // show downlod new version app
                  print("Version change");
                  _showDialog(context);
               }
            }else
               {
                  print("Inactive status");

                  SharedPreferences prefs = await SharedPreferences
                      .getInstance();
                  prefs.setBool("isLoggedIn", false);
                  prefs.setString("username", null);
                  prefs.setString("mobile", null);
                  Navigator.of(context).pop();
                  Navigator.of(context).pushReplacementNamed("/login");
               }
         } catch (e) {
            print("Exception occurred="+e.toString());            //_apiInterface.postExecution(null, -1);

         }
      }else{
         //_apiInterface.postExecution(null, -1);
         _showReloadDialog(context, uri , body ,apiInterface,result_code,1);
      }

   }

   static void callAuthGetApi(String uri , String body ,BuildContext context,ApiInterface apiInterface,int result_code) async
   {
      bool isConnected = true;
      _apiInterface = apiInterface;
      // check internet connection
      try {
         final result = await InternetAddress.lookup('google.com');
         if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            print('connected');
         }
      } on SocketException catch (_) {
         print('not connected');
         isConnected = false;
         //_apiInterface.postExecution(null, -1);
      }
      if(isConnected) {
         try {
            // get App version number here

            PackageInfo packageInfo = await PackageInfo.fromPlatform();

            String version = packageInfo.version;
            print("project code = " + version);


            var jsonData = json.decode(body);

            jsonData["app_version"] = version;

            // get os
            if (!Platform.isAndroid) {
               jsonData["requested_from"] = "ios";
            } else {
               jsonData["requested_from"] = "android";
            }


            print("JSON Data get = " + jsonData.toString());
            SharedPreferences prefs = await SharedPreferences.getInstance();
            String token = prefs.getString("accessToken");

            print("Token="+token);
            var data = await http.get(uri,
                headers: {"Content-Type": "application/json", "Authorization": "Basic "+token}).timeout(
                const Duration(seconds: 10));

            if (data.statusCode == 200) {
               jsonData = json.decode(data.body);

               _apiInterface.postExecution(jsonData, result_code);
               // print(jsonData.toString());

//             if(jsonData["responseCode"] != null)
//               _apiInterface.postExecution(jsonData, result_code);
//             else
//                {
//                   print("Log Out");
//
//                   SharedPreferences prefs = await SharedPreferences.getInstance();
//                   prefs.setBool("isLoggedIn", false);
//                   prefs.setString("username", null);
//                   prefs.setString("mobile", null);
//                   Navigator.of(context).pop();
//                   Navigator.of(context).pushReplacementNamed("/login");
//
//                }
               // _showDialog( context);
            }else
               {
                  SharedPreferences prefs = await SharedPreferences.getInstance();
                   prefs.setBool("isLoggedIn", false);
                   prefs.setString("username", null);
                   prefs.setString("mobile", null);
                   Navigator.of(context).pop();
                   Navigator.of(context).pushReplacementNamed("/login");
               }

         } catch (e) {
            print("Exception occurred="+e.toString());
            //_apiInterface.postExecution(null, -1);

         }
      }else{
         _showReloadDialog(context, uri , body ,apiInterface,result_code,2);

      }

   }


   static void callGETApi(String uri , String body ,BuildContext context,ApiInterface apiInterface,int result_code) async
   {
      bool isConnected = true;
      _apiInterface = apiInterface;
      // check internet connection
      try {
         final result = await InternetAddress.lookup('google.com');
         if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            print('connected');
         }
      } on SocketException catch (_) {
         print('not connected');
         isConnected = false;
         //_apiInterface.postExecution(null, -1);
      }
      if(isConnected) {
         try {

            print("uri="+uri);

            var data = await http.get(uri,
                headers: {"Content-Type": "application/json"}).timeout(
                const Duration(seconds: 10));

            if (data.statusCode == 200) {
               var jsonData = json.decode(data.body);

               print("JSON body =" + data.body);

               //  if(jsonData["responseCode"] != null)
               _apiInterface.postExecution(jsonData, result_code);
//            else
//            {
//               print("Log Out");
//
//               SharedPreferences prefs = await SharedPreferences.getInstance();
//               prefs.setBool("isLoggedIn", false);
//               prefs.setString("username", null);
//               prefs.setString("mobile", null);
//               Navigator.of(context).pop();
//               Navigator.of(context).pushReplacementNamed("/login");
//
//            }
            }else{
               print("Log Out");

               SharedPreferences prefs = await SharedPreferences.getInstance();
               prefs.setBool("isLoggedIn", false);
               prefs.setString("username", null);
               prefs.setString("mobile", null);
               Navigator.of(context).pop();
               Navigator.of(context).pushReplacementNamed("/login");
            }

         } catch (e) {
            print("Exception occurred="+e.toString());
            //_apiInterface.postExecution(null, -1);

         }
      }else{
         _apiInterface.postExecution(null, -1);



      }

   }



   static void _showDialog(context) {
      // flutter defined function
      showDialog(
         barrierDismissible: false,
         context: context,
         builder: (BuildContext context) {
            // return object of type Dialog
            return AlertDialog(
               title: new Text("Message"),
               content: (!Platform.isAndroid)? new Text("Download new version for i0s"):new Text("Download new version for Android"),
               actions: <Widget>[
                  // usually buttons at the bottom of the dialog
                  new FlatButton(
                     child: new Text("Download"),
                     onPressed: () {

//                        LaunchReview.launch();
                        launchURL();
                     },
                  ),
               ],
            );
         },
      );
   }

   static void _showReloadDialog(context,String uri , String body ,ApiInterface apiInterface,int result_code,int method_type) {
      // flutter defined function
      //  method_type = 1    for post auth api
      // method_type = 2   for get auth api


      showDialog(
         barrierDismissible: false,
         context: context,
         builder: (BuildContext context) {
            // return object of type Dialog
            return AlertDialog(
               content: new Text("Unable to load data ,check your internet connection"),
               title: new Text("Try Again"),
               actions: <Widget>[
                  // usually buttons at the bottom of the dialog
                  new FlatButton(
                     child: new Text("Load"),
                     onPressed: () {

                        if(method_type == 1)
                           {
                              callAuthApi(uri, body, context, apiInterface, result_code);
                           }
                        if(method_type == 2)
                           {
                              callAuthGetApi(uri, body, context, apiInterface, result_code);
                           }
//                        LaunchReview.launch();
                           Navigator.pop(context);
                   //     launchURL();
                     },
                  ),
               ],
            );
         },
      );
   }

}